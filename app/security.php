<?php

class Security
{
	static private $key = '132;246;140;143;66;209;36;146;145;234;230;2;154;28;93;44;67;244;192;235;23;33;62;210;156;51;232;73;84;25;240;55;174;160;119;218;19;32;123;43;105;194;8;216;47;125;99;189;5;112;42;249;20;128;88;56;122;26;113;243;217;201;130;83;126;136;96;152;203;147;202;188;6;98;24;135;177;92;220;104;169;45;223;198;106;115;254;158;18;138;164;70;1;241;236;205;49;71;50;100;89;197;114;22;190;221;58;103;74;195;9;48;82;78;17;54;247;237;250;153;144;215;211;53;238;10;186;239;157;252;101;90;141;200;76;155;35;176;127;204;118;61;226;120;242;208;207;57;116;213;199;34;180;16;72;91;124;134;86;60;231;40;109;117;68;85;227;129;163;214;12;251;107;253;21;224;179;39;133;196;29;102;80;148;69;87;248;79;4;182;38;75;111;255;175;142;94;3;178;131;14;139;165;228;172;161;46;77;150;63;31;233;13;159;97;52;151;27;149;225;206;187;162;81;166;167;108;171;191;168;193;184;0;65;15;137;183;64;222;95;181;30;219;185;110;59;245;170;11;121;173;37;7;229;212;41';
	static private $separator = ';';

	static function fgeturl($url){
	  $result = file_get_contents($url);
	  /*
	  $curl_handle=curl_init();
	  curl_setopt($curl_handle, CURLOPT_URL,$url);
	  curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
	  curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
	  curl_setopt($curl_handle, CURLOPT_USERAGENT, 'CochaHoteles');
	  curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
	  $result = curl_exec($curl_handle);
	  if($result === false)
	  {
		return "{error:Curl error:". curl_error($curl_handle)."}";
	  }
	  curl_close($curl_handle);		
	  */
	  return $result;
	}

	static function encrypt($string)
	{
		$arrKey = explode(self::$separator, self::$key);
		$cArray = str_split($string);
  
		$encrypted = array();
		foreach ($cArray as $i => $char) {
			$encrypted[] = $arrKey[ord($char)];
		}

		$rtnString = implode(self::$separator, $encrypted);
		return $rtnString;
	}

	static function decrypt($string)
	{
		$arrKey = explode(self::$separator, self::$key);
		$valueArray = explode(self::$separator, $string);
		$decrypted = '';

		foreach ($valueArray as $i => $value) {
			$j = array_search($value, $arrKey);
			$decrypted .= chr($j);
		}

		return $decrypted;
	}

}

?>