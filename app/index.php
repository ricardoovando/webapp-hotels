<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);
include('CochaHeader.php');
require("security.php");

$env = getenv("ENVIRONMENT_TYPE");
$tmKey = "GTM-P8JGWB";  

$appVersion = '1.21.34';

$useDevLibraries = false;

//if ($env != "PROD") {
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
//}

$cocha = new CochaHeader($env);

$JSVARS = array();
$JSVARS["APP_TYPE"]				= 1;
$JSVARS["FLOW_ID"] 				= (isset($_GET['fid'])?$_GET['fid']:uuid('h'));
$JSVARS["URL_IMAGE"]   			= "http://cms.cocha.com/";
$JSVARS["APP_VERSION"] 			= $appVersion;
$JSVARS["USER_TOKEN"]  			= "WEBMOBILE";
$JSVARS["PASSWORD_TOKEN"] 		= "WEBMOBILE@cocha.com";
$JSVARS["COCHA_LIBS_BASE"] 		= "../vendor";
$JSVARS["COCHA_LIBS_FOLDER"] 	= "dist";
$JSVARS["COCHA_LIBS_TYPE"] 		= ".min";
$JSVARS["APP_NAME"] 			= "HT.B2C.COCHA.COM";
$JSVARS["PRODUCT_NAME"] 		= "Hotels";
$JSVARS["DEFAULT_SOCIAL_IMAGE"] = "https://cms.cocha.com/images/ico_header/logo-cocha.png";
$JSVARS["USER_EMAIL"]			= "";
$JSVARS["URL_CAMPAIGN_BACK"] 	= 'https://cms-adm.cocha.com/images/package-portal/background-hoteles.jpg';

//$JSVARS["COOKIE_NAME"]			= 'token_cocha';

$cookieSuffix = ( $env === 'PROD' ) ? '_p' : ( $env === 'QA' ? '_p' : '_d' ) ;
$JSVARS["COOKIE_NAME"] = 'cocha_cross'.$cookieSuffix;
		
if ($env == "PROD") {
	if(isset($_GET["d"]) && $_GET["d"] == 1){
	  $consoleRedefinition = "";
	} else {
	  $consoleRedefinition = "console.log=console.info=console.error=console.warn=console.debug=console.dir=console.groupCollapsed=console.time=function(){return;}";
	}
	
	$JSVARS["APP_NAME"] 					= "HT.B2C.COCHA.COM";
	$JSVARS["DESKTOP_URL"]					= 'http://www.cocha.com';
	$JSVARS["CMS_MOBILE_API"] 				= 'http://cms.cocha.com/mobile-api';
	$JSVARS["URL_CRM"] 						= 'http://backend.cochadigital.com/?entryPoint=';
	$JSVARS["HOST_HOTELS_SERVICES"] 		= "https://mid.cocha.com/hotels/v1/";
	$JSVARS["HOST_HOTELS_SERVICES_NOCACHE"] = "https://hotels-mid.cocha.com/v1/";

	$JSVARS["HEADER_COCHA"] 				= "https://cms-int.cocha.com/cmsIntranet/index.php?css=vh&seccion=header";			
	$JSVARS["FOOTER_COCHA"] 				= "https://cms-int.cocha.com/cmsIntranet/index.php?css=vh&seccion=footer";		
	
	$JSVARS["MID_URL"] 						= 'https://mid.cocha.com/traveldeals/v1';
	$JSVARS["CMS_URL"] 						= 'https://cms.cocha.com/mobile-api';
	$JSVARS["URL_HOTELS"]					= "https://www.cocha.com";
	$JSVARS["QUOTE_URL"]    				= "https://".((isset($_GET["ambiente"]) && !empty($_GET["ambiente"]))?$_GET["ambiente"]:"crm.cocha.com");
	$JSVARS["HOST_CRM"]						= "https://crm.cocha.com";
	$JSVARS["LEAD_URL"]						= $JSVARS["HOST_CRM"]."/prototipo/Backend/Controllers/LeadsController.php/Leads";
	$JSVARS["BASE_URL"]						= "https://www.cocha.com/hoteles";
}
else {

	$consoleRedefinition = "";
	$JSVARS["DESKTOP_URL"]= 'http://portalibeqa.cocha.com';

	if ($env == "QA") {
		$JSVARS["APP_NAME"] 					= "HT.B2C.COCHA.COM-QA";
		$JSVARS["CMS_MOBILE_API"] 				= 'http://cms-qa.cocha.com/mobile-api';
		$JSVARS["URL_CRM"] 						= 'http://qa-backend.cochadigital.com/?entryPoint=';
		$JSVARS["HOST_HOTELS_SERVICES"] 		= "https://mid-qa.cocha.com/hotels/v1/";
		$JSVARS["HOST_HOTELS_SERVICES_NOCACHE"] = "https://mid-qa.cocha.com/hotels/v1/";

		$JSVARS["HEADER_COCHA"] 				= "https://cms-int.cocha.com/cmsIntranet/index.php?css=vh&seccion=header";
		$JSVARS["FOOTER_COCHA"] 				= "https://cms-int.cocha.com/cmsIntranet/index.php?css=vh&seccion=footer";

		$JSVARS["MID_URL"] 						= 'https://mid-qa.cocha.com/traveldeals/v1';
		$JSVARS["CMS_URL"] 						= 'https://cms-qa.cocha.com/mobile-api';
		//$JSVARS["URL_HOTELS"]					= "https://hotel-qa.cocha.com";
		//$JSVARS["URL_HOTELS"]					= "https://wapp-qa.cocha.com/hotels-test/";
		$JSVARS["URL_HOTELS"]					= "https://www-qa.cocha.com";
		$JSVARS["QUOTE_URL"]    				= "https://".((isset($_GET["ambiente"]) && !empty($_GET["ambiente"]))?$_GET["ambiente"]:"dev2.cochadigital.com");
		$JSVARS["HOST_CRM"]						= "https://dev2.cochadigital.com";
		$JSVARS["LEAD_URL"]						= $JSVARS["HOST_CRM"]."/prototipo/Backend/Controllers/LeadsController.php/Leads";
		$JSVARS["BASE_URL"]						= "https://wapp-qa.cocha.com/hoteles";
	} else {
		$JSVARS["APP_NAME"] 					= "HT.B2C.COCHA.COM-DESA";
		$JSVARS["CMS_MOBILE_API"] 				= "http://cms-desa.cocha.com/mobile-api";
		$JSVARS["URL_CRM"] 						= "http://desa-backend.cochadigital.com/?entryPoint=";
		$JSVARS["HOST_HOTELS_SERVICES"] 		= "http://mid-desa.cocha.com/hotels/v1/";		
		$JSVARS["HOST_HOTELS_SERVICES_NOCACHE"] = "http://mid-desa.cocha.com/hotels/v1/";
		//$JSVARS["HOST_HOTELS_SERVICES"] 		= "http://mid-qa.cocha.com/hotels/v1/";		
		$JSVARS["HEADER_COCHA"] 				= "http://cms-int.cocha.com/cmsIntranet/index.php?css=vh&seccion=header";			
		$JSVARS["FOOTER_COCHA"] 				= "http://cms-int.cocha.com/cmsIntranet/index.php?css=vh&seccion=footer";
		
		$JSVARS["MID_URL"] 						= 'http://mid-desa.cocha.com/traveldeals/v1';
		$JSVARS["CMS_URL"] 						= 'http://cms-desa.cocha.com/mobile-api';
		//$JSVARS["URL_HOTELS"]					= "http://hotel-desa.cocha.com";	
		//$JSVARS["URL_HOTELS"]					= "http://wapp-desa.cocha.com/hotels-test/";	
		$JSVARS["URL_HOTELS"]					= "http://wapp-desa.cocha.com";			
		if($useDevLibraries) {
			$JSVARS["COCHA_LIBS_BASE"] 		= "../dev";
			$JSVARS["COCHA_LIBS_FOLDER"] 	= "src";
			$JSVARS["COCHA_LIBS_TYPE"] 		= "";
		}
		$JSVARS["QUOTE_URL"]    				= "https://".((isset($_GET["ambiente"]) && !empty($_GET["ambiente"]))?$_GET["ambiente"]:"dev2.cochadigital.com");
		$JSVARS["HOST_CRM"]						= "https://dev2.cochadigital.com";
		$JSVARS["LEAD_URL"]						= $JSVARS["HOST_CRM"]."/prototipo/Backend/Controllers/LeadsController.php/Leads";
		$JSVARS["BASE_URL"]						= "http://wapp-desa.cocha.com/hoteles";
	}

	if (in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1','::1'))){
		$JSVARS["APP_NAME"] = "HT.WLOCAL.COCHA.COM";
	}
}

//$JSVARS["HOST_HOTELS_SERVICES"] 		= "http://local.cocha.com:1337/";
//$JSVARS["HOST_HOTELS_SERVICES_NOCACHE"] = "http://local.cocha.com:1337/";


$JSVARS["END_POINT_TOKEN"]    				= "--";

$JSVARS["END_POINT_GEO_SEARCH"]    			= $JSVARS["HOST_HOTELS_SERVICES"]."geosearch/";
$JSVARS["END_POINT_HOTEL_SEARCH"]  			= $JSVARS["HOST_HOTELS_SERVICES"]."hotels/";
$JSVARS["END_POINT_HOTEL_SEARCH_NOCACHE"]  	= $JSVARS["HOST_HOTELS_SERVICES_NOCACHE"]."hotels/";

$JSVARS["END_POINT_HOTEL_DETAILS"] 			= $JSVARS["HOST_HOTELS_SERVICES"]."hotel/";
$JSVARS["END_POINT_HOTEL_ROOMS"]   			= $JSVARS["HOST_HOTELS_SERVICES"]."rooms/";
$JSVARS["END_POINT_REGION_SEARCH"] 			= $JSVARS["HOST_HOTELS_SERVICES"]."region/";
$JSVARS["END_POINT_HOTEL_REGION_SEARCH"]	= $JSVARS["HOST_HOTELS_SERVICES"]."hotelRegion/";

$JSVARS["END_POINT_ROOM_AVAILABILITY"]  	= $JSVARS["HOST_HOTELS_SERVICES"]."room/";
$JSVARS["END_POINT_CHECKOUT_SABRE"]  		= $JSVARS["HOST_HOTELS_SERVICES"]."checkout/";

$JSVARS["END_POINT_CHECKBOOK"] 				= $JSVARS["HOST_HOTELS_SERVICES"]."checkbook";	
$JSVARS["END_POINT_RESERVATION"]			= $JSVARS["HOST_HOTELS_SERVICES"]."bookHotel";	
$JSVARS["END_POINT_CURRENCY"]  				= $JSVARS["HOST_HOTELS_SERVICES"]."exchangeRate/";
$JSVARS["END_POINT_INTEREST_POINTS"] 		= $JSVARS["HOST_HOTELS_SERVICES"]."points/";	
$JSVARS["END_POINT_BUSINESS_ID"]			= $JSVARS["HOST_HOTELS_SERVICES"]."businessNumber/";

$JSVARS["END_POINT_NEMO"]					= $JSVARS["HOST_HOTELS_SERVICES"]."nemo/";
$JSVARS["END_POINT_VALIDATE_SEARCH_NEMO"]	= $JSVARS["HOST_HOTELS_SERVICES"]."nemoSearch/";
$JSVARS["END_POINT_CREDIT_CARD"]			= $JSVARS["HOST_HOTELS_SERVICES"]."cardNumber/";

$JSVARS["END_POINT_CREATE_SESSION"]			= $JSVARS["HOST_HOTELS_SERVICES"]."token/";
$JSVARS["END_POINT_GET_SESSION"]			= $JSVARS["HOST_HOTELS_SERVICES"]."session/";	
$JSVARS["END_POINT_DELETE_SESSION"]			= $JSVARS["HOST_HOTELS_SERVICES"]."deleteSession/";

$JSVARS["END_POINT_SEARCH_BOOKING"]			= $JSVARS["HOST_HOTELS_SERVICES"]."booking/";
$JSVARS["END_POINT_BKG_DEEPLINK"]			= $JSVARS["HOST_HOTELS_SERVICES"]."deeplinkBKG/";
$JSVARS["END_POINT_SEARCH_BOOKING_DETAILS"] = $JSVARS["HOST_HOTELS_SERVICES"]."bookingDetails/";

$JSVARS["END_POINT_SEND_VOUCHER"] 			= $JSVARS["HOST_HOTELS_SERVICES"]."sendVoucher/";
$JSVARS["END_POINT_GET_VOUCHER"] 			= $JSVARS["HOST_HOTELS_SERVICES"]."getVoucher/";

$JSVARS["END_POINT_CANCEL_BOOKING"] 		= $JSVARS["HOST_HOTELS_SERVICES"]."cancelBooking/";

$Header = "";
$Footer = "";	

$phone	= "";
$name	= "";
$email 	= "";
$user 	= "";	
$partner= "";

$hideContent = false;

if(isset($_GET['source']) && $_GET['source']==='crm'){
    $currentUrl = (isset($_SERVER['HTTPS']) ?"https":"http")."://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    if(!$cocha->hasCookie()) {
        $hideContent = true;
        ?>
        <div class='alert alert-info'>
            <p>
                Para utilizar este enlace, debe loguearse en Cocha.
            </p>
            <br>
            <a href="<?= $cocha->linkLogin ?>"  target="_blank" class='btn btn-transparent'>Ingresar</a>
            <a href="<?= $currentUrl ?>"  target="_self" class='btn btn-transparent'>Intentar de nuevo</a>
        </div>
        <?php
    } else {
        $arrResult = $cocha->getUserInfo();
        if(!isset($arrResult['user'])){
            $hideContent = true;                    
            ?>
            <div class='alert alert-info'>
                <p>
                    La sesión ha expirado, para utilizar este enlace, debe loguearse en Cocha.
                </p>
                <br>
                <a href="<?= $cocha->linkLogin ?>"  target="_black" class='btn btn-transparent'>Ingresar</a>
                <a href="<?= $currentUrl ?>"  target="_self" class='btn btn-transparent'>Intentar de nuevo</a>
            </div>
            <?php
        }        
    }
}

if(isset($_REQUEST['creacookiedecentauri'])){
	setcookie('cocha_cross'.$cookieSuffix ,'lacookiedecentauri', time() + (86400 * 30), "/", ".cocha.com"); // 86400 = 1-day
	//setcookie('token_cocha' ,'lacookiedecentauri', time() + (86400 * 30), "/"); // 86400 = 1-day		
}

if(isset($_GET['cotizacion'])) {
    if(!empty($_GET['cotizacion'])) {
        $quoteMode = true;
        $JSVARS["QUOTE"] = $_GET['cotizacion'];
    } else {
      echo "ID DE COTIZACION VACIA";
      $quoteMode = false;
      $JSVARS["QUOTE"] = '0';
      exit;
    }
}
  
$link_logout = ($env == 'PROD') ? "https://cms-int.cocha.com/" : ( ($env == 'QA') ? 'https://cms-int.cocha.com/':'http://intra-desa.cocha.com/');
$link_logout.= "cmsIntranet/logoutAgent.php";

try {
	//if(isset($_COOKIE['token_cocha'])){
    if($cocha->hasCookie() && !$hideContent){
        $arrResult = $cocha->getUserInfo();
		if(isset($arrResult['user'])){
			$phone	   = $arrResult['phone'];
			$name	   = $arrResult['name'];
			$email 	   = $arrResult['email'];
			$user 	   = $arrResult['user'];
			$partner   = $arrResult['partner'];
            $header = $cocha->getHeader();
            $footer = $cocha->getFooter();
            $JSVARS["SESSION_TOKEN"] = $cocha->sessionToken;
            $JSVARS["USER_INFO"] = json_encode($arrResult);
		} else {
            ?>
            <br>
            <br>
            <br>
            <div class='alert alert-info'>
                <p>
                    La sesión ha expirado, debe volver a ingresar para poder utilizar la aplicación como vendedor.
                </p>
                <br>
                <a href="<?= $cocha->linkLogin ?>"  target="_blank" onclick="javascript: deleteSession();" class='btn btn-transparent'>Volver a ingresar</a>
                <a href="<?= $cocha->linkLogout ?>" target="_blank" onclick="javascript: deleteSession();" class='btn btn-transparent'>No mostrar este mensaje</a>
            </div>
            <?php
		}
		$JSVARS["USER_EMAIL"] = $email;
		$JSVARS["USER_PARTNER"] = $partner;
		$JSVARS["USER_PHONE_COUNTRY"] = $cocha->getPhone('country',$phone);
		$JSVARS["USER_PHONE_CODE"] = $cocha->getPhone('code',$phone);
		$JSVARS["USER_PHONE_NUMBER"] = $cocha->getPhone('number',$phone);
	} else {
		//echo "Cookie not set, defaults to B2C";
	}	
	
} catch(Exception $e){
	var_dump($e);
}


?>
<!DOCTYPE html>
<html lang="es" ng-app>
<head>
	<!-- STATIC CODE -->
	<meta charset="utf-8" />	
	<base href="<?= getenv('BASE_HREF') ? getenv('BASE_HREF') : '/' ?>">
	<link rel="stylesheet" type="text/css" href="vendor/cocha-header-footer/dist/style.min.css">
	
	<link rel="stylesheet" type="text/css" href="vendor/components-font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/cocha-fonts/dist/style.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/cocha-access-remote/dist/style.min.css">		
	<link rel="stylesheet" type="text/css" href="styles/style.min.css?v=<?= $appVersion ?>">

    <link rel="shortcut icon" href="https://cms.cocha.com/images/favicon/favicon.ico" sizes="32x32">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="https://cms.cocha.com/images/favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="https://cms.cocha.com/images/favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="https://cms.cocha.com/images/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="https://cms.cocha.com/images/favicon/apple-touch-icon-57x57.png">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">

	<title>Cocha - Reserva de Hoteles por Destino</title>
	<meta name="description" content="Cocha - Reserva de Hoteles por Destino">

	<!-- Google Tag Manager -->
	<noscript>
		<iframe src="//www.googletagmanager.com/ns.html?id=<?= $tmKey ?>" height="0" width="0" style="display:none;visibility:hidden">
		</iframe>
	</noscript>
	<script>
	(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','<?= $tmKey ?>');
</script>
<!-- End Google Tag Manager -->
<!-- END OF STATIC CODE -->
</head>
<body>
	
	<!-- Try to use localstorage -->
	<script>

		<?php
		    if(isset($_GET['source']) && $_GET['source']==='crm'){
		?>
		    sessionStorage.removeItem('quote');
		    sessionStorage.removeItem('quote_url');
		<?php
		    }
		?>

	    function deleteSession(){
	      var cookieName = '<?= $JSVARS["COOKIE_NAME"] ?>';
	      var expiryDate = new Date();
	      expiryDate.setTime(expiryDate.getTime() - 86400 * 1000);
	      var cookieString = cookieName+'=';
	      cookieString += ';max-age=0';
	      cookieString += ';expires=' + expiryDate.toUTCString();
	      cookieString += ';path=/';
	      cookieString += ';domain=cocha.com';
	      document.cookie = cookieString;
	      setTimeout(function(){ window.location.reload(true); },3000);
	    }

		try {
			localStorage.test = 'Probando si navegador soporta localStorage'; //Navegacion inc�gnito en Safari no soporta LocalStorage
			localStorage.removeItem('test');
		}
		catch (e) {
			location.replace('http://' + location.host + '/views/utils/error-private.html');
		}
		</script>
		<!-- End of trying to use localstorage -->
		<?php
            if(isset($quoteMode) && $quoteMode){
        ?>
        <script>
            sessionStorage.setItem("quote","<?=$JSVARS['QUOTE'] ?>");
            sessionStorage.setItem("quote_url","<?=$JSVARS['QUOTE_URL'] ?>");
        </script>
        <?php
            }
        ?>
		<!-- Setting global JS variables -->
		<script>
		<?php
		foreach ($JSVARS as $key => $value) {
			if (is_string($value)) {
				echo "window.$key='$value'; ";
			}
			else {
				echo "window.$key=$value; ";
			}
		}
        if(!isset($quoteMode) || !$quoteMode){
			?>
            if (sessionStorage.getItem('quote')){
                window.QUOTE     = sessionStorage.getItem('quote');
                window.QUOTE_URL = sessionStorage.getItem('quote_url');
            } else {
                window.QUOTE     = '0';
                window.QUOTE_URL = "<?=$JSVARS['QUOTE_URL'] ?>";
			}
			<?php
        }		
		echo $consoleRedefinition;			
		?>
	</script>
	<!-- End of setting global JS variables -->
	
	<?php
		if(!$hideContent){        
			if(!isset($quoteMode) || !$quoteMode){
	?>
		<div ui-view="header" ng-if="showheader"><?= (!empty($header))? $header :""; ?></div>
	<?php
			}
	?>
	<div ui-view="body" class="main" class="ng-cloak"></div>
	<?php
			if(!isset($quoteMode) || !$quoteMode){ ?>
				<div ui-view="footer" id='footerView' ng-if="showfooter"><?= (!empty($footer))? $footer :""; ?></div> <?php
			}
		}
	?>

	<!-- Modals -->
	<div ng-include="'views/utils/common-modals.html?v=<?= $appVersion ?>'"></div>
	<div ng-include="'views/utils/hotels-modals.html?v=<?= $appVersion ?>'"></div>
	<div ng-include="'views/utils/home-modals.html?v=<?= $appVersion ?>'"></div>	

	<script src="https://maps.google.com/maps/api/js?key=AIzaSyDao14S3DnFL7Da3lp20FuFudz5VvV_Uzo"></script>
	<script src="vendor/requirejs/require.js?v=<?= $appVersion ?>" data-main="scripts/main.js?v=<?= $appVersion ?>"></script>


	<?php
	  if (in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1','::1'))) {
		$livereloadPort = getenv('LIVERELOAD') ? getenv('LIVERELOAD') : '35729';
		echo "<script src='http://localhost:" . $livereloadPort ."/livereload.js'></script>";
	  }
	?>

</body>
</html>
<?php
function uuid($prefix = 'gn') {
  $t = explode(" ",microtime());
  return sprintf('%04s-%04s-%04s-%04x%04x%04x',
    $prefix . substr( uniqid(),-6),
    substr("00000000".dechex($t[1]),-4),   // get 8HEX of unixtime
    substr("0000".dechex(round($t[0]*65536)),-4), // get 4HEX of microtime
    mt_rand(0,0xffff), mt_rand(0,0xffff), mt_rand(0,0xffff));
};
