<?php

class CochaHeader
{
	public $linkLogout;	
	public $linkLogin;	
	public $cookieName = 'cocha_cross';
	public $headerUrl;
	public $footerUrl;
	public $sessionToken;
	private $getSessionEndPoint;
	private $environment;
	private $userInfo;
    
    function __construct($env) {
    	$this->environment = $env = (!empty($env) ? $env : 'DESA');
    	$this->cookieName.= ($this->environment === 'DESA' ) ? '_d' : '_p';
    	$this->sessionToken = isset($_COOKIE[$this->cookieName]) ? $_COOKIE[$this->cookieName] : null;
    	
    	//$this->linkLogout = (($this->environment == 'PROD') ? "https://cms-int.cocha.com/" : ( ($this->environment == 'QA') ? 'https://cms-qa.cocha.com/':'http://intra-desa.cocha.com/'))."cmsIntranet/logoutAgent.php";
    	$this->linkLogout = (($this->environment != 'DESA') ? "https://www.cocha.com/" : "http://wapp-desa.cocha.com/" )."?logout";
		$this->linkLogin  = (($this->environment != 'DESA') ? "https://www.cocha.com/" : 'http://wapp-desa.cocha.com/')."?login";
    	//$this->linkLogin  = (($this->environment != 'DESA') ? "https://cms-int.cocha.com/" : 'http://intra-desa.cocha.com/')."cmsIntranet/loginAgentSession.php?seccion=login";
    	$this->headerUrl  = (($this->environment != 'PROD') ? "http://intra-desa.cocha.com/" : 'http://cms-int.cocha.com/')."cmsIntranet/index.php?seccion=header&css=vh";
    	$this->footerUrl  = (($this->environment != 'PROD') ? "http://intra-desa.cocha.com/" : 'http://cms-int.cocha.com/')."cmsIntranet/index.php?seccion=footer&css=vh";
    	//$this->headerUrl  = "http://cms-int.cocha.com/cmsIntranet/index.php?seccion=header&css=vh";
    	//$this->footerUrl  = "http://cms-int.cocha.com/cmsIntranet/index.php?seccion=footer&css=vh";
    	$this->getSessionEndPoint = (($this->environment != 'DESA') ? "https://mid.cocha.com/hotels/v1/" : "http://mid-desa.cocha.com/hotels/v1/")."session/";
    }

	function fgeturl($url){
		return @file_get_contents($url);
	}

	function getUserInfo(){
		$this->userInfo = json_decode($this->fgeturl($this->getSessionEndPoint.$this->sessionToken),true);
		return $this->userInfo;
	}

	function getHeader(){
		$header = $this->fgeturl($this->headerUrl);
		$mytime = time() + (1 * 3600);
		setlocale(LC_TIME, "");
		date_default_timezone_set('UTC');
		setlocale(LC_TIME, 'es_CL');
		$myDate = ucfirst( strftime("%A",$mytime)  ).' '.date('j',$mytime).' de '.ucfirst(strftime('%B', mktime(0, 0, 0, date("n",$mytime), 10))).', '.date('Y',$mytime);  	  
		$header = str_replace("%nombre_agente%",$this->userInfo['name'].'<br>'.$this->userInfo['phone'], $header);
		$header = str_replace("%email_agente%",$this->userInfo['email'], $header);
		//$header = str_replace("logoutAgent.php",$this->linkLogout, $header);
		$header = str_replace("http://","https://", $header);
		$header = str_replace('<a href="logoutAgent.php">','<a href="'.$this->linkLogout.'" target="_blank"  onclick="javascript: setTimeout(function(){ window.location.reload(true); },3000);" >', $header);				
		if ($this->environment !== "PROD") {
			$header	= utf8_decode($header);
		}
		$header = str_replace("%fecha%",utf8_encode($myDate),$header);		
		return $header;
	}

	function getFooter(){
		$footer = $this->fgeturl($this->footerUrl);
		$footer = str_replace("http://","https://", $footer);				
		return $footer;
	}

	function hasCookie(){
		return !is_null($this->sessionToken);
	}

	function getPhone($type,$value){
	    $value = trim($value);
	    $arrValues = explode(" ",$value);
	    $result = "";
	    switch($type){
	        case "country":
	            if(count($arrValues)==3){
	                $result = $arrValues[0];
	            } else {
	               $result = "56";    
	            }
	            break;
	        case "code":
	            if(count($arrValues)==3){
	                $result = $arrValues[1];
	            } elseif (count($arrValues) == 2){
	                $result = $arrValues[0];
	            } elseif(strlen($value) == 10) {
	                $result = substr($value, 0,2);
	            } elseif(strlen($value) == 9) {
	                $result = substr($value, 0,1);                
	            } else {
	                $result = "2";
	            }     
	            break;
	        case "number":
	            if(count($arrValues)==3){
	                $result = $arrValues[2];
	            } elseif (count($arrValues) == 2){
	                $result = $arrValues[1];
	            } else {
	                $result = substr($value, -8);                
	            }           
	            break;        
	        default:
	            break;
	    }
	    return $result;
	}
	
}

?>