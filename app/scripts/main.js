require.config({
	waitSeconds: 200,
	urlArgs: 'v=' + window.APP_VERSION,
	paths: {
		//Bower dependencies
		'jquery': '../vendor/jquery/dist/jquery.min',
		'jquery-ui': '../vendor/jquery-ui/ui/minified/jquery-ui.min',
		'bootstrap': '../vendor/bootstrap/dist/js/bootstrap.min',
		'lodash': '../vendor/lodash/dist/lodash.min',
		'angular': '../vendor/angular/angular.min',
		'angular-cookies': '../vendor/angular-cookies/angular-cookies.min',
		'angular-i18n': '../vendor/angular-i18n/angular-locale_es-cl',
		'angular-animate': '../vendor/angular-animate/angular-animate.min',
		'angular-ui-route': '../vendor/angular-ui-router/release/angular-ui-router.min',
		'angular-touch': '../vendor/angular-touch/angular-touch.min',
		'angular-ui-bootstrap': '../vendor/angular-bootstrap/ui-bootstrap-tpls.min',
		'angular-storage': '../vendor/ngstorage/ngStorage.min',
		'angular-ui-slider': '../vendor/angular-ui-slider/src/slider',

		'angular-img-fallback': '../vendor/angular-img-fallback/angular.dcb-img-fallback.min',
		'angular-ui-mask': '../vendor/angular-ui-mask/dist/mask.min',
		'ng-map': '../vendor/ngmap/build/scripts/ng-map.min',

		'moment': '../vendor/moment/min/moment.min',
		'moment-locale': '../vendor/moment/locale/es',
		//Cocha Modules
		'cocha-common-services': window.COCHA_LIBS_BASE + '/cocha-common-services/' + window.COCHA_LIBS_FOLDER + '/cocha-common-services' + window.COCHA_LIBS_TYPE,
		'cocha-access-remote': window.COCHA_LIBS_BASE + '/cocha-access-remote/' + window.COCHA_LIBS_FOLDER + '/cocha-access-remote' + window.COCHA_LIBS_TYPE,
		'cocha-access-local': window.COCHA_LIBS_BASE + '/cocha-access-local/' + window.COCHA_LIBS_FOLDER + '/cocha-access-local' + window.COCHA_LIBS_TYPE,
		'cocha-utils-data': window.COCHA_LIBS_BASE + '/cocha-utils-data/' + window.COCHA_LIBS_FOLDER + '/cocha-utils-data' + window.COCHA_LIBS_TYPE,
		'cocha-common-constants': window.COCHA_LIBS_BASE + '/cocha-common-constants/' + window.COCHA_LIBS_FOLDER + '/cocha-common-constants' + window.COCHA_LIBS_TYPE,
		'cocha-utils-filters': window.COCHA_LIBS_BASE + '/cocha-utils-filters/' + window.COCHA_LIBS_FOLDER + '/cocha-utils-filters' + window.COCHA_LIBS_TYPE,
		'cocha-utils-translation': window.COCHA_LIBS_BASE + '/cocha-utils-translation/' + window.COCHA_LIBS_FOLDER + '/cocha-utils-translation' + window.COCHA_LIBS_TYPE,
		'cocha-calendar-directive': window.COCHA_LIBS_BASE + '/cocha-calendar-directive/' + window.COCHA_LIBS_FOLDER + '/cocha-calendar-directive' + window.COCHA_LIBS_TYPE,
		'cocha-common-directives': window.COCHA_LIBS_BASE + '/cocha-common-directives/' + window.COCHA_LIBS_FOLDER + '/cocha-common-directives' + window.COCHA_LIBS_TYPE,
		'cocha-header-footer': window.COCHA_LIBS_BASE + '/cocha-header-footer/' + window.COCHA_LIBS_FOLDER + '/cocha-header-footer' + window.COCHA_LIBS_TYPE

	},
	shim: {
		'angular': {
			'deps': ['jquery'],
			'exports': 'angular'
		},
		'angular-cookies': ['angular'],
		'angular-i18n': ['angular'],
		'angular-animate': ['angular'],
		'angular-ui-route': ['angular'],
		'angular-touch': ['angular'],
		'angular-storage': ['angular'],
		'angular-img-fallback': ['angular'],
		'angular-ui-bootstrap': ['angular', 'bootstrap'],

		'app': ['angular'],
		'bootstrap': ['jquery'],
		'moment-locale': ['moment'],
		'jquery-ui': ['jquery'],
		'angular-ui-slider': ['angular', 'jquery-ui'],
		'angular-ui-mask': ['angular'],
		'ng-map': ['angular'],

		//Cocha Modules
		'cocha-common-services': ['angular'],
		'cocha-access-remote': ['angular'],
		'cocha-access-local': ['angular'],
		'cocha-utils-data': ['angular'],
		'cocha-common-constants': ['angular'],
		'cocha-utils-filters': ['lodash', 'moment', 'angular', 'cocha-common-constants'],
		'cocha-utils-translation': ['angular', 'lodash'],
		'cocha-common-directives': ['angular'],
		'cocha-calendar-directive': ['angular'],
		'cocha-header-footer': ['angular']

	},
	priority: [
		'jquery',
		'angular',
		'moment'
	]
});

//http://code.angularjs.org/1.2.1/docs/guide/bootstrap#overview_deferred-bootstrap
window.name = 'NG_DEFER_BOOTSTRAP!';

require([
	'angular',
	'app',
	'angular-cookies',
	'angular-i18n',
	'angular-animate',
	'angular-ui-route',
	'angular-touch',
	'angular-ui-bootstrap',
	'angular-storage',
	'angular-ui-slider',
	'jquery',
	'bootstrap',
	'lodash',
	'moment',
	'moment-locale',
	'angular-img-fallback',
	'angular-ui-mask',
	'ng-map',
	//Cocha Modules
	'cocha-common-services',
	'cocha-access-remote',
	'cocha-access-local',
	'cocha-utils-data',
	'cocha-common-constants',
	'cocha-utils-filters',
	'cocha-utils-translation',
	'cocha-common-directives',
	'cocha-calendar-directive',
	'cocha-header-footer',
], function(angular, app) {
	'use strict';

	angular.element().ready(function() {
		angular.resumeBootstrap([app.name]);
	});
});
