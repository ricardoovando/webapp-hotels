define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.filters.dateFormatFilter', [])
		.filter('dateFormatFilter', function() {
			return function(val, type) {

				if (moment(val).isValid()) {
					switch (type) {
						case 'month':
							return moment.parseZone(val).format('DD MMM YY');
						case 'day':
							return moment.parseZone(val).format('ddd D MMM');
						case 'long-day':
							return moment.parseZone(val).format('dddd DD [de] MMMM');
						case 'short-month':
							return moment.parseZone(val).format('DD MMM');
						case 'short-month-name':
							return moment.parseZone(val).format('MMM');
						case 'long-date':
							return moment.parseZone(val).format('ddd DD [de] MMMM [de] YYYY');
						case 'hotel-date':
							return moment.parseZone(val).format('ddd DD [de] MMM [de] YYYY');
						case 'hotel-date-itinerary':
							return moment.parseZone(val).format('dddd, DD [de] MMM [de] YYYY');
						case 'day-hour':
							return moment.parseZone(val).format('DD [de] MMMM HH[:]mm [hrs]');
						case 'day-name':
							return moment.parseZone(val).format('dddd');
						case 'only-month-name':
							return moment.parseZone(val).format('MMMM');
						case 'calendar-day':
							return moment.parseZone(val).format('DD');
						default:
							throw new Error("cDateFilter can have types: " + "'month', " + "'day', " + "'long-day', " + "'short-month', " + "'short-month-name', " + "'long-date', " + "'day-hour', " + "'day-name', " + "'only-month-name', " + "'calendar-day'");
					}
				}
			};
		});
});
