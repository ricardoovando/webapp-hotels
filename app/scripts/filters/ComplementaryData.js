define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.filters.ComplementaryData', [])
		.filter('plural', function() {
			return function(number, singular, plural) {
				return (parseInt(number, 10) !== 1) ? (plural || 's') : (singular || '');
			};
		})
		.filter('categoryData', function(StaticDataService) {
			return function(category, type) {
				return (category) ? (StaticDataService.getCategoryData(type, category) || '') : '';
			};
		})
		.filter('activeCategory', function() {
			return function(categories) {
				return !_.isUndefined(_.find(categories, function(category, key) {
					return (category && key !== 'partner');
				}));
			};
		})
		.filter('taxesMessage', function() {
			return function(room) {
				if (room.supplierCode === 'SAB' && (room.totalTax === 0 || room.totalTax === null)) {
					return 'incluye impuestos';
				}
				else if ((room.supplierCode !== 'SAB' && room.supplierCode !== 'TRV') || (room.supplierCode === 'SAB' && room.totalTax > 0)) {
					return 'No incluye impuestos';
				}
				else if (room.supplierCode === 'TRV' && room.totalTax === null) {
					return 'incluye impuestos';
				}
				else if (room.supplierCode === 'TRV' && room.totalTax === 0) {
					return 'incluye impuestos';
				}
				else if (room.supplierCode === 'TRV' && room.totalTax > 0) {
					return 'No incluye impuestos';
				}
				else {
					return '';
				}
			};
		})
		.filter('objectLength', function() {
			return function(input) {
				if (!angular.isObject(input)) {
					return 0;
				}
				return Object.keys(input).length;
			};
		});
});
