define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.filters.ObjectKeyLengthFilter', [])
		.filter('ObjectKeyLength', function() {
			return function(input) {
				if (!angular.isObject(input)) {
					//throw Error("Usage of non-objects with keylength filter!!");
					return 0;
				}
				return Object.keys(input).length;
			};
		});
});
