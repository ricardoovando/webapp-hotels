define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.filters.trustUrlFilter', [])
		.filter('trustUrl', function($sce) {
			return function(url) {
				return $sce.trustAsResourceUrl(url);
			};
		})
		.filter('trustHtml', function($sce) {
			return function(html) {
				return $sce.trustAsHtml(html);
			};
		});
});
