define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.filters.USDtoCLPFilter', [])
		.filter('USDtoCLPFilter', function(CurrencyService) {
			return function(val, currency) {
				var exchangeRate = CurrencyService.getExchangeRate();
				var newValue = 0;
				if (typeof currency === 'undefined') {
					newValue = parseFloat(val) * parseFloat(exchangeRate);
				}
				else {
					newValue = parseFloat(val) * parseFloat(currency);
				}

				/*
				if (isNaN(exchangeRate) && (typeof currency !== 'undefined')) {
					newValue = val * currency;
				}
				else {
					newValue = val * exchangeRate;
				}*/
				return '$' + Math.ceil(newValue).toFixed(0).replace(/\d(?=(\d{3})+$)/g, '$&.');
			};
		});
});
