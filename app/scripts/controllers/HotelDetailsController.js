define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.controllers.HotelDetailsController', ['ngMap'])
		.controller('HotelDetailsController', function($compile, NgMap, $location, $document, $window, $sce, $scope, $uibModal, $state, $stateParams, $timeout, AppCommonService, AnalyticsService, HotelSearchDataService, HotelDetailsDataService, HotelMapDataService, B2BDataService, CurrencyService, QuoteService) {

			var callbacksOK = {
				CURRENCY_OK: function(exchangeRate) {
					$scope.content.currency = exchangeRate;
					if ($stateParams.corporateCode) {
						AppCommonService.validateSearchNemo($stateParams.corporateCode, callbacksOK.NEMO_SUCCESS, callbacksError.ERROR_NEMO);
					}
					else {
						callbacksOK.NEMO_SUCCESS();
					}
				},
				NEMO_SUCCESS: function() {
					getHotelRegion($stateParams.hotel);
				},
				ORIGIN_SUCCESS: function(_data) {
					if ($scope.view.hotelRequestData === null) {
						$scope.view.hotelRequestData = setRequestData(_data);
					}
					HotelDetailsDataService.searchHotelInfo($stateParams, null, null, callbacksOK, callbacksError);
					$scope.view.hotelSearchData = HotelSearchDataService.getHotelSearchButtonData($stateParams, _data);
					$scope.view.regionName = _data.regionNameLong;
				},
				SEARCH_SUCCESS: function(_data) {
					setSearchContent(_data, true);
				},
				SEARCH_ROOMS_SUCCESS: function(_data) {
					setSearchContent(_data);
					AppCommonService.refreshUrl("hotel-details", _data.stateData);
				}
			};

			var callbacksError = {
				ERROR_RESERVATION: internalError,
				ERROR_PAYMENTURL: internalError,
				ERROR_NETWORK: networkError,
				ERROR_INTERNAL: internalError,
				ERROR_NORESULT: noresults,
				CURRENCY_ERROR: currencyError,
				ERROR_AVAILABILITY: noRoomAvailabilityError,
				ERROR_NEMO: function() {
					customError('Nemo invalido, por favor utilice otro nemo para la búsqueda.');
				}
			};

			//$scope.showMap = false;
			$scope.view = HotelDetailsDataService.getViewData();
			$scope.view.textCategory = "<span class='category-title'>Cocha Preferred Partners</span> ";
			$scope.view.textCategory += "es un conjunto de hoteles con alianzas especiales que nos permiten entregar beneficios exclusivos a nuestros clientes, tales como: <br> ";
			$scope.view.textCategory += "<ul>";
			$scope.view.textCategory += "<li> - Upgrade de habitación </li> ";
			$scope.view.textCategory += "<li> - Early Check - in / Late Check-out</li>";
			$scope.view.textCategory += "<li> - Food & beverage credit / Spa services credit </li>";
			$scope.view.textCategory += "<li> - Complimentary wifi, comidas y transfer </li>";
			$scope.view.textCategory += "<li> - Entre otros servicios, descuentos y cortesías </li>";
			$scope.view.textCategory += "</ul>";
			$scope.view.textCategory += "<p>​La lista anterior es sujeta a disponibilidad y variará por cada hotel. Para mayor información contacta al Departamento Lujo. </p>";
			$scope.view.textCategory += "Para acceder a estos beneficios revisa las tarifas de SABRE.";

			$scope.view.charges = '';
			$scope.view.chargeLoader = true;
			$scope.view.chargesRoomReference = null;

			$scope.totalChildrenPax = 0;
			$scope.totalAdultsPax = 0;

			$scope.content = HotelDetailsDataService.getContentData($stateParams);
			calcPax();
			CurrencyService.getExchangeRate(callbacksOK.CURRENCY_OK, callbacksError);
			var stateData = HotelDetailsDataService.getDatelessParams($stateParams);
			if (stateData !== false) {
				AppCommonService.refreshUrl('hotel-details', stateData);
			}
			addListeners();

			$scope.content.B2Buser = B2BDataService;

			function setRequestData(_data) {
				var requestData = {};
				requestData.destination = _data.id;
				requestData.full_name_region = _data.regionNameLong;
				requestData.arrival = $stateParams.arrival;
				requestData.departure = $stateParams.departure;
				_.forEach($stateParams, function(value, key) {
					if (key.toString().indexOf('room') !== -1 && typeof $stateParams[key] !== 'undefined' && $stateParams[key] !== null) {
						requestData[key] = $stateParams[key];
					}
				});
				return requestData;
			}

			function getHotelRegion(hotelId) {
				$scope.view.hotelRequestData = HotelDetailsDataService.getHotelData($stateParams.hotel);
				if ($scope.view.hotelRequestData !== null) { //if you can, take it from the local storage
					var placeData = {
						id: $scope.view.hotelRequestData.destination,
						regionName: $scope.view.hotelRequestData.full_name_region,
						regionNameLong: $scope.view.hotelRequestData.full_name_region
					};
					callbacksOK.ORIGIN_SUCCESS(placeData);
				}
				else { //or just ask the webservice for the info
					HotelSearchDataService.getPlaceDestination(hotelId, callbacksOK, callbacksError, 'hotel');
				}
			}

			function searchRooms(data, params) {
				HotelDetailsDataService.searchRooms(data, params, null, callbacksOK, callbacksError);
			}

			function setSearchContent(_data, firstLoad) {
				if (firstLoad !== 'undefined' && firstLoad === true) {
					AnalyticsService.detailImpression(_data, $scope.view.hotelRequestData);
					$timeout(function() {
						$scope.view.pageLoader = false;
						NgMap.getMap("map2").then(function(map) {
							$scope.map = map;
							$timeout(function() {
								google.maps.event.trigger($scope.map, 'resize');
								var latlng = new google.maps.LatLng($scope.content.hotelInfo.latitude, $scope.content.hotelInfo.longitude);
								$scope.map.setCenter(latlng);
								$scope.map.setZoom(13);
								google.maps.event.trigger($scope.map, 'resize');
							}, 1500);
						});
					}, 1500);
					if (_data.roomsBySupplier.hasOwnProperty('EAN')) {
						$scope.view.chargesRoomReference = _data.roomsBySupplier.EAN[0];
					}
				}
				$scope.view.detailsLoader = false;
				$scope.view.lowestPriceId = _data.lowestId;
				$scope.content.hotelInfo = _data.info;
				$scope.content.rooms = _data.rooms;
				$scope.content.roomsBySupplier = _data.roomsBySupplier;
				$scope.content.lowest = _data.lowest;
				$scope.content.imageMain = (_data.info.images.length > 0) ? _data.info.images[0].url : '';
				$scope.content.defaultValues = _data.defaultValues;
				$scope.content.services = _data.services;
				$scope.$applyAsync();
			}

			$scope.getImageName = function(urlImage) {
				if (typeof urlImage !== 'undefined' && urlImage.length > 0 && urlImage !== '') {
					var ext = urlImage.substring(urlImage.lastIndexOf('.') + 1);
					var type = urlImage.substring(urlImage.length - ((ext.length) + 3), urlImage.length - (ext.length + 1));
					if (type !== '_b') {
						return urlImage;
					}

					var image = urlImage.substring(0, urlImage.length - ((ext.length) + 3));
					return (image + '_z.' + ext);
				}
				else {
					return "error.jpg";
				}
			};

			$scope.getHotelMap = function(hotelId, hotelName, latitude, longitude, rating, address) {

				$scope.view.loader = true;
				$scope.view.mapType = 'hotel';
				$scope.content.mapData.hotel = hotelId;
				$scope.content.mapData.hotelName = hotelName;
				$scope.content.mapData.latitude = latitude;
				$scope.content.mapData.longitude = longitude;
				$scope.content.mapData.rating = rating;
				$scope.content.mapData.address = address;
				$scope.content.mapData.mapId = 'mapModal';
				$scope.view.closestPoints = [];
				//HotelMapDataService.getHotelPoints($scope.content.mapData, callbacksOK, callbacksError);

				$uibModal.open({
					animation: true,
					controller: 'ModalController',
					templateUrl: 'hotel-full-map',
					windowClass: 'modal-map',
					scope: $scope,
					resolve: {
						content: function() {
							return null;
						}
					}
				});
			};

			$scope.searchCharges = function() {
				var okCallback = function(_data) {
					$scope.view.charges = _data.checkInInstructions;
					$scope.view.chargeLoader = false;
					$scope.$applyAsync();
				};
				var errorCallback = {
					ERROR_INTERNAL: function() {
						$scope.view.chargeLoader = false;
						$scope.$applyAsync();
					}
				};
				if (!_.isNull($scope.view.chargesRoomReference)) {
					HotelDetailsDataService.getHotelCharges($scope.view.chargesRoomReference, okCallback, errorCallback);
				}
				else {
					if ($scope.content.roomsBySupplier.hasOwnProperty('EAN') && $scope.content.roomsBySupplier.EAN.length > 0) {
						var data = $scope.content.roomsBySupplier.EAN[0];
						HotelDetailsDataService.getHotelCharges(data, okCallback, errorCallback);
					}
					else {
						$scope.view.chargeLoader = false;
						$scope.$applyAsync();
					}
				}
			};

			$scope.openGallery = function() {
				$scope.view.mapType = 'photos-hotel';
				$uibModal.open({
					scope: $scope,
					size: 'lg',
					templateUrl: 'details-gallery',
					controller: 'ModalController',
					resolve: {
						content: function() {
							return null;
						}
					}
				});
			};

			$scope.getImageSupplier = function(strSupplier) {
				return "../img/" + strSupplier + ".png";
			};

			$scope.showImage = function(index) {
				$scope.content.imageMain = $scope.content.hotelInfo.images[index].url;
			};

			$scope.showDetail = function(e, point, index) {
				NgMap.getMap("map").then(function(map) {
					$scope.map = map;
					$scope.point = point;
					$scope.map.showInfoWindow('info-' + index, this);
				});
			};

			$scope.saveQuote = function(flagBooking, room, roomPosition, hotelPosition, doneFn) {
				room.tripAdvisorUrl = $scope.content.hotelInfo.tripAdvisorUrl || null;
				var quoteCbOK = function(_response) {
					doneFn();
					try {
						top.postMessage('OK', $window.QUOTE_URL);
					}
					catch (e) {
						console.error('sin comunicacion a crm');
					}

					var totalPaxs = totalAdults() + totalChildrens();

					$uibModal.open({
						size: ($scope.view.smallView ? 'xs' : 'lg'),
						templateUrl: 'quote-success',
						windowClass: 'common-modal',
						controller: 'ModalController',
						resolve: {
							content: function() {
								return {
									prices: _response.product.prices,
									assistance: {
										usd: ($scope.view.assistanceChoice ? $scope.content.assistancePrice.usd.perPerson * totalPaxs : 0),
										clp: ($scope.view.assistanceChoice ? $scope.content.assistancePrice.clp.perPerson * totalPaxs : 0)
									},
									transfer: {
										usd: ($scope.view.transferChoice ? $scope.content.transfer.prices.usd.perPerson * totalPaxs : 0),
										clp: ($scope.view.transferChoice ? $scope.content.transfer.prices.clp.perPerson * totalPaxs : 0)
									},
									adults: totalAdults(),
									children: totalChildrens(),
									msg: _response.msg,
									currency: 'USD'
								};
							}
						}
					});
				};

				QuoteService.sendQuote({
					flagBooking: flagBooking,
					room: room,
					roomPosition: roomPosition,
					hotelPosition: hotelPosition,
					hotel: $scope.content.hotelInfo,
					quote: $window.QUOTE,
					regionName: $scope.view.regionName
				}, quoteCbOK, callbacksError, doneFn);
			};

			$scope.checkout = function(flagBooking, room, roomPosition, hotelPosition) {
				hotelPosition = hotelPosition;
				if (typeof room !== 'undefined') {
					if (flagBooking) {
						$window.open(room.deepLink, '_blank');
						AnalyticsService.cart($scope.content.hotelInfo, $scope.view.hotelRequestData, roomPosition, room);
					}
					else {
						HotelDetailsDataService.saveRoomData(room);
						$timeout(function() {
							$window.open($state.href('checkout', {
								force: 1,
								fid: $window.FLOW_ID
							}, {
								absolute: true
							}), '_blank');
						}, 2000);
						AnalyticsService.cart($scope.content.hotelInfo, $scope.view.hotelRequestData, roomPosition, room);
					}
				}
			};

			$scope.toogleTabService = function(type) {
				$scope.view[type] = !$scope.view[type];
			};

			$scope.filterRooms = function() {
				searchRooms($stateParams);
			};

			function internalError() {
				$uibModal.open({
					scope: $scope,
					size: 'sm',
					templateUrl: 'hotels-error-reservation'
				});
				$scope.view.isBooking = false;
			}

			function customError(msg) {
				$scope.view.msgError = (typeof msg !== 'undefined' && msg !== '') ? msg : '';
				$uibModal.open({
					scope: $scope,
					size: 'sm',
					templateUrl: 'hotels-error-reservation-msg-only'
				});
			}

			function currencyError() {
				$uibModal.open({
					scope: $scope,
					size: 'sm',
					templateUrl: 'hotels-error-currency'
				});
				$scope.view.isBooking = false;
			}

			function networkError() {
				$uibModal.open({
					scope: $scope,
					size: 'sm',
					templateUrl: 'hotels-error-network'
				});
				$scope.view.isBooking = false;
			}

			function noresults(flagNoModal) {
				$scope.content.rooms = [];
				$scope.view.detailsLoader = false;
				if (typeof flagNoModal === 'undefined') {
					$uibModal.open({
						scope: $scope,
						size: 'sm',
						templateUrl: 'rooms-no-results'
					});
				}
			}

			function noRoomAvailabilityError() {
				$scope.view.loadingResults = false;
				$uibModal.open({
					size: 'sm',
					templateUrl: 'hotels-error-availability'
				});
				return;
			}

			function addListeners() {
				$scope.$on('FilterDataUpdated', function(event, data) {
					data.searchboxData.hotel = $scope.content.hotel;
					$scope.content.rooms = [];
					$scope.view.detailsLoader = true;
					$timeout(function() {
						searchRooms(data.searchboxData, data.filterData);
					}, 1800);

					console.info("[HotelDetailsController]", "Filter params updated and received broadcast, refreshing results found. Event: ", event);
				});
			}

			function calcPax(cb) {
				var search = $location.search();
				$scope.totalAdultsPax = 0;
				$scope.totalChildrenPax = 0;
				_.each(search, function(roompax, index) {
					if (index.indexOf('room') >= 0 && roompax.length > 0) {
						var pax = roompax.split(',');
						$scope.totalAdultsPax += parseInt(pax[0]);
						if ((pax.length - 1) > 0) {
							$scope.totalChildrenPax += (pax.length - 1);
						}
					}
				});
				if (typeof cb === 'function') {
					cb();
				}
			}

			function totalAdults() {
				return $scope.totalAdultsPax;
			}

			function totalChildrens() {
				return $scope.totalChildrenPax;
			}
		});
});
