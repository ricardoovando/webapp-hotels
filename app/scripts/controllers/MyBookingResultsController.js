define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.controllers.MyBookingResultsController', [])
		.controller('MyBookingResultsController', function($state, $stateParams, $timeout, $window, $uibModal, $scope, MyBookingResultsDataService, HotelReservationParserService, cUtilsFields, cUtilsObject, B2BDataService) {

			$scope.content = MyBookingResultsDataService.getContentData();
			$scope.view = MyBookingResultsDataService.getViewData();

			$scope.functions = {
				getStatusName: function(bookStatus) {

					bookStatus = (typeof bookStatus !== 'undefined' && bookStatus !== null) ? bookStatus.toLowerCase() : '';
					switch (bookStatus) {
						case 'ac':
							return 'Activa';
						case 'co':
							return 'Consumida';
						case 'de':
							return 'Cancelada, en proceso devolución';
						case 'no':
							return 'Cancelada, sin devolución';
						case 'xs':
							return 'Cancelada, devolución procesada';
						case 'pe':
							return 'Pendiente';
						default:
							return 'Estado Indeterminado';
					}
				},
				search: function(parameters) {
					$scope.view.loadingResults = true;
					$scope.$applyAsync();
					MyBookingResultsDataService.getReservationData(parameters, callbacksOK.RESERVATION_FOUND, callbacksError);
				},
				goBack: function() {
					$state.go('my-booking');
				},
				goToHome: function() {
					$state.go('home');
				},
				goToDetail: function(result, openInSamePage) {
					openInSamePage = (typeof openInSamePage === 'undefined') ? false : openInSamePage;
					if (result.hasOwnProperty('supplier') && result.hasOwnProperty('spnr') && !_.isNull(result.spnr)) {
						if (result.supplier === 'BKG') {
							MyBookingResultsDataService.getBookingDeeplink(result, callbacksOK.DEEPLINK_FOUND, callbacksError);
						}
						else if (result.supplier === 'EAN') {
							MyBookingResultsDataService.saveBookingData(result);
							//$state.go('my-booking-detail');
							if (openInSamePage) {
								$state.go('my-booking-detail');
							}
							else {
								$window.open($state.href('my-booking-detail'), '_blank');
							}
						}
						else if (!result.supplier && result.spnr.charAt(0) === 'B') {
							result.supplier = "BKG";
							MyBookingResultsDataService.getBookingDeeplink(result, callbacksOK.DEEPLINK_FOUND, callbacksError);
						}
						else {
							errorModal("supplier aún no soportado.");
						}
					}
					else {
						errorModal('no existe supplier o codigo para consultar la reserva');
					}
				}
			};

			var callbacksOK = {
				RESERVATION_FOUND: function() {
					//$state.go('my-booking');
					//actualizar data
					//end loader
					$scope.view.pageIndex = 0;
					$scope.view.loadingResults = false;
					$scope.$applyAsync();
					$scope.content.results = MyBookingResultsDataService.getSearchResults();
				},
				DEEPLINK_FOUND: function(_url) {
					$window.open(_url, '_blank');
					//data = data;
					//go to deeplink url
					//alert(JSON.stringify(data));
					//$scope.$apply();

				}
			};

			var callbacksError = {
				ERROR_INTERNAL: errorInternal,
				ERROR_NORESULT: errorNoResult
			};

			$scope.content = MyBookingResultsDataService.getContentData();
			if ($scope.content.results.length === 0 && $stateParams.refresh === null) {
				var parameters = MyBookingResultsDataService.getSavedSearchParameters();
				MyBookingResultsDataService.getReservationData(parameters, callbacksOK.RESERVATION_FOUND, callbacksError);
			}
			else if ($scope.content.results.length === 1 && !$scope.content.isB2B) {
				var result = MyBookingResultsDataService.getSearchResults();
				if (result[0].supplier === "EAN") {
					$scope.functions.goToDetail(result[0], true);
				}
				else if (result[0].supplier === "BKG") {
					MyBookingResultsDataService.getBookingDeeplink(result[0], callbacksOK.DEEPLINK_FOUND, callbacksError);
				}
				else {
					errorModal('supplier [' + result[0].supplier + '] no soportado ');
				}
			}

			$scope.view = MyBookingResultsDataService.getViewData();

			addListeners();

			$scope.searchReservation = function() {

				$scope.view.errors = MyBookingResultsDataService.getSearchErrors($scope.content, $scope.view.errors, !(_.isNull(B2BDataService)));
				//MyBookingResultsDataService.getReservationData($scope.content);
				$scope.$watch('content', function() {
					$scope.view.errors = MyBookingResultsDataService.getSearchErrors($scope.content, $scope.view.errors, !(_.isNull(B2BDataService)));
				}, true);

				if (!cUtilsObject.containsValueDeep($scope.view.errors, true)) {
					MyBookingResultsDataService.getReservationData($scope.content, callbacksOK.RESERVATION_FOUND, callbacksError);
					//callbacksOK.RESERVATION_FOUND();
				}
				else {
					formError();
				}

			};

			$scope.getBookingDeeplink = function(result) {
				MyBookingResultsDataService.getBookingDeeplink(result.spnr, callbacksOK.DEEPLINK_FOUND, callbacksError);
			};

			function formError() {
				$uibModal.open({
					templateUrl: 'my-booking-form-error',
					size: 'sm'
				});
			}

			function errorModal(msg) {
				$scope.view.loadingResults = false;
				$scope.view.modalText = (typeof msg !== 'undefined') ? msg : 'error interno';
				$uibModal.open({
					size: 'sm',
					scope: $scope,
					templateUrl: 'hotels-error-msg'
				});
			}

			$scope.collapseCalendar = function(type) {
				$scope.view.collapseCalendar[type] = true;
			};

			$scope.closeModalCalendar = function() {
				window.scrollTo(0, 0);
				$timeout(function() {
					$scope.view.openCalendarXs.arrival = false;
					$scope.view.openCalendarXs.departure = false;
				}, 500);
			};

			$scope.openCalendar = function(_type, _event) {
				closeCollapses(_type);
				if (_event) {
					_event.stopImmediatePropagation();
				}
				var noType;
				if (typeof _type !== 'undefined' && _type.indexOf('book') !== -1) {
					noType = _type === 'bookarrival' ? 'bookdeparture' : 'bookarrival';
				}
				else {
					noType = _type === 'arrival' ? 'departure' : 'arrival';
				}

				if ($scope.view.smallView === false) {
					if ($scope.view.collapseCalendar[noType] === true) {
						$scope.view.collapseCalendar[_type] = !$scope.view.collapseCalendar[_type];
						if (!$scope.view.collapseCalendar[_type]) {
							$scope.$broadcast('cCalendarDirective:goToMonth', _type);
						}
					}
					else {
						$scope.view.collapseCalendar[noType] = true;
						$timeout(function() {
							$scope.view.collapseCalendar[_type] = !$scope.view.collapseCalendar[_type];
							if (!$scope.view.collapseCalendar[_type]) {
								$scope.$broadcast('cCalendarDirective:goToMonth', _type);
							}
						}, 500);
					}
				}
				else {
					$scope.view.openCalendarXs[_type] = true;
					$scope.$broadcast('cCalendarDirective:goToMonth', _type + '-xs');
				}

			};

			$scope.cleanEmail = function(input) {
				return cUtilsFields.cleanEmail(input);
			};

			$scope.cleanName = function(input) {
				//return cUtilsFields.cleanName(input);
				return HotelReservationParserService.cleanName(input, true);
			};

			$scope.remainOpen = function(event) {
				event.stopImmediatePropagation();
			};

			function closeCollapses() {
				if ($scope.view.closeCollapsesCount === 1) {
					angular.element(window).click(function() {
						$scope.view.collapseCalendar.departure = true;
						$scope.view.collapseCalendar.arrival = true;
						$scope.view.collapseCalendar.bookdeparture = true;
						$scope.view.collapseCalendar.bookarrival = true;
						$scope.$apply();
					});
					$scope.$on('$destroy', function() {
						angular.element(window).off('click');
					});
					$scope.view.closeCollapsesCount++;
				}
			}

			function errorInternal() {
				$scope.view.loadingResults = false;
				errorModal("Se produjo un error inesperado, por favor contactar a hoteles online");
				$scope.$applyAsync();
			}

			function errorNoResult() {
				$scope.view.loadingResults = false;
				//errorModal("No se encuentra ningún resultado de acuerdo a tus criterios de búsqueda. Favor revisa los datos  ingresados");
				$scope.view.pageIndex = 0;
				$scope.content.results = [];
				$scope.$applyAsync();
			}

			$scope.selectStatus = function(_statusData) {
				$scope.content.statusBooking = _statusData;
				$scope.view.showStatusOption = false;
			};

			$scope.toggleStatusOption = function(_event, value) {
				closeCollapses();
				if (_event) {
					_event.stopImmediatePropagation();
				}
				if (typeof value !== 'undefined') {
					$scope.view.showStatusOption = value;
				}
				else {
					$scope.view.showStatusOption = !$scope.view.showStatusOption;
				}
			};

			function checkForWidth(scope) {
				$timeout(function() {
					var win = angular.element($window);
					if (win !== null && typeof win !== 'undefined') {
						if (win.outerWidth() <= 991) {
							scope.view.properWidth = false;
						}
						if (win.outerWidth() <= 768) {
							scope.view.smallView = true;
						}
					}
					else {
						checkForWidth(scope);
					}
				}, 1000);
			}

			$scope.page = function(_type) {
				if (_type === "next") {
					$scope.view.pageIndex += 20;
				}
				else {
					$scope.view.pageIndex -= 20;
				}
			};

			function addListeners(scope) {
				checkForWidth(scope);

			}

		});
});
