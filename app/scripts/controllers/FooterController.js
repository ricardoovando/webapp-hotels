/*
define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.controllers.FooterController', [])
		.controller('FooterController', function($uibModal, $scope, $timeout, AppCommonService, cUtilsObject, cUtilsFields) {
			$scope.content = {
				contact: {
					name: '',
					foid: {
						entire: '',
						digit: '',
						number: ''
					},
					email: '',
					countryCode: '56',
					areaCode: '9',
					phoneNumber: '',
					currentUrl: '',
					subscribe: false,
				},
				subscribe: {
					subscribeEmail: '',
					terms: false
				}

			};
			$scope.view = {
				subscribingState: '',
				activeMap: false,
				mapLoader: null,
				errors: {
					emailNotSet: false,
					emailInvalid: false,
					susbcribe: false
				}
			};

			$scope.mapLocation = {
				center: '-33.414445,-70.60225',
				currentLocation: false,
				zoom: 18,
				centered: false,
				options: {
					scrollwheel: false
				}
			};

			$scope.activateMap = function() {
				if ($scope.view.activeMap === true) {
					return;
				}
				$scope.view.mapLoader = true;
				$scope.view.activeMap = true;
				$timeout(function() {
					$scope.view.mapLoader = false;
				}, 2800);
			};

			function validateParams() {
				$scope.view.errors = {
					emailNotSet: cUtilsObject.isVoid($scope.content.subscribe.subscribeEmail),
					emailInvalid: cUtilsFields.isInvalidEmail($scope.content.subscribe.subscribeEmail),
					subscribe: !$scope.content.subscribe.terms
				};
			}
			var watcher = null;
			$scope.subscribe = function() {
				validateParams();
				var callbackOK = function() {
					$scope.view.subscribingState = 'success';
					$scope.content.subscribe.terms = false;
					$scope.content.subscribe.subscribeEmail = '';
					$timeout(function() {
						$scope.view.subscribingState = '';
					}, 4000);
				};

				var callbackError = function() {
					$scope.view.subscribingState = 'error';
					$timeout(function() {
						$scope.view.subscribingState = '';
					}, 4000);
				};
				if (!watcher) {
					watcher = $scope.$watch('content.subscribe', function() {
						validateParams();
					}, true);
				}
				if (!cUtilsObject.containsValueDeep($scope.view.errors, true) && $scope.content.subscribe.terms) {
					$scope.view.subscribingState = 'loading';
					AppCommonService.subscribeNewsletterSimple($scope.content.subscribe.subscribeEmail, callbackOK, callbackError);
					watcher();
				}
			};

			$scope.clickToCall = function(_event) {
				_event.stopImmediatePropagation();
				AppCommonService.openClickToCallModal($scope.content.contact);
			};

			$scope.geoLocation = function() {
				$scope.activateMap();
				$scope.mapLocation.centered = false;
				$scope.mapLocation.currentLocation = false;

				if (navigator.geolocation) {
					navigator.geolocation.getCurrentPosition(function() {
						$timeout(function() {
							$scope.mapLocation = {
								currentLocation: true,
								zoom: 14,
								centered: true
							};
						}, 100);

					}, function() {
						$uibModal.open({
							templateUrl: 'error-modal',
							controller: 'ModalController',
							size: 'sm',
							resolve: {
								content: function() {
									return {
										title: 'Error',
										message: 'Por favor revisa tu configuración de ubicación'
									};
								}
							}
						});
					});
				}


			};

			$scope.feedBack = function() {
				$scope.feedback = angular.element('#fby-screen .fby-tab a');
				$scope.feedback.click();
			};

			$scope.iconMap = {
				url: '../../img/icon_map_ubication.png',
				scaledSize: [25, 40],
				origin: [0, 0],
				anchor: [25, 40]
			};

			$scope.open = function(_page) {
				$timeout(function() {
					if (_page === 'terms') {
						window.open('http://cms.cocha.com/terminos-y-condiciones', '_blank');
					}
					else if (_page === 'policy') {
						window.open('http://cms.cocha.com/politicas-de-privacidad', '_blank');
					}
					else if (_page === 'branch') {
						window.open('http://cms.cocha.com/sucursales.html', '_blank');
					}
				}, 500);
			};

			$scope.offices = [{
				"name": "Casa Matriz",
				"address": "El Bosque Norte 0430",
				"commune": "Las Condes",
				"region": "SANTIAGO",
				"latitud": "-33.413946",
				"length": "-70.603017",
				"officeHours": "lunes a viernes de 09:15 a 18:45 hs.",
				"phone": "56224641000",
				"phone2": null
			}, {
				"name": "Aeropuerto Internacional",
				"address": "Terminal Internacional Santiago",
				"commune": "Pudahuel",
				"region": "SANTIAGO",
				"latitud": "-33.391035",
				"length": "-70.794603",
				"officeHours": "Atención presencial a pasajero 24 horas.",
				"phone": "56226901201",
				"phone2": null
			}, {
				"name": "Aeromaster",
				"address": "Av. Apoquindo 3000 Of. 401",
				"commune": "Las Condes",
				"region": "SANTIAGO",
				"latitud": "-33.417229",
				"length": "-70.598526",
				"officeHours": "lunes a viernes de 09:15 a 18:45 hs.",
				"phone": "56224642000",
				"phone2": null
			}, {
				"name": "Arauco Maipú",
				"address": "Av. Américo Vespucio 399 - Segundo Piso, local 640",
				"commune": "Maipú",
				"region": "SANTIAGO",
				"latitud": "-33.456687",
				"length": "-70.571723",
				"officeHours": "lunes a domingo de 10:00 a 21:30 hrs",
				"phone": "56224641801-56224641802-56224641803-56224641804-56224641805",
				"phone2": null
			}, {
				"name": "Cocha Lyon",
				"address": "Nueva de Lyon 145 of 602",
				"commune": "Providencia",
				"region": "SANTIAGO",
				"latitud": "-33.420351",
				"length": "-70.611002",
				"officeHours": "lunes a viernes de 09:15 a 18:45 hs.",
				"phone": "56224641850",
				"phone2": null
			}, {
				"name": "Pedro de Valdivia",
				"address": "Pedro de Valdivia 0169",
				"commune": "Providencia",
				"region": "SANTIAGO",
				"latitud": "-33.425156",
				"length": "-70.611758",
				"officeHours": "lunes a viernes de 09:15 a 18:45 hs.",
				"phone": "56224641600",
				"phone2": null
			}, {
				"name": "Apoquindo",
				"address": "Av. Apoquindo 3000 Of. 1401",
				"commune": "Las Condes",
				"region": "SANTIAGO",
				"latitud": "-33.417166",
				"length": "-70.598489",
				"officeHours": "lunes a viernes de 09:15 a 18:45 hs.",
				"phone": "56224642060",
				"phone2": null
			}, {
				"name": "Bucarest",
				"address": "Alcantara 44 Loc 2",
				"commune": "Las Condes",
				"region": "SANTIAGO",
				"latitud": "-33.415383",
				"length": "-70.589668",
				"officeHours": "lunes a viernes de 09:15 a 18:45 hs.",
				"phone": "56224641500",
				"phone2": null
			}, {
				"name": "Ciudad Empresarial",
				"address": "Alcantara 44 piso 2",
				"commune": "Huechuraba",
				"region": "SANTIAGO",
				"latitud": "-33.415383",
				"length": "-70.589668",
				"officeHours": "lunes a viernes de 09:15 a 18:45 hs.",
				"phone": "56224642450",
				"phone2": null
			}, {

				"name": "Crillon",
				"address": "Agustinas 1039",
				"commune": "Santiago",
				"region": "SANTIAGO",
				"latitud": "-33.440844",
				"length": "-70.651252",
				"officeHours": "Abierto los sábado de 10:00 a 14:00 hrs.",
				"phone": "56224643170",
				"phone2": "56224643189"
			}, {
				"name": "El Bosque",
				"address": "Apoquindo 3650 of. 602",
				"commune": "Las Condes",
				"region": "SANTIAGO",
				"latitud": "-33.415927",
				"length": "-70.592395",
				"officeHours": "lunes a viernes de 09:15 a 18:45 hs.",
				"phone": "56224641700",
				"phone2": null
			}, {
				"name": "Galerías",
				"address": "Huérfanos 1160 L.22",
				"commune": "Santiago",
				"region": "SANTIAGO",
				"latitud": "-33.439847",
				"length": "-70.653146",
				"officeHours": "lunes a viernes de 09:15 a 18:45 hs.",
				"phone": "56224641900",
				"phone2": null
			}, {
				"name": "Helvecia",
				"address": "Vitacura 2771 Of. 304",
				"commune": "Las Condes",
				"region": "SANTIAGO",
				"latitud": "-33.414931",
				"length": "-70.603863",
				"officeHours": "lunes a viernes de 09:15 a 18:45 hs.",
				"phone": "56224641100",
				"phone2": null
			}, {
				"name": "Isidora",
				"address": "Roger de Flor 2950",
				"commune": "Las Condes",
				"region": "SANTIAGO",
				"latitud": "-33.416453",
				"length": "-70.599202",
				"officeHours": "lunes a viernes de 09:15 a 18:45 hs.",
				"phone": "56224642300",
				"phone2": null
			}, {
				"name": "Orsini",
				"address": "Av. Apoquindo 3000 Of. 401",
				"commune": "Las Condes",
				"region": "SANTIAGO",
				"latitud": "-33.417238",
				"length": "-70.598499",
				"officeHours": "lunes a viernes de 09:15 a 18:45 hs.",
				"phone": "56224642000",
				"phone2": null
			}, {

				"name": "Parque Arauco",
				"address": "Kennedy 5413 Loc 551, primer piso",
				"commune": "Las Condes",
				"region": "SANTIAGO",
				"latitud": "-33.494316",
				"length": "-70.748245",
				"officeHours": "lunes a domingo de 10:00 a 21:00 hs.",
				"phone": "56224642200",
				"phone2": null
			}, {
				"name": "Santa Magdalena",
				"address": "Santa Magdalena 75 Of. 203",
				"commune": "Providencia",
				"region": "SANTIAGO",
				"latitud": "-33.420665",
				"length": "-70.609558",
				"officeHours": "lunes a viernes de 09:15 a 18:45 hs.",
				"phone": "56224641170",
				"phone2": null
			}, {
				"name": "Vitacura",
				"address": "Vitacura 2771 Of. 305",
				"commune": "Las Condes",
				"region": "SANTIAGO",
				"latitud": "-33.414931",
				"length": "-70.603927",
				"officeHours": "lunes a viernes de 09:15 a 18:45 hs.",
				"phone": "56224641400",
				"phone2": null
			}, {
				"name": "Alto Las Condes",
				"address": "Av. Presidente Kennedy 9001, nivel 2, local 2183.",
				"commune": "Las Condes",
				"region": "SANTIAGO",
				"latitud": "-33.3908732",
				"length": "-70.5484775",
				"officeHours": "lunes a domingo de 10:00 a 22:00 hs.",
				"phone": "56224641830",
				"phone2": null
			}, {
				"name": "Ripley Parque Arauco",
				"address": "Av. Kennedy 5413",
				"commune": "Las Condes",
				"region": "SANTIAGO",
				"latitud": "-33.494405",
				"length": "-70.748194",
				"officeHours": "lunes a domingo de 11:00 a 21:00 hs.",
				"phone": "56226941000",
				"phone2": "Anexos: 1610 - 1625 -1633"
			}, {
				"name": "Ripley Arauco Maipú",
				"address": "Av. Américo Vespucio 399 - Segundo Piso",
				"commune": "Maipú",
				"region": "SANTIAGO",
				"latitud": "-33.481314",
				"length": "-70.753969",
				"officeHours": "lunes a domingo de 11:00 a 21:00 hrs",
				"phone": "56224643119",
				"phone2": null
			}, {
				"name": "Ripley Tobalaba",
				"address": "Av. Camilo Henríquez 3296, Puente Alto - Segundo Piso",
				"commune": "Las Condes",
				"region": "SANTIAGO",
				"latitud": "-33.569017",
				"length": "-70.554867",
				"officeHours": "lunes a domingo de 11:00 a 21:00 hrs",
				"phone": "56224643118",
				"phone2": null
			}, {
				"name": "Ripley Plaza Oeste",
				"address": "Av. Américo Vespucio 1501",
				"commune": "Cerrillos",
				"region": "SANTIAGO",
				"latitud": "-33.514918",
				"length": "-70.717368",
				"officeHours": "lunes a domingo de 11:00 a 21:00 hs.",
				"phone": "56226941000",
				"phone2": "Anexos: 8701 - 8702"
			}, {
				"name": "Ripley Mall del Centro",
				"address": "Av. 21 de Mayo 698",
				"commune": "Santiago Centro",
				"region": "SANTIAGO",
				"latitud": "-33.435241",
				"length": "-70.651139",
				"officeHours": "lunes a domingo de 11:00 a 21:00 hs.",
				"phone": "56226941000",
				"phone2": "Anexos: 1940 - 4064"
			}, {
				"name": "Ripley Plaza Vespucio",
				"address": "Av. Vicuña Mackenna 7110",
				"commune": "La Florida",
				"region": "SANTIAGO",
				"latitud": "-33.508184",
				"length": "-70.613774",
				"officeHours": "lunes a domingo de 11:00 a 21:00 hs.",
				"phone": "56226941000 ",
				"phone2": "Anexos: 1670 - 1674"
			}, {
				"name": "Ripley Alto Las Condes",
				"address": "Av. Kennedy 9001",
				"commune": "Las condes",
				"region": "SANTIAGO",
				"latitud": "-33.388846",
				"length": "-70.545262",
				"officeHours": "lunes a domingo de 11:00 a 21:00 hs.",
				"phone": "56226941000",
				"phone2": "Anexos: 8825 - 8874 - 8855 - 8061 - 8062"
			}, {
				"name": "Ripley Costanera Center",
				"address": "Av. Andrés Bello 2447 local 1300 piso 5",
				"commune": "Las Condes",
				"region": "SANTIAGO",
				"latitud": "-33.417656",
				"length": "-70.608300",
				"officeHours": "lunes a domingo de 11:00 a 21:00 hs.",
				"phone": "5622464230",
				"phone2": "56224642414"
			}, {
				"name": "Ripley Plaza Egaña",
				"address": "Avda. Larraín 5262",
				"commune": "La Reina",
				"region": "SANTIAGO",
				"latitud": "-33.453307",
				"length": "-70.570619",
				"officeHours": "lunes a domingo de 11:00 a 21:00 hs.",
				"phone": "56224643121",
				"phone2": "Anexos: 7749-7748"
			}, {
				"name": "Antofagasta",
				"address": "Baquedano 239, piso 2, oficina 206.",
				"commune": "Antofagasta",
				"region": "Antofagasta",
				"latitud": "-23.5888398",
				"length": "-70.3909332",
				"officeHours": "lunes a viernes de 8:30 a 18:30 hs.",
				"phone": "56223943690",
				"phone2": null
			}, {
				"name": "La Serena",
				"address": "Arturo Prat 464",
				"commune": "La Serena",
				"region": "Coquimbo",
				"latitud": "-29.901974",
				"length": "-71.250259",
				"officeHours": "lunes a jueves de 09:30 a 18:30 hs., viernes de 9:30 a 18:00 hs. y sábados de 11:00 a 14:00 hs.",
				"phone": "051-2219320-051-2220161",
				"phone2": "Anexos:6001-6002-6003-6004"
			}, {
				"name": "Viña del Mar",
				"address": "Av. Libertad 1191 - local 2 (esquina 13 norte)",
				"commune": "Viña del Mar",
				"region": "VALPARAISO",
				"latitud": "-33.010234",
				"length": "-71.549014",
				"officeHours": "lunes a viernes de 09:30 a 19:00 hs.",
				"phone": "56322465210",
				"phone2": null
			}, {
				"name": "Concepción",
				"address": "Calle Barros Arana 631 - piso 2 (local ubicado en la Galería Universitaria).",
				"commune": "Concepción",
				"region": "Concepción",
				"latitud": "-36.828655",
				"length": "-73.043474",
				"officeHours": "Lunes a viernes 09:15 a 18:50 hs. y sábados de 10:30 a 13:00 hs.",
				"phone": "56412910175",
				"phone2": "Anexos: 6001-6002-6003-6004"
			}, {
				"name": "Atacama",
				"address": "Calle Los Carreras 716.",
				"commune": "Copiapó",
				"region": "Copiapó",
				"latitud": "-27.3666663",
				"length": "-70.3311026",
				"officeHours": "lunes a viernes de 09:00 a 13:45 hs. y de 15:30 a 19:00 hs.",
				"phone": "56223943650",
				"phone2": null
			}, {
				"name": "Ripley Calama",
				"address": "Balmaceda 3242",
				"commune": "Calama",
				"region": "Calama",
				"latitud": "-22.449478",
				"length": "-68.920598",
				"officeHours": "lunes a domingo de 11:00 a 21:00 hs.",
				"phone": "56224642416",
				"phone2": "Anexo: 2416"
			}, {
				"name": "Ripley Antofagasta",
				"address": "Arturo Prat 530, piso 4",
				"commune": "Antofagasta",
				"region": "ANTOFAGASTA",
				"latitud": "-23.648195",
				"length": "-70.397075",
				"officeHours": "lunes a jueves de 09:30 a 18:30 hs., viernes de 9:30 a 18:00 hs. y sábados de 11:00 a 14:00 hs.",
				"phone": "562638061",
				"phone2": "562638062"
			}, {
				"name": "Ripley La Serena",
				"address": "Gregorio Cordovéz 499",
				"commune": "La Serena",
				"region": "Coquimbo",
				"latitud": "-29.903192",
				"length": "-71.249961",
				"officeHours": "lunes a domingo de 10:30 a 21:00 hs.",
				"phone": "56224643157 - 56224643158 - 56224643162",
				"phone2": null
			}, {

				"name": "Ripley Marina Arauco",
				"address": "Av. Libertad 1348",
				"commune": "Viña del mar",
				"region": "VALPARAISO",
				"latitud": "-33.008507",
				"length": "-71.548262",
				"officeHours": "lunes a domingo de 11:00 a 21:00 hs.",
				"phone": "56412910175",
				"phone2": "Anexos: 2566 - 6950"
			}, {

				"name": "Ripley Talca",
				"address": "Ocho Oriente 1212 - Segundo piso",
				"commune": "Talca",
				"region": "Talca",
				"latitud": "-35.425434",
				"length": "-71.655532",
				"officeHours": "lunes a domingo de 10:00 a 20:00 hs.",
				"phone": "56224642423",
				"phone2": "56224641069"
			}, {
				"name": "Ripley Los Ángeles",
				"address": "Valdivia 440",
				"commune": "Los Ángeles",
				"region": "Los Ángeles",
				"latitud": "-37.468168",
				"length": "-72.352215",
				"officeHours": "lunes a domingo de 11:00 a 21:00 hs.",
				"phone": "56224642415",
				"phone2": "562246402421"
			}, {
				"name": "Ripley Puerto Montt",
				"address": "Juan Soler Manfredini N°10, Mall Paseo Costanera del Mar - Sexto piso",
				"commune": "Puerto Montt",
				"region": "Puerto Montt",
				"latitud": "-41.472096",
				"length": "-72.943290",
				"officeHours": "lunes a domingo de 09:45 a 20:15 hs.",
				"phone": "5624643136",
				"phone2": null
			}, {
				"name": "Ripley Mall Plaza El Trébol",
				"address": "Av. Jorge Alessandri 3177, piso 3",
				"commune": "Talcahuano",
				"region": "Bio Bio",
				"latitud": "-36.7802751",
				"length": "-73.0608296",
				"officeHours": "lunes a domingo de 11:00 a 21:00 hs.",
				"phone": "5624643136",
				"phone2": null
			}, {
				"name": "Ripley Portal Temuco",
				"address": "Av. Alemania 0671, local 1150, piso 3.",
				"commune": "Temuco",
				"region": "Temuco",
				"latitud": "-38.7349423",
				"length": "-72.6130103",
				"officeHours": "lunes a domingo de 11:00 a 21:00 hs.",
				"phone": "56224641014",
				"phone2": null
			}];
		});
});
*/
