define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.controllers.HotelConfirmationController', [])
		.controller('HotelConfirmationController', function($state, $scope, $sce, HotelReservationDataService, B2BDataService, AnalyticsService) {

			try {
				$scope.content = {
					summary: HotelReservationDataService.getRoomSummary(),
					rooms: HotelReservationDataService.getReservationRooms(),
					passengers: HotelReservationDataService.getReservationPassengers(),
					contact: HotelReservationDataService.getContact(),
					destination: HotelReservationDataService.getPlaceDestination(),
					pnr: HotelReservationDataService.getSPNR(),
					departure: HotelReservationDataService.getSelectedDeparture(),
					arrival: HotelReservationDataService.getSelectedArrival(),
					prices: HotelReservationDataService.getPrices(),
					cancellationPolicy: HotelReservationDataService.getCancellationPolicy(),
					exchange: HotelReservationDataService.getReservationExchange(),
					availabilityData: HotelReservationDataService.getAvailabilityData(),
					hotelPhone: HotelReservationDataService.getHotelPhone(),
					B2Buser: B2BDataService,
					supplier: HotelReservationDataService.getSupplier(),
					roomInfo: HotelReservationDataService.getReservationRoomInfo(),
					confirmationNumber: HotelReservationDataService.getConfirmationNumber(),
					rateName: HotelReservationDataService.getRateName(),
					considerations: HotelReservationDataService.getConsiderations()
				};
				$scope.view = {};
			}
			catch (e) {
				console.error("[HotelConfirmationController]", "Could not load data.\n", e.stack);
				//$state.go('home');
				return;
			}

			try {
				AnalyticsService.checkout($scope.content);
			}
			catch (e) {
				console.error("[HotelConfirmationController]", "Could not track the purchase.\n", e.stack);
			}
			finally {
				HotelReservationDataService.clearHotelReservation();
			}

			$scope.getCancellationPolicy = function(_policy) {
				if (!_policy) {
					return ($scope.content.roomInfo.refundable === false ? 'Esta tarifa no permite devolución' : 'Sin información Disponible');
				}
				else {
					return $sce.trustAsHtml(_policy);
				}
			};

			$scope.getTextAmenity = function(amenity) {
				if (amenity.name === 'BREAKFAST' && amenity.included) {
					if (!_.isNull(amenity.text) && amenity.text !== '') {
						if (amenity.text.toLowerCase().indexOf("todo incluido") > -1) {
							return "Todo incluido";
						}
						else if (amenity.text.toLowerCase().indexOf("media") > -1) {
							return "Media pensión";
						}
						else if (amenity.text.toLowerCase().indexOf("completa") > -1) {
							return "Media completa";
						}
						else {
							return "Desayuno incluido";
						}
					}
					else {
						return "Desayuno incluido";
					}
				}
				else if (amenity.name === 'INTERNET' && amenity.included) {
					return "Internet Gratuito";
				}
				else if (amenity.name === 'PARKING' && amenity.included) {
					return "Estacionamiento gratuito";
				}
				else {
					return "";
				}

			};

			$scope.getIconAmenity = function(amenity) {
				if (amenity.name === 'BREAKFAST' && amenity.included) {
					if (!_.isNull(amenity.text) && amenity.text !== '') {
						if (amenity.text.toLowerCase().indexOf("todo incluido") > -1) {
							return "cch-amenities-all-inclusive";
						}
						else if (amenity.text.toLowerCase().indexOf("media") > -1) {
							return "cch-amenities-half-pension";
						}
						else if (amenity.text.toLowerCase().indexOf("completa") > -1) {
							return "cch-amenities-full-pension";
						}
						else {
							return "cch-amenities-breakfast";
						}
					}
					else {
						return "cch-amenities-breakfast";
					}
				}
				else if (amenity.name === 'INTERNET' && amenity.included) {
					return "cch-wifi";
				}
				else if (amenity.name === 'PARKING' && amenity.included) {
					return "cch-parking";
				}
				else {
					return "";
				}
			};

			$scope.getChargePolicy = function(room) {
				var msg = '';
				if (room.supplierCode === 'SAB') {
					if (room.guaranteeSurchargeRequired === 'D') {
						msg += 'Se realizarán cargos a La tarjeta de crédito en cualquier momento hasta por la totalidad del monto en reserva.';
					}
					else if (room.guaranteeSurchargeRequired === 'G') {
						msg += 'La tarjeta de crédito sera utilizada para garantizar la reserva. No se efectuarán cargos.';
					}
					else {
						msg += 'Sin información disponible';
					}
				}
				return msg;
			};

			$scope.goToHome = function() {
				$state.go('home');
			};
		});
});
