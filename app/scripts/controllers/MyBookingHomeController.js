define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.controllers.MyBookingHomeController', [])
		.controller('MyBookingHomeController', function($state, $timeout, $window, $uibModal, $scope, MyBookingResultsDataService) {

			function gotoDetail(_result, _openInSamePage) {
				_openInSamePage = (typeof _openInSamePage === 'undefined') ? false : _openInSamePage;
				if (_result.hasOwnProperty('supplier') && _result.hasOwnProperty('spnr') && !_.isNull(_result.spnr)) {
					if (_result.supplier === 'BKG') {
						MyBookingResultsDataService.getBookingDeeplink(_result, callbacksOK.DEEPLINK_FOUND, callbacksError);
					}
					else if (_result.supplier === 'EAN') {
						MyBookingResultsDataService.saveBookingData(_result);
						if (_openInSamePage) {
							$state.go('my-booking-detail');
						}
						else {
							$timeout(function() {
								$window.open($state.href('my-booking-detail'), '_blank');
							}, 1800);
						}
					}
					else if (!_result.supplier && _result.spnr.charAt(0) === 'B') {
						_result.supplier = "BKG";
						MyBookingResultsDataService.getBookingDeeplink(_result, callbacksOK.DEEPLINK_FOUND, callbacksError);
					}
					else {
						errorModal("Supplier aún no soportado.");
					}
				}
				else {
					errorModal('No existe supplier o codigo para consultar la reserva');
				}
			}

			var callbacksOK = {
				RESERVATION_FOUND: function() {
					if ($scope.content.isB2B) {
						$state.go('results-booking', {
							refresh: true
						});
					}
					else {
						//B2C
						var searchResults = MyBookingResultsDataService.getSearchResults();
						if (!searchResults.length || searchResults.length === 0) {
							$state.go('results-booking', {
								refresh: true
							});
						}
						else {
							if (searchResults[0].hasOwnProperty('supplier')) {
								gotoDetail(searchResults[0]);
							}
							else {
								errorModal(" error en la búsqueda [reserva sin supplier] por favor contacte a Hoteles Online ");
							}
						}
					}
				},
				DEEPLINK_FOUND: function(_url) {
					$window.open(_url, '_blank');
				}
			};
			var callbacksError = {
				ERROR_INTERNAL: errorInternal,
				ERROR_NORESULT: errorNoResult
			};

			$scope.content = MyBookingResultsDataService.getContentData();
			$scope.view = MyBookingResultsDataService.getViewData();

			$scope.search = function(parameters) {
				//alert(JSON.stringify(parameters))
				MyBookingResultsDataService.getReservationData(parameters, callbacksOK.RESERVATION_FOUND, callbacksError);
			};

			function errorModal(msg) {
				$scope.view.loadingResults = false;
				$scope.view.modalText = (typeof msg !== 'undefined') ? msg : 'error interno';
				$uibModal.open({
					size: 'sm',
					scope: $scope,
					templateUrl: 'hotels-error-msg'
				});
			}

			function errorInternal() {
				$scope.view.loadingResults = false;
				$scope.$applyAsync();
				errorModal("Se produjo un error al ejecutar la búsqueda");
			}

			function errorNoResult() {
				$scope.view.loadingResults = false;
				$scope.$applyAsync();
				$state.go('results-booking', {
					refresh: true
				});
			}

		});
});
