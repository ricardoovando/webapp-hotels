define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.controllers.HotelPaymentController', []).controller('HotelPaymentController', function($window, $timeout, $scope, $state, $sce, cCommonConstants, HotelReservationDataService, B2BDataService, AnalyticsService) {

		$scope.view = {
			waiting: true, // TODO VOLVER A TRUE AL TERMINAR
			error: false,
			errorDetail: {
				isPaymentError: false,
				title: '',
				text: '',
				cancelText: '',
				actionText: '',
				cancel: function() {},
				action: function() {}
			},
			smallView: angular.element($window).outerWidth() < 767,
		};

		var emitCallbackOK = {
			GO_CONFIRMATION: function() {
				console.info('[PaymentController] Confirmacion correcta');
				//AnalyticsService.trackInteraction('confirmacion', 'pago', 'pagook ' + $stateParams.token, false);
				//AnalyticsService.trackPurchase($scope.content, parseFloat(ResultsDataService.getExchangeRate()));
				$scope.view.waiting = false;
				$scope.view.error = false;
				//PricingDataService.cleanSession();
				$state.go('hotel-confirmation');
			}
		};

		var emitCallbacksError = {
			ERROR_INTERNAL: function() {
				console.error('[PaymentController] Error interno');
				//AnalyticsService.trackInteraction('confirmacion', 'pago', 'pagofallido internal ' + $stateParams.token, false);
				$scope.view.waiting = false;
				$scope.view.error = true;
				$scope.view.errorDetail = {
					isPaymentError: false,
					title: '¡Lo sentimos!',
					text: 'Algo salió mal en el proceso y este no ha sido efectuado correctamente, por favor intenta nuevamente. Si el problema continúa nos comunicaremos contigo a la brevedad.',
					cancelText: 'Salir',
					actionText: 'Reintentar',
					cancel: exit,
					action: retryEmission
				};
			},
			ERROR_BOOKING: function() {
				console.error('[PaymentController] Error Booking');
				//AnalyticsService.trackInteraction('confirmacion', 'pago', 'pagofallido internal ' + $stateParams.token, false);
				$scope.view.waiting = false;
				$scope.view.error = true;
				$scope.view.errorDetail = {
					isPaymentError: false,
					title: '¡Lo sentimos!',
					text: 'Hubo un problema al intentar reservar, por favor contacta a Hoteles Online (hotelesonline@cocha.com) ',
					cancelText: 'Salir',
					actionText: 'Reintentar',
					cancel: exit,
					action: retryEmission
				};
			},
			ERROR_TRAVELIO_AUTH: function() {
				console.error('[PaymentController] Error Travelio Auth');
				//AnalyticsService.trackInteraction('confirmacion', 'pago', 'pagofallido internal ' + $stateParams.token, false);
				$scope.view.waiting = false;
				$scope.view.error = true;
				$scope.view.errorDetail = {
					isPaymentError: false,
					title: '¡Lo sentimos!',
					text: 'Hubo un problema al intentar la reserva, su usuario no tiene los permisos suficientes',
					cancelText: 'Salir',
					actionText: 'Reintentar',
					cancel: exit,
					action: retryEmission
				};
			},
			ERROR_HOTEL: function() {
				console.error('[PaymentController] Error Booking');
				//AnalyticsService.trackInteraction('confirmacion', 'pago', 'pagofallido internal ' + $stateParams.token, false);
				$scope.view.waiting = false;
				$scope.view.error = true;
				$scope.view.errorDetail = {
					isPaymentError: false,
					title: '¡Lo sentimos!',
					text: 'Las habitaciones seleccionadas acaban de ser reservadas por otra persona. Ya no se encuentran disponibles. Por favor realizar una nueva búsqueda.',
					cancelText: 'Salir',
					actionText: 'Reintentar',
					cancel: exit,
					action: retryEmission
				};
			},
			ERROR_CARD: function() {
				console.error('[PaymentController] Error de tarjeta');
				//AnalyticsService.trackInteraction('confirmacion', 'pago', 'pagofallido internal ' + $stateParams.token, false);
				$scope.view.waiting = false;
				$scope.view.error = true;
				$scope.view.errorDetail = {
					isPaymentError: true,
					title: '¡Lo sentimos!',
					text: 'Hubo un problema con la tarjeta, puede ser por alguno de los siguientes motivos:',
					cancelText: 'Salir',
					actionText: 'Reintentar',
					cancel: exit,
					action: retryEmission
				};
			},
			ERROR_PAYMENT: function() {
				console.error('[PaymentController] Error en transaccion');
				//AnalyticsService.trackInteraction('confirmacion', 'pago', 'pagofallido payment' + $stateParams.token, false);
				$scope.view.waiting = false;
				$scope.view.error = true;
				$scope.view.errorDetail = {
					isPaymentError: true,
					title: 'Lo sentimos, hubo un problema en el pago :',
					text: 'Por favor reintente y si el problema persiste contactar a Hoteles Online (hotelesonline@cocha.com)',
					cancelText: 'Salir',
					actionText: 'Reintentar',
					cancel: exit,
					action: retryEmission
				};
			},
			ERROR_PAYDATA: function() {
				console.error('[PaymentController] Error en transaccion');
				//AnalyticsService.trackInteraction('confirmacion', 'pago', 'pagofallido payment' + $stateParams.token, false);
				$scope.view.waiting = false;
				$scope.view.error = true;
				$scope.view.errorDetail = {
					isPaymentError: true,
					title: 'Lo sentimos, hubo un problema en el pago :',
					text: 'Por favor reintente y si el problema persiste contactar a Hoteles Online (hotelesonline@cocha.com)',
					cancelText: 'Salir',
					actionText: 'Reintentar',
					cancel: exit,
					action: retryEmission
				};
			},
			ERROR_CHECKPAY: function() {
				console.error('[PaymentController] Error en transaccion');
				//AnalyticsService.trackInteraction('confirmacion', 'pago', 'pagofallido checkpay' + $stateParams.token, false);

				$scope.view.waiting = false;
				$scope.view.error = true;
				$scope.view.errorDetail = {
					isPaymentError: true,
					title: 'Lo sentimos, hubo un problema en el pago :',
					text: 'por favor contacta a Hoteles Online (hotelesonline@cocha.com)',
					cancelText: 'Salir',
					actionText: 'Reintentar',
					cancel: exit,
					action: retryEmission
				};
			}
		};

		try {
			$scope.content = {
				token: HotelReservationDataService.getReservationToken(),
				backOfficePhone: cCommonConstants.getSalesPhoneNumber(),
				B2Buser: B2BDataService
			};
			emission();
			AnalyticsService.checkoutStep(3);
		}
		catch (e) {
			console.error("[HotelPaymentController]", "Could not load data.\n", e.stack);
			//$state.go('home');
			return;
		}

		function emission() {
			$scope.view.waiting = true;
			HotelReservationDataService.bookHotel($scope.content.token, emitCallbackOK, emitCallbacksError, !(_.isNull($scope.content.B2Buser)));
		}
		/*
		function refreshURL() {
			if (!_.isNull($scope.content.B2Buser)) {
				$state.go('checkout');
			}
			else {
				$scope.view.waiting = true;
				HotelReservationDataService.refreshPaymentURL($scope.content.token, refreshCallbacksOK, refreshCallbacksError);
			}
		}
		*/

		function exit() {
			//ANALYTICS
			$state.go('home');
			return;
		}

		function retryEmission() {
			if ($scope.content.B2BUser !== null) {
				$state.go('checkout');
			}
			else {
				//ANALYTICS
				emission();
			}
		}
		/*
		function retryRefreshURL() {
			//ANALYTICS
			refreshURL();
			return;
		}
		*/

	});
});
