define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.controllers.ModalController', ['ngMap'])
		.controller('ModalController', function(NgMap, $uibModalInstance, $scope, $uibModal, $timeout, HotelMapDataService, content) {
			if (content !== null) {
				$scope.content = content;
			}

			var okCallbacks = {
				HOTEL_MAP_SEARCH: function(_data) {
					$scope.view.closestPoints = _data.closest;

					$timeout(function() {
						$scope.view.loader = false;
						$scope.$parent.$apply();
					}, 100);

					var mapName = ($scope.content.mapData.hasOwnProperty('mapId') ? $scope.content.mapData.mapId : 'map');
					NgMap.getMap(mapName).then(function(map) {
						$scope.map = map;
						google.maps.event.trigger($scope.map, 'resize');
						var latlng = new google.maps.LatLng($scope.content.mapData.latitude, $scope.content.mapData.longitude);
						//$scope.map.options = {};
						//$scope.map.options.scrollwheel = false;
						$scope.map.setCenter(latlng);
						$scope.map.setZoom(13);
						google.maps.event.trigger($scope.map, 'resize');

					});
				},
				REGION_MAP_SEARCH: function(_stateData, _hotels) {
					$scope.$parent.content.regionMapData.lastStateDataSaved = _stateData;
					$scope.view.loader = false;
					$scope.view.processedHotels = _hotels;

					NgMap.getMap("div-region-map").then(function(map) {
						$scope.map = map;
						google.maps.event.trigger($scope.map, 'resize');
						//var latlng = new google.maps.LatLng(_hotels[0].latitude, _hotels[0].longitude);
						var latlng = new google.maps.LatLng($scope.view.processedHotels[0].latitude, $scope.view.processedHotels[0].longitude);
						$scope.map.setCenter(latlng);
						$scope.map.setZoom(13);
						//google.maps.event.trigger($scope.map, 'resize');
					});
				}
			};

			var callbacksError = {
				ERROR_RESERVATION: internalError,
				ERROR_PAYMENTURL: internalError,
				ERROR_NETWORK: networkError,
				ERROR_INTERNAL: internalError,
				ERROR_NO_RESULTS: noresults
			};

			if (typeof $scope.view !== 'undefined') {
				$scope.view.loader = true;
				if ($scope.view.hasOwnProperty('mapType')) {
					switch ($scope.view.mapType) {

						case 'hotel': //////////////////////////////////////////////////////////////////////////////////
							$scope.view.closestPoints = [];
							HotelMapDataService.getHotelPoints($scope.content.mapData, okCallbacks, callbacksError);
							break;
						case 'region': ////////////////////////////////////////////////////////////////////////////////

							var isEqual = _.isEqual($scope.content.regionMapData.stateData, $scope.content.regionMapData.lastStateDataSaved);
							var serviceMapResults = [];
							if (isEqual === false) {
								$timeout(function() {
									serviceMapResults = HotelMapDataService.processHotels($scope.content.regionMapData, $scope.view.fullResults);
									okCallbacks.REGION_MAP_SEARCH($scope.content.regionMapData.stateData, serviceMapResults);
								}, 50);
							}
							else {
								$timeout(function() {
									serviceMapResults = HotelMapDataService.getProcessedHotels();
									okCallbacks.REGION_MAP_SEARCH($scope.content.regionMapData.stateData, serviceMapResults);
								}, 50);
							}
							break;
						case 'photos-hotel': ////////////////////////////////////////////////////////////////////////////
							$scope.view.loader = false;
							break;
						default: ////////////////////////////////////////////////////////////////////////////////////////

							$timeout(function() {
								$scope.view.loader = false;
								$scope.$parent.$applyAsync();
							}, 1900);
							break;
					}
				}
				else {

					$timeout(function() {
						$scope.view.loader = false;
						$scope.$parent.$applyAsync();
					}, 1900);
				}
			}

			$scope.getImageName = function(urlImage) {
				if (typeof urlImage !== 'undefined' && urlImage.length > 0 && urlImage !== '') {
					var ext = urlImage.substring(urlImage.lastIndexOf('.') + 1);
					var type = urlImage.substring(urlImage.length - ((ext.length) + 3), urlImage.length - (ext.length + 1));
					if (type !== '_b') {
						return urlImage;
					}

					var image = urlImage.substring(0, urlImage.length - ((ext.length) + 3));
					return (image + '_z.' + ext);
				}
				else {
					return "error.jpg";
				}
			};

			function internalError() {
				$uibModal.open({
					scope: $scope,
					size: 'sm',
					templateUrl: 'hotels-error-reservation'
				});

			}

			function networkError() {
				$uibModal.open({
					scope: $scope,
					size: 'sm',
					templateUrl: 'hotels-error-network'
				});

			}

			function noresults() {
				$uibModal.open({
					scope: $scope,
					size: 'sm',
					templateUrl: 'hotels-no-results-map-data'
				});

			}
			$scope.showNamePlace = function(e, point, index) {
				$scope.view.point = point;
				$scope.map.showInfoWindow('hotel-map-iw', "custom-marker-" + index);
			};

			$scope.showHotelInfo = function(e, point, index) {
				$scope.view.point = point;
				$scope.map.showInfoWindow('region-map-iw', "hotel-marker-" + index);
			};
		});
});
