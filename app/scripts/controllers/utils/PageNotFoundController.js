define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.controllers.PageNotFoundController', [])
		.controller('PageNotFoundController', function($scope) {
			$scope.content = {};

			$scope.view = {};

		});
});
