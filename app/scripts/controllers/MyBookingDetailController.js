define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.controllers.MyBookingDetailController', ['ngMap'])
		.controller('MyBookingDetailController', function(NgMap, $uibModalStack, $stateParams, $state, $timeout, $window, $uibModal, $scope, MyBookingDetailDataService, HotelReservationParserService, cUtilsFields, HotelMapDataService) {

			var callbacksOK = {
				BOOKING_DETAIL_FOUND: function(_data) {
					$scope.view.loadingResult = false;
					$scope.content.result = _data;
					$scope.content.itineraryId = _data.hotelItineraryRespDTO.hotelItineraryResponse.itinerary.itineraryId;
				},
				MAIL_SENT: function() {
					//levantar modal de respuesta, bajar el otro
					//alert('enviado ' + JSON.stringify(_data));
					showSuccessVoucherModal();
				},
				CANCELLATION_OK: function() {
					$uibModalStack.dismissAll();
					$scope.showCancellationResultModal();
					$scope.view.cancellationLoader = false;
					$scope.$applyAsync();
					//alert(JSON.stringify(_data));					
				},
				HOTEL_MAP_SEARCH: showMap
			};

			var callbacksError = {
				ERROR_INTERNAL: errorInternal,
				ERROR_NORESULT: errorNoResult,
				ERROR_SEND_VOUCHER: errorSendVoucher
			};

			$scope.content = MyBookingDetailDataService.getContentData($stateParams);
			$scope.view = MyBookingDetailDataService.getViewData();
			getBookingDetail();

			$scope.cancelReservation = function() {
				$scope.view.cancellationLoader = true; //callbacksOK.CANCELLATION_OK();
				$scope.$applyAsync();
				MyBookingDetailDataService.cancelBooking($scope.content, callbacksOK.CANCELLATION_OK, callbacksError);
			};
			$scope.functions = {
				goBack: function() {
					$state.go('my-booking');
				},
				goToHome: function() {
					$state.go('home');
				}
			};

			$scope.refresh = function() {
				$uibModalStack.dismissAll();
				$window.location.reload();
			};

			$scope.selectReason = function(_reason) {
				$scope.content.reason = _reason;
			};

			$scope.showCancellationModal = function() {
				$uibModal.open({
					size: 'lg',
					scope: $scope,
					templateUrl: 'hotels-cancellation-confirm'
				});
			};

			$scope.showCancellationReasonsModal = function() {
				$uibModal.open({
					size: 'lg',
					scope: $scope,
					templateUrl: 'hotels-cancellation-reasons'
				});
			};

			$scope.showCancellationResultModal = function() {
				$uibModal.open({
					size: 'lg',
					scope: $scope,
					templateUrl: 'hotels-cancellation-result'
				});
			};

			function showSuccessVoucherModal() {
				$scope.view.errors.sendVoucher = false;
				$uibModal.open({
					size: 'sm',
					scope: $scope,
					templateUrl: 'hotels-success-voucher'
				});
			}

			function errorSendVoucher() {
				$scope.view.errors.sendVoucher = true;
				$scope.resendVoucher();
				//showSuccessVoucherModal();
			}

			function getBookingDetail() {
				var params = MyBookingDetailDataService.getBookingData();
				$scope.content.supplier = params.supplier;
				$scope.content.spnr = params.spnr;
				$scope.content.email = params.email;
				$scope.content.businessNumber = params.businessNumber;
				$scope.content.security = _.has(params, 'token') ? params.token : '';
				//$scope.content.seeVoucherUrl = MyBookingDetailDataService.getVoucherUrl(params, 'see');
				$scope.content.seeVoucherUrl = '';
				$scope.content.getVoucherUrl = MyBookingDetailDataService.getVoucherUrl(params, 'get');
				//alert(JSON.stringify($scope.content));
				MyBookingDetailDataService.getBookingDetails(params, callbacksOK.BOOKING_DETAIL_FOUND, callbacksError);
			}

			$scope.showNamePlace = function(point, $index) {
				point = point;
				$index = $index;
			};

			$scope.seeVoucher = function() {
				var params = MyBookingDetailDataService.getBookingData();
				$scope.content.seeVoucherUrl = MyBookingDetailDataService.getVoucherUrl(params, 'see');
				$uibModal.open({
					size: 'lg',
					scope: $scope,
					templateUrl: 'hotels-see-voucher'
				});
			};

			$scope.getVoucher = function() {
				$window.open($scope.content.getVoucherUrl, '_blank');
			};

			$scope.getMapInfo = function() {
				var mapData = {
					hotel: $scope.content.result.hotelDetailDTO.hotelDetails.id,
					hotelName: $scope.content.result.hotelDetailDTO.hotelDetails.name,
					latitude: $scope.content.result.hotelDetailDTO.hotelDetails.latitude,
					longitude: $scope.content.result.hotelDetailDTO.hotelDetails.longitude,
					rating: $scope.content.result.hotelDetailDTO.hotelDetails.starRating,
					address: $scope.content.result.hotelDetailDTO.hotelDetails.address,
					mapId: 'mapModal'
				};
				if ($scope.view.closestPoints.length > 0) {
					showMap({
						closest: $scope.view.closestPoints,
						map: mapData
					});
				}
				else {
					$scope.view.closestPoints = [];
					HotelMapDataService.getHotelPoints(mapData, callbacksOK, callbacksError);
				}
			};

			function showMap(_data) {
				$scope.view.closestPoints = _data.closest;

				$timeout(function() {
					$scope.view.modalLoader = false;
					$scope.$applyAsync();
				}, 100);

				var mapName = 'mapModal';
				NgMap.getMap(mapName).then(function(map) {
					$scope.map = map;
					google.maps.event.trigger($scope.map, 'resize');
					var latlng = new google.maps.LatLng(_data.map.latitude, _data.map.longitude);
					//$scope.map.options = {};
					//$scope.map.options.scrollwheel = false;
					$scope.map.setCenter(latlng);
					$scope.map.setZoom(13);
					google.maps.event.trigger($scope.map, 'resize');
				});

				$uibModal.open({
					size: 'lg',
					scope: $scope,
					templateUrl: 'hotels-map-voucher'
				});

			}

			$scope.showRoomDetail = function(_index) {
				var value = $scope.view.roomDescription[_index];
				$scope.view.roomDescription = {};
				if (typeof value !== 'undefined') {
					$scope.view.roomDescription[_index] = !value;
				}
				else {
					$scope.view.roomDescription[_index] = false;
				}
			};

			$scope.showHotelInformation = function() {
				$scope.view.hotelInformation = !$scope.view.hotelInformation;
			};

			$scope.resendVoucher = function() {
				//alert('resend');
				$uibModal.open({
					size: 'sm',
					scope: $scope,
					templateUrl: 'hotels-send-voucher'
				});
			};

			$scope.sendVoucherToAddress = function() {
				//$scope.content.email;
				MyBookingDetailDataService.sendVoucher($scope.content, callbacksOK.MAIL_SENT, callbacksError);
			};

			$scope.showRateDetail = function() {
				$uibModal.open({
					size: 'sm',
					scope: $scope,
					templateUrl: 'hotels-rate-detail'
				});
			};

			$scope.getBookingStatusClass = function() {
				var status = null;
				try {
					status = $scope.content.result.status;
				}
				catch (e) {
					return 'my-bkg-status-processing';
				}
				switch (status) {
					case 'AC':
						return 'my-bkg-status-active';
					case 'CO':
						return 'my-bkg-status-consumed';
					case 'DE':
					case 'NO':
					case 'XS':
						return 'my-bkg-status-canceled';
					default:
						return 'my-bkg-status-processing';
				}
			};

			$scope.getBookingStatus = function() {
				var status = null;
				try {
					status = $scope.content.result.status;
				}
				catch (e) {
					return 'Reserva en Estado Indeterminado';
				}
				switch (status) {
					case 'AC':
						return 'Reserva Activa';
					case 'CO':
						return 'Reserva Consumida';
					case 'DE':
						return 'Reserva Cancelada | En proceso de devolución';
					case 'NO':
						return 'Reserva Cancelada | Sin Devolución';
					case 'XS':
						return 'Reserva Cancelada | Devolución Procesada';
					case 'PE':
						return 'Reserva Pendiente';
					default:
						return 'Reserva en Estado Indeterminado';
				}
			};

			$scope.searchBooking = function() {
				if ($scope.content.isB2B) {
					$state.go('results-booking');
				}
				else {
					$state.go('my-booking');
				}
			};

			$scope.goToHome = function() {
				$state.go('home');
			};

			$scope.cleanEmail = function(input) {
				return cUtilsFields.cleanEmail(input);
			};

			function errorInternal(msg) {
				$uibModalStack.dismissAll();
				$scope.view.loadingResults = false;
				$scope.view.cancellationLoader = false;
				$scope.view.modalText = (typeof msg === 'undefined') ? "Se produjo un error al intentar realizar la acción, contacte con Hoteles Cocha" : msg;
				$scope.$applyAsync();
				$uibModal.open({
					size: 'sm',
					scope: $scope,
					templateUrl: 'hotels-error-msg'
				});
			}

			function errorNoResult() {
				$scope.view.loadingResults = false;
				$scope.view.modalText = "No se encontraron resultados";
				$scope.$applyAsync();
				$uibModal.open({
					size: 'sm',
					scope: $scope,
					templateUrl: 'hotels-error-msg'
				});
			}

			$scope.getIconAmenity = function(amenityName) {
				switch (amenityName) {
					case 'breakfast':
						return "cch-amenities-breakfast";
					case 'internet':
						return "cch-wifi";
					case 'parking':
						return "cch-parking";
					case 'all_inclusive':
						return "cch-amenities-all-inclusive";
					case 'half_pension':
						return "cch-amenities-half-pension";
					case 'full_pension':
						return "cch-amenities-full-pension";
					default:
						return '';
				}
			};

		});
});
