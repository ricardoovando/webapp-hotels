define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.controllers.HotelResultsController', ['ngAnimate'])
		.controller('HotelResultsController', function($document, $window, $scope, $uibModal, $state, $stateParams, $location, $anchorScroll, $timeout, AnalyticsService, HotelSearchDataService, HotelResultsDataService, AppCommonService, CurrencyService) {

			var callbacksOK = {
				ORIGIN_SUCCESS: function(_place) {
					$scope.content.search.places.destination = _place;
					if ($scope.content.search.corporateCode) {
						AppCommonService.validateSearchNemo($scope.content.search.corporateCode, callbacksOK.NEMO_SUCCESS, callbacksError.ERROR_NEMO);
					}
					else {
						callbacksOK.NEMO_SUCCESS();
					}
				},
				NEMO_SUCCESS: function() {
					searchHotels($scope.content.filters, $scope.content.sort);
				},
				SEARCH_SUCCESS: function(data, filters, sort, results, numberResults, exchangeRate) {
					CurrencyService.setExchangeRate(exchangeRate);
					$scope.content.currency = data.exchangeRate;
					$scope.content.results = results; //HotelResultsDataService.getResultsFound(results, true);
					$scope.content.numberResults = numberResults;
					var requestData = $scope.content.results[0].request;
					if (requestData.isPreferred === true) {
						$scope.view.sortOption = {
							key: 'default',
							//key: 'rating2',
							value: 'Ranking'
						};
						$scope.content.sort = 'default';
						//$scope.content.sort = 'rating2';
					}
					if ($stateParams.corporateCode && $stateParams.corporateCode.toString() !== '') {
						requestData.corporateCode = $stateParams.corporateCode;
					}
					if ($stateParams.reservationType) {
						requestData.reservationType = $stateParams.reservationType;
					}
					$scope.content.regionMapData.stateData = requestData;
					AppCommonService.refreshUrl('hotel-results', requestData);
					$scope.view.loadingResults = false;

					if (filters.start === 0 && filters.limit === 20) {
						AnalyticsService.hotelSearch(requestData);

						$scope.view.fullResults = [];
						var noLimitFilters = _.cloneDeep(filters);
						noLimitFilters.start = 0;
						noLimitFilters.limit = 9999;
						HotelSearchDataService.searchHotels($scope.content.search, callbacksOK.FULL_SEARCH_SUCCESS, callbacksError, noLimitFilters, sort);
					}

					if (filters.limit === 20) {
						AnalyticsService.hotelImpression(results, filters.start);
					}

					$scope.$broadcast('regionSearchFinished', {
						data: data,
						filters: filters,
						sort: sort
					});
					$scope.$applyAsync();
				},
				FULL_SEARCH_SUCCESS: function(data, filters, sort, results) {
					$scope.view.fullResults = results;
					$scope.$applyAsync();
				},
				SEARCH_ROOMS_SUCCESS: function(hotel, idHotel) {
					$scope.view.detailsLoader[idHotel] = false;
					$scope.content.hotelDetails[idHotel] = hotel;
					$scope.$applyAsync();
				}
			};

			var callbacksError = {
				ERROR_NO_RESULTS: noResultsError,
				ERROR_NORESULT: noRoomsResultsError,
				ERROR_RESERVATION: internalError,
				ERROR_PAYMENTURL: internalError,
				ERROR_NETWORK: networkError,
				ERROR_INTERNAL: internalError,
				//CURRENCY_ERROR: currencyError
				ERROR_NEMO: function() {
					customError('Nemo Invalido, por favor utilice otro nemo para buscar.');
				}
			};

			try {
				$scope.content = HotelResultsDataService.getContentData($stateParams);
				$scope.view = HotelResultsDataService.getViewData();
				$scope.view.backgroundY = 0;
				//AppCommonService.refreshUrl('hotel-results', $stateParams);
				//HotelSearchDataService.getSearchCurrencyAndResults(callbacksOK.CURRENCY_OK, callbacksError);
				HotelSearchDataService.getPlaceDestination($stateParams.destination, callbacksOK, callbacksError);
			}
			catch (e) {
				console.warn("[HotelResultsController]", "Could not load data from storage, performing a new search.\n", e.stack);
				$state.go('hotel-search', $stateParams);
				return;
			}
			addListeners();

			function verifyIfClosed(value, result) {
				if (value === true && $scope.view.mode.value === 'sticky') {
					$scope.view.opened.value = result;
				}
				else {
					$scope.view.opened.value = '';
				}
			}

			function searchHotels(updatedParams, sortParam) {
				$scope.view.showInfoBox = true;
				$scope.content.numberResults = 0;
				if (!updatedParams.hasOwnProperty('preferredHotelId') && !_.isEmpty(updatedParams.preferredHotelId)) {
					$scope.content.search.preferredHotelId = updatedParams.preferredHotelId;
				}
				$scope.$applyAsync();
				$scope.view.reservationType = ($scope.content.search.reservationType) ? $scope.content.search.reservationType : (updatedParams.reservationType ? updatedParams.reservationType : '');
				var suppliers = [];
				_.forEach(updatedParams.supplier, function(value, key) {
					if (value) {
						key = (key === 'EAN' ? 'STAND' : key);
						suppliers.push(key);
					}
				});
				$scope.view.supplierFilter = suppliers.join(',');
				//alert($scope.view.supplierFilter);

				HotelSearchDataService.searchHotels($scope.content.search, callbacksOK.SEARCH_SUCCESS, callbacksError, updatedParams, sortParam);
			}

			function addListeners() {
				$scope.$on('FilterParametersUpdated', function(event, updatedParams) {
					$scope.view.loadingResults = true;
					console.info("[HotelResultsController]", "Filter params updated and received broadcast, refreshing results found.");
					$scope.content.filters = updatedParams;
					//$scope.content.filters.start = 0;
					//$scope.content.filters.limit = 20;
					var sort = null;
					if (!updatedParams.hasOwnProperty('preferredHotelId') && !_.isEmpty(updatedParams.preferredHotelId)) {
						sort = $scope.content.sort;
					}
					searchHotels(updatedParams, sort);
				});
				/*				
				$scope.$on('SearchUpdated', function(event, stateData) {
					$scope.view.loadingResults = true;
					console.info("[HotelResultsController]", "Search updated and received broadcast, refreshing results found. Event: ", event, stateData);
					//when the filters are updated , we need to search for the current sorting value
					//update scope.content.search
					$scope.content.search.places.destination = null;
					$scope.content.search.dates.arrival = stateData.arrival;
					$scope.content.search.dates.departure = stateData.departure;
					$scope.content.search.passengers.adults = HotelSearchDataService.getAdultsFromParameters(stateData);
					$scope.content.search.passengers.children = HotelSearchDataService.getChildrenFromParameters(stateData);
					$scope.content.search.passengers.rooms = HotelSearchDataService.getNumberRoomsFromParameters(stateData);
					$scope.content.search.rooms = HotelSearchDataService.getRoomsFromParameters(stateData);

					$scope.content.filters = {};
					$scope.content.filters.start = 0;
					$scope.content.filters.limit = 20;

					$scope.content.flagResetFilters = true;

					fillPlacesAndSearch(stateData.destination);
					$scope.$applyAsync();
				});
				*/
				$timeout(function() {
					angular.element(document).ready(function() {

						angular.element($window).scroll(function() {
							if ($scope.view.editSearch.value === false || $scope.view.filters.value === false) {
								return;
							}
							if (angular.element(document.getElementById('#search-info-wrapper-id'))) {

								var preMode = $scope.view.mode.value;

								if (typeof $document.find('#search-info-wrapper-id').offset() !== 'undefined' && angular.element($window).scrollTop() < $document.find('#search-info-wrapper-id').offset().top) {
									$document.find('#search-info-box-id').removeClass('transparent-background');
									$document.find('#search-info-fixed-background-id').removeClass('search-info-fixed-background-show');
									$scope.view.mode.value = 'normal';
									if ($scope.view.opened.value === 'edit') {
										$scope.view.editSearch.value = false;
									}
									if ($scope.view.opened.value === 'filters') {
										$scope.view.filters.value = false;
									}
								}
								else {
									$document.find('#search-info-box-id').addClass('transparent-background');
									$document.find('#search-info-fixed-background-id').addClass('search-info-fixed-background-show');
									$scope.view.mode.value = 'sticky';
									$scope.view.editSearch.value = true;
									$scope.view.editSearch.arrow = true;
									$scope.view.filters.value = true;
									$scope.view.filters.arrow = true;
								}
								if (typeof $document.find('#search-info-wrapper-id').offset() !== 'undefined' && angular.element($window).scrollTop() > $document.find('#search-info-wrapper-id').offset().top + 230) {
									$scope.view.mode.value = 'sticky';
								}
								if ($scope.view.mode.value !== preMode) {
									$scope.$applyAsync();
								}
							}
						});
					});
				}, 500);

				$scope.$watch('view.editSearch.value', function(newValue) {
					verifyIfClosed(newValue, 'edit');
				});
				$scope.$watch('view.filters.value', function(newValue) {
					verifyIfClosed(newValue, 'filters');
				});
			}

			//function fillPlacesAndSearch(destination) {
			//	HotelSearchDataService.getPlaceDestination(destination, callbacksOK, callbacksError);
			//}
			/*
			function closeCollapses() {
				angular.element($window).click(function() {
					$scope.view.collapseSelectBox = true;
					$scope.$apply();
				});

				$scope.$on('$destroy', function() {
					angular.element($window).off('scroll');
					angular.element($window).off('click');
				});
			}*/

			$scope.filterFn = function(parameters, sort) {
				//when the filters are updated , we need to search for the current sorting value
				$scope.view.loadingResults = true;
				console.info("[HotelResultsController]", "Filter params updated , refreshing results found.");
				$scope.content.filters = parameters;
				$scope.$applyAsync();
				//$scope.content.filters.start = 0;
				//$scope.content.filters.limit = 20;
				$timeout(function() {
					searchHotels(parameters, sort);
				}, 100);
			};

			$scope.getRegionMap = function() {
				$scope.view.loader = true;
				$scope.view.mapType = 'region';
				$scope.content.regionMapData.currency = $scope.content.currency;
				if ($scope.view.fullResults.length === 0) {
					$uibModal.open({
						size: 'sm',
						scope: $scope,
						templateUrl: 'region-map-error'
					});
					return;
				}

				$uibModal.open({
					size: 'lg',
					scope: $scope,
					templateUrl: 'region-template-map',
					controller: 'ModalController',
					resolve: {
						content: function() {
							return null;
						}
					}
				});
			};

			$scope.isOnQuotation = function() {
				if (!$window.QUOTE || $window.QUOTE === '0') {
					return false;
				}
				return true;
			};

			function internalError() {
				$scope.view.loadingResults = false;

				$uibModal.open({
					scope: $scope,
					size: 'sm',
					templateUrl: 'hotels-error-reservation'
				});
			}

			function networkError() {
				$scope.view.loadingResults = false;
				$uibModal.open({
					scope: $scope,
					size: 'sm',
					templateUrl: 'hotels-error-network'
				});
			}

			function noResultsError() {
				$scope.view.loadingResults = false;
				//$state.go('home');
				$uibModal.open({
					size: 'sm',
					templateUrl: 'hotels-no-results'
				});
			}

			function noRoomsResultsError() {
				$scope.view.loadingResults = false;
				$uibModal.open({
					size: 'sm',
					templateUrl: 'rooms-no-results'
				});
			}

			function customError(msg) {
				$scope.view.msgError = (typeof msg !== 'undefined' && msg !== '') ? msg : '';
				$uibModal.open({
					scope: $scope,
					size: 'sm',
					templateUrl: 'hotels-error-reservation-msg-only'
				});
			}

		});
});
