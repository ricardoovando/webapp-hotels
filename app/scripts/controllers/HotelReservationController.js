define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.controllers.HotelReservationController', [])
		.controller('HotelReservationController', function($stateParams, $sce, $window, $uibModal, $timeout, $state, $scope, cCommonConstants, cUtilsFields, cUtilsObject, cUtilsDates, HotelSearchDataService, HotelReservationDataService, HotelReservationParserService, StaticDataService, CurrencyService, B2BDataService, AnalyticsService, AppCommonService) {

			var trackingData = null;
			window.scrollTo(0, 0);

			var callbacksOK = {
				CURRENCY_OK: function(exchangeRate) {
					$scope.content.currency = exchangeRate;
					//HotelReservationDataService.getAvailability($scope.content, callbacksOK.AVAILABILITY_OK, callbacksError);
				},
				AVAILABILITY_OK: function(responseData) {
					$timeout(function() {
						$scope.view.pageLoader = false;
					}, 1500);
					$scope.view.availabilityData = responseData;
					if (($scope.view.availabilityData.LCInfo && $scope.view.availabilityData.LCInfo.isBlocked) || $scope.content.generalData.supplier === 'SAB') {
						$scope.content.paymentMethod = 'TC';
						$scope.content.generalData.corporateCode = "";
						$scope.$applyAsync();
					}

					fillContentData(responseData);
					HotelSearchDataService.getPlaceDestination($scope.content.hotel, callbacksOK, callbacksError, 'hotel');
				},
				ORIGIN_SUCCESS: function(responseData) {
					$scope.content.idRegion = responseData.id;
					$scope.content.destination = responseData.regionNameLong;
				},
				TOKEN_OK: function(responseData) {
					HotelReservationDataService.confirmHotel(responseData, $scope.view.registerEmail, $scope.content, $scope.view.availabilityData, callbacksOK, callbacksError);
				},
				CARDNUMBER_OK: function(responseData) {
					$scope.content.generalData.B2B.cardForeign = responseData.isForeign;
					$scope.view.errors.cardNumberInvalid = responseData.isInvalid;
					$scope.content.generalData.B2B.cardType = responseData.cardType;
					$scope.view.loader.cardNumber = false;
				},
				BUSINESSID_OK: function(_result) {
					$scope.view.errors.businessIdInvalid = !_result.isValid;
					$scope.view.errors.creditLineMismatch = _result.blockPayment;
					$scope.view.assignedNemo = _result.nemo;

					if ($scope.view.availabilityData.LCInfo && $scope.view.availabilityData.LCInfo.isBlocked) {
						$scope.view.businessIdLcValid = false;
						_result.isValidLC = false;
					}
					else {
						$scope.view.businessIdLcValid = _result.isValidLC;
					}

					if (_result.isValidLC) {
						$scope.view.creditLineValidationText = "";
						$scope.content.paymentMethod = 'LC';
					}
					else {
						$scope.view.creditLineValidationText = "Para este negocio no es posible acceder a este medio de pago.Para mayor información contacta a finanzas.";
						$scope.content.paymentMethod = 'TC';
					}

					$scope.$applyAsync();
					$scope.view.loader.businessId = false;
				},
				RESERVATION_OK: goToPayment,
				PAYMENT_COMPLETE: goToEmission,
				PAYMENT_PENDING: goToPayment
			};

			var callbacksError = {
				ERROR_RESERVATION: goToPayment,
				ERROR_PAYMENTURL: internalError,
				ERROR_NETWORK: networkError,
				ERROR_INTERNAL: internalError,
				ERROR_AVAILABILITY: errorAvailability,
				ERROR_RESERVATION_GENERAL: msgReservationError,
				CURRENCY_ERROR: currencyError,
				CREDIT_CARD_ERROR: function() {
					$scope.view.errors.cardNumberInvalid = true;
					$scope.content.generalData.B2B.cardForeign = -1;
					$scope.content.generalData.B2B.cardType = -1;
					$scope.view.loader.cardNumber = false;
				},
				BUSINESS_ID_ERROR: function() {
					$scope.view.errors.businessIdInvalid = true;
					$scope.view.loader.businessId = false;
					//internalError()
				},
				/*
				BLOCKED_NEMO: function(_data) {
					$scope.view.isNemoBlocked = true;
					msgError('El cliente ' + _data.paramNemo + ' está bloqueado actualmente.');
				}
				*/
				ERROR_BUSINESS: function() {
					msgReservationError('Error en el número de negocio');
				},
				ERROR_EXCEEDEDCARD: function() {
					msgReservationError('Monto de reserva excede monto disponible en tarjeta.');
				},
				ERROR_INVALIDCARD: function(_data) {
					var msg = 'Tarjeta Invalida: ' + (_data && _data.description ? _data.description : '');
					msgReservationError(msg);
				},
				ERROR_BUSINESSCARD: function() {
					msgReservationError('Tarjeta y negocio rechazados');
				},
				ERROR_ATTEMPTS: function() {
					msgReservationError('Excedió el número de intentos para el pago');
				},
				ERROR_PRICE_CRASH: function() {
					msgReservationError('Hay una diferencia de precios, por favor elija otra habitación');
				},
				ERROR_CARD: function(responseData) {
					HotelReservationDataService.confirmHotel(responseData, $scope.view.registerEmail, $scope.content, $scope.view.availabilityData, callbacksOK, callbacksError);
				},
				ERROR_PAYMENT: function(responseData) {
					HotelReservationDataService.confirmHotel(responseData, $scope.view.registerEmail, $scope.content, $scope.view.availabilityData, callbacksOK, callbacksError);
				},
				ERROR_PAYDATA: function(responseData) {
					HotelReservationDataService.confirmHotel(responseData, $scope.view.registerEmail, $scope.content, $scope.view.availabilityData, callbacksOK, callbacksError);
				},
				ERROR_CHECKPAY: function(responseData) {
					HotelReservationDataService.confirmHotel(responseData, $scope.view.registerEmail, $scope.content, $scope.view.availabilityData, callbacksOK, callbacksError);
				},
				ERROR_BOOKING: function(responseData) {
					HotelReservationDataService.confirmHotel(responseData, $scope.view.registerEmail, $scope.content, $scope.view.availabilityData, callbacksOK, callbacksError);
				},
				ERROR_HOTEL: function(responseData) {
					HotelReservationDataService.confirmHotel(responseData, $scope.view.registerEmail, $scope.content, $scope.view.availabilityData, callbacksOK, callbacksError);
				},
				ERROR_TRAVELIO_AUTH: function(responseData) {
					HotelReservationDataService.confirmHotel(responseData, $scope.view.registerEmail, $scope.content, $scope.view.availabilityData, callbacksOK, callbacksError);
				},
			};

			$scope.closeWindow = function() {
				window.close();
			};

			try {
				var forceLoad = (($stateParams.hasOwnProperty('force')) ? $stateParams.force : false);
				var loadRoomData = HotelReservationDataService.getRoomData(forceLoad);
				if (forceLoad) {
					if (loadRoomData) {
						AppCommonService.refreshUrl('checkout', {
							force: null
						});
					}
					else {
						msgError("No se pudo cargar la información de la habitación, por favor intente seleccionando la habitación nuevamente.");
						return;
					}
				}
				$scope.content = HotelReservationDataService.getContentData();
				//alert(JSON.stringify($scope.content));
				$scope.view = HotelReservationDataService.getViewData($scope.content);
				$scope.view.LCpaymentMethod = false;
				$scope.view.dictionary = {
					message1: $sce.trustAsHtml("<h3>Estamos Procesando su solicitud</h3><br><h2>NO CIERRE ESTA VENTANA</h2> "),
					message2: "Este proceso tardará menos de 2 minutos",
					message3: $sce.trustAsHtml("Ante cualquier consulta comuníquese con Soporte Hoteles: <b>Mail</b>: hotelesonline@cocha.com <b>Anexos</b>: Paulina Vega 1282 - Pilar Ramírez 1015 - Nataly Mardones 1338")
				};
				CurrencyService.getExchangeRate(callbacksOK.CURRENCY_OK, callbacksError);
				HotelReservationDataService.getAvailability($scope.content, callbacksOK.AVAILABILITY_OK, callbacksError);
			}
			catch (e) {
				console.error("[HotelReservationController]", "Could not load data.\n", e.stack);
				//$state.go('home');
				return;
			}
			$scope.content.generalData.B2B.user = B2BDataService;
			trackData();
			$scope.flag = {};
			$scope.flag.test = ($stateParams.hasOwnProperty('flag') ? $stateParams.flag : '');

			$scope.validateField = function(type) {
				switch (type) {
					case 'cardNumber':
						$scope.view.cardNumber = true;
						$scope.view.loader.cardNumber = true;
						HotelReservationDataService.validateCardNumber($scope.content.generalData.B2B[type], callbacksOK.CARDNUMBER_OK, callbacksError);
						break;
					case 'businessId':
						$scope.view.businessId = true;
						$scope.view.loader.businessId = true;
						HotelReservationDataService.validateBusinessId($scope.content.generalData.B2B[type], callbacksOK.BUSINESSID_OK, callbacksError, $scope.content.generalData.corporateCode, $scope.view.userEmail);
						break;
				}
			};

			$scope.selectPaymentsType = function(code) {
				$scope.content.generalData.B2B.paymentsType = code;
				if (code !== 'VC') {
					$scope.view.collapse.payments = true;
				}
			};

			$scope.toggleNightsCollapse = function(index) {
				if (typeof $scope.view.collapse.roomSummary[index] === 'undefined') {
					$scope.view.collapse.roomSummary[index] = true;
				}
				else {
					$scope.view.collapse.roomSummary[index] = !$scope.view.collapse.roomSummary[index];
				}
			};

			$scope.range = function(min, max, step) {
				step = step || 1;
				var input = [];
				for (var i = min; i <= max; i += step) {
					input.push(i);
				}
				return input;
			};

			$scope.selectB2Bmethod = function(_method) {
				if (!$scope.view.businessIdLcValid && _method === 'LC') {
					return;
				}
				$scope.content.paymentMethod = _method;
				$scope.$applyAsync();
			};

			$scope.selectLCcurrency = function(_currency) {
				$scope.content.generalData.B2B.LCcurrency = _currency;
			};

			$scope.toggleCollapse = function(type) {
				$scope.view.collapse[type] = !$scope.view.collapse[type];
			};

			$scope.selectYear = function(yearNumber) {
				$scope.content.generalData.B2B.expirationYear = yearNumber;
				$scope.view.collapse.years = true;
				$scope.view.nameYear = yearNumber;
				$scope.view.yearSelected = true;
			};

			$scope.selectMonth = function(monthNumber, monthName) {
				$scope.content.generalData.B2B.expirationMonth = monthNumber;
				$scope.view.collapse.months = true;
				$scope.view.nameMonth = monthName;
				$scope.view.monthSelected = true;
			};

			$scope.selectNumberPayments = function(numberPayments) {
				$scope.content.generalData.B2B.payments = numberPayments;
				$scope.view.collapse.payments = true;
				$scope.view.namePayments = numberPayments;
			};

			$scope.formatId = function(input, passenger) {
				var output = input;

				if (passenger.identifier.identifierClass === 'RUT') {
					output = input.replace(/[^kK\d]/ig, '');
					if (/\d+[kK\d]/.test(output)) {
						var id = output.substring(0, output.length - 1);
						var verifier = output.substring(output.length - 1);
						output = parseInt(id).toLocaleString() + '-' + verifier.toUpperCase();
						passenger.identifier.number = id;
						passenger.identifier.validationNumber = verifier.toUpperCase();
					}
					else {
						passenger.identifier.number = output;
						passenger.identifier.validationNumber = '';
					}
				}
				else {
					output = cUtilsFields.cleanId(input);
					passenger.identifier.number = output;
					passenger.identifier.validationNumber = '';
				}

				return output;
			};

			$scope.setGender = function(passenger, gender, parentIndex) {
				$scope.view.collapse.gender[parentIndex] = true;
				passenger.gender = gender;
			};

			$scope.setBornDate = function(passenger, type, element, parentIndex) {
				$scope.view.collapse[type][parentIndex] = true;
				if (_.isObject(element)) {
					passenger.bornDate[type] = element.index;
				}
				else {
					passenger.bornDate[type] = element;
				}
				$scope.changeViewDays(passenger);
			};

			$scope.range = function(min, max, step) {
				step = step || 1;
				var input = [];
				for (var i = min; i <= max; i += step) {
					input.push(i);
				}
				return input;
			};

			$scope.setIdentifier = function(_passenger, _document, _parentIndex) {
				$scope.view.collapse.document[_parentIndex] = true;
				_passenger.identifier.identifierClass = _document.key;
				$scope.view.passengersIds[_parentIndex] = $scope.formatId($scope.view.passengersIds[_parentIndex], _passenger);
			};

			$scope.setIdentifierCountry = function(_passenger, _country, _parentIndex) {
				$scope.view.collapse.country[_parentIndex] = true;
				_passenger.identifier.country = _country.code;
			};

			$scope.getCountryName = function(_code) {
				var out = _.find($scope.view.options.country, {
					code: _code
				});
				return (out ? out.name : '');
			};

			$scope.changeViewDays = function(passenger) {
				$scope.view.options.date[passenger.id - 1].days = cUtilsDates.getRangeDays(passenger.bornDate);

				if ($scope.view.options.date[passenger.id - 1].days.length < passenger.bornDate.day) {
					passenger.bornDate.day = '';
				}
			};

			$scope.toogleOptions = function(index, keyEvent, collapse) {
				if ($scope.view.closeCollapsesCount === 1) {
					closeCollapses();
					$scope.view.closeCollapsesCount++;
				}
				if (keyEvent) {
					keyEvent.stopImmediatePropagation();
				}
				_.forEach($scope.view.collapse, function(category, categoryName) {
					_.forEach(category, function(item, itemIndex) {
						if (itemIndex === index && categoryName === collapse) {
							category[itemIndex] = !category[itemIndex];
						}
						else {
							category[itemIndex] = true;
						}
					});
				});
			};

			$scope.selectFirstOption = function(_keyEvent, _id, _collapse, _index) {
				if (_keyEvent.keyCode === 40) {
					_keyEvent.preventDefault();
					$scope.view.collapse[_collapse][_index] = false;
					var element = angular.element('#' + _id + _index);
					if (element) {
						element.focus();
					}
				}
			};

			$scope.moveOnOptions = function(_keyEvent, _index, _parentIndex, _parentId, _idOrigin, _function, _param1, _param2, _param3) {
				if (_keyEvent.keyCode === 13) {
					var element = angular.element('#' + _parentId + _parentIndex);
					var tabIndex = null;
					var nextElement = null;
					element.focus();
					if (_function === 'setBornDate') {
						$scope[_function](_param1, _param2, _param3, _parentIndex);
					}
					else {
						$scope[_function](_param1, _param2, _param3);
					}
					tabIndex = element[0].tabIndex;
					nextElement = angular.element(document.body).find('[tabindex=' + (tabIndex + 1) + ']');
					$timeout(function() {
						nextElement.focus();
					});
				}
				var id = 0;
				if (_keyEvent.keyCode === 40) {
					_keyEvent.preventDefault();
					id = (_index + 1);
					var element_down = angular.element('#' + _idOrigin + id + '-' + _parentIndex);
					if (element_down) {
						element_down.focus();
					}
				}
				if (_keyEvent.keyCode === 38) {
					_keyEvent.preventDefault();
					id = (_index - 1);
					var element_up = angular.element('#' + _idOrigin + id + '-' + _parentIndex);
					if (element_up) {
						element_up.focus();
					}
				}
			};

			$scope.showTerms = function() {
				$uibModal.open({
					templateUrl: 'booking-terms',
					size: 'md'
				});
			};

			$scope.showPrivacy = function() {
				$uibModal.open({
					templateUrl: 'booking-privacy',
					size: 'lg'
				});
			};

			$scope.cleanName = function(input) {
				//return cUtilsFields.cleanName(input);
				return HotelReservationParserService.cleanName(input);
			};

			$scope.cleanEmail = function(input) {
				return cUtilsFields.cleanEmail(input);
			};

			$scope.reload = function() {
				$window.location.reload();
			};

			$scope.goBack = function() {
				$window.close();
			};

			$scope.retry = function() {
				//ANALYTICS
				console.info("[HotelResultDirective]", "Retrying block hotel with data: ", $scope.content);
				bookHotel();
			};

			$scope.confirmHotel = function() {
				console.info("[HotelResultDirective]", "Blocking hotels with data: ", $scope.content);
				bookHotel();
			};

			$scope.selectPaymentMethod = function(_method) {
				$scope.content.paymentMethod = _method;
				//AnalyticsService.selectPaymentMethod(_method);
			};

			$scope.getChargePolicy = function(room) {
				var msg = '';
				if (room.supplierCode === 'SAB') {
					if (room.guaranteeSurchargeRequired === 'D') {
						msg += 'Se realizarán cargos a La tarjeta de crédito en cualquier momento hasta por la totalidad del monto en reserva.';
					}
					else if (room.guaranteeSurchargeRequired === 'G') {
						msg += 'La tarjeta de crédito sera utilizada para garantizar la reserva. No se efectuarán cargos.';
					}
					else {
						msg += 'Sin información disponible';
					}
				}
				return msg;
			};

			$scope.selectBedType = function(index, bedType) {
				$scope.content.passengers[index].bedType = bedType;
			};

			function fillContentData(data) {
				//console.info("CONTENT", $scope.content);
				//alert(JSON.stringify(data));
				//alert(JSON.stringify(data.hotelRoomResponse));
				if (!_.isEmpty(data)) {
					$scope.content.prices.main = ($scope.content.generalData.supplier === 'SAB' || $scope.content.generalData.supplier === 'TRV') ? data.checkoutDetailDTO.totalPricing : data.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.total;
					if ($scope.content.generalData.supplier === "SAB" || $scope.content.generalData.supplier === "TRV") {
						$scope.content.prices.totalCLP = (parseFloat(data.checkoutDetailDTO.totalPricing) * parseFloat($scope.content.currency)).toFixed(0);
					}
					else {
						$scope.content.prices.totalCLP = (parseFloat(data.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.total) * parseFloat($scope.content.currency)).toFixed(0);
					}
					var bedType = ($scope.content.generalData.supplier === 'SAB' || $scope.content.generalData.supplier === 'TRV') ? 1 : data.hotelRoomResponse.bedTypes.bedType[0].id;
					for (var i = 0; i < $scope.content.passengers.length; i++) {
						$scope.content.passengers[i].bedType = bedType;
					}
					$scope.content.cancellationPolicy = ($scope.content.generalData.supplier === 'SAB' || $scope.content.generalData.supplier === 'TRV') ? data.policies : data.hotelRoomResponse.rateInfos.rateInfo.cancellationPolicy;
					if ($scope.content.generalData.supplier === 'SAB' || $scope.content.generalData.supplier === "TRV") {
						$scope.view.hotelStarRating = data.checkoutDetailDTO.rating;
					}
				}
			}

			$scope.getCancellationPolicy = function(_info) {
				var msg = '';
				if (_info.cancellationPolicy !== null && _info.cancellationPolicy.length > 0) {
					msg += _info.cancellationPolicy;
				}
				else {
					msg += (_info.refundable === false ? 'Esta tarifa no permite devolución' : 'Sin información Disponible');
				}
				return msg;
			};

			function closeCollapses() {
				angular.element($window).click(function() {
					$scope.toogleOptions(null, null, null);
					$scope.$apply();
				});

				$scope.$on('$destroy', function() {
					angular.element($window).off('click');
					console.info("[ReservationController]", "Saving passengers and contact data");
					//FlightsBookingDataService.setPassengers($scope.content.passengers);
					//FlightsBookingDataService.setContact($scope.content.contact);
				});
			}

			function trackData() {
				AnalyticsService.checkoutStep(1);
			}

			//called when the payment button is pressed
			function bookHotel() {
				$scope.view.LCpaymentMethod = ($scope.content.paymentMethod === 'LC') ? true : false;

				if ($scope.content.paymentMethod === 'LC' && $scope.view.availabilityData.LCInfo && $scope.view.availabilityData.LCInfo.showMessage && !$scope.view.acceptLCterms) {
					$uibModal.open({
						size: 'sm',
						templateUrl: 'LC-accept-terms'
					});
					return;
				}

				if (!$scope.view.acceptTerms) {
					$uibModal.open({
						size: 'sm',
						templateUrl: 'flights-accept-terms'
					});
					return;
				}

				if ($scope.content.generalData.supplier === 'SAB') {
					var validation = false;
					//children validation
					_.forEach($scope.content.rooms, function(value, index) {
						if ($scope.content.rooms[index].children > 0) {
							validation = true;
						}
					});
					if (validation) {
						var msgChildren = "<p>Sabre no permite realizar reservas incluyendo niños.</p>";
						msgChildren += "<p> Consulta las Políticas Generales para ver si el hotel permite niños y vuelve a realizar la reserva incluyendo adultos solamente. </p>";
						msgChildren += "<p> Para mayor información contacta a Hoteles Online. </p>";
						msgReservationError(msgChildren);
						return;
					}
				}

				$scope.view.errors = HotelReservationDataService.getConfirmationErrors($scope.content.paymentMethod, $scope.content.contact, $scope.content.passengers, $scope.content.generalData, $scope.view.errors, !(_.isNull($scope.content.generalData.B2B.user)));
				$scope.$watch('content.contact', function() {
					$scope.view.errors = HotelReservationDataService.getConfirmationErrors($scope.content.paymentMethod, $scope.content.contact, $scope.content.passengers, $scope.content.generalData, $scope.view.errors, !(_.isNull($scope.content.generalData.B2B.user)));
				}, true);

				$scope.$watch('content.passengers', function() {
					$scope.view.errors = HotelReservationDataService.getConfirmationErrors($scope.content.paymentMethod, $scope.content.contact, $scope.content.passengers, $scope.content.generalData, $scope.view.errors, !(_.isNull($scope.content.generalData.B2B.user)));
				}, true);

				if (!cUtilsObject.containsValueDeep($scope.view.errors, true)) {
					//ANALYTICS

					try { //tracking
						AnalyticsService.checkoutStep(2);
					}
					catch (e) {
						console.error("[ReservationController]", "Could not track the reservation.\n", e.stack);
					}

					$scope.view.isBooking = true;

					try {
						HotelReservationDataService.getToken($scope.content, $scope.view.availabilityData, callbacksOK, callbacksError, $scope.flag.test);
						console.info("[ReservationController]", "Getting payment URL ");
						////HotelReservationDataService.refreshPaymentURL(token, callbacksOK, callbacksError);
					}
					catch (e) {
						console.info("[ReservationController]", "Error getting token with data: ", $scope.content, e.message);
					}
				}
				else {
					formError();
					//ANALYTICS
					console.warn("[ReservationController]", "Not blocking because there's errors found: ", $scope.view.errors);
				}
			}

			function formError() {
				$uibModal.open({
					templateUrl: 'reservation-form-error',
					size: 'sm'
				});
			}

			function goToPayment(_url) {
				if (_.isNull($scope.content.generalData.B2B.user)) {
					//Timeout to wait for ga() in service to be resolved
					$timeout(function() {
						$window.location = _url;
						//}, 300);
					}, 1000);
				}
				else {
					$timeout(function() {
						//$window.location = $state.href('hotel-payment-b2b', {}, {
						//	absolute: true
						//});
						$state.go('hotel-payment-b2b');
					}, 2500);
				}

				dataLayer.push({
					event: 'checkout step',
					category: 'Hotels',
					checkoutStep: 3,
					products: trackingData
				});

			}

			function goToEmission() {
				//alert("EMISION");
				$state.go('hotels-payment');
			}

			function msgReservationError(msg) {
				if (typeof $scope.view === 'undefined') {
					$scope.view = {};
				}
				$scope.view.msgError = (typeof msg !== 'undefined' && msg !== '') ? msg : '';
				$uibModal.open({
					scope: $scope,
					size: 'sm',
					templateUrl: 'hotels-error-reservation-msg-only'
				});
				$scope.view.isBooking = false;
			}

			function msgError(msg) {
				if (typeof $scope.view === 'undefined') {
					$scope.view = {};
				}
				$scope.view.msgError = (typeof msg !== 'undefined' && msg !== '') ? msg : '';
				$uibModal.open({
					scope: $scope,
					size: 'sm',
					templateUrl: 'hotels-error-reservation-general'
				});
			}

			function internalError(msg) {
				$scope.view.loader.cardNumber = false;
				$scope.view.loader.businessId = false;
				$scope.view.msgError = (typeof msg !== 'undefined' && msg !== '') ? msg : '';
				$uibModal.open({
					scope: $scope,
					size: 'sm',
					templateUrl: 'hotels-error-reservation-msg-only'
				});
				$scope.view.isBooking = false;
			}

			function currencyError() {
				$uibModal.open({
					scope: $scope,
					size: 'sm',
					templateUrl: 'hotels-error-currency'
				});
				$scope.view.isBooking = false;
			}

			function errorAvailability() {
				$uibModal.open({
					scope: $scope,
					size: 'sm',
					templateUrl: 'hotels-error-availability'
				});
				$scope.view.isBooking = false;
			}

			function networkError() {
				$scope.view.loader.cardNumber = false;
				$scope.view.loader.businessId = false;
				$uibModal.open({
					scope: $scope,
					size: 'sm',
					templateUrl: 'hotels-error-network'
				});
				$scope.view.isBooking = false;
			}

		});
});
