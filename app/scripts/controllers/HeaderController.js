/* 
define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.controllers.HeaderController', [])
		.controller('HeaderController', function($uibModal, $scope, $timeout, $document, $window, AppCommonService) {
			$scope.view = {
				elementId: null,
				chevrons: {
					others: false,
					campaigns: false
				},
				phonePopup: false,
				secondaryOptions: false,
				showArrow: true,
				leftWidth: 0,
				choiceOnSecond: false,
				moreSites: false
			};
			$scope.content = {
				contact: {
					name: '',
					foid: '',
					email: '',
					countryCode: '',
					areaCode: '',
					phoneNumber: '',
					currentUrl: '',
					subscribe: false
				}
			};
			var win = null;
			var arrow = null;
			var arrowXs = null;
			var arrowContainer = null;
			var element = null;
			var elementLeft = null;
			var elementWidth = null;
			var closeCollapsesCount = 1;

			$scope.addListeners = function() {
				win = angular.element($window);
				arrow = angular.element('#arrow-id');
				arrowXs = angular.element('#arrow-id-xs');
				arrowContainer = angular.element('#arrow-container-id');
				if (win && (arrow || (arrowXs && arrowContainer))) {
					win.on('scroll', onScroll);
					$timeout(function() {
						$scope.selectSection('logo-id', 'normal', null);
						$scope.selectSection('logo-id-xs', 'small', null);
					}, 2000);
				}
			};

			var onScroll = function() {
				if (win.scrollTop() > 121) {
					$scope.view.phonePopup = false;
					$scope.$apply();
				}
			};

			$scope.$on('$destroy', function() {
				if (win) {
					win.off('scroll');
				}
			});

			$scope.selectSection = function(_element, _size, _secondOptions) {
				try {
					element = angular.element('#' + _element);
					elementLeft = element.offset().left;
					elementWidth = element.outerWidth();
					var leftWidth = elementLeft + (elementWidth / 2) - 10;
					if (_size === 'normal') {
						arrow.css({
							'left': leftWidth + 'px',
							'transition': 'left 0.2s'
						});
					}
					else {
						arrowXs.css({
							'left': leftWidth + 'px',
							'transition': 'left 0.2s'
						});
					}

					$scope.view.elementId = _element;
					$scope.view.leftWidth = leftWidth;
					$scope.view.choiceOnSecond = _secondOptions;
				}
				catch (err) {}

			};

			$scope.clickToCall = function(_event) {
				_event.stopImmediatePropagation();
				AppCommonService.openClickToCallModal($scope.content.contact);
			};

			var closeAll = function() {
				win.click(function() {
					$scope.view.phonePopup = false;
					$scope.$apply();
				});
				$scope.$on('$destroy', function() {
					win.off('click');
				});
			};

			$scope.phonePopup = function(_event) {
				if (closeCollapsesCount === 1) {
					closeAll();
					closeCollapsesCount++;
				}
				_event.stopImmediatePropagation();
				$scope.view.phonePopup = !$scope.view.phonePopup;
			};

			$scope.changeOptions = function() {
				$scope.view.secondaryOptions = !$scope.view.secondaryOptions;
				if ($scope.view.choiceOnSecond) {
					if ($scope.view.secondaryOptions) {
						arrowXs.css({
							'left': $scope.view.leftWidth + 'px',
							'transition': 'left 0.2s'
						});
					}
					else {
						arrowXs.css({
							'left': '100%',
							'transition': 'left 0.2s'
						});
					}
				}
				else {
					if ($scope.view.secondaryOptions) {
						arrowXs.css({
							'left': '-' + $scope.view.leftWidth + 'px',
							'transition': 'left 0.2s'
						});
					}
					else {
						arrowXs.css({
							'left': $scope.view.leftWidth + 'px',
							'transition': 'left 0.2s'
						});
					}
				}
			};

			$scope.openMoreSites = function(_event) {
				_event.stopImmediatePropagation();
				$scope.view.moreSites = !$scope.view.moreSites;
				var body = $document[0].body;

				if ($scope.view.moreSites) {
					body.classList.add('no-scroll');
				}
				else {
					body.classList.remove('no-scroll');
				}
			};

			$scope.goToFlights = function() {
				$window.location = 'http://vuelos.cocha.com/';
			};
			$scope.goToHotels = function() {
				$window.location = '/hoteles';
			};
			$scope.goToPackage = function() {
				$window.location = '/paquetes';
			};
		});
});
*/
