define(['angular'], function(angular) {
	'use strict';
	angular.module('COCHAHotels.controllers.HomeController', [])
		.controller('HomeController', function($location, $interval, StaticDataService, HomeDataService, $scope, $stateParams, $uibModal, $state, $window, GeoSearchDataService, HotelSearchDataService, B2BDataService) {

			$scope.view = HomeDataService.getViewData();
			var win, mainBackgroundId, searchBoxWrapperHome;
			$scope.view.backgroundY = 0;
			$scope.view.banner = $window.URL_CAMPAIGN_BACK;
			$scope.content = HomeDataService.getContentData();

			/*var onOffersOk = function(_data) {
				offers = _data.offers;
				filteredOffers = offers;
				$scope.content.offers.title = _data.title;
				$scope.content.offers.count = offers.length;
				$scope.view.regions = _data.regions;
				$scope.moreOffers();
			};*/

			var onScroll = function() {
				var winScrollTop = win.scrollTop();
				var y = $scope.view.backgroundY - (winScrollTop / 2.8);
				mainBackgroundId.css({
					'top': y + 'px'
				});
				$scope.$apply();
			};

			$scope.addListeners = function() {
				searchBoxWrapperHome = angular.element('#search-box-wrapper-home-id');
				mainBackgroundId = angular.element('#main-background-id');
				win = angular.element(window);
				if (win && searchBoxWrapperHome && mainBackgroundId) {
					$scope.view.smallView = win.outerWidth() < 768;
					$scope.view.height = win.outerHeight() - 279;
					if ($scope.view.height >= 700 - 279) {
						$scope.view.height = 700 - 279;
					}
					searchBoxWrapperHome.css('height', $scope.view.height + 'px');
					if (!$scope.view.smallView) {
						win.on('scroll', onScroll);
					}
				}

				$scope.$on('$destroy', function() {
					if (win) {
						win.off('scroll');
						win.off('click');
					}
				});
			};

			var offers = null;
			var filteredOffers = null;
			var offset = 0;

			HotelSearchDataService.initializeStorage();
			//HomeDataService.getCampaign('hoteles-home-web', onOffersOk);
			$scope.content.B2Buser = B2BDataService;
			$scope.toggleFilterCollapse = function(_type, _event) {
				if ($scope.view.closeCollapsesCount === 1) {
					closeCollapses();
					$scope.view.closeCollapsesCount++;
				}
				_event.stopImmediatePropagation();
				$scope.view.filterCollapse[_type] = !$scope.view.filterCollapse[_type];
			};

			$scope.onOffersViewClicked = function() {
				//AnalyticsService.offersView();
			};

			$scope.gotoMyBkg = function() {
				$state.go('my-booking');
			};

			$scope.regionsFilter = function(_regionIndex, _event) {
				_event.stopImmediatePropagation();
				$scope.view.selectors.offers.value = false;
				$scope.view.selectors.dates.value = false;
				if (_regionIndex === 0 && $scope.view.regions[_regionIndex].checked === true) { //all regions
					return;
				}
				else if (_regionIndex === 0) { //all regions
					filteredOffers = offers;
					$scope.view.regions.map(function(region) {
						region.checked = false;
						return region;
					});
					$scope.view.regions[0].checked = true;
				}
				else {
					$scope.view.regions[_regionIndex].checked = !$scope.view.regions[_regionIndex].checked;
					$scope.view.regions[0].checked = false;
					var selectedRegions = [];
					$scope.view.regions.forEach(function(region) {
						if (region.checked === true) {
							selectedRegions.push(region.id);
						}
					});

					if (selectedRegions.length === 0 || selectedRegions.length === ($scope.view.regions.length - 1)) { //no regions or all regions
						$scope.regionsFilter(0, _event);
					}
					else {
						filteredOffers = _.filter(offers, function(offer) {
							return (offer.destinations[0] && selectedRegions.indexOf(offer.destinations[0].region.id) > -1);
						});
					}
				}

				$scope.content.offers.items = [];
				offset = 0;
				$scope.moreOffers();
			};

			$scope.moreOffers = function() {
				//AnalyticsService.offersViewMore();
				$scope.view.loadingOffers = true;
				$scope.content.offers.items = $scope.content.offers.items.concat(filteredOffers.slice(offset, offset + 10));
				offset += 10;
				$scope.view.remainingOffers = (offset < filteredOffers.length);
				$scope.view.loadingOffers = false;
			};

			angular.element(window).click(function() {
				$scope.view.filterCollapse.region = true;
				$scope.view.filterCollapse.date = true;
				//$scope.$apply();
			});

			$scope.$on('$destroy', function() {
				angular.element(window).off('click');
			});

			function closeCollapses() {
				angular.element(window).click(function() {
					$scope.view.filterCollapse.region = true;
					$scope.view.filterCollapse.date = true;
					$scope.$apply();
				});

				$scope.$on('$destroy', function() {
					angular.element(window).off('click');
				});
			}

		});
});
