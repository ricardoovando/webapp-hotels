define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.services.HomeParserService', [])
		.factory('HomeParserService', function() {

			return {
				parseCampaign: function(_data) {
					var campaign = null;
					if (!_data[0]) {
						console.error("[HomeDataService]", "Campaign not found");
					}
					else if (_data[0].deals.length === 0) {
						console.error("[HomeDataService]", "Campaign without offers");
					}
					else {
						campaign = {
							title: _data[0].title,
							offers: _data[0].deals,
							/*deals: _.filter(_data[0].deals, function(deal) {
								return (deal.tags.indexOf('destaca-home-vuelos') > -1);
							}),*/
							regions: [{
								id: 0,
								name: 'Todas las regiones',
								checked: true
							}]
						};

						_.sortBy(_data[0].regions, 'name').forEach(function(region) {
							region.checked = false;
							campaign.regions.push(region);
						});

					}
					return campaign;
				}
				/*
				parseResults: function(_data) {
					_data = _data;
					var hotels = [];
					var obj = {
						image: 'http://images.travelnow.com/hotels/2000000/1200000/1194600/1194581/1194581_187_b.jpg',
						name: 'Vista Sol Buenos Aires',
						country: 'AR',
						destination: 'Buenos Aires',
						promo: 10,
						rating: 4,
						prices: {
							minFamiliar: 43
						}
					};
					hotels.push(obj);
					obj = {
						image: 'http://images.travelnow.com/hotels/5000000/4320000/4317400/4317341/4317341_66_b.jpg',
						name: 'Galerias Hotel ',
						country: 'AR',
						destination: 'Buenos Aires',
						promo: 43,
						rating: 4,
						prices: {
							minFamiliar: 48
						}
					};
					hotels.push(obj);
					obj = {
						image: 'http://images.travelnow.com/hotels/6000000/5950000/5943500/5943425/5943425_9_b.jpg',
						name: 'BA Stop Hostel',
						country: 'AR',
						destination: 'Buenos Aires',
						promo: 10,
						rating: 2,
						prices: {
							minFamiliar: 24
						}
					};
					hotels.push(obj);
					obj = {
						image: 'http://images.travelnow.com/hotels/1000000/670000/662600/662591/662591_247_b.jpg',
						name: 'Loi Suites Recoleta Hotel',
						country: 'AR',
						destination: 'Buenos Aires',
						promo: 10,
						rating: 4,
						prices: {
							minFamiliar: 156
						}
					};
					hotels.push(obj);
					obj = {
						image: 'http://images.travelnow.com/hotels/1000000/30000/28300/28280/28280_77_b.jpg',
						name: 'Aspen Towers Hotel',
						country: 'AR',
						destination: 'Buenos Aires',
						promo: 30,
						rating: 4,
						prices: {
							minFamiliar: 57
						}
					};
					hotels.push(obj);
					obj = {
						image: 'http://images.travelnow.com/hotels/2000000/1440000/1437500/1437487/1437487_46_b.jpg',
						name: 'Argentina Tango Hotel',
						country: 'AR',
						destination: 'Buenos Aires',
						promo: 10,
						rating: 5,
						prices: {
							minFamiliar: 30
						}
					};
					hotels.push(obj);
					obj = {
						image: 'http://images.travelnow.com/hotels/1000000/10000/4400/4362/4362_102_b.jpg',
						name: 'El Conquistador Hotel ',
						country: 'AR',
						destination: 'Buenos Aires',
						promo: 10,
						rating: 4.5,
						prices: {
							minFamiliar: 54
						}
					};
					hotels.push(obj);

					var response = {
						groups: hotels,
						filters: []
					};

					return response;
				}*/
			};
		});
});
