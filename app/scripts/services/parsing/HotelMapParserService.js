define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.services.HotelMapParserService', [])
		.factory('HotelMapParserService', function() {

			return {
				getSearchRequestData: function(_data) {
					var parsed = '';
					parsed += _data.latitude;
					parsed += '/' + _data.longitude;
					return parsed;
				}
			};

		});
});
