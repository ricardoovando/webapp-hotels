define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.services.MyBookingParserService', [])
		.factory('MyBookingParserService', function(AppCommonService, B2BDataService) {

			function isValidParameter(_collection, _key) {
				if (!_.isUndefined(_collection[_key]) && !_.isNull(_collection[_key]) && _collection[_key] !== '') {
					return true;
				}
				else {
					return false;
				}
			}

			var parameters = {};
			parameters.searchBooking = {
				mandatoryFields: [],
				optionalFields: ['bookingNumber', 'businessNumber', 'spnr', 'surname', 'email', 'hotelName', 'bookDateIni', 'bookDateEnd', 'checkInIni', 'checkInEnd', 'statusBooking']
			};
			parameters.bookingDeepLink = {
				mandatoryFields: ['spnr'],
				optionalFields: []
			};
			parameters.bookingDetails = {
				mandatoryFields: ['supplier', 'spnr'],
				optionalFields: []
			};
			parameters.sendVoucher = {
				mandatoryFields: ['supplier', 'spnr'],
				optionalFields: []
			};
			parameters.getVoucherUrl = {
				mandatoryFields: ['supplier', 'spnr'],
				optionalFields: ['disposition']
			};
			parameters.cancelBooking = {
				mandatoryFields: ['itineraryId', 'email', 'reason'],
				optionalFields: ['security']
			};

			function addSearchParameters(_data, _type) {
				//alert(JSON.stringify(_data));
				var params = {};
				_.forEach(parameters[_type].mandatoryFields, function(value, key) {
					if (isValidParameter(_data, parameters[_type].mandatoryFields[key])) {
						params[parameters[_type].mandatoryFields[key]] = ((typeof(_data[parameters[_type].mandatoryFields[key]]) !== 'object') ? _data[parameters[_type].mandatoryFields[key]] : _data[parameters[_type].mandatoryFields[key]].code);
					}
				});
				var checkedParams = AppCommonService.serialize(params, true);
				var optional = {};
				_.forEach(parameters[_type].optionalFields, function(value, key) {
					if (isValidParameter(_data, parameters[_type].optionalFields[key])) {
						optional[parameters[_type].optionalFields[key]] = ((typeof(_data[parameters[_type].optionalFields[key]]) !== 'object') ? _data[parameters[_type].optionalFields[key]] : _data[parameters[_type].optionalFields[key]].code);
					}
				});

				if (B2BDataService !== null) {
					optional.channel = 'B2B';
				}
				else {
					optional.channel = 'B2C';
				}
				optional.country = 'CL';

				var optionalParams = AppCommonService.serialize(optional);
				if (checkedParams.length > 0 && checkedParams.indexOf('?') > -1) {
					optionalParams = optionalParams.replace("?", "&");
				}

				return (checkedParams + optionalParams);
			}

			return {
				getParameters: function(_data, _type) {
					return addSearchParameters(_data, _type);
				}
			};
		});
});
