define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.services.HotelDetailsParserService', [])
		.factory('HotelDetailsParserService', function($window, HotelSearchParserService, AppCommonService) {

			function addSort(_sort, optionalParams) {
				if (!_.isNull(_sort) && _sort !== '' && typeof _sort !== 'undefined') {
					optionalParams.sort = _sort;
				}
				return optionalParams;
			}

			function addFilters(_filters, optionalParams) {
				if (!_filters) {
					return optionalParams;
				}

				if (!_.isEmpty(_filters.price)) {
					optionalParams.minAveragePrice = _filters.price[0];
					optionalParams.maxAveragePrice = _filters.price[1];
				}

				if (_filters.hasOwnProperty('showLimited')) {
					_filters.showLimited = true;
				}

				var cancellation = [];
				_.forEach(_filters.cancellation, function(value) {
					if (value !== false) {
						cancellation.push(value);
					}
				});

				if (cancellation.length > 0) {
					optionalParams.cancellation = cancellation.join(',');
				}

				var supplier = [];
				_.forEach(_filters.supplier, function(value) {
					if (value !== false) {
						supplier.push(value);
					}
				});

				if (supplier.length > 0) {
					optionalParams.supplier = supplier.join(',');
				}

				var paymentType = [];
				_.forEach(_filters.paymentType, function(value) {
					if (value !== false) {
						paymentType.push(value);
					}
				});
				if (paymentType.length === 1) {
					optionalParams.paymentType = paymentType.join(',');
				}

				if (!_.isEmpty(_filters.offers) && (_filters.offers[0] !== 0 || _filters.offers[1] !== 100)) {
					optionalParams.minPromotion = _filters.offers[0];
					optionalParams.maxPromotion = _filters.offers[1];
				}

				if (_filters.corporateCode && _filters.corporateCode.toString().toLowerCase() !== '') {
					optionalParams.corporateCode = _filters.corporateCode;
				}

				if (_filters.reservationType) {
					optionalParams.paymentType = _filters.reservationType;
				}

				_.forEach(_filters.amenities, function(value) {
					if (value !== false) {
						optionalParams[value] = 'true';
					}
				});

				return optionalParams;
			}

			return {
				getDetailsRequestData: function(_data, _filters, _sort) {
					//put together query string
					var parameters = "";
					parameters += _data.hotel + '/';
					parameters += moment(_data.arrival).format("YYYY-MM-DD") + '/';
					parameters += moment(_data.departure).format("YYYY-MM-DD") + '/';
					var optionalParams = {};
					_.each(_data, function(value, key) {
						if (HotelSearchParserService.isValidRoom(key, _data)) {
							if (key === "room1") {
								parameters += _data[key] + "/";
							}
							else {
								optionalParams[key] = _data[key];
							}
						}
					});
					if (_data.corporateCode && _data.corporateCode.toString().toLowerCase() !== '') {
						optionalParams.corporateCode = _data.corporateCode;
					}

					if (_data.reservationType) {
						optionalParams.paymentType = _data.reservationType;
					}

					optionalParams = addFilters(_filters, optionalParams);
					optionalParams = addSort(_sort, optionalParams);
					//optionalParams.free = true;
					if ($window.APP_TYPE !== 1) {
						optionalParams.free = $window.APP_TYPE;
					}
					if ($window.QUOTE && $window.QUOTE !== '0') {
						optionalParams.quote = true;
					}
					return (parameters + AppCommonService.serialize(optionalParams));
				}

			};
		});
});
