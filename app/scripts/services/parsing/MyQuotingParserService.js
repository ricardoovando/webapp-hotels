define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.services.MyQuotingParserService', [])
		.factory('MyQuotingParserService', function($window, $stateParams, $filter, $location) {

			var _parsedData = {
				productType: "HOTEL",
				quote: 0,
				domain: $window.QUOTE_URL,
				hotel: {
					name: "",
					location: "",
					image: "",
					stars: "",
					checkin: "",
					checkout: "",
					nights: 0,
					includes: [{
						TYPE: "",
						free: true,
						description: ""
					}],
					hotelAmenities: [],
					supplier: "EAN",
					instructions: "",
					checkInTime: "4:00 PM",
					checkOutTime: "12:00 PM",
					rooms: [],
					tripAdvisor: '',
					tripAdvisorUrl: null,
					tripAdvisorReviewCount: "",
					occupancyPerRoom: 3,
					coordinates: "",
					roomDescription: "",
					bedTypes: [],
					refundable: false,
					recommended: false,
					roomImages: []
				},
				prices: {
					usd: {
						base: 0,
						taxes: 0,
						fee: 0,
						feeB2b: 0,
						total: 0,
						assistency: 0,
						transfer: 0,
						discount: 0
					},
					clp: {
						base: 0,
						taxes: 0,
						fee: 0,
						feeB2b: 0,
						total: 0,
						assistency: 0,
						transfer: 0,
						discount: 0
					}
				},
				additionals: {
					assistency: false,
					transfer: false
				},
				fareDescription: "",
				deepLink: ""

			};
			var search = $location.search();

			function parserData(quoteData, cb) {
				var toret = angular.copy(_parsedData);

				try {
					toret.quote = $window.QUOTE;
					toret.hotel.name = quoteData.room.request.hotelName;
					toret.hotel.location = quoteData.room.request.address;
					toret.hotel.image = quoteData.room.request.hotelImage;
					toret.hotel.stars = quoteData.room.request.starRating;
					toret.hotel.checkin = quoteData.room.request.arrival + 'T00:00:00-03:00'; // 2017-10-30T00:00:00-04:00
					toret.hotel.checkout = quoteData.room.request.departure + 'T00:00:00-03:00'; // 2017-10-30T00:00:00-04:00
					toret.hotel.nights = get_nigths(quoteData.room.request.arrival + 'T00:00:00-03:00', quoteData.room.request.departure + 'T00:00:00-03:00');
					toret.hotel.includes = get_includes(quoteData.room.amenities);
					toret.hotel.hotelAmenities = get_includes(quoteData.room.amenities);
					toret.hotel.supplier = quoteData.room.supplierCode;
					toret.hotel.occupancyPerRoom = quoteData.room.rateOccupancyPerRoom;
					toret.hotel.roomDescription = quoteData.room.roomDescription;
					toret.hotel.refundable = quoteData.room.refundable;
					toret.hotel.tripAdvisorUrl = quoteData.room.tripAdvisorUrl || null;
					toret.hotel.coordinates = [quoteData.hotel.latitude, quoteData.hotel.longitude].join(',');
					//toret.deepLink = generateDeeplink(quoteData.room.request);
					toret.hotel.roomImages = quoteData.room.images;
					toret.hotel.rooms = getRooms(search);
					toret.destination_regionName = quoteData.regionName;

					if (quoteData.hotel.summary && quoteData.hotel.summary.name_region) {
						toret.destination_cityName = quoteData.hotel.summary.name_region;
					}
					else if (quoteData.hotel.city) {
						toret.destination_cityName = quoteData.hotel.city;
					}
					else {
						toret.destination_cityName = _.head(quoteData.regionName.split(','));
					}
					toret.destination_countryName = quoteData.hotel.country;
					toret.mealPlan = getMealPlan(quoteData.room.amenities);

					var usd = 0;
					var clp = 0;
					if (quoteData.room.supplierCode === 'BKG') {
						usd = $filter('moneyFilter')(quoteData.room.netPrice);
						clp = $filter('USDtoCLPFilter')(quoteData.room.netPrice, quoteData.room.exchangeRate);
					}
					else {
						if (quoteData.room.hasOwnProperty('netNightlyPrice') && quoteData.room.netNightlyPrice != null && quoteData.room.netNightlyPrice > 0) {
							usd = $filter('moneyFilter')(quoteData.room.netNightlyPrice);
							clp = $filter('USDtoCLPFilter')(quoteData.room.netNightlyPrice, quoteData.room.exchangeRate);
						}
						else {
							usd = $filter('moneyFilter')(quoteData.room.price);
							clp = $filter('USDtoCLPFilter')(quoteData.room.price, quoteData.room.exchangeRate);
						}
					}

					toret.prices.usd.base = usd;
					toret.prices.clp.base = clp;

					cb(null, toret);
				}
				catch (err) {
					cb(new Error('[QuoteService][parserData][Error] al parsear precios:' + err.message + ' toret:' + JSON.stringify(toret)));
				}

			}

			function get_nigths(arrival, departure) {
				if (!arrival || !departure) {
					return null;
				}
				arrival = moment(arrival, 'YYYY-MM-DD HH:mm');
				departure = moment(departure, 'YYYY-MM-DD HH:mm');
				var diferencias_noches = departure.diff(arrival, 'days');
				return diferencias_noches;
			}

			function get_includes(amenities, hotelAmenities) {
				if (!hotelAmenities) {
					hotelAmenities = false;
				}
				if (!amenities) {
					return null;
				}
				var includes = [];
				_.each(amenities, function(item) {
					if (item.included) {
						includes.push({
							TYPE: item.name,
							free: item.included,
							description: item.name
						});
					}
					else if (hotelAmenities) {
						includes.push({
							TYPE: item.name,
							free: item.included,
							description: item.name
						});
					}
				});
				return includes;
			}

			function getRooms(searchData) {
				var rooms = [
					searchData.room1,
					searchData.room2 || null,
					searchData.room3 || null,
					searchData.room4 || null,
					searchData.room5 || null,
					searchData.room6 || null,
					searchData.room7 || null,
					searchData.room8 || null
				];
				var roomParams = [];
				for (var i = 0; i < rooms.length; i++) {
					if (rooms[i]) {
						var aux = rooms[i].split(',');
						var adults = aux.shift();
						var ages = aux.join(',');
						var roomdata = {
							adults: parseInt(adults),
							children: (aux.length),
							ages: ages || null
						};
						roomParams.push(roomdata);
					}
				}
				return roomParams;
			}

			function getMealPlan(amenities) {
				var mealPlan = '';
				var plan = _.filter(amenities, {
					name: 'BREAKFAST'
				});
				if (plan.length > 0 && plan[0].text !== null) {
					mealPlan = plan[0].text;
				}
				return mealPlan;
			}

			return {
				parserData: function(_quoteData, _cb) {
					parserData(_quoteData, _cb);
				},
				getQuoteHotelRequestUrl: function() {
					return $window.HOST_HOTELS_SERVICES_NOCACHE + 'quote/';
				},
				getParams: function(_supplierCode, _roomid, _rateCode) {
					var rateCode = 0;
					if (_rateCode) {
						rateCode = encodeURIComponent(_rateCode);
					}
					return ':hotelID/:arrival/:departure/:roomID/:supplier/:rateCode/:quote'
						.replace(':hotelID', $stateParams.hotel || arguments[3])
						.replace(':arrival', $stateParams.arrival)
						.replace(':departure', $stateParams.departure)
						.replace(':roomID', encodeURIComponent(_roomid) || 0)
						.replace(':supplier', _supplierCode)
						.replace(':rateCode', rateCode)
						.replace(':quote', $window.QUOTE || 0);
				}
			};
		});
});
