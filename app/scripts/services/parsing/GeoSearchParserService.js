define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.services.GeoSearchParserService', [])
		.factory('GeoSearchParserService', function(B2BDataService) {

			return {
				getSearchRequestData: function(_code) {
					var out = encodeURIComponent(_code);
					out += "?channel=";
					out += (B2BDataService !== null) ? 'B2B' : 'B2C';
					out += "&country=CL";
					return out;
				},
				getRoomsData: function(stateData) {
					var roomCount = 0;
					var adultCount = 0;
					var childrenCount = 0;
					var childrenAges = [];
					_.forEach(stateData, function(value, key) {
						if (key.indexOf('room') > -1) {
							roomCount++;
							var roomData = ((typeof(stateData[key]) === 'number') ? new Array('' + stateData[key]) : stateData[key].split(','));
							adultCount += parseInt(roomData[0], 10);
							childrenCount += (roomData.length - 1);
							for (var i = 1; i < roomData.length; i++) {
								childrenAges.push(roomData[i]);
							}
						}
					});
					return {
						rooms: roomCount,
						adults: adultCount,
						children: childrenCount,
						ages: childrenAges
					};
				},
				getUrlBooking: function(stateData, place, isMobile) {
					var url = 'http://hoteles.cocha.com/searchresults.html?';
					var aid = '392610';
					if (isMobile === true) {
						aid = '397451';
					}
					var roomsData = this.getRoomsData(stateData);
					url += 'aid=' + aid;
					url += '&from_autocomplete=1';
					url += '&lang=es';
					url += '&si=ai%2Cco%2Cci%2Cre%2Cdi';
					url += '&checkin_monthday=' + moment(stateData.arrival).format("D");
					url += '&checkin_year_month=' + moment(stateData.arrival).format("YYYY") + '-' + moment(stateData.arrival).format("M");
					url += '&checkout_monthday=' + moment(stateData.departure).format("D");
					url += '&checkout_year_month=' + moment(stateData.departure).format("YYYY") + '-' + moment(stateData.departure).format("M");
					url += '&interval_of_time=any';
					url += '&flex_checkin_year_month=any';
					url += '&no_rooms=' + roomsData.rooms; //nr rooms
					url += '&group_adults=' + roomsData.adults; //adults
					url += '&group_children=' + roomsData.children;
					url += ((roomsData.children > 0) ? '&age=' + roomsData.ages.join('&age=') : '');
					url += '&dest_type=' + place.dest_type;
					url += '&dest_id=' + place.dest_id;
					return url;
				}
			};
		});
});
