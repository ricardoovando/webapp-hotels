define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.services.HotelSearchParserService', [])
		.factory('HotelSearchParserService', function(AppCommonService, $window) {

			function addSort(_sort, optionalParams) {
				if (_.trim(_sort) !== '') {
					optionalParams.sortBy = _sort;
				}
				else {
					optionalParams.sortBy = 'default';
				}
				return optionalParams;
			}

			function addFilters(_filters, optionalParams) {
				if (!(_.isEmpty(_filters))) {
					if (!_.isEmpty(_filters.price)) {
						if (_filters.price.min !== 0 || _filters.price.max !== 1000) {
							optionalParams.minAveragePrice = _filters.price.min;
							optionalParams.maxAveragePrice = _filters.price.max;
						}
					}
					if (!_.isEmpty(_filters.ratings)) {
						if (_filters.ratings.min !== 0 || _filters.ratings.max !== 5) {
							optionalParams.starsRange = _filters.ratings.min + "," + _filters.ratings.max;
						}
					}
					if (!_.isEmpty(_filters.tripAdvisor)) {
						if (_filters.tripAdvisor.min !== 0 || _filters.tripAdvisor.max !== 5) {
							optionalParams.minRating = _filters.tripAdvisor.min;
							optionalParams.maxRating = _filters.tripAdvisor.max;
						}
					}
					if (!_.isEmpty(_filters.cancellation)) {
						var cancellations = [];
						_.forEach(_filters.cancellation, function(value, key) {
							if (value !== false) {
								cancellations.push(key);
							}
						});
						optionalParams.cancellation = cancellations.join(',');
					}
					if (!_.isEmpty(_filters.category)) {
						var categories = [];
						_.forEach(_filters.category, function(value, key) {
							if (value !== false) {
								categories.push(key);
							}
						});
						optionalParams.category = categories.join(',');
					}
					if (!_.isEmpty(_filters.amenities)) {
						var amenities = [];
						_.forEach(_filters.amenities, function(value, key) {
							if (value !== false) {
								amenities.push(key);
							}
						});
						optionalParams.amenities = amenities.join(',');
					}
					if (!_.isEmpty(_filters.foodPlan)) {
						var foodPlan = [];
						_.forEach(_filters.foodPlan, function(value, key) {
							if (value !== false) {
								foodPlan.push(key);
							}
						});
						var foodPlanAmenities = foodPlan.join(',');
						if (optionalParams.amenities && foodPlanAmenities) {
							optionalParams.amenities = optionalParams.amenities + ',' + foodPlanAmenities;
						}
						else {
							optionalParams.amenities = foodPlanAmenities;
						}
					}
					if (!_.isEmpty(_filters.paymentType)) {
						var paymentTypes = [];
						_.forEach(_filters.paymentType, function(value, key) {
							if (value !== false) {
								paymentTypes.push(key);
							}
						});
						if (paymentTypes.length !== 2) {
							optionalParams.paymentType = paymentTypes.join(',');
						}
					}
					if (!_.isEmpty(_filters.supplier)) {
						var suppliers = [];
						_.forEach(_filters.supplier, function(value, key) {
							if (value !== false) {
								suppliers.push(key);
							}
						});
						optionalParams.supplier = suppliers.join(',');
					}
					if (typeof _filters.preferredHotelId !== 'undefined' && !_.isNull(_filters.preferredHotelId) && _filters.preferredHotelId !== '') {
						optionalParams.preferredHotelId = _filters.preferredHotelId;
					}

					if (typeof _filters.corporateCode !== 'undefined' && !_.isNull(_filters.corporateCode) && _filters.corporateCode !== '') {
						optionalParams.corporateCode = _filters.corporateCode;
					}

					if (typeof _filters.reservationType !== 'undefined' && !_.isNull(_filters.reservationType) && _filters.reservationType !== '') {
						optionalParams.reservationType = _filters.reservationType;
					}

					if (_.isNumber(_filters.start)) {
						optionalParams.start = _filters.start;
					}
					else {
						optionalParams.start = 0;
					}
					if (_.isNumber(_filters.limit)) {
						optionalParams.limit = _filters.limit;
					}
					else {
						optionalParams.limit = 20;
					}
				}
				//return filters;
				return optionalParams;
			}

			return {
				isValidRoom: function(key, collection) {
					if (key.indexOf("room") > -1 && typeof collection[key] !== 'undefined' && collection[key] !== null) {
						return true;
					}
					else {
						return false;
					}
				},
				countRooms: function(searchData) {
					var count = 0;
					var self = this;
					_.forEach(searchData, function(value, key) {
						if (self.isValidRoom(key, searchData)) {
							count++;
						}
					});
					return count;
				},
				countPassengers: function(searchData) {
					var count = 0;
					var arrRoom = [];
					var self = this;
					_.forEach(searchData, function(value, key) {
						if (self.isValidRoom(key, searchData)) {
							arrRoom = searchData[key].toString().split(',');
							count += (parseInt(arrRoom[0], 10) + arrRoom.length - 1);
						}
					});
					return count;
				},
				getPlaceRequestData: function(_code) {
					var parameters = "";
					parameters += _code;
					return parameters;
				},
				getSearchRequestData: function(_data, _filters, _sort) {
					//put together query string
					//var parameters = "?";
					var parameters = "";
					if (_data.hasOwnProperty('places')) {
						//parameters += "regionId=" + _data.places.destination.id;
						parameters += _data.places.destination.id + '/';
					}

					parameters += moment(_data.dates.arrival).format("YYYY-MM-DD") + '/';
					parameters += moment(_data.dates.departure).format("YYYY-MM-DD") + '/';

					var optionalParams = {};

					_.each(_data.rooms, function(value, key) {
						if (key === 0) {
							parameters += _data.rooms[key].adults;
							if (_data.rooms[key].children > 0) {
								_.each(_data.rooms[key].ages, function(value2, key2) {
									parameters += "," + _data.rooms[key].ages[key2];
								});
							}
							parameters += '/';
						}
						else {
							optionalParams['room' + (key + 1)] = _data.rooms[key].adults;
							if (_data.rooms[key].children > 0) {
								_.each(_data.rooms[key].ages, function(value2, key2) {
									optionalParams['room' + (key + 1)] += "," + _data.rooms[key].ages[key2];
								});
							}
						}
					});

					if (_data.hasOwnProperty('preferredHotelId') && !_.isNull(_data.preferredHotelId) && _data.preferredHotelId !== '' && typeof _data.preferredHotelId !== 'undefined') {
						optionalParams.preferredHotelId = _data.preferredHotelId;
					}

					optionalParams = addFilters(_filters, optionalParams);

					if ($window.APP_TYPE !== 1) {
						optionalParams.free = $window.APP_TYPE;
					}
					if ($window.QUOTE && $window.QUOTE !== '0') {
						optionalParams.quote = true;
					}

					if (typeof _data.corporateCode !== 'undefined' && !_.isNull(_data.corporateCode) && _data.corporateCode !== '') {
						optionalParams.corporateCode = _data.corporateCode;
					}

					if (_data.reservationType) {
						optionalParams.paymentType = _data.reservationType;
					}

					if (!_filters.hasOwnProperty('preferredHotelId') || _.isNull(_filters.preferredHotelId)) {
						optionalParams = addSort(_sort, optionalParams);
					}

					parameters += AppCommonService.serialize(optionalParams);

					return parameters;
				}
			};
		});
});
