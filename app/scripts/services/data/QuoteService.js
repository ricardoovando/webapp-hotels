define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.services.QuoteService', [])
		.factory('QuoteService', function(cAccessRemote, MyQuotingParserService) {

			return {
				sendQuote: function(quoteData, _callbackOK, _callbacksError, _callbackDone) {
					MyQuotingParserService.parserData(quoteData, function(err, parsedData) {
						if (err) {
							console.error(err.message);
							_callbackDone();
							if (err.code === '-1') {
								_callbacksError.ERROR_AVAILABILITY();
							}
							else {
								_callbacksError.ERROR_INTERNAL();
							}
						}
						else {
							var url = MyQuotingParserService.getQuoteHotelRequestUrl();

							var params = MyQuotingParserService.getParams(parsedData.hotel.supplier, quoteData.room.roomId, quoteData.room.rateCode, quoteData.room.request.hotel || 0);

							cAccessRemote.executeHTTPPost(url + params, parsedData, _callbackOK, function() {
								_callbacksError.ERROR_INTERNAL();
								_callbackDone();
							});
						}
					});
				}
			};
		});

});
