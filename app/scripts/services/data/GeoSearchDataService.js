define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.services.GeoSearchDataService', [])
		.factory('GeoSearchDataService', function($window, $state, cAccessRemote, GeoSearchParserService) {
			return {
				searchPlace: function(_code, _okCallback, _errorCallbacks) {
					var url = $window.END_POINT_GEO_SEARCH;
					var parameters = GeoSearchParserService.getSearchRequestData(_code);

					var onInternalError = function(error, _data) {
						_errorCallbacks(_data);
						return;
					};
					var onResponseOK = function(_data) {
						_okCallback(_data);
						return;
					};
					cAccessRemote.executeHTTPGet(url + parameters, onResponseOK, onInternalError);
				},
				getDeeplink: function(_stateData, _place, _isMobile) {
					return GeoSearchParserService.getUrlBooking(_stateData, _place, _isMobile);
				}
			};
		});
});
