define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.services.CurrencyService', [])
		.factory('CurrencyService', function($window, cAccessLocal, cAccessRemote) {

			var exchangeRate = '--';
			exchangeRate = getRate();
			var exchangeRetryCount = 0;

			function getRate(_callbackOK, _callbacksError) {
				if (exchangeRetryCount > 3) {
					if (typeof _callbacksError !== 'undefined') {
						_callbacksError.CURRENCY_ERROR();
						return '--';
					}
					return '--';
				}

				if (!isNaN(exchangeRate) && !_.isNull(exchangeRate)) {
					if (typeof _callbackOK !== 'undefined') {
						_callbackOK(exchangeRate);
					}
					else {
						return exchangeRate;
					}
				}
				else {
					console.info("[HotelsResultsDataService] get currency");
					var url = $window.END_POINT_CURRENCY;
					var parameters = getCurrencyParameters();
					console.info("[HotelSearchRegionDataService]", "Search region with data: ", parameters);
					var onResponseOK = function(_data) {
						if (isNaN(_data.exchangeAmount) || _.isNull(_data.exchangeAmount)) {
							exchangeRetryCount++;
							getRate(_callbackOK, _callbacksError);
						}
						else {
							exchangeRate = _data.exchangeAmount;
							exchangeRetryCount = 0;
						}

						if (typeof _callbackOK !== 'undefined') {
							_callbackOK(exchangeRate);
						}
						else {
							return exchangeRate;
						}
					};
					var onResponseError = function() {
						getRate(_callbackOK, _callbacksError);
					};
					cAccessRemote.executeHTTPGet(url + parameters, onResponseOK, onResponseError);
				}
				exchangeRetryCount++;
			}

			function getCurrencyParameters() {
				var params = "";
				params += "?channel=B2B";
				params += "&country=CL";
				return params;
			}

			return {
				getExchangeRate: function(_callbackOK, _callbacksError) {
					if (typeof _callbackOK !== 'undefined') {
						getRate(_callbackOK, _callbacksError);
					}
					else {
						return getRate();
					}
				},
				setExchangeRate: function(_exchangeRate) {
					exchangeRate = _exchangeRate;
					return exchangeRate;
				}
			};
		});

});
