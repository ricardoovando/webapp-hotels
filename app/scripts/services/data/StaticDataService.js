define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.services.StaticDataService', [])
		.factory('StaticDataService', function($window) {
			var categoryData = {
				title: {
					luxury: 'lujo',
					boutique: 'boutique',
					smart: 'económico',
					corporate: 'ejecutivo',
					style: 'vanguardia'
				},
				icon: {
					luxury: 'cch-luxury',
					boutique: 'cch-boutique',
					smart: 'cch-economic',
					corporate: 'cch-executive',
					style: 'cch-vanguard'
				},
				tooltip: {
					luxury: 'Elegancia, sobriedad y glamour, con un servicio de excelencia y lleno de detalles',
					boutique: 'Personalidad única con tamaño reducido, y acento en la decoración de los ambientes.',
					smart: 'La mejor combinación precio/calidad, en lugares estratégicos con alta conectividad',
					corporate: 'Excelente combinación de precio, comodidad, ubicación y tecnología',
					style: 'No siguen la moda, sino que la imponen; decoración y espacios que dan que hablar'
				}
			};

			function getTextOption(selected, values) {
				for (var i = 0; i < values.length; i++) {
					if (selected === values[i].code) {
						return values[i].name;
					}
				}
				return "";
			}

			return {
				DEFAULT_TITLE: 'Reserva tus Vuelos Baratos en Cocha',
				DEFAULT_DESCRIPTION: 'Encuentra ofertas de vuelos al mejor precio. Compara entre nuestra amplia oferta de vuelos baratos para tu conveniencia y reserva vuelos online.',
				DEFAULT_KEYWORDS: 'vuelos, vuelos baratos, pasajes baratos, viajes, agencias de viajes, viajes a europa, agencias de turismo, paquetes turisticos',
				getMetadata: function(_stateName) {
					_stateName = _stateName;
					var metadata = {
						title: this.DEFAULT_TITLE,
						description: this.DEFAULT_DESCRIPTION,
						keywords: this.DEFAULT_KEYWORDS
					};
					return metadata;
				},
				getBookingStatusOptions: function() {
					return [{
						code: 'AC',
						name: 'Reserva Activa',
						short: 'Reserva Activa'
					}, {
						code: 'CO',
						name: 'Reserva Consumida',
						short: 'Reserva Consumida'
					}, {
						code: 'DE',
						name: 'Reserva Cancelada | En análisis de devolución',
						//short: 'Reserva Canc. | En análisis dev.'
						short: 'Canc. | En análisis dev.'
					}, {
						code: 'NO',
						name: 'Reserva Cancelada | Sin devolución',
						//short: 'Reserva Canc. | Sin dev.'
						short: 'Canc. | Sin dev.'
					}, {
						code: 'XS',
						name: 'Reserva Cancelada | Devolución procesada',
						//short: 'Reserva Canc. | Dev. procesada'
						short: 'Canc. | Dev. procesada'
					}];
				},
				getMonthsOptions: function() {
					return [{
						code: '01',
						name: 'Enero',
					}, {
						code: '02',
						name: 'Febrero',
					}, {
						code: '03',
						name: 'Marzo',
					}, {
						code: '04',
						name: 'Abril',
					}, {
						code: '05',
						name: 'Mayo',
					}, {
						code: '06',
						name: 'Junio',
					}, {
						code: '07',
						name: 'Julio',
					}, {
						code: '08',
						name: 'Agosto',
					}, {
						code: '09',
						name: 'Septiembre',
					}, {
						code: '10',
						name: 'Octubre',
					}, {
						code: '11',
						name: 'Noviembre',
					}, {
						code: '12',
						name: 'Diciembre',
					}];
				},
				getPaymentsOptionB2B: function() {
					return [{
						code: 'VN',
						name: 'Sin Cuotas',
						range: []
					}, {
						code: 'SI',
						name: '3 Cuotas sin interés',
						range: []
					}, {
						code: 'VC',
						name: 'Cuotas con interés',
						range: [4, 24]
					}];
				},
				getAmenitiesHotelValues: function() {
					return [{
						code: 'WIFI',
						name: 'Wifi Gratis'
					}, {
						code: 'PARKING',
						name: 'Estacionamiento'
					}, {
						code: 'AIR',
						name: 'Aire Acondicionado'
					}, {
						code: 'POOL',
						name: 'Piscina'
					}, {
						code: 'GYM',
						name: 'Gimnasio'
					}, {
						code: 'SPA',
						name: 'Servicios SPA'
					}, {
						code: 'BATH',
						name: 'Bañera de Hidromasaje'
					}];
				},
				getFoodPlanValues: function() {
					return [{
						code: 'ALL-INCLUSIVE',
						name: 'Todo Incluido'
					}, {
						code: 'BREAKFAST',
						name: 'Desayuno'
					}];
				},
				getHotelAmenitiesTextSelected: function(arrSelected) {
					var defaultTexts = this.getHotelDetailsSelectTextOptions();
					var dataTexts = this.getAmenitiesHotelValues();
					if (_.isEmpty(arrSelected)) {
						return defaultTexts.amenities;
					}
					var strSelected = [];
					var count = 0;
					_.forEach(arrSelected, function(value) {
						if (typeof value === 'string') {
							strSelected.push(getTextOption(value, dataTexts));
						}
						count++;
					});
					if (strSelected.length > 0) {
						/*
						var text = strSelected.join(",");
						if (text.length > 20) {
							return 'Multiples Valores Seleccionados';
						}
						else {
							return text;
						}*/
						if (strSelected.length === dataTexts.length) {
							return defaultTexts.amenities;
						}
						else if (strSelected.length === 1) {
							return strSelected[0];
						}
						else {
							return 'Multiples selecciones';
						}
					}
					else {
						return defaultTexts.amenities;
					}
				},
				getFoodPlanTextSelected: function(arrSelected) {
					var defaultTexts = this.getHotelDetailsSelectTextOptions();
					var dataTexts = this.getFoodPlanValues();
					if (_.isEmpty(arrSelected)) {
						return defaultTexts.foodPlan;
					}
					var strSelected = [];
					var count = 0;
					_.forEach(arrSelected, function(value) {
						if (typeof value === 'string') {
							strSelected.push(getTextOption(value, dataTexts));
						}
						count++;
					});
					if (strSelected.length > 0) {
						/*
						var text = strSelected.join(",");
						if (text.length > 20) {
							return 'Multiples Valores Seleccionados';
						}
						else {
							return text;
						}*/
						if (strSelected.length === dataTexts.length) {
							return defaultTexts.foodPlan;
						}
						else if (strSelected.length === 1) {
							return strSelected[0];
						}
						else {
							return 'Multiples selecciones';
						}
					}
					else {
						return defaultTexts.foodPlan;
					}
				},
				getCancellationTextSelected: function(arrSelected) {
					var defaultTexts = this.getHotelDetailsSelectTextOptions();
					var dataTexts = this.getCancellationValues();
					if (_.isEmpty(arrSelected)) {
						return defaultTexts.cancellation;
					}
					var strSelected = [];
					var count = 0;
					_.forEach(arrSelected, function(value) {
						if (typeof value === 'string') {
							strSelected.push(getTextOption(value, dataTexts));
						}
						count++;
					});
					if (strSelected.length > 0) {
						/*
						var text = strSelected.join(",");
						if (text.length > 20) {
							return 'Multiples Valores Seleccionados';
						}
						else {
							return text;
						}*/
						if (strSelected.length === dataTexts.length) {
							return defaultTexts.cancellation;
						}
						else if (strSelected.length === 1) {
							return strSelected[0];
						}
						else {
							return 'Multiples selecciones';
						}
					}
					else {
						return defaultTexts.cancellation;
					}
				},
				getCategoryTextSelected: function(arrSelected) {
					var defaultTexts = this.getHotelDetailsSelectTextOptions();
					var dataTexts = this.getCategoryValues();
					if (_.isEmpty(arrSelected)) {
						return defaultTexts.category;
					}
					var strSelected = [];
					var count = 0;
					_.forEach(arrSelected, function(value) {
						if (typeof value === 'string') {
							strSelected.push(getTextOption(value, dataTexts));
						}
						count++;
					});
					if (strSelected.length > 0) {
						/*
						var text = strSelected.join(",");
						if (text.length > 20) {
							return 'Multiples Valores Seleccionados';
						}
						else {
							return text;
						}*/
						if (strSelected.length === dataTexts.length) {
							return defaultTexts.category;
						}
						else if (strSelected.length === 1) {
							return strSelected[0];
						}
						else {
							return 'Multiples selecciones';
						}
					}
					else {
						return defaultTexts.category;
					}
				},
				getPaymentTextSelected: function(arrSelected) {
					var defaultTexts = this.getHotelDetailsSelectTextOptions();
					var dataTexts = this.getPaymentTypeValues();
					if (_.isEmpty(arrSelected)) {
						return defaultTexts.payment;
					}
					var strSelected = [];
					//alert(JSON.stringify(arrSelected));
					var count = 0;
					_.forEach(arrSelected, function(value) {
						if (typeof value === 'string') {
							strSelected.push(getTextOption(value, dataTexts));
						}
						count++;
					});
					/*
					for (var i = 0; i < arrSelected.length; i++) {
						if (typeof arrSelected[i] === 'string') {
							strSelected.push(getTextOption(arrSelected[i], dataTexts));
						}
					}
					*/
					if (strSelected.length > 0) {
						/*
						var text = strSelected.join(",");
						if (text.length > 20) {
							return 'Multiples Valores Seleccionados';
						}
						else {
							return text;
						}
						*/
						if (strSelected.length === dataTexts.length) {
							return defaultTexts.payment;
						}
						else if (strSelected.length === 1) {
							return strSelected[0];
						}
						else {
							return defaultTexts.payment;
						}
					}
					else {
						return defaultTexts.payment;
					}
				},
				getSupplierTextSelected: function(arrSelected) {
					var defaultTexts = this.getHotelDetailsSelectTextOptions();
					var dataTexts = this.getSupplierValues();
					if (_.isEmpty(arrSelected)) {
						return defaultTexts.supplier;
					}
					var strSelected = [];
					var count = 0;
					_.forEach(arrSelected, function(value) {
						if (typeof value === 'string') {
							strSelected.push(getTextOption(value, dataTexts));
						}
						count++;
					});
					if (strSelected.length > 0) {
						/*
						var text = strSelected.join(",");
						if (text.length > 20) {
							return 'Multiples Valores Seleccionados';
						}
						else {
							return text;
						}*/
						if (strSelected.length === dataTexts.length) {
							return defaultTexts.supplier;
						}
						else if (strSelected.length === 1) {
							return strSelected[0];
						}
						else {
							return 'Multiples selecciones';
						}
					}
					else {
						return defaultTexts.supplier;
					}
				},
				getAmenitiesTextSelected: function(arrSelected) {
					var defaultTexts = this.getHotelDetailsSelectTextOptions();
					var dataTexts = this.getAmenitiesValues();
					if (_.isEmpty(arrSelected)) {
						return defaultTexts.amenities;
					}
					var strSelected = [];
					var count = 0;
					_.forEach(arrSelected, function(value) {
						if (typeof value === 'string') {
							strSelected.push(getTextOption(value, dataTexts));
						}
						count++;
					});
					if (strSelected.length > 0) {
						/*var text = strSelected.join(",");
						if (text.length > 20) {
							return 'Multiples Valores Seleccionados';
						}
						else {
							return text;
						}*/
						if (strSelected.length === dataTexts.length) {
							return defaultTexts.amenities;
						}
						else if (strSelected.length === 1) {
							return strSelected[0];
						}
						else {
							return 'Multiples selecciones';
						}
					}
					else {
						return defaultTexts.amenities;
					}
				},
				getPriceValues: function() {
					return [0, 1000];
				},

				getOfferValues: function() {
					return [0, 100];
				},

				getAmenitiesValues: function() {
					return [{
						code: 'breakfast',
						name: 'Desayuno'
					}, {
						code: 'internet',
						name: 'Internet'
					}, {
						code: 'parking',
						name: 'Estacionamiento'
					}];
				},

				getCancellationValues: function() {
					return [{
						code: 'refundable',
						name: 'Reembolsable'
					}, {
						code: 'nonrefundable',
						name: 'No reembolsable'
					}];
					/*
                    , {
                        code: 'none',
                        name: 'Sin política de cancelación'
                    }
					 */
				},

				getCategoryValues: function() {
					/*
					return [{
						code: 'Selected Partners',
						name: 'Preferred Partners'
					}];
					*/
					return [{
						code: 'Selected Partners',
						name: 'Cocha Preferred Partners'
					}, {
						code: 'Recomendado-Luxury',
						name: 'Lujo'
					}, {
						code: 'Recomendado-Style',
						name: 'Recomendado'
					}];
					/*}, {
						code: 'Recomendado-Boutique',
						name: 'Boutique'
					}, {
						code: 'Recomendado-Corporate',
						name: 'Ejecutivo'
					}, {
						code: 'Recomendado-Smart',
						name: 'Económico'*/
				},
				getSupplierValues: function() {
					var supplierOptions = [{
						code: 'EAN',
						name: 'Expedia Affiliate Network'
					}, {
						code: 'BKG',
						name: 'Booking'
					}, {
						code: 'SAB',
						name: 'Sabre'
					}, {
						code: 'EANPKGR',
						name: 'Expedia Package Rate'
					}];
					var appType = $window.APP_TYPE;
					if (appType === 2) {
						supplierOptions.push({
							code: 'TRV',
							name: 'Kuality'
						});
						/*
						supplierOptions.push({
							code: 'EANPKGR',
							name: 'Expedia Package Rate'
						});*/
					}
					else if (appType === 3) {
						supplierOptions.push({
							code: 'TRV',
							name: 'Kuality'
						});
					}
					return supplierOptions;
				},

				getPaymentTypeValues: function() {
					return [{
						code: 'prepaid',
						name: 'Paga en Cocha'
					}, {
						code: 'inhotel',
						name: 'Paga al hotel'
					}];
				},

				getCabinOptions: function() {
					return [{
						code: 'Y',
						name: 'Turista'
					}, {
						code: 'S',
						name: 'Premium Economy'
					}, {
						code: 'C',
						name: 'Ejecutiva'
					}, {
						code: 'F',
						name: 'Primera'
					}];
				},
				getCabinResultsOptions: function() {
					return {
						'COACH': 'Turista',
						'PREMIUM_COACH': 'Premium Economy',
						'BUSINESS': 'Ejecutiva',
						'FIRST': 'Primera'
					};
				},
				getHotelDetailsFilters: function(_type) {
					var filters = {};
					filters.price = this.getPriceValues();
					filters.offers = this.getOfferValues();
					filters.cancellation = this.getCancellationValues();
					filters.supplier = this.getSupplierValues();
					filters.paymentType = this.getPaymentTypeValues();
					if (_type && _type === 'hotel') {
						filters.amenities = this.getAmenitiesHotelValues();
					}
					else {
						filters.amenities = this.getAmenitiesValues();
					}
					filters.category = this.getCategoryValues();
					filters.foodPlan = this.getFoodPlanValues();
					return filters;
				},
				getHotelDetailsSort: function() {
					return [{
						code: 'default',
						name: 'Proveedor'
					}, {
						code: 'price',
						name: 'Precio (Menor a Mayor)'
					}, {
						code: '-price',
						name: 'Precio (Mayor a Menor)'
					}, {
						code: 'promotion',
						name: 'Ofertas (Menor a Mayor)'
					}, {
						code: '-promotion',
						name: 'Ofertas (Mayor a Menor)'
					}];
				},
				getHotelDetailsCollapseOptions: function() {
					return {
						cancellation: true,
						category: true,
						supplier: true,
						amenities: true,
						foodPlan: true,
						payment: true,
						sort: true,
						showRoomsOptions: true,
						tabservice: true,
						tabpay: true
					};
				},
				getHotelDetailsSelectTextOptions: function(defaultValues) {
					if (!defaultValues || _.isEmpty(defaultValues)) {
						return {
							cancellation: 'Seleccione política de cancelación',
							category: 'Seleccione Categoria',
							payment: 'Seleccione tipo de pago',
							amenities: 'Seleccione servicios del hotel',
							foodPlan: 'Seleccione plan de comida',
							supplier: 'Seleccione proveedor',
							sort: 'Proveedor'
						};
					}
					else {
						//alert(JSON.stringify(defaultValues));
						return {
							category: this.getCategoryTextSelected(defaultValues.category),
							cancellation: this.getCancellationTextSelected(defaultValues.cancellation),
							payment: this.getPaymentTextSelected(defaultValues.paymentType),
							supplier: this.getSupplierTextSelected(defaultValues.supplier),
							amenities: this.getAmenitiesTextSelected(defaultValues.amenities),
							foodPlan: this.getFoodPlanTextSelected(defaultValues.foodPlan),
							sort: 'Proveedor'
						};
					}
				},
				getTranslations: function() {
					return [{
						es: 'vuelos',
						en: 'flights'
					}, {
						es: 'pasajeros',
						en: 'passengers'
					}, {
						es: 'lugares',
						en: 'places'
					}, {
						es: 'origen',
						en: 'origin'
					}, {
						es: 'destino',
						en: 'destination'
					}, {
						es: 'fechas',
						en: 'dates'
					}, {
						es: 'salida',
						es2: 'ida',
						en: 'departures'
					}, {
						es: 'regreso',
						es2: 'vuelta',
						en: 'returnings'
					}, {
						es: 'salida',
						es2: 'ida',
						en: 'departure'
					}, {
						es: 'regreso',
						es2: 'vuelta',
						en: 'returning'
					}, {
						es: 'adulto',
						en: 'adult'
					}, {
						es: 'niño',
						en: 'child'
					}, {
						es: 'infante',
						en: 'infant'
					}, {
						es: 'R.U.T.',
						code: 'RUT'
					}, {
						es: 'pasaporte',
						code: 'PP'
					}, {
						es: 'masculino',
						code: 'M'
					}, {
						es: 'femenino',
						code: 'F'
					}];
				},
				getGenderOptions: function() {
					return [{
						key: 'M',
						value: 'Masculino'
					}, {
						key: 'F',
						value: 'Femenino'
					}];
				},
				getIdentifierOptions: function() {
					return [{
						key: 'RUT',
						value: 'R.U.T.'
					}, {
						key: 'PP',
						value: 'Pasaporte'
					}];
				},
				getDefaultHotelsStorageData: function() {
					return {
						search: {
							dates: {
								arrival: moment().add(7, 'days').format('YYYY-MM-DD'),
								departure: moment().add(10, 'days').format('YYYY-MM-DD')
							},
							destination: {
								id: '',
								name: ''
							},
							type: '',
							rooms: [{
								adults: 1,
								children: 0,
								ages: []
							}]

						}
					};
				},
				getSearchboxLimits: function() {
					return {
						'Lugares': 6,
						'Hoteles': 6,
						'Puntos de Interes': 4,
						'Aeropuerto': 3
					};
				},
				getLogosSuppliers: function() {
					return {
						cocha: "img/EAN.png",
						booking: "img/BKG.png",
						sabre: "img/SAB.png"
					};
				},
				getCategoryData: function(_type, _category) {
					return categoryData[_type][_category];
				}
			};
		});
});
