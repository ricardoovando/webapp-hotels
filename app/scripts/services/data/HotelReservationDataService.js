define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.services.HotelReservationDataService', [])
		.factory('HotelReservationDataService', function($window, cUtilsObject, cUtilsFields, $stateParams, cAccessLocal, HotelSearchDataService, cAccessRemote, HotelReservationParserService, StaticDataService, cCommonConstants, AppCommonService, B2BDataService) {

			function isInvalidName(input) {
				//return cUtilsObject.isVoid(input) || !/^[a-z\u00F1\u00D1\u00E1\u00C1\u00E9\u00C9\u00ED\u00CD\u00F3\u00D3\u00FA\u00DA\-\']{2,}(\s{0,1}[a-z\u00F1\u00D1\u00E1\u00C1\u00E9\u00C9\u00ED\u00CD\u00F3\u00D3\u00FA\u00DA\-\']+)*$/ig.test(input) || input.length < 2;
				return cUtilsObject.isVoid(input) || !/^[a-z\-]{2,}(\s{0,1}[a-z\-]+)*$/ig.test(input) || input.length < 2;
			}

			function validateBusinessIdFormat(businessId) {
				var noFormatBusinessId = businessId.replace(/-/g, "");
				if (noFormatBusinessId.length === 7) {
					return true;
				}
				else {
					return false;
				}
			}

			function validateCardFormat(cardNumber, returnBoolean) {
				var cardType = '';
				var response = {};
				if ((cardNumber.length === 13 || cardNumber.length === 16) && (cardNumber.substring(0, 1) === '4')) {
					if (typeof returnBoolean !== 'undefined') {
						return true;
					}
					cardType = 'visa';
				}
				else if ((cardNumber.length === 16) && (parseInt(cardNumber.substring(0, 2)) >= 51 && parseInt(cardNumber.substring(0, 2)) <= 55)) {
					if (typeof returnBoolean !== 'undefined') {
						return true;
					}
					else {
						cardType = 'mastercard';
					}
				}
				else if ((cardNumber.length === 15) && (parseInt(cardNumber.substring(0, 2)) >= 34 && parseInt(cardNumber.substring(0, 2)) <= 37)) {
					if (typeof returnBoolean !== 'undefined') {
						return true;
					}
					else {
						cardType = 'amex';
					}
				}
				else if ((cardNumber.length === 14 || cardNumber.length === 16) && (
						(parseInt(cardNumber.substring(0, 3)) >= 300 && parseInt(cardNumber.substring(0, 3)) <= 305) ||
						(parseInt(cardNumber.substring(0, 2)) === 36 || parseInt(cardNumber.substring(0, 2)) === 38) ||
						(parseInt(cardNumber.substring(0, 2)) >= 51 && parseInt(cardNumber.substring(0, 2)) <= 55))) {
					if (typeof returnBoolean !== 'undefined') {
						return true;
					}
					else {
						cardType = 'diners';
					}
				}
				else {
					if (typeof returnBoolean !== 'undefined') {
						return false;
					}
					else {
						response = {
							isInvalid: true,
							isForeign: -1,
							cardType: -1
						};
						return response;
					}
				}
				response = {
					isInvalid: false,
					isForeign: -1,
					cardType: cardType
				};
				return response;
			}

			return {
				getRoomData: function() {
					var roomData = null;
					try {
						roomData = cAccessLocal.getStorageData('roomData');
						cAccessLocal.deleteStorageData('roomData');
						cAccessLocal.setSessionData(roomData, 'reservation', 'roomData');
						return true;
					}
					catch (err) {
						console.info("[HotelReservationDataService] roomData not found on localstorage");
						return false;
					}
				},
				getContentData: function() {
					var data;
					try {
						//bring every field from the session data
						data = cAccessLocal.getSessionData('hotel', 'reservation', 'userReservationData');
						//uses default for B2B data
						data.generalData.B2B = this.getContentB2BData();
						cAccessLocal.deleteSessionData('hotel', 'reservation', 'userReservationData');
					}
					catch (e) {
						data = {
							departure: this.getSelectedDeparture(),
							arrival: this.getSelectedArrival(),
							hotel: this.getSelectedHotel(),
							rId: this.getSelectedRoomId(),
							rKey: this.getSelectedRoomKey(),
							passengers: this.getPassengers(),
							rooms: this.getRooms(),
							roomInfo: this.getRoomInfo(),
							contact: {
								names: '',
								surnames: '',
								email: (!B2BDataService) ? '' : $window.USER_EMAIL,
								phone: {
									country: (!B2BDataService) ? '56' : $window.USER_PHONE_COUNTRY,
									code: (!B2BDataService) ? '9' : $window.USER_PHONE_CODE,
									number: (!B2BDataService) ? '' : $window.USER_PHONE_NUMBER
								}
							},
							prices: {
								currency: 'USD',
								totalCLP: 0,
								main: {}
							},
							generalData: {
								bedType: null,
								B2B: this.getContentB2BData(),
								supplier: this.getSelectedSupplier(),
								corporateCode: this.getCorporateCode()
							},
							paymentMethod: this.getPaymentMethod(),
							specialRequest: '',
							idRegion: 0,
							destination: '',
							currency: 1,
						};
					}
					return data;
				},
				getRoomInfo: function() {
					var roomData = {};
					try {
						roomData = cAccessLocal.getSessionData('reservation', 'roomData');
						return roomData;
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no room data found: ", roomData);
						return roomData;
					}
				},
				getContentB2BData: function() {
					return {
						businessId: '',
						user: '',
						cardNumber: 0,
						cardType: -1,
						cardForeign: -1,
						cvvNumber: 0,
						expirationYear: 2016,
						expirationMonth: 1,
						paymentsType: 'VN',
						payments: 4,
						LCcurrency: 'CLP'
					};
				},
				getViewData: function(content) {
					return {
						loader: {
							businessId: false,
							cardNumber: false
						},
						hotelStarRating: this.getSelectedHotelStarRating(),
						isNemoBlocked: false,
						creditLineValidationText: 'Para acceder a esta forma de pago primero debes ingresar el negocio.',
						msgTime: 5000,
						pageLoader: true,
						isBooking: false,
						acceptTerms: false,
						acceptLCterms: false,
						registerEmail: false,
						userEmail: $window.USER_EMAIL,
						namePayments: '',
						nameMonth: 'Mes de expiración',
						nameYear: 'Año de expiración',
						monthSelected: false,
						yearSelected: false,
						errors: {},
						totalPassengers: {
							adults: this.getTotalAdults(),
							children: this.getTotalChildren(),
							rooms: this.getTotalRooms()
						},
						collapse: {
							document: _.fill(new Array(content.rooms.length), true),
							country: _.fill(new Array(content.rooms.length), true),
							payments: true,
							months: true,
							years: true,
							passengers: true,
							roomSummary: {}
						},
						focus: {
							names: _.fill(new Array(content.rooms.length), false),
							surnames: _.fill(new Array(content.rooms.length), false),
							lastname: _.fill(new Array(content.rooms.length), false),
							document: _.fill(new Array(content.rooms.length), false),
							country: _.fill(new Array(content.rooms.length), false),
							documentNumber: _.fill(new Array(content.rooms.length), false),
							contactName: false,
							contactSurname: false,
							contactEmail: false,
							contactPhoneCountry: false,
							contactPhoneCode: false,
							contactPhoneNumber: false,
							contactTravelAgent: false,
							specialRequest: false,
							bedType: false,
							businessId: false,
							cardNumber: false,
							expirationMonth: false,
							expirationYear: false,
						},
						options: {
							identifier: StaticDataService.getIdentifierOptions(),
							country: cCommonConstants.getCountries(),
							date: [],
							paymentsTypes: StaticDataService.getPaymentsOptionB2B(),
							months: StaticDataService.getMonthsOptions()
						},
						passengersIds: this.getPassengersIds(content),
						fieldsConfig: null, //FlightsBookingDataService.getPassengerDataConfig(),
						closeCollapsesCount: 1,
						availabilityData: {},
						maskOptionsBusinessId: {
							maskDefinitions: {
								'9': /\d/,
								'*': /[0-9A-Za-z]/
							},
							allowInvalidValue: true,
							clearOnBlur: false
						},
						maskOptionsCreditCard: {
							maskDefinitions: {
								'9': /\d|\./
							},
							allowInvalidValue: true,
							clearOnBlur: false
						}
					};
				},
				createSabreResponse: function(_data) {
					var total = this.getTotalFromRoom();
					var nrRooms = this.getTotalRooms();
					var rateKey = "sabre";
					var bedTypes = [{
						id: 1
					}];
					var resp = {};
					var properties = 'hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.total';
					resp = _.set(resp, properties, total);
					properties = 'hotelRoomResponse.bedTypes.bedType';
					resp = _.set(resp, properties, bedTypes);
					properties = 'hotelRoomResponse.rateInfos.rateInfo.cancellationPolicy';
					resp = _.set(resp, properties, '');

					for (var i = 0; i < nrRooms; i++) {
						properties = 'hotelRoomResponse.rateInfos.rateInfo.roomGroup.room[' + i + '].rateKey';
						resp = _.set(resp, properties, rateKey);
					}

					properties = 'hotelRoomResponse.roomTypeCode';
					resp = _.set(resp, properties, _data.rKey);
					properties = 'hotelRoomResponse.rateCode';
					resp = _.set(resp, properties, _data.rId);
					return resp;
				},
				getTotalFromRoom: function() {
					try {
						var roomData = cAccessLocal.getSessionData('reservation', 'roomData');
						return roomData.price;
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no room data found ", e.message);
						return null;
					}
				},
				getReservationRoomInfo: function() {
					try {
						var roomData = cAccessLocal.getSessionData('hotel', 'reservation', 'roomInfo');
						return roomData;
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no room data found ", e.message);
						return null;
					}
				},
				getConfirmationNumber: function() {
					try {
						var confirmationNumber = cAccessLocal.getSessionData('hotels', 'reservation', 'confirmationNumber');
						return confirmationNumber;
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no confirmation number found ", e.message);
						return null;
					}
				},
				getPassengersIds: function(content) {
					var data = _.fill(new Array(content.rooms.length), '');
					//alert("content rooms" + JSON.stringify(content.passengers));
					if (content.passengers[0].identifier.number !== null) {
						for (var i = 0; i < content.passengers.length; i++) {
							var identifier = content.passengers[i].identifier.number;
							var validationNumber = ((content.passengers[i].identifier.identifierClass === 'RUT') ? '-' + content.passengers[i].identifier.validationNumber : '');
							data[i] = ((content.passengers[i].identifier.identifierClass === 'RUT') ? parseInt(identifier).toLocaleString() + validationNumber : identifier);
						}
					}
					return data;
				},
				validateBusinessIdFormat: function(businessId) {
					return validateBusinessIdFormat(businessId);
				},
				validateBusinessId: function(businessId, okCallback, errorCallback, corporateCode, email) {
					var response = validateBusinessIdFormat(businessId);
					if (response === false) {
						errorCallback.BUSINESS_ID_ERROR(!response);
						return;
					}
					var url = $window.END_POINT_BUSINESS_ID;
					if (corporateCode && corporateCode !== '') {
						url = $window.END_POINT_NEMO;
					}

					var parameters = HotelReservationParserService.getBusinessIdRequestData(businessId, corporateCode, email);

					var okResponse = function(_data) {
						//var response = HotelReservationParserService.parseBusinessIdResponse(_data);
						var response = _data;
						if (response !== -1) {
							okCallback(response);
						}
						else {
							errorCallback.BUSINESS_ID_ERROR();
						}
					};

					var errorInternal = function() {
						errorCallback.ERROR_INTERNAL;
					};

					cAccessRemote.executeHTTPGet(url + parameters, okResponse, errorInternal);
				},
				validateCardNumber: function(cardNumber, okCallback, errorCallback) {
					var response = validateCardFormat(cardNumber);
					if (response.isInvalid) {
						errorCallback.CREDIT_CARD_ERROR(response);
						return;
					}
					var url = $window.END_POINT_CREDIT_CARD;
					var parameters = HotelReservationParserService.getCardNumberRequestData(cardNumber);
					//okCallback(response);
					var okResponse = function(_data) {
						//var svcResponse = HotelReservationParserService.parseCreditCardResponse(_data, response.cardType);
						var svcResponse = _data;
						if (svcResponse !== -1) {
							okCallback(svcResponse);
						}
						else {
							errorCallback.CREDIT_CARD_ERROR();
						}
					};

					var errorInternal = function() {
						errorCallback.ERROR_INTERNAL;
					};
					cAccessRemote.executeHTTPGet(url + parameters, okResponse, errorInternal);
				},
				refreshPaymentURL: function(token, _callbacksOK, _callbacksError) {
					console.info("[HotelReservationDataService] refreshPaymentURL");

					var url = $window.END_POINT_RESERVATION;
					var requestData = {
						token: token
					};

					var parseResponseOK = function(data) {
						if (data.code === "000") { //000 = Pago completo
							//ANALYTICS
							_callbacksOK.PAYMENT_COMPLETE();
						}
						else if (data.code === "010" && data.url) { //010 = Pago pendiente
							//ANALYTICS
							_callbacksOK.PAYMENT_PENDING(data.url);
						}
						else {
							//ANALYTICS
							_callbacksError.ERROR_PAYMENTURL();
						}
						return;
					};

					var callbackErrorInternal = function() {
						//ANALYTICS
						_callbacksError.ERROR_INTERNAL();
					};
					//booking
					cAccessRemote.executeHTTPPost(url, requestData, parseResponseOK, callbackErrorInternal);
				},
				getAvailability: function(_data, _okCallback, _errorCallbacks) {
					var url = (_data.generalData.supplier === 'SAB' || _data.generalData.supplier === 'TRV') ? $window.END_POINT_CHECKOUT_SABRE : $window.END_POINT_ROOM_AVAILABILITY;
					var parameters = HotelReservationParserService.getAvailabilityRequestData(_data);
					console.info("[HotelReservationDataService]", "availablity request", url, parameters);

					var onInternalError = function(_error, _data) {
						console.error("[HotelReservationDataService]", "availablity response", _data);
						if (_data && _data.data && _data.data.error && _data.data.code === -1) {
							//ANALYTICS
							_errorCallbacks.ERROR_AVAILABILITY();
						}
						else {
							var msg = (_data && _data.data && _data.data.error && _data.data.msg ? _data.data.msg : 'Error en servicio de disponibilidad');
							_errorCallbacks.ERROR_INTERNAL(msg);
						}

					};

					var onResponseOK = function(_data) {
						console.error("[HotelReservationDataService]", "availablity response", _data);
						/*
						if (_data.LCInfo && _data.LCInfo.isBlocked) {
							_errorCallbacks.BLOCKED_NEMO(_data.LCInfo);
						}
						*/
						_okCallback(_data);
					};
					cAccessRemote.executeHTTPGet(url + parameters, onResponseOK, onInternalError);
				},
				getRooms: function() {
					try {
						var roomData = cAccessLocal.getSessionData('reservation', 'roomData');
						var rooms = [];
						var count = 0;
						if (roomData.hasOwnProperty('request')) {
							for (var k in roomData.request) {
								if (k.search("room") !== -1 && typeof roomData.request[k] !== 'undefined' && roomData.request[k] !== null) {
									var arrRoom = String(roomData.request[k]).split(",");
									rooms[count] = {};
									rooms[count].adults = parseInt(arrRoom[0], 10);
									rooms[count].children = arrRoom.length - 1;
									rooms[count].ages = [];
									rooms[count].order = k.substr(-1);
									for (var i = 1; i < arrRoom.length; i++) {
										rooms[count].ages.push(arrRoom[i]);
									}
									count++;
								}
							}
						}

						return rooms;
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no room data found ", e.message);
						return null;
					}
				},
				getCorporateCode: function() {
					try {
						var roomData = cAccessLocal.getSessionData('reservation', 'roomData');
						return roomData.request.corporateCode;
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no room data found ", e.message);
						return null;
					}
				},
				getTotalAdults: function() {
					try {
						var roomData = cAccessLocal.getSessionData('reservation', 'roomData');
						var adults = 0;
						if (roomData.hasOwnProperty('request')) {
							for (var k in roomData.request) {
								if (k.search("room") !== -1 && typeof roomData.request[k] !== 'undefined' && roomData.request[k] !== null) {
									var arrRoom = String(roomData.request[k]).split(",");
									adults += parseInt(arrRoom[0], 10);
								}
							}
						}
						return adults;
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no room data found ", e.message);
						return null;
					}
				},
				getTotalChildren: function() {
					try {
						var roomData = cAccessLocal.getSessionData('reservation', 'roomData');
						var children = 0;
						if (roomData.hasOwnProperty('request')) {
							for (var k in roomData.request) {
								if (k.search("room") !== -1 && typeof roomData.request[k] !== 'undefined' && roomData.request[k] !== null) {
									var arrRoom = String(roomData.request[k]).split(",");
									children += arrRoom.length - 1;
								}
							}
						}
						return children;
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no room data found ", e.message);
						return null;
					}
				},
				getTotalRooms: function() {
					try {
						var roomData = cAccessLocal.getSessionData('reservation', 'roomData');
						var rooms = 0;
						if (roomData.hasOwnProperty('request')) {
							for (var k in roomData.request) {
								if (k.search("room") !== -1 && typeof roomData.request[k] !== 'undefined' && roomData.request[k] !== null) {
									rooms++;
								}
							}
						}
						return rooms;
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no room data found ", e.message);
						return null;
					}
				},
				getSelectedSupplier: function() {
					try {
						var roomData = cAccessLocal.getSessionData('reservation', 'roomData');
						return roomData.supplierCode;
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no room data found ", e.message);
						return null;
					}
				},
				getPassengers: function() {
					try {
						var roomData = cAccessLocal.getSessionData('reservation', 'roomData');

						var rooms = [];
						var count = 0;
						if (roomData.hasOwnProperty('request')) {
							for (var k in roomData.request) {
								if (k.search("room") !== -1 && typeof roomData.request[k] !== 'undefined' && roomData.request[k] !== null) {
									rooms[count] = this.getDefaultPassengerData();
									count++;
								}
							}
						}
						return rooms;
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no room data found ", e.message);
						return null;
					}
				},
				getDefaultPassengerData: function() {
					return {
						names: "",
						surnames: "",
						lastname: "",
						bedType: null,
						identifier: {
							identifierClass: 'RUT',
							number: null,
							country: null,
							validationNumber: null
						}
					};
				},
				getSelectedDeparture: function() {
					try {
						var roomData = cAccessLocal.getSessionData('reservation', 'roomData');
						return roomData.request.departure;
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no room data found ", e.message);
						return null;
					}
				},
				getSelectedHotelStarRating: function() {
					try {
						var roomData = cAccessLocal.getSessionData('reservation', 'roomData');
						return roomData.request.starRating;
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no room data found ", e.message);
						return null;
					}
				},
				getSelectedArrival: function() {
					try {
						var roomData = cAccessLocal.getSessionData('reservation', 'roomData');
						return roomData.request.arrival;
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no room data found ", e.message);
						return null;
					}
				},
				getSelectedHotel: function() {
					try {
						var roomData = cAccessLocal.getSessionData('reservation', 'roomData');
						return roomData.request.hotel;
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no room data found ", e.message);
						return null;
					}
				},
				getSelectedRoomId: function() {
					try {
						var roomData = cAccessLocal.getSessionData('reservation', 'roomData');
						return roomData.request.rId;
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no room data found ", e.message);
						return null;
					}
				},
				getSelectedRoomKey: function() {
					try {
						var roomData = cAccessLocal.getSessionData('reservation', 'roomData');
						return roomData.request.rKey;
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no room data found ", e.message);
						return null;
					}
				},
				getConfirmationErrors: function(_paymentMethod, _contact, _passengers, _generalData, _existingErrors, isB2B) {
					var errors = {
						passengersErrors: [],
						contactErrors: {
							emailNotSet: cUtilsObject.isVoid(_contact.email),
							emailInvalid: cUtilsFields.isInvalidEmail(_contact.email),
							phoneEmpty: cUtilsObject.isVoid(_contact.phone.country) || cUtilsObject.isVoid(_contact.phone.code) || cUtilsObject.isVoid(_contact.phone.number),
							phoneInvalid: !/^\d{1,3}$/.test(_contact.phone.country) || !/^\d{1,3}$/.test(_contact.phone.code) || !/^\d{5,10}$/.test(_contact.phone.number),
							phoneCountryError: cUtilsObject.isVoid(_contact.phone.country) || !/^\d{1,3}$/.test(_contact.phone.country),
							phoneCodeError: cUtilsObject.isVoid(_contact.phone.code) || !/^\d{1,3}$/.test(_contact.phone.code),
							phoneNumberError: cUtilsObject.isVoid(_contact.phone.number) || !/^\d{6,10}$/.test(_contact.phone.number)
						}
					};

					if (isB2B) {
						errors.businessIdInvalid = (_existingErrors.businessIdInvalid || !validateBusinessIdFormat(_generalData.B2B.businessId));
						errors.creditLineMismatch = (_existingErrors.creditLineMismatch);

						if (_paymentMethod === 'TC') {
							errors.cardNumberInvalid = (_existingErrors.cardNumberInvalid || !validateCardFormat(_generalData.B2B.cardNumber, true));
						}
						if (_paymentMethod === 'LC') {
							errors.cardNumberInvalid = false;
						}
					}

					_.forEach(_passengers, function(passenger) {
						var passengerErrors = {
							nameNotSet: cUtilsObject.isVoid(passenger.names),
							nameInvalid: isInvalidName(passenger.names),
							surnamesNotSet: cUtilsObject.isVoid(passenger.surnames),
							surnamesInvalid: isInvalidName(passenger.surnames),
							identifierClassNotSet: false,
							identifierNumberNotSet: false,
							identifierNumberFormatInvalid: false,
							identifierNumberRepeated: false,
							identifierVerificatorNotSet: false,
							identifierVerificatorInvalid: false,
							identifierCountryNotSet: false,
							bedType: false
						};

						if (_generalData.supplier === "SAB") {
							passengerErrors.lastnameNotSet = cUtilsObject.isVoid(passenger.lastname);
							passengerErrors.lastnameInvalid = cUtilsObject.isVoid(passenger.lastname);
						}

						//BedType
						passengerErrors.bedType = (cUtilsObject.isVoid(passenger.bedType) || !/^\d{1,10}$/.test(passenger.bedType));
						//Identifier

						passengerErrors.identifierClassNotSet = cUtilsObject.isVoid(passenger.identifier.identifierClass);
						passengerErrors.identifierNumberNotSet = cUtilsObject.isVoid(passenger.identifier.number);

						if (!passengerErrors.identifierClassNotSet) {
							if (passenger.identifier.identifierClass === 'RUT') {
								passengerErrors.identifierNumberFormatInvalid = !(/^[\d]{7,8}$/.test(passenger.identifier.number) && parseInt(passenger.identifier.number) > 999999);
								passengerErrors.identifierVerificatorNotSet = cUtilsObject.isVoid(passenger.identifier.validationNumber);
								if (!passengerErrors.identifierNumberFormatInvalid && !passengerErrors.identifierVerificatorNotSet) {
									passengerErrors.identifierVerificatorInvalid = !cUtilsFields.verifyDigit(passenger.identifier.number, passenger.identifier.validationNumber);
								}
							}
							else {
								passengerErrors.identifierCountryNotSet = cUtilsObject.isVoid(passenger.identifier.country);
								passengerErrors.identifierNumberFormatInvalid = !(/^[\da-zA-Z]{4,}$/.test(passenger.identifier.number));
								passengerErrors.identifierVerificatorNotSet = false;
								passengerErrors.identifierVerificatorInvalid = false;
							}
						}

						if (!passengerErrors.identifierNumberNotSet) {
							var id = passenger.identifier.number + '' + passenger.identifier.validationNumber;
							var aux = _.filter(_passengers, function(pax) {
								var idPax = pax.identifier.number + '' + pax.identifier.validationNumber;
								return idPax === id;
							});
							passengerErrors.identifierNumberRepeated = aux.length > 1;
						}

						errors.passengersErrors.push(passengerErrors);
					});
					return errors;
				},
				getConsiderations: function() {
					try {
						return cAccessLocal.getSessionData('hotels', 'reservation', 'considerations');
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no considerations found ", e.message);
						return null;
					}
				},
				getRateName: function() {
					try {
						return cAccessLocal.getSessionData('hotels', 'reservation', 'rateName');
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no rate name found ", e.message);
						return null;
					}
				},
				getContact: function() {
					try {
						return cAccessLocal.getSessionData('hotel', 'reservation', 'contact');
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no contact data found ", e.message);
						return null;
					}
				},
				getPlaceDestination: function() {
					try {
						return cAccessLocal.getSessionData('hotel', 'reservation', 'destination');
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no destination data found ", e.message);
						return null;
					}
				},
				getPrices: function() {
					try {
						return cAccessLocal.getSessionData('hotel', 'reservation', 'prices');
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no price data found ", e.message);
						return null;
					}
				},
				getPaymentId: function() {
					try {
						return cAccessLocal.getSessionData('hotel', 'reservation', 'paymentId');
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no paymentId data found ", e.message);
						return null;
					}
				},
				setProductsTrackingData: function(products) {
					cAccessLocal.setSessionData(products, 'hotel', 'reservation', 'productsData');
				},
				getProductsTrackingData: function() {
					try {
						return cAccessLocal.getSessionData('hotel', 'reservation', 'productsData');
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no product data found ", e.message);
						return null;
					}
				},
				clearHotelReservation: function() {
					console.info("[HotelReservationDataService]", "clearHotelReservation");
					try {
						cAccessLocal.deleteSessionData('hotel', 'results');
						cAccessLocal.deleteSessionData('hotel', 'reservation', 'passengerDataConfig');
						cAccessLocal.deleteSessionData('hotel', 'reservation');
						cAccessLocal.deleteSessionData('hotel', 'tracking', 'trackingData');
						console.info("[HotelReservationDataService]", "Cleared hotel storage");
					}
					catch (e) {
						console.info("[HotelReservacionDataService]", "Hotel storage is not initialized");
					}
				},
				getReservationPassengers: function() {
					try {
						return cAccessLocal.getSessionData('hotel', 'reservation', 'passengers');
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no passengers data", e.message);
						return null;
					}
				},
				getReservationRooms: function() {
					try {
						return cAccessLocal.getSessionData('hotel', 'reservation', 'rooms');
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no rooms data ", e.message);
						return null;
					}
				},
				getRoomSummary: function() {
					var roomTitle = ['', 'individual', 'doble', 'triple', 'cuadruple', 'quintuple', 'sextuple', 'septuple', 'octuple', 'nonuple', 'decuple'];
					var rooms = cAccessLocal.getSessionData('hotel', 'reservation', 'rooms');
					var roomInfo = cAccessLocal.getSessionData('hotel', 'reservation', 'roomInfo');
					var arrivalDate = moment(roomInfo.request.arrival);
					var departureDate = moment(roomInfo.request.departure);
					var summary = {
						adults: 0,
						children: 0,
						maxAdults: 0,
						nrRooms: rooms.length,
						nights: departureDate.diff(arrivalDate, 'days'),
						arrival: roomInfo.request.arrival,
						departure: roomInfo.request.departure,
						roomTitle: 'individual',
						roomInfo: roomInfo
					};
					_.forEach(rooms, function(value, index) {
						if (rooms[index].adults > 0) {
							summary.adults += rooms[index].adults;
							if (rooms[index].adults > summary.maxAdults) {
								summary.maxAdults = rooms[index].adults;
							}
						}
						if (rooms[index].children > 0) {
							summary.children += rooms[index].children;
						}
					});
					summary.roomTitle = roomTitle[summary.maxAdults];
					return summary;
				},
				getReservationExchange: function() {
					try {
						return cAccessLocal.getSessionData('hotel', 'reservation', 'reservationExchange');
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no currency exchange data ", e.message);
						return null;
					}
				},
				getCancellationPolicy: function() {
					try {
						return cAccessLocal.getSessionData('hotel', 'reservation', 'cancellationPolicy');
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no policy data ", e.message);
						return null;
					}
				},
				getReservationToken: function() {
					try {
						return cAccessLocal.getSessionData('hotel', 'reservation', 'reservationToken');
					}
					catch (e) {
						return '';
					}
				},
				getSPNR: function() {
					try {
						return cAccessLocal.getSessionData('hotel', 'reservation', 'SPNR');
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no supplier data ", e.message);
						return null;
					}
				},
				getSupplier: function() {
					try {
						return cAccessLocal.getSessionData('hotel', 'reservation', 'supplier');
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no supplier data ", e.message);
						return null;
					}
				},
				getAvailabilityData: function() {
					try {
						return cAccessLocal.getSessionData('hotel', 'reservation', 'availabilityData');
					}
					catch (e) {
						console.error("[HotelReservationDataService]", "no availability data ", e.message);
						return null;
					}
				},
				getHotelPhone: function() {
					try {
						return cAccessLocal.getSessionData('hotels', 'reservation', 'hotelPhone');
					}
					catch (e) {
						return '';
					}
				},
				getPaymentMethod: function() {
					try {
						return cAccessLocal.getSessionData('hotels', 'reservation', 'paymentMethod');
					}
					catch (e) {
						if (!B2BDataService) {
							return 'WebPay';
						}
						else {
							return 'TC';
						}
					}
				},
				getToken: function(_data, _availabilityData, _okCallback, _errorCallback, _flagTest) {
					var url = $window.END_POINT_CHECKBOOK;
					var callbackOK = function(data) {
						var phone = "";
						var rateName = "";
						var considerations = "";
						var confirmationNumber = "";
						if (data.status === 'CompleteB2C') {
							console.info("[HotelReservationDataService]", "Emission OK. Data:", data);
							phone = (data.hasOwnProperty('hotelPhone') ? data.hotelPhone : '');
							cAccessLocal.setSessionData(data.idCochaPago, 'hotels', 'reservation', 'paymentId');
							cAccessLocal.setSessionData(phone, 'hotels', 'reservation', 'hotelPhone');
							_okCallback.TOKEN_OK(data);
						}
						else if (data.status === 'PriceDifferenceB2C') {
							console.info("[PricingDataService]", "Price error: ", data);
							_errorCallback.ERROR_PRICE_CRASH();
						}
						else if (data.status === 'DuplicateReservationB2C') {
							_okCallback.TOKEN_OK(data);
						}
						else if (data.status === 'CompleteB2B') {
							console.info("[HotelReservationDataService]", "Reservation OK. Data:", data);
							phone = (data.hotelPhone ? data.hotelPhone : '');
							rateName = (data.codigoTarifa ? data.codigoTarifa : '');
							considerations = (data.consideraciones ? data.consideraciones : '');
							confirmationNumber = (data.codigoReserva ? data.codigoReserva : (data.numConfirm ? data.numConfirm : ''));
							cAccessLocal.setSessionData(data.spnr, 'hotels', 'reservation', 'paymentId');
							cAccessLocal.setSessionData(confirmationNumber, 'hotels', 'reservation', 'confirmationNumber');
							cAccessLocal.setSessionData(phone, 'hotels', 'reservation', 'hotelPhone');
							cAccessLocal.setSessionData(rateName, 'hotels', 'reservation', 'rateName');
							cAccessLocal.setSessionData(considerations, 'hotels', 'reservation', 'considerations');
							_okCallback.TOKEN_OK(data);
						}
						else if (data.status === 'PriceDifferenceB2B') {
							console.info("[PricingDataService]", "Price error: ", data);
							_errorCallback.ERROR_PRICE_CRASH();
						}
						else if (data.status === 'DuplicateReservationB2B') {
							_okCallback.TOKEN_OK(data);
						}
						else {
							console.error("[PricingDataService]", "Unknown error");
							_errorCallback.ERROR_INTERNAL(JSON.stringify(data));
						}
					};

					var callbackErrorInternal = function(error, response) {

						response.data = (response.data) ? response.data : {
							code: 'default'
						};
						if (response.data.name === 'NetworkError') {
							console.error("[PricingDataService]", "Network error calling reserve service", error);
							_errorCallback.ERROR_NETWORK(response.data);
						}
						else
						if (response.data.name === 'BookingError') {
							console.error("[PricingDataService]", "Error on reservation service", error, response.data);
							_errorCallback.ERROR_BOOKING(response.data);
						}
						else
						if (response.data.name === 'TravelioAuthError') {
							console.error("[PricingDataService]", "Error on reservation service", error, response.data);
							_errorCallback.ERROR_TRAVELIO_AUTH(response.data);
						}
						else
						if (response.data.name === 'HotelError') {
							console.error("[PricingDataService]", "Error reserving hotel", error, response.data);
							_errorCallback.ERROR_HOTEL(response.data);
						}
						else
						if (response.data.name === 'FlightError') {
							console.error("[PricingDataService]", "Error reserving flight", error, response.data);
							_errorCallback.ERROR_FLIGHT(response.data);
						}
						else
						if (response.data.name === 'ExpiredError') {
							console.error("[PricingDataService]", "Reservation time is over", error, response.data);
							_errorCallback.ERROR_EXPIRED(response.data);
						}
						else
						if (response.data.name === 'BusinessError' || response.data.name === 'BusinessError-fatal') {
							console.error("[PricingDataService]", "Error with business number", error, response.data);
							_errorCallback.ERROR_BUSINESS(response.data, response.data.name.indexOf('-fatal'));
						}
						else
						if (response.data.name === 'CardError' || response.data.name === 'CardError-fatal') {
							console.error("[PricingDataService]", "Error with card", error, response.data);
							_errorCallback.ERROR_CARD(response.data, response.data.name.indexOf('-fatal'));
						}
						else
						if (response.data.name === 'ExceededCardError') {
							console.error("[PricingDataService]", "Error with card limit", error, response.data);
							_errorCallback.ERROR_EXCEEDEDCARD(response.data);
						}
						else
						if (response.data.name === 'InvalidCardError') {
							console.error("[PricingDataService]", "Error with card data", error, response.data);
							_errorCallback.ERROR_INVALIDCARD(response.data);
						}
						else
						if (response.data.name === 'CardBusinessError' || response.data.name === 'CardBusinessError-fatal') {
							console.error("[PricingDataService]", "Error with business and card", error, response.data);
							_errorCallback.ERROR_BUSINESSCARD(response.data, response.data.name.indexOf('-fatal'));
						}
						else
						if (response.data.name === 'AttemptsError' || response.data.name === 'AttemptsError-fatal') {
							console.error("[PricingDataService]", "Exceeded the number of attempts", error, response.data);
							_errorCallback.ERROR_ATTEMPTS(response.data, response.data.name.indexOf('-fatal'));
						}
						else
						if (response.data.name === 'PaymentError' || response.data.name === 'PaymentError-fatal') {
							console.error("[PricingDataService]", "Error with the payment method", error, response.data);
							_errorCallback.ERROR_PAYMENT(response.data, response.data.name.indexOf('-fatal'));
						}
						else
						if (response.data.name === 'PaymentDataError' || response.data.name === 'PaymentDataError-fatal') {
							console.error("[PricingDataService]", "Error with the payment data", error, response.data);
							_errorCallback.ERROR_PAYDATA(response.data, response.data.name.indexOf('-fatal'));
						}
						else
						if (response.data.name === 'CheckError') {
							console.error("[PricingDataService]", "Error checking pay", error, response.data);
							_errorCallback.ERROR_CHECKPAY(response.data);
						}
						else {
							console.error("[PricingDataService]", "Error happened", error, response.data);
							var flowId = ($window.FLOW_ID) ? '(id:' + $window.FLOW_ID + ')' : '';
							var errorMsg = (response.data && response.data.description ? ' ¡Lo sentimos! <br> Hubo un problema al intentar reservar, por favor intente nuevamente <br> error: ' + response.data.description + ' <br> ' + flowId : JSON.stringify(response.data));
							_errorCallback.ERROR_INTERNAL(errorMsg);
						}
					};

					cAccessLocal.setSessionData(_data, 'hotel', 'reservation', 'userReservationData');
					var dataRequest = HotelReservationParserService.getRequestReservationData(_data, _availabilityData);

					_flagTest = _flagTest;
					var option = '';
					if (!_.isNull(_data.generalData.B2B.user)) {
						option = 'b2b';
					}
					//cAccessRemote.executeHTTPPost(url, dataRequest, callbackOK, callbackErrorInternal);
					cAccessRemote.executeHTTPPostAsync(url, dataRequest, callbackOK, callbackErrorInternal);
				},
				confirmHotel: function(_data, newsLetterRegistration, reservationData, availabilityData, _okCallbacks, _errorCallbacks) {

					console.info("[HotelReservationDataService] confirmHotel");

					cAccessLocal.setSessionData(reservationData.passengers, 'hotel', 'reservation', 'passengers');
					cAccessLocal.setSessionData(reservationData.contact, 'hotel', 'reservation', 'contact');
					cAccessLocal.setSessionData(reservationData.destination, 'hotel', 'reservation', 'destination');
					reservationData.prices.currency = (reservationData.generalData.B2B.cardForeign) ? 'USD' : 'CLP';
					cAccessLocal.setSessionData(reservationData.prices, 'hotel', 'reservation', 'prices');

					cAccessLocal.setSessionData(reservationData.currency, 'hotel', 'reservation', 'reservationExchange');
					var cancellationPolicy = (reservationData.generalData.supplier === 'SAB') ? reservationData.roomInfo.cancellationPolicy : reservationData.cancellationPolicy;

					cAccessLocal.setSessionData(cancellationPolicy, 'hotel', 'reservation', 'cancellationPolicy');

					cAccessLocal.setSessionData(reservationData.rooms, 'hotel', 'reservation', 'rooms');
					cAccessLocal.setSessionData(availabilityData, 'hotel', 'reservation', 'availabilityData');
					cAccessLocal.setSessionData(reservationData.generalData.supplier, 'hotel', 'reservation', 'supplier');
					cAccessLocal.setSessionData(reservationData.roomInfo, 'hotel', 'reservation', 'roomInfo');

					if (_.isNull(_data)) {
						_data = {};
					}
					_data.supplier = reservationData.generalData.supplier;

					if (!_.isNull(reservationData.generalData.B2B.user)) {
						cAccessLocal.setSessionData(_data, 'hotel', 'reservation', 'responseB2B');
					}
					var CRMRegister = function(_url, _pnr, _reservationData, callback) {
						var callbackOK = function(data) {
							console.info("[HotelReservationController]", "Success subscription", data);
							callback(_url);
						};

						var callbackError = function(data) {
							console.error("[HotelReservationController]", "Error in subscription", data);
							callback(_url);
						};
						AppCommonService.subscribeNewsletter(_data, newsLetterRegistration, _pnr, _reservationData, callbackOK, callbackError);
					};
					CRMRegister = CRMRegister;

					var CRMAddLead = function(reservationData, _spnr) {
						var callbackOK = function(data) {
							console.info("[HotelReservationDataService][CRMAddLead]", "Success Lead", data);
						};

						var callbackError = function(data) {
							console.error("[HotelReservationDataService][CRMAddLead]", "Error in Lead", data);
						};
						AppCommonService.registerLead(reservationData, _spnr, callbackOK, callbackError);
					};

					if (!_.isNull(reservationData.generalData.B2B.user)) {

						if (_data && _.has(_data, 'url') && _.has(_data, 'spnr') && _.has(_data, 'token')) {
							cAccessLocal.setSessionData(_data.spnr, 'hotel', 'reservation', 'SPNR');
							cAccessLocal.setSessionData(_data.token, 'hotel', 'reservation', 'reservationToken');
						}
						_okCallbacks.RESERVATION_OK();

					}
					else {
						//cAccessLocal.setSessionData(_data.spnr, 'hotel', 'reservation', 'SPNR');
						//cAccessLocal.setSessionData(_data.token, 'hotel', 'reservation', 'reservationToken');
						//_okCallbacks.RESERVATION_OK();

						if (_data && _.has(_data, 'url') && _.has(_data, 'spnr') && _.has(_data, 'token')) {
							console.info("[HotelReservationDataService]", "Confirmation OK with data: ", _data);
							cAccessLocal.setSessionData(_data.spnr, 'hotel', 'reservation', 'SPNR');
							cAccessLocal.setSessionData(_data.token, 'hotel', 'reservation', 'reservationToken');
							//ANALYTICS
							//CRMRegister(_data.url, _data.spnr, reservationData, _okCallbacks.RESERVATION_OK);
							CRMAddLead(reservationData.contact, reservationData.passengers, _data.spnr);
							_okCallbacks.RESERVATION_OK();
						}
						else {
							//ANALYTICS
							console.error("[HotelReservationDataService]", "Confirmation error with data: ", _data);
							//CRMRegister(null, null, reservationData, _errorCallbacks.ERROR_RESERVATION);
							_errorCallbacks.ERROR_RESERVATION();
						}
					}

				},
				bookHotel: function(_token, _callbacksOK, _errorCallbacks, isB2B) {
					var url = $window.END_POINT_RESERVATION;
					var data = {
						token: _token
					};

					var callbackErrorInternal = function(msg) {
						//ANALYTICS
						_errorCallbacks.ERROR_INTERNAL(msg);
					};

					var parseResponseOK = function(data) { //B2C
						if (data.status === 'CompleteB2C') {
							console.info("[HotelReservationDataService]", "Emission OK. Data:", data);
							var phone = (data.hotelPhone ? data.hotelPhone : '');
							cAccessLocal.setSessionData(data.idCochaPago, 'hotels', 'reservation', 'paymentId');
							cAccessLocal.setSessionData(phone, 'hotels', 'reservation', 'hotelPhone');
							_callbacksOK.GO_CONFIRMATION();
						}
						else if (data.status === 'DuplicateReservationB2C') {
							_callbacksOK.GO_CONFIRMATION();
						}
						else {
							console.error("[ReservationDataService]", "Unknown error");
							_errorCallbacks.ERROR_INTERNAL(JSON.stringify(data));
						}

					};

					var parseResponseB2B = function(data) {

						data = (data) ? data : {
							code: 'default'
						};
						if (data.status && data.status === 'CompleteB2B') {
							_callbacksOK.GO_CONFIRMATION();
						}
						else if (data.name === 'BookingError') {
							console.error("[PricingDataService]", "Error on reservation service", data);
							_errorCallbacks.ERROR_BOOKING();
						}
						else if (data.name === 'TravelioAuthError') {
							console.error("[PricingDataService]", "Error on reservation service", data);
							_errorCallbacks.ERROR_TRAVELIO_AUTH();
						}
						else if (data.name === 'HotelError') {
							console.error("[PricingDataService]", "Error reserving hotel", data);
							_errorCallbacks.ERROR_HOTEL();
						}
						else if (data.name === 'CardError') {
							console.error("[PricingDataService]", "Error with card", data);
							_errorCallbacks.ERROR_CARD();
						}
						else if (data.name === 'PaymentError' || data.name === 'PaymentError-fatal') {
							console.error("[PricingDataService]", "Error with payment method", data);
							_errorCallbacks.ERROR_PAYMENT();
						}
						else if (data.name === 'PaymentDataError' || data.name === 'PaymentDataError-fatal') {
							console.error("[PricingDataService]", "Error with payment data", data);
							_errorCallbacks.ERROR_PAYDATA();
						}
						else if (data.name === 'CheckError') {
							console.error("[PricingDataService]", "Error checking pay", data);
							_errorCallbacks.ERROR_CHECKPAY();
						}
						else {
							_errorCallbacks.ERROR_INTERNAL(JSON.stringify(data));
						}

					};

					if (!isB2B) {
						cAccessRemote.executeHTTPost(url, data, parseResponseOK, callbackErrorInternal);
					}
					else {
						var responseData = cAccessLocal.getSessionData('hotel', 'reservation', 'responseB2B');
						parseResponseB2B(responseData);
						cAccessLocal.deleteSessionData('hotel', 'reservation', 'responseB2B');
					}
				}
			};
		});
});
