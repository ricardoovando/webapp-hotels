define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.services.AppCommonService', [])
		.factory('AppCommonService', function($uibModal, $window, $state, $location, cAccessRemote, cUtilsFields, cUtilsObject, cMetadata, StaticDataService, AnalyticsService) {

			var callingErrors = {
				nameNotSet: false,
				nameInvalid: false,
				emailNotSet: false,
				emailInvalid: false,
				phoneEmpty: false,
				phoneInvalid: false,
				phoneCountryError: false,
				phoneCodeError: false,
				phoneNumberError: false,
				identifierNumberNotSet: false,
				identifierNumberFormatInvalid: false,
				identifierVerificatorNotSet: false,
				identifierVerificatorInvalid: false
			};
			var CTCFunctions = {
				showPrivacy: function() {
					$uibModal.open({
						animation: true,
						windowClass: 'modal-mobile-include',
						templateUrl: 'booking-privacy',
						size: 'lg'
					});
				},
				validateCallParams: function(_content) {
					callingErrors = {
						nameNotSet: cUtilsObject.isVoid(_content.contact.name),
						nameInvalid: cUtilsFields.isInvalidName(_content.contact.name),
						emailNotSet: cUtilsObject.isVoid(_content.contact.email),
						emailInvalid: cUtilsFields.isInvalidEmail(_content.contact.email),
						phoneEmpty: cUtilsObject.isVoid(_content.contact.countryCode) || cUtilsObject.isVoid(_content.contact.areaCode) || cUtilsObject.isVoid(_content.contact.phoneNumber),
						phoneInvalid: !/^\d{1,3}$/.test(_content.contact.countryCode) || !/^\d{1,3}$/.test(_content.contact.areaCode) || !/^\d{5,10}$/.test(_content.contact.phoneNumber),
						phoneCountryError: cUtilsObject.isVoid(_content.contact.countryCode) || !/^\d{1,3}$/.test(_content.contact.countryCode),
						phoneCodeError: cUtilsObject.isVoid(_content.contact.areaCode) || !/^\d{1,3}$/.test(_content.contact.areaCode),
						phoneNumberError: cUtilsObject.isVoid(_content.contact.phoneNumber) || !/^\d{6,10}$/.test(_content.contact.phoneNumber),
						identifierNumberNotSet: cUtilsObject.isVoid(_content.contact.foid.number),
						identifierNumberFormatInvalid: !(/^[\d]{7,8}$/.test(_content.contact.foid.number) && parseInt(_content.contact.foid.number) > 999999),
						identifierVerificatorInvalid: !cUtilsFields.verifyDigit(_content.contact.foid.number, _content.contact.foid.digit),
					};
					_content.errors = callingErrors;
				},
				subscribe: function(_email, _callbackOK, _callbackError) {
					console.info("[AppCommonService]", "subscribeNewsletterSimple");
					var url = $window.MID_URL + '/crm/newsletter';
					var data = {
						email: _email,
						optIn: 1
					};
					cAccessRemote.executeHTTPPost(url, data, _callbackOK, _callbackError);
				},
				sendCallRequest: function(_content) {
					CTCFunctions.validateCallParams(_content);
					var callbackOk = function() {
						_content.requesting = false;
					};
					var callbackError = function() {
						_content.requesting = false;
					};
					var callbackOk2 = function() {
						return;
					};
					var callbackError2 = function() {
						return;
					};
					if (_content.contact.subscribe) {
						CTCFunctions.subscribe(_content.contact.email, callbackOk2, callbackError2);
					}
					if (!cUtilsObject.containsValueDeep(_content.errors, true)) {
						_content.requesting = true;
						_content.contact.currentUrl = 'https://wapp.cocha.com/vuelos';
						var url = $window.MID_URL + '/click-to-call';
						cAccessRemote.executeHTTPPost(url, _content.contact, callbackOk, callbackError);
					}
				},
				formatId: function(_foidEntire, _foidObj) {
					if (!_foidEntire) {
						return _foidEntire;
					}
					var output;
					output = _foidEntire.replace(/[^kK\d]/ig, '');
					if (/\d+[kK\d]/.test(output)) {
						var id = output.substring(0, output.length - 1);
						var verifier = output.substring(output.length - 1);
						output = parseInt(id).toLocaleString() + '-' + verifier.toUpperCase();
						_foidObj.number = id;
						_foidObj.digit = verifier.toUpperCase();
					}
					else {
						_foidObj.number = output;
						_foidObj.digit = '';
					}

					return output;
				}
			};

			return {
				validateSearchNemo: function(_nemo, _okCallback, _errorCallback) {
					console.info("[Validate Search Nemo] Validate Search Nemo");
					var url = $window.END_POINT_VALIDATE_SEARCH_NEMO + _nemo;

					var onResponseError = function() {
						//ANALYTICS
						_errorCallback();
					};

					var onResponseOK = function() {
						_okCallback();
					};

					console.info("[Validate Search Nemo]", "validate nemo with data: ", _nemo);
					cAccessRemote.executeHTTPGet(url, onResponseOK, onResponseError);
				},
				subscribeNewsletter: function(_data, _register, _pnr, _reservationData, _callbackOK, _callbackError) {
					console.info("[AppCommonService]", "subscribeNewsletter: ", _data, _register, _reservationData);
					var url = $window.URL_CRM + 'WebToLeadCapture';
					var postData = {
						campaign_id: '3e0d4ed8-905d-4dcd-644a-5707188c4046',
						website: $state.href('home', {}, {
							absolute: true
						}) + "hoteles/",
						first_name: _reservationData.passengers[0].names,
						last_name: _reservationData.passengers[0].surnames,
						phone_home: _reservationData.contact.phone.country + '-' + _reservationData.contact.phone.code + '-' + _reservationData.contact.phone.number,
						email1: _reservationData.contact.email,
						ip_registro_c: '',
						recibir_notificaciones_c: _register
					};
					//alert(JSON.stringify(postData));
					cAccessRemote.executeHTTPPost(url, postData, _callbackOK, _callbackError);
				},
				registerLead: function(_reservationData, _spnr, _callbackOK, _callbackError) {
					console.info("[AppCommonService]", "registerLead");
					var url = $window.LEAD_URL;
					var passenger = _reservationData.passengers[0];
					var postData = {
						campana: "a7ee6ef2-23ea-880b-d726-59a9a5c0c163",
						first_name: passenger.names,
						last_name: passenger.surnames,
						phone_mobile: _reservationData.contact.phone.country + '-' + _reservationData.contact.phone.code + '-' + _reservationData.contact.phone.number,
						tipo_documento: passenger.identifier.identifierClass,
						rut: passenger.identifier.number + (passenger.identifier.validationNumber) ? '-' + passenger.identifier.validationNumber : '',
						email: _reservationData.contact.email,
						spnr: _spnr,
						reservationData: _reservationData
					};

					cAccessRemote.executeHTTPPost(url, postData, _callbackOK, _callbackError);
				},
				refreshUrl: function(stateName, stateData) {
					if (stateName.indexOf('checkout') < 0) {
						for (var i = 1; i <= 9; i++) {
							if (typeof stateData['room' + i] === 'undefined') {
								stateData['room' + i] = null;
							}
						}
					}
					if (!stateData.corporateCode) {
						stateData.corporateCode = null;
					}
					if (!stateData.reservationType) {
						stateData.reservationType = null;
					}
					$state.go(stateName, stateData, {
						location: true,
						inherit: true,
						relative: $state.$current,
						notify: false,
						reload: false
					});
				},
				subscribeNewsletterSimple: function(_email, _callbackOK, _callbackError) {
					CTCFunctions.subscribe(_email, _callbackOK, _callbackError);
				},
				contest: function(_data, _callbackOK, _callbackError) {
					console.info("[AppCommonService]", "subscribeNewsletterSimple");
					var url = $window.MID_URL + '/crm/contest';
					var data = {
						email: _data.email,
						destino: _data.destinationCode.code,
						optIn: _data.subscribe ? 1 : 0
					};
					cAccessRemote.executeHTTPPost(url, data, _callbackOK, _callbackError);
				},
				updateMetadata: function(_stateName) {
					try {
						var metadata = StaticDataService.getMetadata(_stateName);
						cMetadata.setTitle(metadata.title);
						cMetadata.setDescription(metadata.description);
					}
					catch (e) {
						AnalyticsService.errorSettingMetadata(e);
					}
				},
				openClickToCallModal: function(_contact) {
					$uibModal.open({
						controller: 'ModalController',
						templateUrl: 'calling-you',
						size: 'md',
						resolve: {
							content: function() {
								return {
									contact: _contact,
									functions: CTCFunctions
								};
							}
						}
					});
				},
				getAgencies: function(_okCallback) {
					var url = $window.MID_URL + '/info/agencies';
					cAccessRemote.executeHTTPGet(url, _okCallback, function() {});
				},
				serialize: function(obj, _isMandatory) {
					var str = [];
					var count = 0;
					var isMandatory = (typeof _isMandatory !== 'undefined') ? _isMandatory : false;
					for (var p in obj) {
						if (obj.hasOwnProperty(p)) {
							if (isMandatory) {
								str.push(encodeURIComponent(obj[p]));
							}
							else {
								str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
							}
							count++;
						}
					}
					var queryString = "";
					if (isMandatory) {
						queryString = str.join("/");
					}
					else {
						queryString = ((count > 0) ? "?" + str.join("&") : '');
					}
					return queryString;
				},
			};
		});
});
