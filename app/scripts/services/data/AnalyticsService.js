define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.services.AnalyticsService', [])
		.factory('AnalyticsService', function(cAnalytics) {

			function isValidRoom(key, collection) {
				if (key.indexOf("room") > -1 && typeof collection[key] !== 'undefined' && collection[key] !== null) {
					return true;
				}
				else {
					return false;
				}
			}

			function countPassengers(searchData) {
				var count = 0;
				var arrRoom = [];
				_.forEach(searchData, function(value, key) {
					if (isValidRoom(key, searchData)) {
						arrRoom = searchData[key].toString().split(',');
						count += (parseInt(arrRoom[0], 10) + arrRoom.length - 1);
					}
				});
				return count;
			}

			function countRooms(searchData) {
				var count = 0;
				_.forEach(searchData, function(value, key) {
					if (isValidRoom(key, searchData)) {
						count++;
					}
				});
				return count;
			}

			return {
				errorSettingMetadata: function(e) {
					cAnalytics.error('Error estableciendo metadata', e);
				},
				hotelSearch: function(searchData) {
					//alert(JSON.stringify(searchData));
					var numberPassengers = countPassengers(searchData);

					try {
						dataLayer.push({
							event: 'search',
							category: 'Hotels',
							query: {
								destination: searchData.full_name_region, //destino
								departure: searchData.arrival, //fecha YYYY-MM-DD
								return: searchData.departure, //fecha YYYY-MM-DD
								passengers: numberPassengers, //numero de pasajeros
								category: 'Hotels'
							}
						});
						//alert(JSON.stringify(dataLayer));
					}
					catch (e) {
						console.error("[HotelResultsController]", "Could not track search.\n", e.stack);
					}
				},
				hotelImpression: function(hotelData, startLimit) {
					var numberRooms = countRooms(hotelData[0].request);
					startLimit = (typeof startLimit === 'undefined' || startLimit === null) ? 0 : startLimit;
					for (var i = 0; i < hotelData.length; i++) {
						try {
							dataLayer.push({
								event: 'impression',
								category: 'Hotels',
								productData: {
									id: hotelData[i].hotelID, //id del hotel
									name: hotelData[i].request.full_name_region, //nombre del destino
									price: hotelData[i].lowestPrice, //precio en usd
									brand: hotelData[i].name, //nombre del hotel
									category: 'Hotels',
									variant: hotelData[i].request.arrival + '|' + hotelData[i].request.departure, //checkin checkout
									list: 'Resultados de hoteles',
									position: startLimit + (i + 1), //posición en el listado
									quantity: numberRooms //numero de habitaciones
								}
							});
						}
						catch (e) {
							console.error("[HotelResultsController]", "Could not track the hotel result.\n", e.stack);
						}
					}
				},
				productClick: function(hotelData, position, startLimit) {
					var numberRooms = countRooms(hotelData.request);
					startLimit = (typeof startLimit === 'undefined' || startLimit === null) ? 0 : startLimit;
					try {
						dataLayer.push({
							event: 'product click',
							category: 'Hotels',
							productData: {
								id: hotelData.hotelID, //id del hotel
								name: hotelData.request.full_name_region, //nombre del destino
								price: hotelData.lowestPrice, //precio en usd
								brand: hotelData.name, //nombre del hotel
								category: 'Hotels',
								variant: hotelData.request.arrival + '|' + hotelData.request.departure, //checkin checkout
								list: 'Resultados de hoteles',
								position: startLimit + (position + 1), //posicion en el listado
								quantity: numberRooms //numero de habitaciones								
							}
						});
					}
					catch (e) {
						console.error("[HotelResultsController]", "Could not track product click.\n", e.stack);
					}
				},
				detailImpression: function(detailData, searchData) {
					//get a default value for position (1)
					//when there is no searchdata  
					var numberRooms = countRooms(searchData);
					try {
						dataLayer.push({
							event: 'detail impression',
							category: 'Hotels',
							productData: {
								id: detailData.info.id, //id del hotel
								name: searchData.full_name_region, //nombre del destino
								price: detailData.lowest, //precio en usd
								brand: detailData.info.name, //nombre del hotel
								category: 'Hotels',
								variant: searchData.arrival + '|' + searchData.departure, //checkin, checkout
								list: 'Resultados de hoteles',
								//position: searchData.position, //posicion en el listado
								position: '1',
								quantity: numberRooms //numero de habitaciones
							}
						});
					}
					catch (e) {
						console.error("[HotelResultsController]", "Could not track the detail impression.\n", e.stack);
					}
				},
				cart: function(detailData, searchData, roomPosition, room) {
					var numberRooms = countRooms(searchData);
					//define room position			
					try {
						dataLayer.push({
							event: 'cart',
							category: 'Hotels',
							productData: {
								id: (detailData.hasOwnProperty('id') ? detailData.id : detailData.hotelID), //id hotel
								name: searchData.full_name_region, //nombre del destino
								price: room.price, //precio en usd
								brand: detailData.name, //nombre del hotel
								category: 'Hotels',
								variant: searchData.arrival + '|' + searchData.departure, //checkin,checkout
								list: 'Detalle del hotel',
								position: roomPosition, //posicion en el listado
								quantity: numberRooms //numero de habitaciones
							}
						});
					}
					catch (e) {
						console.error("[HotelResultsController]", "Could not track the detail impression.\n", e.stack);
					}
				},
				checkoutStep: function(step) {
					try {
						dataLayer.push({
							event: 'checkout step',
							category: 'Hotels',
							checkoutStep: step
						});
					}
					catch (e) {
						console.error("[HotelResultsController]", "Could not track the checkout step " + step +
							".\n", e.stack);
					}
				},
				checkout: function(transactionData) {
					try {
						dataLayer.push({
							event: 'checkout',
							category: 'Hotels',
							purchase: {
								id: transactionData.pnr, //pnr
								affiliation: 'Pago y reserva confirmada', //estado
								revenue: (transactionData.supplier === 'EAN' || transactionData.supplier === 'EANPKGR') ? transactionData.availabilityData.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.nightlyRateTotal : transactionData.availabilityData.checkoutDetailDTO.baseRate, //monto total en usd
								tax: (transactionData.supplier === 'EAN' || transactionData.supplier === 'EANPKGR') ? transactionData.availabilityData.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.surchargeTotal : transactionData.availabilityData.checkoutDetailDTO.taxes //impuestos en usd
							},
							productData: {
								id: transactionData.availabilityData.hotelId, //id del hotel
								name: transactionData.availabilityData.hotelCity, //nombre destino
								price: (transactionData.supplier === 'EAN' || transactionData.supplier === 'EANPKGR') ? transactionData.availabilityData.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.total : transactionData.availabilityData.checkoutDetailDTO.totalPricing,
								//price: transactionData.availabilityData.hotelRoomResponse.rateInfos.rateInfo.chargeableRateInfo.nightlyRateTotal, //precio en usd
								brand: transactionData.availabilityData.hotelName, //nombre del hotel
								category: 'Hotels',
								variant: transactionData.arrival + '|' + transactionData.departure, //checkin,checkout
								//quantity: transactionData.rooms.length //numero de habitaciones
								quantity: 1
							}
						});
					}
					catch (e) {
						console.error("[HotelResultsController]", "Could not track the checkout confirmation .\n", e.stack);
					}
				}
			};
		});
});
