define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.services.B2BDataService', [])

		.provider('B2BDataService', function() {
			this.user = null;
			this.readCookie = function(name) {
				var nameEQ = name + "=";
				var ca = document.cookie.split(';');
				for (var i = 0; i < ca.length; i++) {
					var c = ca[i];
					while (c.charAt(0) === ' ') {
						c = c.substring(1, c.length);
					}
					if (c.indexOf(nameEQ) === 0) {
						var user = c.substring(nameEQ.length, c.length).replace(/\+/g, " ");
						return user;
					}
				}
				return null;
			};
			this.setUser = function(_user) {
				this.user = _user;
			};
			this.createCookie = function(name, value, days) {
				var expires = "";
				if (days) {
					var date = new Date();
					date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
					expires = "; expires=" + date.toUTCString();
				}
				else {
					expires = "";
				}
				document.cookie = name + "=" + value + expires + "; path=/";
			};
			this.eraseCookie = function(name) {
				this.createCookie(name, "", -1);
			};

			this.$get = function() {
				return this.user;
			};
		});

});
