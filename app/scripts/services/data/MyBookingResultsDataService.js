define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.services.MyBookingResultsDataService', [])
		.factory('MyBookingResultsDataService', function($window, B2BDataService, MyBookingParserService, cAccessRemote, HotelReservationDataService, StaticDataService, cUtilsObject, cAccessLocal) {
			var reservationResults = [];

			function isSpnrValid(_spnr) {
				return /([A-Z])+([0-9]{6})+/.test(_spnr);
			}

			function isNameValid(_name) {
				return /([A-Za-z- '\u00C1\u00C9\u00CD\u00D3\u00DA\u00E1\u00E9\u00ED\u00F3\u00FA\u00D1\u00F1]{3,})+/.test(_name);
			}

			function isEmailValid(_email) {
				var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				return re.test(_email);
			}
			/*
			function isDateValid(_date) {
				var dateFormat = "YYYY-MM-DD";
				return moment(_date, dateFormat, true).isValid();
			}
			*/

			function isStatusBookingValid(_data) {
				if (_data.hasOwnProperty('code')) {
					var statusOptions = StaticDataService.getBookingStatusOptions();
					var flag = false;
					_.forEach(statusOptions, function(value) {
						if (value.code === _data.code) {
							flag = true;
							return;
						}
					});
					return flag;
				}
				else {
					return false;
				}
			}

			return {
				getSearchResults: function() {
					return reservationResults;
				},
				getBookingDeeplink: function(_data, _okCallback, _errorCallbacks) {
					console.info("[MyBookingResultsDataService] getBookingDeeplink", _data);
					var parameters = MyBookingParserService.getParameters(_data, 'bookingDeepLink');
					var url = $window.END_POINT_BKG_DEEPLINK;
					reservationResults = [];
					var onResponseOK = function(data) {
						console.debug("[MyBookingResultsDataService]", "Response booking deeplink: ", data);
						try {
							if (_.isNull(data) || _.isEmpty(data)) {
								_errorCallbacks.ERROR_INTERNAL();
							}
							if (data.hasOwnProperty('error') && data.error) {
								_errorCallbacks.ERROR_INTERNAL();
							}

							if (data.hasOwnProperty("url") && data.url !== '') {
								console.info("[MyBookingResultsDataService]", "Found details: " + reservationResults.length);
								_okCallback(data.url);
							}
							else {
								console.warn("[MyBookingResultsDataService]", "No details found");
								_errorCallbacks.ERROR_NORESULT();
							}
						}
						catch (e) {
							console.error("[MyBookingResultsDataService]", "Internal error: ", e.stack);
							_errorCallbacks.ERROR_INTERNAL();
						}
					};

					var onInternalError = function() {
						_errorCallbacks.ERROR_INTERNAL();
					};
					console.info("[MyReservationDataService]", "Search hotel with data: ", url + parameters);
					cAccessRemote.executeHTTPGet(url + parameters, onResponseOK, onInternalError);
				},
				getContentData: function() {
					return {
						bookingNumber: '',
						businessNumber: '',
						spnr: '',
						surname: '',
						email: '',
						hotelName: '',
						bookDateIni: null,
						bookDateEnd: null,
						checkInIni: null,
						checkInEnd: null,
						statusBooking: '',
						isB2B: ((B2BDataService !== null) ? true : false),
						results: reservationResults
					};
				},
				getViewData: function() {
					return {
						pageIndex: 0,
						pageLimit: 20,
						errors: null,
						loadingResults: false,
						collapseCalendar: {
							arrival: true,
							departure: true,
							bookarrival: true,
							bookdeparture: true
						},
						closeCollapsesCount: 1,
						openCalendarXs: {
							arrival: false,
							departure: false,
							bookarrival: false,
							bookdeparture: false
						},
						showStatusOption: false,
						properWidth: true,
						smallView: false,
						statusOptions: StaticDataService.getBookingStatusOptions(),
						maskOptionsBusinessId: {
							maskDefinitions: {
								'9': /\d/,
								'*': /[0-9A-Za-z]/
							},
							allowInvalidValue: true,
							clearOnBlur: false
						},
						maskOptionsNumber: {
							maskDefinitions: {
								'9': /\d|\./
							},
							allowInvalidValue: true,
							clearOnBlur: false
						},
						maskOptionsSpnr: {
							maskDefinitions: {
								'9': /\d|\./,
								'A': /[A-Za-z]/
							},
							allowInvalidValue: true,
							clearOnBlur: false
						}
					};
				},
				getSearchErrors: function(_data, _existingErrors, _isB2B) {
					var errors = {};
					var flag = false;
					if (_isB2B) {
						if (!cUtilsObject.isVoid(_data.bookingNumber)) {
							errors.bookingNumberInvalid = !/^\d{6,10}$/.test(_data.bookingNumber);
							flag = true;
						}
						if (!cUtilsObject.isVoid(_data.businessNumber)) {
							errors.businessNumberInvalid = !HotelReservationDataService.validateBusinessIdFormat(_data.businessNumber);
							flag = true;
						}
						if (!cUtilsObject.isVoid(_data.spnr)) {
							errors.spnrInvalid = !isSpnrValid(_data.spnr);
							flag = true;
						}
						if (!cUtilsObject.isVoid(_data.surname)) {
							errors.surnameInvalid = !isNameValid(_data.surname);
							flag = true;
						}
						if (!cUtilsObject.isVoid(_data.email)) {
							errors.emailInvalid = !isEmailValid(_data.email);
							flag = true;
						}
						if (!cUtilsObject.isVoid(_data.hotelName)) {
							errors.hotelNameInvalid = !isNameValid(_data.hotelName);
							flag = true;
						}
						if (!cUtilsObject.isVoid(_data.bookDateIni) && !cUtilsObject.isVoid(_data.bookDateEnd)) {
							//errors.bookDateIniInvalid = !isDateValid(_data.bookDateIni);
							//errors.bookDateEndInvalid = !isDateValid(_data.bookDateEnd);
							errors.bookDateIniInvalid = false;
							errors.bookDateEndInvalid = false;
							flag = true;
						}
						else {
							if (!cUtilsObject.isVoid(_data.bookDateIni) || !cUtilsObject.isVoid(_data.bookDateEnd)) {
								flag = true;
								errors.bookDateRangeInvalid = true;
							}
						}

						if (!cUtilsObject.isVoid(_data.checkInIni) && !cUtilsObject.isVoid(_data.checkInEnd)) {
							//errors.checkInIniInvalid = !isDateValid(_data.checkInIni);
							//errors.checkInEndInvalid = !isDateValid(_data.checkInEnd);
							errors.checkInIniInvalid = false;
							errors.checkInEndInvalid = false;
							flag = true;
						}
						else {
							if (!cUtilsObject.isVoid(_data.checkInIni) || !cUtilsObject.isVoid(_data.checkInEnd)) {
								flag = true;
								errors.checkInRangeInvalid = true;
							}
						}

						if (!_.isNull(_data.statusBooking) && !cUtilsObject.isVoid(_data.statusBooking) && !_.isEmpty(_data.statusBooking)) {
							errors.statusBookingInvalid = !isStatusBookingValid(_data.statusBooking);
							flag = true;
						}
						if (flag === false) {
							errors.searchFieldsInvalid = true;
						}
						return errors;
					}
					else {
						if (cUtilsObject.isVoid(_data.bookingNumber) || cUtilsObject.isVoid(_data.surname)) {
							errors.searchFieldsInvalid = true;
						}
						else {
							errors.bookingNumberInvalid = !/^\d{6,10}$/.test(_data.bookingNumber);
							errors.surnameInvalid = !isNameValid(_data.surname);
						}
						return errors;
					}
				},
				saveSearchParameters: function(parameters) {
					try {
						cAccessLocal.initializeSessionStorage('mybooking', {
							savedParameters: null
						});
						cAccessLocal.setSessionData(parameters, 'mybooking', 'searchParameters');
					}
					catch (err) {
						console.info("[MyBookingResultsDataService] can't save booking data");
					}
				},
				getSavedSearchParameters: function() {
					var parameters;
					try {
						parameters = cAccessLocal.getSessionData('mybooking', 'searchParameters');
					}
					catch (err) {
						parameters = null;
						console.info("[MyBookingResultsDataService] no saved parameters");
					}
					return parameters;
				},
				getPresetValues: function() {
					var presets = this.getSavedSearchParameters();
					if (!_.isNull(presets)) {
						//alert(JSON.stringify(moment(presets.bookDateIni).format('YYYY')));
						//alert(JSON.stringify(moment(presets.bookDateIni).format('M')));
						//alert(JSON.stringify(moment(presets.bookDateIni).format('D')));
						//new Date(moment(presets.bookDateIni).format('YYYY'), moment(presets.bookDateIni).format('M'), moment(presets.bookDateIni).format('D'));						
						if (!_.isNull(presets.bookDateIni) && presets.bookDateIni !== '') {
							presets.bookDateIni = new Date(parseInt(moment(presets.bookDateIni).format('YYYY'), 10), parseInt(moment(presets.bookDateIni).format('M'), 10) - 1, parseInt(moment(presets.bookDateIni).format('D'), 10));
						}
						if (!_.isNull(presets.bookDateEnd) && presets.bookDateEnd !== '') {
							presets.bookDateEnd = new Date(parseInt(moment(presets.bookDateEnd).format('YYYY'), 10), parseInt(moment(presets.bookDateEnd).format('M'), 10) - 1, parseInt(moment(presets.bookDateEnd).format('D'), 10));
						}
						if (!_.isNull(presets.checkInIni) && presets.checkInIni !== '') {
							presets.checkInIni = new Date(parseInt(moment(presets.checkInIni).format('YYYY'), 10), parseInt(moment(presets.checkInIni).format('M'), 10) - 1, parseInt(moment(presets.checkInIni).format('D'), 10));
						}
						if (!_.isNull(presets.checkInEnd) && presets.checkInEnd !== '') {
							presets.checkInEnd = new Date(parseInt(moment(presets.checkInEnd).format('YYYY'), 10), parseInt(moment(presets.checkInEnd).format('M'), 10) - 1, parseInt(moment(presets.checkInEnd).format('D'), 10));
						}
						//alert(JSON.stringify(presets));
						return presets;
					}
					else {
						return null;
					}
				},
				saveBookingData: function(_bookingData) {
					try {
						cAccessLocal.initializeLocalStorage('mybooking', {
							bookingData: null
						});
						//cAccessLocal.deleteStorageData('mybooking', 'bookingData');
						cAccessLocal.setStorageData(_bookingData, 'mybooking', 'bookingData');
					}
					catch (err) {
						console.info("[MyBookingResultsDataService] can't save booking data");
					}
				},
				getReservationData: function(_data, _okCallback, _errorCallbacks) {
					console.info("[MyReservationDataService] searchReservation", _data);
					var parameters = MyBookingParserService.getParameters(_data, 'searchBooking');
					var url = $window.END_POINT_SEARCH_BOOKING;
					this.saveSearchParameters(_data);
					reservationResults = [];
					var onResponseOK = function(data) {
						console.debug("[MyReservationDataService]", "Response de search reservation: ", data);
						try {
							reservationResults = data;
							if (_.isNull(reservationResults) || _.isEmpty(reservationResults)) {
								reservationResults = [];
							}

							if (reservationResults.length > 0) {
								console.info("[MyReservationDataService]", "Found reservations: " + reservationResults.length);
								_okCallback();
							}
							else {
								console.warn("[MyReservationDataService]", "No reservations found");
								_errorCallbacks.ERROR_NORESULT();
							}
						}
						catch (e) {
							console.error("[MyReservationDataService]", "Internal error: ", e.stack);
							_errorCallbacks.ERROR_INTERNAL();
						}
					};

					var onInternalError = function() {
						_errorCallbacks.ERROR_INTERNAL();
					};
					console.info("[MyReservationDataService]", "Search hotel with data: ", url + parameters);
					cAccessRemote.executeHTTPGet(url + parameters, onResponseOK, onInternalError);
				}
			};
		});
});
