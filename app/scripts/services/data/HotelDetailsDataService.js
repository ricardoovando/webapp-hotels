define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.services.HotelDetailsDataService', [])
		.factory('HotelDetailsDataService', function($location, CurrencyService, $window, $stateParams, cAccessLocal, HotelSearchDataService, cAccessRemote, StaticDataService, HotelDetailsParserService, HotelReservationParserService) {

			function inArray(needle, haystack) {
				var length = haystack.length;
				for (var i = 0; i < length; i++) {
					if (haystack[i] === needle) {
						return true;
					}
				}
				return false;
			}

			function prepareAvailabilityData(_data) {

				var data = {
					hotel: _data.request.hotel,
					rId: _data.roomId,
					rKey: _data.rateCode,
					arrival: _data.request.arrival,
					departure: _data.request.departure,
					generalData: {
						supplier: 'EAN'
					}
				};
				data.rooms = [];
				var count = 0;
				if (_data.hasOwnProperty('request')) {
					for (var k in _data.request) {
						if (k.search("room") !== -1 && typeof _data.request[k] !== 'undefined' && _data.request[k] !== null) {
							var arrRoom = String(_data.request[k]).split(",");
							data.rooms[count] = {};
							data.rooms[count].adults = parseInt(arrRoom[0], 10);
							data.rooms[count].children = arrRoom.length - 1;
							data.rooms[count].ages = [];
							for (var i = 1; i < arrRoom.length; i++) {
								data.rooms[count].ages.push(arrRoom[i]);
							}
							count++;
						}
					}
				}

				return data;
			}

			return {
				getDatelessParams: function(stateData) {
					if (typeof stateData.arrival === 'undefined' || typeof stateData.departure === 'undefined') {
						var defaultValues = StaticDataService.getDefaultHotelsStorageData();
						stateData.arrival = defaultValues.search.dates.arrival;
						stateData.departure = defaultValues.search.dates.departure;
						return stateData;
					}
					return false;
				},
				getContentData: function(stateData) {
					return {
						hotelInfo: {},
						rooms: null,
						roomsBySupplier: null,
						lowest: null,
						imageMain: null,
						hotel: stateData.hotel,
						mapData: stateData,
						currency: '--',
						B2Buser: null,
						defaultValues: null,
						points: []
					};
				},
				getViewData: function() {
					return {
						lowestPriceId: '#',
						detailsLoader: true,
						tabService: true,
						tabPay: true,
						tabPolicy: true,
						urlMap: '',
						closestPoints: [],
						hotelSearchData: null,
						regionName: '',
						pageLoader: true,
						hotelRequestData: null,
						textCategory: '',
						env: $window.ENV
					};
				},
				getHotelCharges: function(_data, _okCallback, _errorCallbacks) {
					var url = $window.END_POINT_ROOM_AVAILABILITY;
					var data = prepareAvailabilityData(_data);
					var parameters = HotelReservationParserService.getAvailabilityRequestData(data);

					var onInternalError = function() {
						_errorCallbacks.ERROR_INTERNAL();
					};

					var onResponseOK = function(_data) {
						_okCallback(_data);
					};
					cAccessRemote.executeHTTPGet(url + parameters, onResponseOK, onInternalError);
				},
				getHotelDetailsFiltersValues: function(defaultValues) {
					if (_.isEmpty(defaultValues)) {
						return {
							amenities: {},
							supplier: {},
							paymentType: {},
							cancellation: {}
						};
					}
					//defaultValues.supplier;
					var arrCancellation = StaticDataService.getCancellationValues();
					var arrSupplier = StaticDataService.getSupplierValues();
					var arrPayment = StaticDataService.getPaymentTypeValues();

					var arrCancellationValues = {};
					var arrSupplierValues = {};
					var arrPaymentValues = {};

					for (var i = 0; i < arrCancellation.length; i++) {
						if (inArray(arrCancellation[i].code, defaultValues.cancellation)) {
							arrCancellationValues[arrCancellation[i].code] = arrCancellation[i].code;
						}
						else {
							arrCancellationValues[arrCancellation[i].code] = false;
						}
					}
					for (i = 0; i < arrSupplier.length; i++) {
						if (inArray(arrSupplier[i].code, defaultValues.supplier)) {
							arrSupplierValues[arrSupplier[i].code] = arrSupplier[i].code;
						}
						else {
							arrSupplierValues[arrSupplier[i].code] = false;
						}
					}
					for (i = 0; i < arrPayment.length; i++) {
						if (inArray(arrPayment[i].code, defaultValues.paymentType)) {
							arrPaymentValues[arrPayment[i].code] = arrPayment[i].code;
						}
						else {
							arrPaymentValues[arrPayment[i].code] = false;
						}
					}
					return {
						amenities: {},
						supplier: arrSupplierValues,
						paymentType: arrPaymentValues,
						cancellation: arrCancellationValues
					};
				},
				getHotelData: function(_hotelId) {
					var hotelData = null;
					try {
						hotelData = cAccessLocal.getStorageData('hotelData', _hotelId.toString());
						cAccessLocal.deleteStorageData('hotelData', _hotelId.toString());
						cAccessLocal.setSessionData(hotelData, 'hotelData');
					}
					catch (err) {
						console.info("[HotelDetailsDataService] hotelData not found on localstorage");
						try {
							hotelData = cAccessLocal.getSessionData('hotelData');
						}
						catch (err) {
							console.info("[HotelDetailsDataService] hotelData not found on sessionstorage");
						}
					}
					return hotelData;
				},
				saveHotelData: function(_hotelData) {
					cAccessLocal.initializeLocalStorage('hotelData', {});
					cAccessLocal.setStorageData(_hotelData, 'hotelData', _hotelData.hotel.toString());
				},
				saveRoomData: function(_roomData) {
					try {
						cAccessLocal.getStorageData('roomData');
						cAccessLocal.setStorageData(_roomData, 'roomData');
						//cAccessLocal.initializeLocalStorage('reservation', {
						//	roomData: null
						//});
						//cAccessLocal.deleteStorageData('reservation', 'roomData');
						//cAccessLocal.setStorageData(_roomData, 'reservation', 'roomData');
					}
					catch (err) {
						console.info("[HotelDetailsDataService] can't save reservation data, try to initialize");
						cAccessLocal.initializeLocalStorage('roomData', null);
						cAccessLocal.setStorageData(_roomData, 'roomData');
					}
				},
				getSelectedSortCriteria: function() {
					var criteria = null;
					try {
						criteria = cAccessLocal.getSessionData('hotels', 'details', 'sort', 'criteria');
					}
					catch (err) {
						criteria = 'default';
					}
					return criteria;
				},
				setSelectedSortCriteria: function(_criteria) {
					cAccessLocal.setSessionData(_criteria, 'hotels', 'details', 'sort', 'criteria');
				},
				searchHotelInfo: function(_data, _filters, _sort, _okCallbacks, _errorCallbacks) {
					console.info("[HotelsDetailsDataService] searchHotels", _data, _filters, _sort);
					var url = $window.END_POINT_HOTEL_DETAILS;
					var parameters = HotelDetailsParserService.getDetailsRequestData(_data, _filters, _sort);

					cAccessLocal.setSessionData(_data, 'hotel', 'search');
					if (!(_.isEmpty(_filters))) {
						cAccessLocal.setSessionData(_filters, 'hotel', 'filters');
					}

					var onResponseOK = function(data) {
						console.debug("[HotelDetailsDataService]", "Response de HotelDetail: ", data);

						//TAGMANAGER
						try {
							//var hotel = HotelDetailsParserService.parseResults(data, parameters);
							var hotel = data;
							cAccessLocal.setSessionData(hotel.info, 'hotel', 'info', 'found');
							cAccessLocal.setSessionData(hotel.rooms, 'hotel', 'rooms', 'found');

							if (!(_.isEmpty(hotel.info))) {
								console.info("[HotelDetailsDataService]", "Found hotel: " + hotel.info);
								_okCallbacks.SEARCH_SUCCESS(hotel);
							}
							else {
								console.warn("[HotelDetailsDataService]", "No hotel found");
								_errorCallbacks.ERROR_NORESULT();
							}
						}
						catch (e) {
							console.error("[HotelDetailsDataService]", "Internal error: ", e.stack);
							_errorCallbacks.ERROR_INTERNAL();
						}
					};

					var onInternalError = function(error, response) {
						if (response && response.status && response.status === 404) {
							_errorCallbacks.ERROR_NORESULT(true);
						}
						else {
							_errorCallbacks.ERROR_INTERNAL();
						}
					};
					console.info("[HotelDetailsDataService]", "Search hotel with data: ", url + parameters);
					cAccessRemote.executeHTTPGet(url + parameters, onResponseOK, onInternalError);
				},
				searchRooms: function(_data, _filters, _sort, _okCallbacks, _errorCallbacks) {
					console.info("[HotelsDetailsDataService] searchRooms", _data, _filters, _sort);
					var url = $window.END_POINT_HOTEL_ROOMS;
					//var url = $window.END_POINT_HOTEL_DETAILS;
					var parameters = HotelDetailsParserService.getDetailsRequestData(_data, _filters, _sort);

					cAccessLocal.setSessionData(_data, 'rooms', 'search');
					if (!(_.isEmpty(_filters))) {
						cAccessLocal.setSessionData(_filters, 'rooms', 'filters');
					}

					var onResponseOK = function(data) {
						console.debug("[HotelDetailsDataService]", "Response de Rooms: ", data);
						//TAGMANAGER
						try {
							//var hotel = HotelDetailsParserService.parseRoomsResults(data, parameters, _filters, _data);
							var hotel = data;
							cAccessLocal.setSessionData(hotel.rooms, 'hotel', 'rooms', 'found');
							if (!(_.isEmpty(hotel.rooms))) {
								console.info("[HotelDetailsDataService]", "Found Rooms: " + hotel.rooms);
								_okCallbacks.SEARCH_ROOMS_SUCCESS(hotel, _data.hotel);
							}
							else {
								console.warn("[HotelDetailsDataService]", "No Rooms found");
								_errorCallbacks.ERROR_NORESULT();
							}
						}
						catch (e) {
							console.error("[HotelDetailsDataService]", "Internal error: ", e.stack);
							_errorCallbacks.ERROR_INTERNAL();
						}
						return;
					};

					var onInternalError = function(error, response) {
						if (response && response.status && response.status === 404) {
							_errorCallbacks.ERROR_NORESULT();
						}
						else {
							_errorCallbacks.ERROR_INTERNAL();
						}
					};
					console.info("[HotelDetailsDataService]", "Search Rooms with data: ", parameters);
					cAccessRemote.executeHTTPGet(url + parameters, onResponseOK, onInternalError);
				}
			};
		});
});
