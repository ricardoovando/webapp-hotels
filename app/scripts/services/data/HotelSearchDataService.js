define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.services.HotelSearchDataService', [])
		.factory('HotelSearchDataService', function($location, CurrencyService, $window, $state, $stateParams, cAccessLocal, cAccessRemote, StaticDataService, HotelSearchParserService) {

			function getChildrenFromArray(rooms) {
				var children = [];
				for (var i = 1; i < rooms.length; i++) {
					children.push(rooms[i]);
				}
				return children;
			}

			return {
				getRoomSabreInfo: function(stateData) {
					var flag = false;
					var count = 0;
					_.forEach(stateData, function(value, key) {
						if (HotelSearchParserService.isValidRoom(key, stateData)) {
							var arrPassengers = value.split(",");
							if (arrPassengers.length > 1) {
								flag = true;
							}
							//count++;
							//if (count > 1) {
							//	flag = true;
							//}
						}
					});
					return (flag || (count > 1)) ? true : false;
				},
				getUrlRegionMap: function(stateParams, option) {

					var url = "";
					url += $location.protocol() + "://" + $location.host() + ":" + $location.port() + "/region-map/";
					url += stateParams.destination + "/";
					url += stateParams.arrival + "/";
					url += stateParams.departure + "";
					var count = 0;
					var separator = "?";
					_.each(stateParams, function(value, key) {
						if (HotelSearchParserService.isValidRoom(key, stateParams)) {
							if (count > 1) {
								separator = "&";
							}
							url += separator + key + "=" + stateParams[key];
							count++;
						}
					});
					url += "&option=" + option;
					return url;
				},
				getUrlHotelMap: function(stateParams, hotelId, option) {

					var url = "";
					url += $state.href('home', {}, {
						absolute: true
					}) + "hotel-map/";

					if (typeof hotelId !== 'undefined') {
						url += hotelId + "/";
					}
					else {
						url += stateParams.hotel + "/";
					}
					url += stateParams.arrival + "/";
					url += stateParams.departure + "";
					var count = 0;
					var separator = "?";
					_.each(stateParams, function(value, key) {
						if (HotelSearchParserService.isValidRoom(key, stateParams)) {
							if (count > 1) {
								separator = "&";
							}
							url += separator + key + "=" + stateParams[key];
							count++;
						}
					});
					url += "&option=" + option;
					return url;
				},

				initializeStorage: function() {
					console.info("[HotelSearchDataService]", "initializeStorage");
					try {
						cAccessLocal.deleteSessionData('hotels', 'results');
						cAccessLocal.deleteSessionData('hotels', 'booking');
						cAccessLocal.deleteSessionData('hotels', 'reservation', 'passengerDataConfig');
						cAccessLocal.deleteSessionData('hotels', 'tracking', 'trackingData');
						console.info("[HotelsDataService]", "Cleared previous hotel storage");
					}
					catch (e) {
						console.info("[HotelsDataService]", "Hotel storage is not initialized");
					}
					cAccessLocal.initializeSessionStorage('hotels', StaticDataService.getDefaultHotelsStorageData());
				},
				getPlaceDestination: function(_code, _okCallbacks, _errorCallbacks, _flagHotel) {
					console.info("[HotelsSearchDataService] searchPlace");
					var url = (typeof _flagHotel === 'undefined' || _flagHotel === null || _flagHotel === '') ? $window.END_POINT_REGION_SEARCH : $window.END_POINT_HOTEL_REGION_SEARCH;

					var parameters = HotelSearchParserService.getPlaceRequestData(_code);

					var onResponseError = function(_error, _data) {
						//ANALYTICS
						_errorCallbacks.ERROR_INTERNAL(_data);
					};

					var onResponseOK = function(_data) {
						_okCallbacks.ORIGIN_SUCCESS(_data);
					};
					console.info("[HotelSearchRegionDataService]", "Search region with data: ", parameters);
					cAccessRemote.executeHTTPGet(url + parameters, onResponseOK, onResponseError);
					//cAccessRemote.executeHTTPGetAsync(url + parameters, onResponseOK, onResponseError);
				},
				searchHotels: function(_data, _okCallback, _errorCallbacks, _filters, _sort, _flag) {
					_flag = _flag;
					console.info("[HotelsSearchDataService] searchHotels", _data, _filters, _sort);
					var url = "";
					if (!_.isUndefined(_filters) && !_.isNull(_filters) && _filters.hasOwnProperty('limit') && _filters.limit === 9999) {
						url = $window.END_POINT_HOTEL_SEARCH_NOCACHE;
					}
					else {
						url = $window.END_POINT_HOTEL_SEARCH;
					}

					var parameters = HotelSearchParserService.getSearchRequestData(_data, _filters, _sort);
					cAccessLocal.setSessionData(_data, 'hotels', 'search');
					if (!(_.isEmpty(_filters))) {
						cAccessLocal.setSessionData(_filters, 'hotels', 'filters');
					}
					var onResponseOK = function(data) {
						console.debug("[HotelsSearchDataService]", "HotelList Response: ", data, parameters);
						//TAGMANAGER
						try {
							var placeData = {};
							if (_data.hasOwnProperty('places')) {
								placeData = _data.places.destination;
							}
							//var hotels = HotelSearchParserService.parseResults(data, parameters, placeData);
							var hotels = data;
							//if (typeof _flag === 'undefined') { //normal hotel list search
							//cAccessLocal.setSessionData(hotels.found, 'hotels', 'results', 'found');
							//}
							//else {
							//cAccessLocal.setSessionData(hotels.found, 'hotels', 'results', _flag);
							//}
							if (hotels.found.length > 0) {
								console.info("[HotelsSearchDataService]", "Found hotels: " + hotels.found.length + moment().format("DD-MM-YYYY HH:mm:ss"));
								_okCallback(_data, _filters, _sort, hotels.found, hotels.total, hotels.exchangeRate);
							}
							else {
								console.warn("[HotelsSearchDataService]", "No results found");
								_errorCallbacks.ERROR_NO_RESULTS();
							}
						}
						catch (e) {
							console.error("[HotelsSearchDataService]", "Internal error: ", e.stack);
							_errorCallbacks.ERROR_INTERNAL();
						}
					};

					var onInternalError = function(error, response) {
						if (response && response.status && response.status === 404) {
							_errorCallbacks.ERROR_NO_RESULTS();
						}
						else {
							_errorCallbacks.ERROR_INTERNAL();
						}
					};
					console.info("[HotelSearchDataService]", "Search hotels with data: ", parameters);
					cAccessRemote.executeHTTPGet(url + parameters, onResponseOK, onInternalError);
				},
				addRoomsToStateParams: function(contentData, stateData) {
					var count = 1;
					_.forEach(contentData.search.rooms, function(value, key) {
						stateData['room' + count] = contentData.search.rooms[key].adults;
						if (contentData.search.rooms[key].children > 0) {
							stateData['room' + count] += "," + contentData.search.rooms[key].ages.join(",");
						}
						count++;
					});
					return stateData;
				},
				getStartFromParameters: function(stateData, content) {
					var start = 0;
					if (!(_.isEmpty(stateData.start))) {
						start = stateData.start;
					}
					else {
						if (typeof content !== 'undefined' && !(_.isEmpty(content.start))) {
							start = content.start;
						}
					}
					return parseInt(start, 10);
				},
				getLimitFromParameters: function(stateData, content) {
					var limit = 20;
					if (!(_.isEmpty(stateData.limit))) {
						limit = stateData.limit;
					}
					else {
						if (typeof content !== 'undefined' && !(_.isEmpty(content.limit))) {
							limit = content.limit;
						}
					}
					return parseInt(limit, 10);
				},
				getNumberRoomsFromParameters: function(stateData) {
					var number_rooms = 0;
					for (var k in stateData) {
						if (HotelSearchParserService.isValidRoom(k, stateData)) {
							number_rooms++;
						}
					}
					return number_rooms;
				},
				getRoomsFromParameters: function(stateData) {
					var rooms = [];
					for (var k in stateData) {
						if (HotelSearchParserService.isValidRoom(k, stateData)) {
							var arr_room = String(stateData[k]).split(",");
							rooms.push({
								adults: arr_room[0],
								children: (arr_room.length - 1),
								ages: ((arr_room.length > 1) ? getChildrenFromArray(arr_room) : [])
							});
						}
					}
					return rooms;
				},
				getAdultsFromParameters: function(stateData) {
					var number_adults = 0;
					for (var k in stateData) {
						if (HotelSearchParserService.isValidRoom(k, stateData)) {
							var arr_room = String(stateData[k]).split(",");
							if (arr_room[0] > 0) {
								number_adults += parseInt(arr_room[0], 10);
							}
						}
					}
					return number_adults;
				},
				getChildrenFromParameters: function(stateData) {
					var number_children = 0;
					for (var k in stateData) {
						if (HotelSearchParserService.isValidRoom(k, stateData)) {
							var arr_room = String(stateData[k]).split(",");
							if (arr_room.length > 1) {
								number_children += (arr_room.length - 1);
							}
						}
					}
					return number_children;
				},
				getSearchData: function() {
					return cAccessLocal.getSessionData('hotels', 'search');
				},
				getDestinationId: function() {
					return cAccessLocal.getSessionData('hotels', 'search', 'destination', 'id');
				},
				getDestinationName: function() {
					return cAccessLocal.getSessionData('hotels', 'search', 'destination', 'name');
				},
				getDateArrival: function(stateData) {
					var arrival = null;

					if (typeof stateData !== 'undefined') {
						arrival = stateData.arrival;
					}
					else if (typeof $stateParams.arrival === 'undefined') {
						//arrival = cAccessLocal.getSessionData('hotels', 'search', 'dates', 'arrival');
						var defaultData = StaticDataService.getDefaultHotelsStorageData();
						arrival = defaultData.search.dates.arrival;
					}
					else {
						arrival = $stateParams.arrival;
					}

					return arrival;
				},
				getDateDeparture: function(stateData) {
					var departure = null;
					if (typeof stateData !== 'undefined') {
						departure = stateData.departure;
					}
					else if (typeof $stateParams.departure === 'undefined') {
						//arrival = cAccessLocal.getSessionData('hotels', 'search', 'dates', 'departure');
						var defaultData = StaticDataService.getDefaultHotelsStorageData();
						departure = defaultData.search.dates.departure;
					}
					else {
						departure = $stateParams.departure;
					}

					return departure;
				},
				getType: function() {
					return cAccessLocal.getSessionData('hotels', 'search', 'type');
				},
				getCorporateCode: function() {
					return ($stateParams.corporateCode) ? $stateParams.corporateCode : null;
				},
				getReservationType: function() {
					return ($stateParams.reservationType) ? $stateParams.reservationType : null;
				},
				getRooms: function(stateData) {
					var rooms = 0;
					var data = (typeof stateData === 'undefined' ? $stateParams : stateData);
					rooms = this.getRoomsFromParameters(data);
					if (rooms.length === 0) {
						//rooms = cAccessLocal.getSessionData('hotels', 'search', 'rooms');
						var defaultValues = StaticDataService.getDefaultHotelsStorageData();
						rooms = defaultValues.search.rooms;
					}
					return rooms;
				},
				getNrAdults: function(stateData) {
					var adults = 0;
					var data = (typeof stateData === 'undefined' ? $stateParams : stateData);
					adults = this.getAdultsFromParameters(data);
					if (adults === 0) {
						var defaultValues = StaticDataService.getDefaultHotelsStorageData();
						var rooms = defaultValues.search.rooms;
						_.forEach(rooms, function(value) {
							if (value.adults > 0) {
								adults += parseInt(value.adults, 10);
							}
						});
					}
					return adults;
				},
				getNrChildren: function(stateData) {
					var children = 0;
					var data = (typeof stateData === 'undefined' ? $stateParams : stateData);
					children = this.getChildrenFromParameters(data);

					if (children === 0) {
						var defaultValues = StaticDataService.getDefaultHotelsStorageData();
						var rooms = defaultValues.search.rooms;
						_.forEach(rooms, function(value) {
							if (value.children > 0) {
								children += parseInt(value.children, 10);
							}
						});
					}
					return children;
				},
				getNrRooms: function(stateData) {
					var rooms = 0;
					var data = (typeof stateData === 'undefined' ? $stateParams : stateData);
					rooms = this.getNumberRoomsFromParameters(data);

					if (rooms === 0) {
						var defaultValues = StaticDataService.getDefaultHotelsStorageData();
						rooms = defaultValues.search.rooms.length;
						/*
						var _data = cAccessLocal.getSessionData('hotels', 'search', 'rooms');
						_.forEach(_data, function(value) {
							if (value.children > 0 || value.adults > 0) {
								rooms++;
							}
						});
						*/
					}
					return rooms;
				},
				getUrlName: function(_place) {
					//var name = ((typeof _place.source !== 'undefined') ? _place.source.name : _place.regionName);
					var name = ((_place.name) ? _place.name : _place.regionName);
					if (typeof name !== 'undefined') {
						name = name.replace(/ /g, '-');
						name = name.replace(/[^A-Za-z0-9-]+/gi, "");
						name = name.toLowerCase();
					}
					else {
						name = '';
					}
					return name;
				},
				getHotelSearchButtonData: function(stateData, placeData) {
					var searchData = {};
					searchData.arrival = stateData.arrival;
					searchData.departure = stateData.departure;
					searchData.destination = placeData.id;
					searchData.name_region = this.getUrlName(placeData);
					if (stateData.corporateCode) {
						searchData.corporateCode = stateData.corporateCode;
					}
					//searchData.full_name_region = ((placeData.hasOwnProperty('source') === 'undefined') ? placeData.source.name : placeData.regionName);
					_.each(stateData, function(value, key) {
						if (HotelSearchParserService.isValidRoom(key, stateData)) {
							searchData[key] = stateData[key];
						}
					});
					return searchData;
				}

			};

		});
});
