define(['angular'], function(angular) {
	'use strict';
	angular.module('COCHAHotels.services.HotelMapDataService', []).factory('HotelMapDataService', function($window, $timeout, $compile, HotelMapParserService, cAccessRemote, HotelSearchDataService, HotelResultsDataService) {
		var serviceMapResults = [];

		function formatMoney(number, decimals, decimal_sep, thousands_sep) {
			var n = number,
				c = isNaN(decimals) ? 2 : Math.abs(decimals), //if decimal is zero we must take it, it means user does not want to show any decimal
				d = decimal_sep || '.', //if no decimal separator is passed we use the dot as default decimal separator (we MUST use a decimal separator)
				t = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep, //if you don't want to use a thousands separator you can pass empty string as thousands_sep value
				sign = (n < 0) ? '-' : '',
				i = parseInt(n = Math.abs(n).toFixed(c)) + '',
				j = ((j = i.length) > 3) ? j % 3 : 0;
			return sign + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '');
		}

		function formatUsd(value) {
			var number = parseFloat(value);
			return number.toFixed(2);
		}

		function convertCurrency(value, exchange) {
			var number = parseFloat(value);
			number = number * parseFloat(exchange);
			return formatMoney(number, 0, ',', '.');
		}

		/* hotel map */
		function processMapData(regions, data) {
			//var map = createMap(data.layer, data.latitude, data.longitude);
			//var map = null;
			var map = data;
			var hotelLayers = [];
			for (var i = 0; i < regions.length && i < 10; i++) {
				var obj = {
					type: 'Feature',
					geometry: {
						type: 'Point',
						coordinates: [parseFloat(regions[i].longitude), parseFloat(regions[i].latitude)]
					},
					properties: {
						title: regions[i].regionName,
						'marker-size': 'medium',
						'marker-color': '#0099CC',
						'marker-symbol': (i + 1)
					}
				};
				hotelLayers.push(obj);
			}
			var main = {
				type: 'Feature',
				geometry: {
					type: 'Point',
					coordinates: [parseFloat(data.longitude), parseFloat(data.latitude)]
				},
				properties: {
					title: data.hotelName,
					'marker-size': 'large',
					'marker-color': '#333333',
					'marker-symbol': 'lodging'
				}
			};
			hotelLayers.push(main);
			return {
				map: map,
				points: hotelLayers,
				closest: regions.slice(0, 10)
			};
		}

		return {

			processHotels: function(_data, hotels) {
				for (var i = 0; i < hotels.length; i++) {

					var logo = [];
					var url = [];
					var priceUsd = [];
					var pricePesos = [];
					var tooltip = [];
					if (typeof hotels[i].rateInfo.rateForSupplier !== 'undefined') {
						/*jshint -W083 */
						_.forEach(hotels[i].rateInfo.rateForSupplier, function(value, key) {
							if (parseInt(hotels[i].rateInfo.rateForSupplier[key].status.code, 10) === 200) {
								var baseRate = (hotels[i].rateInfo.rateForSupplier[key].availability.averageRate) ? hotels[i].rateInfo.rateForSupplier[key].availability.averageRate : hotels[i].rateInfo.rateForSupplier[key].availability.averageBaseRate;
								if (hotels[i].rateInfo.rateForSupplier[key].availability.supplierCode === 'EAN') {
									logo.push(_data.logos.cocha);
									url.push(HotelResultsDataService.getDeeplinkCocha(_data.stateData, hotels[i].hotelID, hotels[i].name));
									tooltip.push('');
									priceUsd.push(formatUsd(baseRate));
									pricePesos.push(convertCurrency(baseRate, _data.currency));
								}
								else if (hotels[i].rateInfo.rateForSupplier[key].availability.supplierCode === 'BKG') {
									logo.push(_data.logos.booking);
									url.push(hotels[i].rateInfo.rateForSupplier[key].availability.deepLink);
									tooltip.push('');
									priceUsd.push(formatUsd(baseRate));
									pricePesos.push(convertCurrency(baseRate, _data.currency));
								}
								else if (hotels[i].rateInfo.rateForSupplier[key].availability.supplierCode === 'SAB') {
									logo.push(_data.logos.sabre);
									url.push('javascript' + String.fromCharCode(58) + 'void(0);');
									tooltip.push('Accede a estas Tarifas desde Turbo');
									priceUsd.push(formatUsd(baseRate));
									pricePesos.push(convertCurrency(baseRate, _data.currency));
								}
							} //end price analysis
						});
						//foreach hotel
					} //if there are prices
					hotels[i].logo = logo;
					hotels[i].url = url;
					hotels[i].priceUsd = priceUsd;
					hotels[i].pricePesos = pricePesos;
					hotels[i].tooltip = tooltip;

					try {
						var str_image = hotels[i].images[0].url;
						hotels[i].images = [];
						hotels[i].images.push(str_image);
					}
					catch (e) {
						hotels[i].images = [];
						hotels[i].images.push("img/logo_cocha.png");
					}

				}
				serviceMapResults = hotels;
				return hotels;
			},
			getProcessedHotels: function() {
				return serviceMapResults;
			},
			getRegionPoints: function(_mapData, _hotels, _okCallback) {

				if (_mapData.lastStateDataSaved !== null) {
					_mapData.lastStateDataSaved.name_hotel = '';
					_mapData.lastStateDataSaved.hotel = '';
				}
				if (_mapData.stateData !== null) {
					_mapData.stateData.name_hotel = '';
					_mapData.stateData.hotel = '';
				}
				//alert(JSON.stringify(_mapData.lastStateDataSaved) + "---" + JSON.stringify(_mapData.stateData));
				var isEqual = _.isEqual(_mapData.stateData, _mapData.lastStateDataSaved);
				if (isEqual === false) {
					_okCallback(_mapData.stateData, serviceMapResults);
				}
				else {
					$timeout(function() {
						_okCallback(_mapData.stateData, serviceMapResults);
					}, 500);
				}
			},
			getHotelPoints: function(_data, _okCallback, _errorCallbacks) {
				var callbackOK = {
					SEARCH_SUCCESS: function(_regions) {
						_okCallback.HOTEL_MAP_SEARCH(processMapData(_regions, _data));
					}
				};
				var callbackError = function() {
					//ANALYTICS
					_errorCallbacks.ERROR_INTERNAL();
				};
				var url = $window.END_POINT_INTEREST_POINTS;
				var parameters = HotelMapParserService.getSearchRequestData(_data);
				cAccessRemote.executeHTTPGet(url + parameters, callbackOK.SEARCH_SUCCESS, callbackError);
			}
		};
	});
});
