define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.services.HomeDataService', [])
		.factory('HomeDataService', function($window, cAccessRemote, HomeParserService) {
			return {

				getViewData: function() {

					return {
						loadingOffers: false,
						filterCollapse: {
							region: true,
							date: true
						},
						regions: [],
						selectors: {
							offers: {
								index: null,
								value: false
							},
							dates: {
								index: null,
								value: false
							}
						},
						viewOffers: true,
						minScreen: false,
						closeCollapsesCount: 1,
					};

				},

				getContentData: function() {
					return {
						B2Buser: null,
						selectedGroups: [],
						allTheDeals: [],
						filters: {
							category: [],
						},
						deals: [],
						offers: {
							items: []
						}
					};
				},

				getCampaign: function(_slug, _callbackOK) {
					console.info("[HomeDataService]", "getDealsSlug");
					var url = $window.CMS_MOBILE_API + "/categorias?slug=" + _slug + "&v=2";
					var callbackError = function(_error, _data) {
						console.error("[HomeDataService]", "Error getting offers: ", _data);
					};
					var parameters = {};
					var callbackOK = function(_data) {
						var campaign = HomeParserService.parseCampaign(_data);
						if (campaign) {
							_callbackOK(campaign);
						}
					};
					cAccessRemote.executeHTTPGet(url + parameters, callbackOK, callbackError);
				}

			};
		});
});
