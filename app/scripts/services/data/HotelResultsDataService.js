define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.services.HotelResultsDataService', [])
		.factory('HotelResultsDataService', function($state, $stateParams, $window, cAccessLocal, HotelSearchDataService, StaticDataService) {

			function cleanString(string) {
				string = string.replace(/ /g, '-');
				string = string.replace(/[^A-Za-z0-9-]+/gi, "");
				string = string.toLowerCase();
				return string;
			}

			return {

				getViewData: function() {
					return {
						appType: $window.APP_TYPE,
						reservationType: $stateParams.reservationType ? $stateParams.reservationType : '',
						supplierFilter: '',
						fullResults: [],
						loadingResults: true,
						collapseSelectBox: true,
						blockingFlights: {
							value: false
						},
						filters: {
							value: true,
							arrow: true
						},
						editSearch: {
							value: true,
							arrow: true
						},
						opened: {
							value: ''
						},
						mode: {
							value: 'normal'
						},
						sortOption: _.find(this.getSortCriteriaOptions(), {
							key: this.getSelectedSortCriteria()
						}),
						sortingOptions: this.getSortCriteriaOptions(),
						filterOptions: {
							priceRange: this.getPricesRange(),
							starRange: this.getStarsRange(),
							tripAdvisorRange: this.getTripAdvisorRange()
						},
						currency: 'USD',
						closeCollapsesCount: 1
					};
				},
				getContentData: function(stateData) {
					var defaultData = StaticDataService.getDefaultHotelsStorageData();
					return {
						results: null,
						numberResults: 0,
						search: {
							places: {
								destination: null
							},
							dates: {
								arrival: ((typeof stateData.arrival !== 'undefined') ? stateData.arrival : defaultData.search.dates.arrival),
								departure: ((typeof stateData.departure !== 'undefined') ? stateData.departure : defaultData.search.dates.departure),
							},
							passengers: {
								adults: HotelSearchDataService.getAdultsFromParameters(stateData),
								children: HotelSearchDataService.getChildrenFromParameters(stateData),
								rooms: HotelSearchDataService.getNumberRoomsFromParameters(stateData)
							},
							rooms: HotelSearchDataService.getRoomsFromParameters(stateData),
							start: HotelSearchDataService.getStartFromParameters(stateData),
							limit: HotelSearchDataService.getLimitFromParameters(stateData),
							corporateCode: HotelSearchDataService.getCorporateCode(),
							reservationType: HotelSearchDataService.getReservationType()
						},
						filters: {
							start: 0,
							limit: 20
						},
						//sort: 'rating2',
						sort: 'default',
						currency: 0,
						flagResetFilters: false,
						regionMapData: {
							currency: 1,
							logos: StaticDataService.getLogosSuppliers(),
							stateData: null,
							layer: 'div-region-map',
							lastStateDataSaved: null
						}
					};
				},
				getResultsFound: function(resultsFound, forceLoad) {
					var results;
					if (_.isEmpty(resultsFound) || typeof resultsFound === 'undefined') {
						if (forceLoad) {
							results = cAccessLocal.getSessionData('hotels', 'results', 'found');
						}
						else {
							results = [];
						}
					}
					else {
						results = resultsFound;
					}
					return results;
				},
				getDeeplinkCocha: function(stateData, hotelId, strHotelName) {
					var strRooms = '';
					_.each(stateData, function(value, key) {
						if (key.search("room") !== -1 && typeof stateData[key] !== 'undefined' && stateData[key] !== null) {
							strRooms += "&" + key + "=" + stateData[key];
						}
					});

					var deepLink = $state.href('home', {}, {
						absolute: true
					}) + "hotel/" + cleanString(strHotelName) + "/" + hotelId + "/" + stateData.arrival + "/" + stateData.departure + "?" + strRooms;
					return deepLink;
				},
				getHotelNameResultsFound: function(location) {
					var resultsFound = [];
					try {
						return cAccessLocal.getSessionData('hotels', 'results', location);
					}
					catch (err) {
						return resultsFound;
					}
				},

				getSortCriteriaOptions: function() {
					return [{
						key: 'price',
						value: 'Precio (Menor a Mayor)'
					}, {
						key: '-price',
						value: 'Precio (Mayor a Menor)'
					}, {
						key: '-stars',
						value: 'Estrellas (Menor a Mayor)'
					}, {
						key: 'stars',
						value: 'Estrellas (Mayor a Menor)'
					}, {
						key: '-rating',
						value: 'TripAdvisor (Menor a Mayor)'
					}, {
						key: 'rating',
						value: 'TripAdvisor (Mayor a Menor)'
					}, {
						key: 'default',
						//key: 'rating2',
						value: 'Ranking'
					}];
					/*
                    {
                        key: 'promotion',
                        value: 'Ofertas'
                    },					 
					 */
				},
				getFiltersParams: function(_type) {
					return cAccessLocal.getSessionData('hotels', 'results', 'filters', _type, 'applied');
				},
				getPricesRange: function() {
					return [0, 1000];
				},
				getStarsRange: function() {
					return [1, 5];
				},
				getTripAdvisorRange: function() {
					return [1, 5];
				},
				getSelectedSortCriteria: function() {
					var criteria = null;
					try {
						criteria = cAccessLocal.getSessionData('hotels', 'results', 'sort', 'criteria');
					}
					catch (err) {
						//criteria = 'rating2';
						criteria = 'default';
					}
					return criteria;
				},
				setSelectedSortCriteria: function(_criteria) {
					cAccessLocal.setSessionData(_criteria, 'hotels', 'results', 'sort', 'criteria');
				},
				setFiltersParams: function(_params) {
					cAccessLocal.setSessionData(_params, 'hotels', 'results', 'filters', 'arrival', 'applied');
					cAccessLocal.setSessionData(_params, 'hotels', 'results', 'filters', 'departure', 'applied');
				}
			};
		});
});
