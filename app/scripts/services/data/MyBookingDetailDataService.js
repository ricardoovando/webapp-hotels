define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.services.MyBookingDetailDataService', [])
		.factory('MyBookingDetailDataService', function($window, B2BDataService, MyBookingParserService, cAccessRemote, cAccessLocal) {
			/*
			function isEmailValid(_email) {
				var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				return re.test(_email);
			}*/

			return {
				getContentData: function() {
					return {
						isB2B: ((B2BDataService !== null) ? true : false),
						result: null,
						supplier: null,
						spnr: null,
						email: '',
						itineraryId: null,
						reason: 'COP'
					};
				},
				getViewData: function() {
					return {
						loadingResult: true,
						modalLoader: true,
						cancellationLoader: false,
						properWidth: true,
						smallView: false,
						hotelInformation: true,
						closestPoints: [],
						point: null,
						modalText: '',
						errors: {
							sendVoucher: false
						},
						roomDescription: {}
					};
				},
				getBookingData: function() {
					var bookingData = null;
					try {
						bookingData = cAccessLocal.getStorageData('mybooking', 'bookingData');
						cAccessLocal.deleteStorageData('mybooking', 'bookingData');
						cAccessLocal.setSessionData(bookingData, 'mybooking', 'bookingData');
					}
					catch (err) {
						try {
							bookingData = cAccessLocal.getSessionData('mybooking', 'bookingData');
						}
						catch (err) {
							console.info("[MyBookingResultsDataService] bookingData not found on localstorage");
						}
					}
					return bookingData;
				},
				sendVoucher: function(_data, _okCallback, _errorCallbacks) {
					console.info("[MyBookingResultsDataService] sendVoucher", _data);
					var parameters = MyBookingParserService.getParameters(_data, 'sendVoucher');
					var url = $window.END_POINT_SEND_VOUCHER;
					var onResponseOK = function(data) {
						console.debug("[MyBookingResultsDataService]", "Response send voucher: ", data);
						try {
							if (_.isNull(data) || _.isEmpty(data)) {
								_errorCallbacks.ERROR_SEND_VOUCHER();
							}
							if (data.hasOwnProperty('error') && data.error) {
								_errorCallbacks.ERROR_SEND_VOUCHER();
							}
							else if (data.response === "OK") {
								console.info("[MyBookingResultsDataService]", "Response send voucher: ", data);
								_okCallback(data);
							}
							else {
								console.error("[MyBookingResultsDataService]", "Internal error: invalid response " + JSON.stringify(data));
								_errorCallbacks.ERROR_SEND_VOUCHER();
							}
						}
						catch (e) {
							console.error("[MyBookingResultsDataService]", "Internal error: ", e.stack);
							_errorCallbacks.ERROR_INTERNAL();
						}
					};
					var onInternalError = function() {
						_errorCallbacks.ERROR_INTERNAL();
					};
					console.info("[MyBookingResultsDataService]", "Send voucher with data: ", url + parameters);
					cAccessRemote.executeHTTPGet(url + parameters, onResponseOK, onInternalError);
				},
				getBookingDetails: function(_data, _okCallback, _errorCallbacks) {
					console.info("[MyBookingResultsDataService] getBookingDetails", _data);
					var parameters = MyBookingParserService.getParameters(_data, 'bookingDetails');
					var url = $window.END_POINT_SEARCH_BOOKING_DETAILS;
					var onResponseOK = function(data) {
						console.debug("[MyBookingResultsDataService]", "Response booking details: ", data);
						try {
							if (_.isNull(data) || _.isEmpty(data)) {
								_errorCallbacks.ERROR_NORESULT();
							}
							if (data.hasOwnProperty('error') && data.error) {
								_errorCallbacks.ERROR_INTERNAL();
							}
							else {
								console.info("[MyBookingResultsDataService]", "Found details: ", data);
								_okCallback(data);
							}
						}
						catch (e) {
							console.error("[MyBookingResultsDataService]", "Internal error: ", e.stack);
							_errorCallbacks.ERROR_INTERNAL();
						}
					};
					var onInternalError = function() {
						_errorCallbacks.ERROR_INTERNAL();
					};
					console.info("[MyBookingResultsDataService]", "Search booking details with data: ", url + parameters);
					cAccessRemote.executeHTTPGet(url + parameters, onResponseOK, onInternalError);
				},
				cancelBooking: function(_data, _okCallback, _errorCallbacks) {
					console.info("[MyBookingDetailDataService] cancelBooking", _data);
					var parameters = MyBookingParserService.getParameters(_data, 'cancelBooking');
					var url = $window.END_POINT_CANCEL_BOOKING;
					var onResponseOK = function(data) {
						console.debug("[MyBookingDetailDataService]", "Response cancellation booking: ", data);
						try {
							if (data.hasOwnProperty('error') && data.error) {
								_errorCallbacks.ERROR_INTERNAL();
							}
							else if (typeof(data.cancellationNumber) === 'undefined' || _.isNull(data.cancellationNumber) || data.cancellationNumber === '') {
								_errorCallbacks.ERROR_INTERNAL();
							}
							else {
								console.info("[MyBookingDetailDataService]", "cancellation response:", data);
								_okCallback(data);
							}
						}
						catch (e) {
							console.error("[MyBookingDetailDataService]", "Internal error: ", e.stack);
							_errorCallbacks.ERROR_INTERNAL();
						}
					};
					var onInternalError = function() {
						_errorCallbacks.ERROR_INTERNAL();
					};
					console.info("[MyBookingDetailDataService]", "cancel booking with data:", url + parameters);
					cAccessRemote.executeHTTPGet(url + parameters, onResponseOK, onInternalError);
				},
				getVoucherUrl: function(_params, _type) {
					if (_type === 'get') {
						_params.disposition = 'attachment';
					}
					else {
						_params.disposition = 'inline';
					}
					var parameters = MyBookingParserService.getParameters(_params, 'getVoucherUrl');
					var url = $window.END_POINT_GET_VOUCHER;
					return (url + parameters);
				}
			};
		});
});
