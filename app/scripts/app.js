define([
		'angular',
		//App's
		//Controllers
		'controllers/HomeController',
		'controllers/HotelResultsController',
		'controllers/HotelDetailsController',
		'controllers/HotelReservationController',
		'controllers/HotelPaymentController',
		'controllers/HotelConfirmationController',
		//'controllers/HeaderController',
		//'controllers/FooterController',
		'controllers/utils/PageNotFoundController',
		'controllers/utils/ModalController',
		'controllers/MyBookingHomeController',
		'controllers/MyBookingResultsController',
		'controllers/MyBookingDetailController',

		//Directive		
		'directives/HotelSearchBoxDirective',
		'directives/HotelSearchInfoDirective',
		'directives/HotelSearchFiltersDirective',
		'directives/HotelCategoryDirective',
		'directives/MainLoaderDirective',

		'directives/SelectOnClickDirective',
		'directives/SmoothScrollDirective',
		'directives/HotelResultDirective',
		'directives/HotelAmenitiesDirective',
		'directives/HotelResultPaginationDirective',

		'directives/HotelRoomsDirective',
		'directives/HotelProgramDirective',

		'directives/TripAdvisorRatingDirective',
		'directives/OutsideClickDirective',
		'directives/OnLoad',
		'directives/MobileTap',

		'directives/StarsRatingDirective',
		'directives/HotelDetailsFiltersDirective',
		'directives/ForceUppercaseDirective',
		'directives/BackgroundFallbackDirective',
		'directives/MyBookingSearchBoxDirective',

		//Services
		'services/data/AppCommonService',
		'services/data/AnalyticsService',

		'services/data/GeoSearchDataService',
		'services/data/HotelMapDataService',
		'services/data/HomeDataService',

		'services/data/HotelSearchDataService',
		'services/data/StaticDataService',
		'services/data/CurrencyService',
		'services/data/HotelResultsDataService',
		'services/data/HotelDetailsDataService',
		'services/data/HotelReservationDataService',
		'services/data/B2BDataService',
		'services/data/MyBookingResultsDataService',
		'services/data/MyBookingDetailDataService',
		'services/data/QuoteService',
		'services/parsing/MyQuotingParserService',

		'services/parsing/GeoSearchParserService',
		'services/parsing/HotelSearchParserService',
		'services/parsing/HotelDetailsParserService',
		'services/parsing/HotelReservationParserService',
		'services/parsing/HotelMapParserService',
		'services/parsing/HomeParserService',
		'services/parsing/MyBookingParserService',

		//Filter
		'filters/ObjectKeyLengthFilter',
		'filters/USDtoCLPFilter',
		'filters/trustUrlFilter',
		'filters/moneyFilter',
		'filters/dateFormatFilter',
		'filters/ComplementaryData',
	] /*deps*/ ,
	function(angular)
	/*invoke*/
	{
		'use strict';
		return angular
			.module('COCHAHotels', [
				//Vendor modules
				'ui.router',
				'ngAnimate',
				'ngTouch',
				'ngStorage',
				'ngMap',
				'ngCookies',
				'ui.bootstrap',
				'ui.slider',
				'dcbImgFallback',
				//Cocha modules
				'cCommonServices',
				'cAccessRemote',
				'cAccessLocal',
				'cUtilsData',
				'cUtilsFilters',
				'cCommonConstants',
				'cUtilsTranslation',
				'cCalendarDirective',
				'cHeaderFooter',
				'ui.mask',
				//App

				//Directives
				'COCHAHotels.directives.HotelSearchBoxDirective',
				'COCHAHotels.directives.HotelSearchInfoDirective',
				'COCHAHotels.directives.HotelSearchFiltersDirective',
				'COCHAHotels.directives.MainLoaderDirective',

				'COCHAHotels.directives.SelectOnClickDirective',
				'COCHAHotels.directives.SmoothScrollDirective',
				'COCHAHotels.directives.HotelResultDirective',
				'COCHAHotels.directives.HotelCategoryDirective',
				'COCHAHotels.directives.HotelRoomsDirective',
				'COCHAHotels.directives.HotelProgramDirective',
				'COCHAHotels.directives.TripAdvisorRatingDirective',
				'COCHAHotels.directives.OutsideClickDirective',
				'COCHAHotels.directives.StarsRatingDirective',
				'COCHAHotels.directives.HotelDetailsFiltersDirective',
				'COCHAHotels.directives.HotelAmenitiesDirective',
				'COCHAHotels.directives.ForceUppercaseDirective',
				'COCHAHotels.directives.BackgroundFallbackDirective',
				'COCHAHotels.directives.HotelResultPaginationDirective',
				'COCHAHotels.directives.OnLoad',
				'COCHAHotels.directives.MobileTap',

				'COCHAHotels.directives.MyBookingSearchBoxDirective',

				//Pages controllers
				'COCHAHotels.controllers.HomeController',
				'COCHAHotels.controllers.HotelResultsController',
				'COCHAHotels.controllers.HotelDetailsController',
				'COCHAHotels.controllers.HotelReservationController',
				'COCHAHotels.controllers.HotelPaymentController',
				'COCHAHotels.controllers.HotelConfirmationController',
				//'COCHAHotels.controllers.HeaderController',
				//'COCHAHotels.controllers.FooterController',
				'COCHAHotels.controllers.MyBookingHomeController',
				'COCHAHotels.controllers.MyBookingResultsController',
				'COCHAHotels.controllers.MyBookingDetailController',

				//Utils controllers
				'COCHAHotels.controllers.PageNotFoundController',
				'COCHAHotels.controllers.ModalController',

				//Services
				'COCHAHotels.services.AppCommonService',
				'COCHAHotels.services.AnalyticsService',
				'COCHAHotels.services.HomeDataService',
				'COCHAHotels.services.StaticDataService',
				'COCHAHotels.services.CurrencyService',
				'COCHAHotels.services.GeoSearchDataService',

				'COCHAHotels.services.HotelSearchDataService',
				'COCHAHotels.services.HotelResultsDataService',
				'COCHAHotels.services.HotelDetailsDataService',
				'COCHAHotels.services.HotelReservationDataService',
				'COCHAHotels.services.HotelMapDataService',
				'COCHAHotels.services.B2BDataService',
				'COCHAHotels.services.MyBookingResultsDataService',
				'COCHAHotels.services.MyBookingDetailDataService',
				'COCHAHotels.services.QuoteService',
				'COCHAHotels.services.MyQuotingParserService',
				//Parser services
				'COCHAHotels.services.HomeParserService',
				'COCHAHotels.services.HotelMapParserService',
				'COCHAHotels.services.GeoSearchParserService',
				'COCHAHotels.services.HotelSearchParserService',
				'COCHAHotels.services.HotelDetailsParserService',
				'COCHAHotels.services.HotelReservationParserService',
				'COCHAHotels.services.MyBookingParserService',

				//Filters
				'COCHAHotels.filters.USDtoCLPFilter',
				'COCHAHotels.filters.ObjectKeyLengthFilter',
				'COCHAHotels.filters.trustUrlFilter',
				'COCHAHotels.filters.moneyFilter',
				'COCHAHotels.filters.dateFormatFilter',
				'COCHAHotels.filters.ComplementaryData'
			])
			.config(function($cookiesProvider, $locationProvider, $windowProvider, $stateProvider, $compileProvider, $urlRouterProvider, B2BDataServiceProvider, cAccessRemoteProvider, cUtilsTranslationProvider, StaticDataServiceProvider, cAnalyticsProvider, cMetadataProvider) {
				var expireDate = new Date();
				expireDate.setDate(expireDate.getDate() + 30);
				$cookiesProvider.defaults.path = '/';
				$cookiesProvider.defaults.domain = 'cocha.com';
				$cookiesProvider.defaults.expires = expireDate;
				$locationProvider.html5Mode(true); // use the HTML5 History API
				$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);

				if ($windowProvider.$get().APP_NAME === "HT.B2C.COCHA.COM") {
					$compileProvider.debugInfoEnabled(false);
				}

				cAnalyticsProvider.setAppName($windowProvider.$get().PRODUCT_NAME);

				cUtilsTranslationProvider.setTranslations(StaticDataServiceProvider.$get().getTranslations());

				cMetadataProvider.setDefaultTitle(StaticDataServiceProvider.$get().DEFAULT_TITLE);
				cMetadataProvider.setDefaultDescription(StaticDataServiceProvider.$get().DEFAULT_DESCRIPTION);
				cMetadataProvider.setDefaultKeywords(StaticDataServiceProvider.$get().DEFAULT_KEYWORDS);
				cMetadataProvider.setDefaultSocialImage($windowProvider.$get().DEFAULT_SOCIAL_IMAGE);

				var headerView = {
					templateUrl: "views/partials/main-header.html",
					controller: "HeaderController"
				};
				var footerView = {
					templateUrl: "views/partials/main-footer.html",
					controller: "FooterController"
				};
				var reservationHeaderView = {
					templateUrl: "views/partials/checkout-header.html"
				};
				var reservationFooterView = {
					templateUrl: "views/partials/checkout-footer.html"
				};
				var cookieName = $windowProvider.$get().COOKIE_NAME;
				var hotelUser = B2BDataServiceProvider.readCookie(cookieName);
				if (!_.isUndefined(hotelUser) && !_.isNull(hotelUser)) {
					B2BDataServiceProvider.setUser(hotelUser);
					headerView = null;
					footerView = null;
				}

				var quote = $windowProvider.$get().QUOTE;
				if (typeof quote !== 'undefined' && quote !== '0') {
					headerView = null;
					footerView = null;
					var head = document.getElementById('headerView');
					if (head) {
						document.getElementById('headerView').innerHTML = '';
					}
					var foot = document.getElementById('footerView');
					if (foot) {
						document.getElementById('footerView').innerHTML = '';
					}
				}

				var searchboxViews = {
					"body": {
						templateUrl: "views/pages/home.html",
						controller: "HomeController"
					}
				};
				var hotelResultsViews = {
					"body": {
						templateUrl: "views/pages/hotel-list-results.html",
						controller: "HotelResultsController"
					}
				};
				var roomResultsViews = {
					"body": {
						templateUrl: "views/pages/hotel-details.html",
						controller: "HotelDetailsController"
					}
				};
				var reservationViews = {
					"body": {
						templateUrl: "views/pages/reservation.html",
						controller: "HotelReservationController"
					}
				};
				var MyBookingHomeViews = {
					"body": {
						templateUrl: "views/pages/my-booking/home.html",
						controller: "MyBookingHomeController"
					}
				};
				var MyBookingResultsViews = {
					"body": {
						templateUrl: "views/pages/my-booking/search-result.html",
						controller: "MyBookingResultsController"
					}
				};
				var MyBookingDetailViews = {
					"body": {
						templateUrl: "views/pages/my-booking/booking-details.html",
						controller: "MyBookingDetailController"
					}
				};

				if (window.QUOTE && window.QUOTE !== '0') {
					headerView = null;
					footerView = null;
				}

				if (headerView !== null) {
					searchboxViews.header = headerView;
					hotelResultsViews.header = headerView;
					roomResultsViews.header = headerView;
					reservationViews.header = reservationHeaderView;
					MyBookingHomeViews.header = headerView;
					MyBookingResultsViews.header = headerView;
					MyBookingDetailViews.header = headerView;
				}
				if (footerView !== null) {
					searchboxViews.footer = footerView;
					hotelResultsViews.footer = footerView;
					roomResultsViews.footer = footerView;
					reservationViews.footer = reservationFooterView;
					MyBookingHomeViews.footer = footerView;
					MyBookingResultsViews.footer = footerView;
					MyBookingDetailViews.footer = footerView;
				}

				//DOCS: https://github.com/angular-ui/ui-router/wiki
				$stateProvider
					.state('home', {
						url: "/",
						views: searchboxViews
					})
					.state('my-booking', {
						url: "/mi-reserva",
						views: MyBookingHomeViews
					})
					.state('my-booking-default', {
						url: "/mi-reserva/:emptyparam",
						views: MyBookingHomeViews
					})
					.state('results-booking', {
						url: "/resultados-reserva/",
						views: MyBookingResultsViews,
						params: {
							refresh: null
						}
					})
					.state('my-booking-detail', {
						url: "/detalle-reserva/",
						views: MyBookingDetailViews
					})
					.state('hotel-results', {
						url: "/hoteles/:name_region/:destination/:arrival/:departure?room1&room2&room3&room4&room5&room6&room7&room8&room9&start&limit&corporateCode&reservationType",
						views: hotelResultsViews
					})
					.state('hotel-results-dateless', {
						url: "/hoteles/:name_region/:destination?room1&room2&room3&room4&room5&room6&room7&room8&room9&start&limit&corporateCode&reservationType",
						views: hotelResultsViews
					})
					.state('hotel-details', {
						url: "/hotel/:name_hotel/:hotel/:arrival/:departure?room1&room2&room3&room4&room5&room6&room7&room8&room9&corporateCode&fid&reservationType",
						views: roomResultsViews
					})
					.state('hotel-details-dateless', {
						url: "/hotel/:name_hotel/:hotel?room1&room2&room3&room4&room5&room6&room7&room8&room9&corporateCode&fid&reservationType",
						views: roomResultsViews
					})
					.state('checkout', {
						url: "/checkout/:force?fid",
						views: reservationViews
					})
					.state('hotel-payment', {
						url: "/pago/:token",
						views: {
							"body": {
								templateUrl: "views/pages/hotel-payment.html",
								controller: "HotelPaymentController"
							}
						}
					})
					.state('hotel-payment-b2b', {
						url: "/pagob2b/",
						views: {
							"body": {
								templateUrl: "views/pages/hotel-payment.html",
								controller: "HotelPaymentController"
							}
						}
					})
					.state('hotel-confirmation', {
						url: "/confirmacion",
						views: {
							"body": {
								templateUrl: "views/pages/hotel-confirmation.html",
								controller: "HotelConfirmationController"
							}
						}
					})
					.state('no-encontrado', {
						url: "/no-encontrado",
						views: {
							"body": {
								templateUrl: "views/utils/error-404.html",
								controller: "PageNotFoundController"
							}
						}
					});
				$urlRouterProvider.when('/', '');
				$urlRouterProvider.otherwise('/no-encontrado');
			})
			.run(function($window, $rootScope, $timeout, $location) {
				console.info("[App]", "App running");

				$window.cotizacion = (!$location.search().cotizacion) ? null : $location.search().cotizacion;

				$rootScope.showheader = true;
				$rootScope.showfooter = true;
				if ($window.cotizacion) {
					$rootScope.showheader = false;
					$rootScope.showfooter = false;
				}

				$rootScope.$on('$stateChangeSuccess', function() {
					$timeout(function() {
						/*
						var url, urlArray = $window.location.href.split('cocha.com');
						if (urlArray.length > 1) {
							url = urlArray[1];
						}
						else {
							url = '/';
						}

						urlArray = url.split('/busqueda/');
						if (urlArray.length > 1) {
							urlArray = urlArray[1].split('/');
							url = url + (url.indexOf("?") > -1 ? '&' : '?') + 'q_destino=' + urlArray[0] + '-' + urlArray[1] + '&q_categoria=Flights';
						}

						ga('set', 'page', url);
						ga('send', 'pageview');
						ga('cocha.set', 'page', url);
						ga('cocha.send', 'pageview');
						console.info("Page View", url);
						*/
					}, 300);
				});

				moment.locale('es', {
					monthsShort: function(momentToFormat) {
						return 'ene_feb_mar_abr_may_jun_jul_ago_sep_oct_nov_dic'.split('_')[momentToFormat.month()];
					},
					weekdaysShort: function(momentToFormat) {
						return 'dom_lun_mar_mié_jue_vie_sáb'.split('_')[momentToFormat.day()];
					}
				});
			});
	});
