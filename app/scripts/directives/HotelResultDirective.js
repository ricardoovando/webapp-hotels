define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.directives.HotelResultDirective', []).directive('hotelResult', function($window, $location, $anchorScroll, $timeout, $sce, HotelSearchDataService, HotelResultsDataService, HotelDetailsDataService, AnalyticsService, $uibModal, $state, $stateParams, QuoteService) {
		return {
			restrict: 'E',

			templateUrl: 'views/directives/hotel-result.html',
			controller: function($scope) {
				var sortTimer = null;

				var callbacksOK = {
					SEARCH_ROOMS_SUCCESS: function(hotel, idHotel) {
						if (hotel.lowest > 0 && hotel.lowest) {
							$scope.view.lowestPriceHotel[idHotel] = hotel.lowest;
							$scope.view.lowestSupplierHotel[idHotel] = hotel.lowestId;
						}
						$scope.view.detailsLoader[idHotel] = false;
						$scope.content.hotelDetails[idHotel] = hotel;
					}
				};

				var callbacksError = {
					ERROR_NORESULT: noRoomsResultsError,
					ERROR_RESERVATION: internalError,
					ERROR_PAYMENTURL: internalError,
					ERROR_NETWORK: networkError,
					ERROR_INTERNAL: internalError,
					ERROR_AVAILABILITY: noRoomAvailabilityError
				};
				$scope.view.lowestPriceHotel = {};
				$scope.view.lowestSupplierHotel = {};
				$scope.view.collapseAmenity = {};
				$scope.view.imageRoomSupplier = '';
				$scope.view.loader = true;
				$scope.view.detailsCollapse = {};
				$scope.view.detailsLoader = {};
				$scope.view.loadingResults = true;
				$scope.view.loaderCheckoutIndex = {};
				$scope.view.collapseSelectBox = true;
				$scope.view.env = $window.ENV;
				$scope.view.goToDetailLoader = false;
				$scope.view.textCategory = "<span class='category-title'>Cocha Preferred Partners</span> ";
				$scope.view.textCategory += "es un conjunto de hoteles con alianzas especiales que nos permiten entregar beneficios exclusivos a nuestros clientes, tales como: <br> ";
				$scope.view.textCategory += "<ul>";
				$scope.view.textCategory += "<li> - Upgrade de habitación </li> ";
				$scope.view.textCategory += "<li> - Early Check - in / Late Check-out</li>";
				$scope.view.textCategory += "<li> - Food & beverage credit / Spa services credit </li>";
				$scope.view.textCategory += "<li> - Complimentary wifi, comidas y transfer </li>";
				$scope.view.textCategory += "<li> - Entre otros servicios, descuentos y cortesías </li>";
				$scope.view.textCategory += "</ul>";
				$scope.view.textCategory += "<p>​La lista anterior es sujeta a disponibilidad y variará por cada hotel. Para mayor información contacta al Departamento Lujo. </p>";
				$scope.view.textCategory += "Para acceder a estos beneficios revisa las tarifas de SABRE en VER DETALLES.";

				$scope.content.hotelDetails = {};
				$scope.content.urlHotelMap = null;
				$scope.content.result = null;
				$scope.content.mapData = $stateParams;

				addListeners();

				function addListeners() {
					$scope.$watch('content.results', function() {
						$scope.view.detailsCollapse = {};
						$scope.content.hotelDetails = {};
						$scope.view.lowestPriceHotel = {};
						$scope.view.lowestSupplierHotel = {};
						$scope.$applyAsync();
					});
					$scope.$watch('view.loadingResults', function(newValue) {
						$scope.view.loadingResults = newValue;
						$scope.view.detailsCollapse = {};
						$scope.content.hotelDetails = {};
						$scope.$applyAsync();
					});
				}

				$scope.getImageName = function(urlImage) {
					if (typeof urlImage !== 'undefined' && urlImage.length > 0 && urlImage !== '') {
						var ext = urlImage.substring(urlImage.lastIndexOf('.') + 1);
						var type = urlImage.substring(urlImage.length - ((ext.length) + 3), urlImage.length - (ext.length + 1));
						if (type !== '_b') {
							return urlImage;
						}

						var image = urlImage.substring(0, urlImage.length - ((ext.length) + 3));
						return (image + '_y.' + ext);
					}
					else {
						return "error.jpg";
					}
				};

				$scope.isOnQuotation = function() {
					if (!$window.QUOTE || $window.QUOTE === '0') {
						return false;
					}
					return true;
				};

				$scope.saveQuote = function(flagBooking, room, roomPosition, hotelPosition, doneFn) {
					room.tripAdvisorUrl = $scope.content.results[hotelPosition].tripAdvisorUrl || null;

					var quoteCbOK = function(_response) {
						doneFn();
						try {
							top.postMessage('OK', $window.QUOTE_URL);
						}
						catch (e) {
							console.error('sin comunicacion a crm');
						}

						var totalPaxs = totalAdults() + totalChildrens();

						$uibModal.open({
							size: ($scope.view.smallView ? 'xs' : 'lg'),
							templateUrl: 'quote-success',
							windowClass: 'common-modal',
							controller: 'ModalController',
							resolve: {
								content: function() {
									return {
										prices: _response.product.prices,
										assistance: {
											usd: ($scope.view.assistanceChoice ? $scope.content.assistancePrice.usd.perPerson * totalPaxs : 0),
											clp: ($scope.view.assistanceChoice ? $scope.content.assistancePrice.clp.perPerson * totalPaxs : 0)
										},
										transfer: {
											usd: ($scope.view.transferChoice ? $scope.content.transfer.prices.usd.perPerson * totalPaxs : 0),
											clp: ($scope.view.transferChoice ? $scope.content.transfer.prices.clp.perPerson * totalPaxs : 0)
										},
										adults: totalAdults(),
										children: totalChildrens(),
										msg: _response.msg,
										currency: 'USD'
									};
								}
							}
						});
					};

					QuoteService.sendQuote({
						flagBooking: flagBooking,
						room: room,
						roomPosition: roomPosition,
						hotelPosition: hotelPosition,
						hotel: $scope.content.results[hotelPosition],
						quote: $window.QUOTE,
						regionName: $scope.content.search.places.destination.regionNameLong
					}, quoteCbOK, callbacksError, doneFn);
				};

				$scope.saveHotelData = function(data, position) {
					$scope.view.goToDetailLoader = {};
					$scope.view.goToDetailLoader[data.request.hotel] = true;
					$scope.$applyAsync();
					HotelDetailsDataService.saveHotelData(data.request);
					AnalyticsService.productClick(data, position, $scope.content.filters.start);
					if ($stateParams.corporateCode && $stateParams.corporateCode !== '') {
						data.request.corporateCode = $stateParams.corporateCode;
					}

					if ($stateParams.reservationType) {
						data.request.reservationType = $stateParams.reservationType;
					}

					data.request.fid = $window.FLOW_ID;
					if ($window.QUOTE && $window.QUOTE !== '0') {
						$state.go('hotel-details', data.request);
					}
					else {
						$timeout(function() {
							data.request.fid = $window.FLOW_ID;
							$window.open($state.href('hotel-details', data.request), '_blank');
							$scope.view.goToDetailLoader = false;
							$scope.$applyAsync();
						}, 1800);
					}
				};

				$scope.checkout = function(flagBooking, room, roomPosition, hotelPosition) {

					hotelPosition = hotelPosition;
					if (typeof room !== 'undefined') {
						if (flagBooking) {
							AnalyticsService.cart($scope.content.results[hotelPosition], $scope.content.results[hotelPosition].request, roomPosition, room);
							$window.open(room.deepLink, '_blank');
						}
						else {
							AnalyticsService.cart($scope.content.results[hotelPosition], $scope.content.results[hotelPosition].request, roomPosition, room);
							HotelDetailsDataService.saveRoomData(room);
							$timeout(function() {
								$window.open($state.href('checkout', {
									force: 1,
									fid: $window.FLOW_ID
								}, {
									absolute: true
								}), '_blank');
							}, 2000);
						}
					}

				};

				$scope.checkImageSupplier = function(supplierCode, isFirst) {
					if (isFirst) {
						$scope.view.imageRoomSupplier = '';
					}
					if ($scope.view.imageRoomSupplier !== supplierCode) {
						$scope.view.imageRoomSupplier = supplierCode;
						return true;
					}
					else {
						return false;
					}
				};

				$scope.sortResults = function(option) {
					$scope.view.collapseSelectBox = !$scope.view.collapseSelectBox;
					//$scope.selectedSort = option;
					$scope.view.sortOption = option;
					//AnalyticsService.sortResults(option.value);
					//$scope.view.loadingResults = true;
					if (sortTimer) {
						$timeout.cancel(sortTimer);
						sortTimer = null;
					}
					sortTimer = $timeout(function() {
						var oldCriteria = $scope.content.sort;
						var newCriteria = option.key;
						if (oldCriteria !== newCriteria) {
							$scope.content.sort = option.key;
							if ($scope.content.filters.hasOwnProperty("preferredHotelId")) {
								$scope.content.filters.preferredHotelId = '';
							}
							$scope.content.filters.start = 0;
							$scope.content.filters.limit = 20;
							$scope.filterFn(
								$scope.content.filters,
								$scope.content.sort
							);
							$scope.$applyAsync();
						}
						sortTimer = null;
					}, 700);
				};

				$scope.collapseSelectBox = function(keyEvent, force) {
					//if ($scope.view.closeCollapsesCount === 1) {
					//    closeCollapses();
					//    $scope.view.closeCollapsesCount++;
					//}
					if (typeof keyEvent !== 'undefined') {
						keyEvent.stopImmediatePropagation();
					}
					//AnalyticsService.openSortSelector($$scope.view.collapseSelectBox);
					if (typeof force !== 'undefined') {
						$scope.view.collapseSelectBox = true;
					}
					else {
						$scope.view.collapseSelectBox = !$scope.view.collapseSelectBox;
					}
				};

				$scope.getHotelMap = function(requestData, hotelId, hotelName, latitude, longitude, rating, address) {
					//$scope.content.urlHotelMap = HotelSearchDataService.getUrlHotelMap(requestData, hotelId);
					$scope.view.loader = true;
					$scope.view.mapType = 'hotel';
					$scope.content.mapData.hotel = hotelId;
					$scope.content.mapData.hotelName = hotelName;
					$scope.content.mapData.latitude = latitude;
					$scope.content.mapData.longitude = longitude;
					$scope.content.mapData.rating = rating;
					$scope.content.mapData.address = address;
					//$scope.content.mapData.layer = 'div-hotel-map';
					//$scope.content.mapData.layer = 'hotel-map';
					$uibModal.open({
						scope: $scope,
						size: 'lg',
						templateUrl: 'hotel-template-map',
						controller: 'ModalController',
						resolve: {
							content: function() {
								return null;
							}
						}
					});
				};

				$scope.showRooms = function(request) {
					$scope.view.detailsLoader[request.hotel] = true;
					var status = toggleDetails(request.hotel);
					if (status) { //if i have to open the detail layer
						if ($scope.content.hotelDetails.hasOwnProperty(request.hotel)) {
							$scope.view.detailsLoader[request.hotel] = false;
						}
						else {
							var params = {};
							if (!_.isEmpty($scope.content.searchFilters.supplier)) {
								params.supplier = $scope.content.searchFilters.supplier;
							}
							if (!_.isEmpty($scope.content.searchFilters.paymentType)) {
								params.paymentType = $scope.content.searchFilters.paymentType;
							}
							params.showLimited = true;
							if ($stateParams.corporateCode && $stateParams.corporateCode !== '') {
								params.corporateCode = $stateParams.corporateCode;
							}
							if ($stateParams.reservationType) {
								params.reservationType = $stateParams.reservationType;
							}
							HotelDetailsDataService.searchRooms(request, params, null, callbacksOK, callbacksError);
						}
					}
				};

				$scope.checkCollapseDetails = function(hotelId) { //check if the detail div is open
					if ($scope.view.detailsCollapse.hasOwnProperty(hotelId) && $scope.view.detailsCollapse[hotelId] === true) {
						return true;
					}
					else {
						return false;
					}
				};

				$scope.checkDataDetails = function(hotelId) { //check if we have the data of the rooms
					if ($scope.content.hotelDetails.hasOwnProperty(hotelId)) {
						return true;
					}
					else {
						return false;
					}
				};

				$scope.checkLoaderDetails = function(hotelId) { //check if the loader is up
					if ($scope.view.detailsLoader.hasOwnProperty(hotelId) && $scope.view.detailsLoader[hotelId] === true) {
						return true;
					}
					else {
						return false;
					}
				};

				$scope.toggleAmenity = function(hotelId, roomId) {
					if ($scope.view.collapseAmenity.hasOwnProperty(hotelId)) {
						if ($scope.view.collapseAmenity[hotelId].hasOwnProperty(roomId)) {
							var state = !$scope.view.collapseAmenity[hotelId][roomId];
							$scope.view.collapseAmenity = {};
							$scope.view.collapseAmenity[hotelId] = {};
							$scope.view.collapseAmenity[hotelId][roomId] = state;
						}
						else {
							$scope.view.collapseAmenity = {};
							$scope.view.collapseAmenity[hotelId] = {};
							$scope.view.collapseAmenity[hotelId][roomId] = true;
						}
					}
					else {
						$scope.view.collapseAmenity = {};
						$scope.view.collapseAmenity[hotelId] = {};
						$scope.view.collapseAmenity[hotelId][roomId] = true;
					}
				};

				$scope.isOnQuotation = function() {
					if (!window.QUOTE) {
						return false;
					}
					return true;
				};

				function totalAdults() {
					var adults = 0;
					for (var i = 1; i <= 8; i++) {
						if ($stateParams['room' + i]) {
							var roomData = $stateParams['room' + i].toString().split(',');
							adults += parseInt(roomData[0], 10);
						}
					}
					return adults;
				}

				function totalChildrens() {
					var children = 0;
					for (var i = 1; i <= 8; i++) {
						if ($stateParams['room' + i] && $stateParams['room' + i].toString().indexOf(',') !== -1) {
							var roomData = $stateParams['room' + i].toString().split(',');
							for (var j = 1; j < roomData.length; j++) {
								children += parseInt(roomData[j], 10);
							}
						}
					}
					return children;
				}

				function toggleDetails(idHotel) {
					if ($scope.view.detailsCollapse.hasOwnProperty(idHotel)) {
						$scope.view.detailsCollapse[idHotel] = !$scope.view.detailsCollapse[idHotel];
					}
					else {
						$scope.view.detailsCollapse[idHotel] = true;
					}
					return $scope.view.detailsCollapse[idHotel];
				}

				function internalError() {
					$scope.view.loadingResults = false;

					$uibModal.open({
						$scope: $scope,
						size: 'sm',
						templateUrl: 'hotels-error-reservation'
					});
					return;
				}

				function networkError() {
					$scope.view.loadingResults = false;
					$uibModal.open({
						$scope: $scope,
						size: 'sm',
						templateUrl: 'hotels-error-network'
					});
					return;
				}

				function noRoomsResultsError() {
					$scope.view.loadingResults = false;
					$uibModal.open({
						size: 'sm',
						templateUrl: 'rooms-no-results'
					});
					return;
				}

				function noRoomAvailabilityError() {
					$scope.view.loadingResults = false;
					$uibModal.open({
						size: 'sm',
						templateUrl: 'hotels-error-availability'
					});
					return;
				}

				function gotoTop($location, $anchorScroll) {
					var old = $location.hash();
					$location.hash('hotel-title');
					$anchorScroll();
					$location.hash(old);
				}
				$scope.page = function(type, start, limit) {
					//$$scope.view.loadingResults = true;
					if (type === 'previous') {
						if ($scope.content.filters.start === 0) {
							return;
						}
						else {
							$scope.content.filters.start -= 20;
							$scope.content.filters.limit = 20;
						}

						gotoTop($location, $anchorScroll);
						$timeout(function() {
							//searchHotels($scope.content.filters, $$scope.content.sort);
							$scope.filterFn(
								$scope.content.filters,
								$scope.content.sort
							);
						}, 200);
					}
					else if (type === 'next') {
						$scope.content.filters.start += 20;
						$scope.content.filters.limit = 20;

						gotoTop($location, $anchorScroll);
						$timeout(function() {
							//searchHotels($$scope.content.filters, $$scope.content.sort);
							$scope.filterFn(
								$scope.content.filters,
								$scope.content.sort
							);
						}, 200);
					}
					else if (type === 'page') {
						$scope.content.filters.start = start;
						$scope.content.filters.limit = limit;
						gotoTop($location, $anchorScroll);
						$timeout(function() {
							//searchHotels($$scope.content.filters, $$scope.content.sort);
							$scope.filterFn(
								$scope.content.filters,
								$scope.content.sort
							);
						}, 200);
					}
				};

			}
		};
	});
});
