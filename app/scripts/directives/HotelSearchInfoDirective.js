define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.directives.HotelSearchInfoDirective', [])
		.directive('hotelSearchInfo', function($uibModal, $state, $window, $timeout, StaticDataService, HotelSearchDataService, HotelResultsDataService
			/*, AnalyticsService*/
		) {
			return {
				restrict: 'E',
				scope: {
					data: '=',
					edit: '=',
					mode: '=',
					calendar: '=',
					buttons: '@',
					options: '=',
					filters: '=',
					currency: '=',
					filterFn: '&',
					type: '=',
					callback: '&callbackFn',
					hasResults: '='
				},
				templateUrl: 'views/directives/hotel-search-info.html',
				link: function($scope) {

					$scope.view = {
						closeCollapsesCount: 1,
						collapseTime: 800,
						collapseFilters: true,
						quantityResults: 8,
						/* quantity for hotel name search */
						showButtons: $scope.buttons === "true",
						filters: StaticDataService.getHotelDetailsFilters(),
						collapse: StaticDataService.getHotelDetailsCollapseOptions(),
						options: StaticDataService.getHotelDetailsSelectTextOptions(),
						currentCollapse: '',
						hotelNameList: null,
						searchText: '',
						placeHolderNameSearch: 'Ingresa el hotel que buscas'
					};

					$scope.view.filters.price = [];
					$scope.view.filters.stars = [];
					$scope.view.filters.tripAdvisor = [];

					$scope.content = {
						filters: {
							edit: false,
							price: [0, 1000],
							stars: [0, 5],
							tripAdvisor: [0, 5],
							paymentType: [],
							supplier: [],
							cancellation: [],
							preferredHotelId: null
						},
						hotelsRequest: null
					};

					if ($scope.options) { //value setting
						updateResults(false);
						//alert('value setting');
					}
					//addListeners($scope);
					//addListenerEmit($scope);
					$scope.view.hotelNameList = HotelResultsDataService.getHotelNameResultsFound('nameSearch');
					checkForWidth($scope);

					function setInfoBoxStatus(_edit, _calendar, _filters, _action) {
						$scope.filters.value = _filters;
						$scope.edit.value = _edit;
						_action = _action;
						//AnalyticsService.infoBoxStatus(_action);
					}

					function closeOtherCollapses(_type) {
						_.forEach($scope.view.collapse, function(value, key) {
							if (_type !== key) {
								$scope.view.collapse[key] = true;
							}
						});
					}

					$scope.openFilters = function() {
						setInfoBoxStatus(true, true, !$scope.filters.value, 'filters');
					};
					$scope.openFiltersOnSticky = function() {
						$timeout(function() {
							setInfoBoxStatus(true, true, false, 'filters');
						}, $scope.view.collapseTime);
					};
					$scope.selectFilter = function(index, type, event, value) {
						if (typeof $scope.content.filters[type][index] !== 'undefined' && $scope.content.filters[type][index].length > 2) {
							$scope.content.filters[type][index] = false;
						}
						else {
							$scope.content.filters[type][index] = value;
						}
						event.stopPropagation();
					};

					$scope.searchPreferredHotel = function(hotelId, hotelName) {
						//alert('click' + hotelId);
						$scope.view.placeHolderNameSearch = hotelName;
						updateResults(true, hotelId);
						//$scope.content.filters.preferredHotelId = hotelId;
						$scope.view.searchText = '';
					};

					$scope.editSearch = function() {
						setInfoBoxStatus(!$scope.edit.value, true, true, 'edit');
					};

					$scope.editSearchOnSticky = function() {
						$timeout(function() {
							setInfoBoxStatus(false, true, true, 'edit');
						}, $scope.view.collapseTime);
					};
					$scope.openCalendarResultsOnSticky = function() {
						$timeout(function() {
							setInfoBoxStatus(true, false, true, 'calendar');
						}, $scope.view.collapseTime);
					};

					$scope.deleteNameSearch = function() {
						$scope.view.searchText = "";
					};
					$scope.restorePlaceHolderNameSearch = function() {
						$scope.view.placeHolderNameSearch = '';
					};
					$scope.closeCollapses = function() {
						$scope.view.collapse.cancellation = true;
						$scope.view.collapse.supplier = true;
						$scope.view.collapse.payment = true;
					};

					$scope.toggleCollapse = function(_event, _type) {
						$scope.view.collapse[_type] = !$scope.view.collapse[_type];
						if ($scope.view.collapse[_type] === false) {
							$scope.view.currentCollapse = _type;
						}
						else {
							$scope.view.currentCollapse = '';
						}
						_event.stopImmediatePropagation();
						closeOtherCollapses(_type);
					};

					function updateResults(bEmit, hotelId) {
						var parameters = {
							price: {
								min: $scope.content.filters.price[0],
								max: $scope.content.filters.price[1]
							},
							ratings: {
								min: $scope.content.filters.stars[0],
								max: $scope.content.filters.stars[1]
							},
							tripAdvisor: {
								min: $scope.content.filters.tripAdvisor[0],
								max: $scope.content.filters.tripAdvisor[1]
							},
							supplier: $scope.content.filters.supplier,
							paymentType: $scope.content.filters.paymentType,
							cancellation: $scope.content.filters.cancellation,
							//preferredHotelId: $scope.content.filters.preferredHotelId
						};
						if (typeof hotelId !== 'undefined') {
							parameters.preferredHotelId = hotelId;
						}

						console.info("[HotelSearchInfoDirective]", "Updating filters params to:" + JSON.stringify(parameters));
						if (bEmit !== false) {
							$scope.filterFn({
								parameters: parameters
							});
						}
					}

					function checkForWidth(scope) {
						$timeout(function() {
							var win = angular.element($window);
							if (win !== null && typeof win !== 'undefined') {
								if (win.width() < 991) {
									scope.view.smallButtons = true;
								}
							}
							else {
								checkForWidth(scope);
							}
						}, 500);
					}

				}
			};
		});
});
