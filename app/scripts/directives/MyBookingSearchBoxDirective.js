define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.directives.MyBookingSearchBoxDirective', [])
		.directive('myBookingSearchBox', function($state, $timeout, $window, $uibModal, MyBookingResultsDataService, HotelReservationParserService, cUtilsFields, cUtilsObject, B2BDataService, StaticDataService) {

			return {
				restrict: 'E',
				scope: {
					mode: '=?',
					templateUrl: '=?',
					searchFn: '&',
					preset: '=?'
				},
				templateUrl: function(element, attrs) {
					return attrs.templateUrl || 'views/directives/my-booking/searchbox.html';
				},
				link: function(scope) {

					scope.content = getContentData();
					scope.view = getViewData();

					addListeners(scope);

					/* datepicker functions */
					scope.today = function() {
						scope.dt = new Date();
					};

					scope.today();

					scope.clear = function() {
						scope.dt = null;
					};

					scope.inlineOptions = {
						customClass: getDayClass,
						minDate: new Date(),
						showWeeks: true
					};

					scope.dateOptions = {
						dateDisabled: false,
						formatYear: 'yy',
						maxDate: new Date(2020, 5, 22),
						minDate: new Date(),
						startingDay: 1
					};
					/*
					// Disable weekend selection
					function disabled(data) {
						var date = data.date,
							mode = data.mode;
						return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
					}*/

					scope.toggleMin = function() {
						scope.inlineOptions.minDate = scope.inlineOptions.minDate ? null : new Date();
						scope.dateOptions.minDate = scope.inlineOptions.minDate;
					};

					scope.toggleMin();

					scope.openDatepicker = function(id) {
						scope.view.datepicker[id].opened = true;
					};

					scope.setDate = function(year, month, day) {
						scope.dt = new Date(year, month, day);
					};

					scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
					scope.format = scope.formats[0];
					scope.altInputFormats = ['M!/d!/yyyy'];

					var tomorrow = new Date();
					tomorrow.setDate(tomorrow.getDate() + 1);
					var afterTomorrow = new Date();
					afterTomorrow.setDate(tomorrow.getDate() + 1);
					scope.events = [{
						date: tomorrow,
						status: 'full'
					}, {
						date: afterTomorrow,
						status: 'partially'
					}];

					function getDayClass(data) {
						var date = data.date,
							mode = data.mode;
						if (mode === 'day') {
							var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

							for (var i = 0; i < scope.events.length; i++) {
								var currentDay = new Date(scope.events[i].date).setHours(0, 0, 0, 0);

								if (dayToCheck === currentDay) {
									return scope.events[i].status;
								}
							}
						}
						return '';
					}

					/* datepicker functions */
					scope.resetForm = function() {
						scope.content = getDefaultValues();
						scope.$applyAsync();
					};

					function getViewData() {
						return {
							errors: null,
							loadingResults: false,
							datepicker: {
								bookDateIni: {
									opened: false
								},
								bookDateEnd: {
									opened: false
								},
								checkInIni: {
									opened: false
								},
								checkInEnd: {
									opened: false
								}
							},
							collapseCalendar: {
								arrival: true,
								departure: true,
								bookarrival: true,
								bookdeparture: true
							},
							closeCollapsesCount: 1,
							openCalendarXs: {
								arrival: false,
								departure: false,
								bookarrival: false,
								bookdeparture: false
							},
							showStatusOption: false,
							properWidth: true,
							smallView: false,
							statusOptions: StaticDataService.getBookingStatusOptions(),
							maskOptionsBusinessId: {
								maskDefinitions: {
									'9': /\d/,
									'*': /[0-9A-Za-z]/
								},
								allowInvalidValue: true,
								clearOnBlur: false
							},
							maskOptionsNumber: {
								maskDefinitions: {
									'9': /\d|\./
								},
								allowInvalidValue: true,
								clearOnBlur: false
							},
							maskOptionsSpnr: {
								maskDefinitions: {
									'9': /\d|\./,
									'A': /[A-Za-z]/
								},
								allowInvalidValue: true,
								clearOnBlur: false
							}
						};
					}

					function getContentData() {
						if (typeof scope.preset !== 'undefined' && scope.preset === true) {
							var presets = MyBookingResultsDataService.getPresetValues();
							if (!_.isEmpty(presets) && !_.isNull(presets)) {
								return presets;
							}
							else {
								return getDefaultValues();
							}
						}
						else {
							return getDefaultValues();
						}
					}

					function getDefaultValues() {
						return {
							bookingNumber: '',
							businessNumber: '',
							spnr: '',
							surname: '',
							email: '',
							hotelName: '',
							bookDateIni: new Date(parseInt(moment().subtract(90, 'day').format('YYYY'), 10), parseInt(moment().subtract(90, 'day').format('M'), 10) - 1, parseInt(moment().subtract(90, 'day').format('D'), 10)),
							bookDateEnd: new Date(),
							checkInIni: null,
							checkInEnd: null,
							statusBooking: '',
							isB2B: ((_.isNull(B2BDataService)) ? false : true)
						};
					}

					function formatBusinessId(businessId) {
						if (businessId.indexOf('-') > -1) {
							return businessId;
						}
						else {
							return businessId.substring(0, 1) + '-' + businessId.substring(1, 4) + '-' + businessId.substring(4, 7);
						}
					}

					scope.searchBooking = function() {

						scope.view.errors = MyBookingResultsDataService.getSearchErrors(scope.content, scope.view.errors, scope.content.isB2B);
						//MyBookingResultsDataService.getReservationData($scope.content);
						scope.$watch('content', function() {
							scope.view.errors = MyBookingResultsDataService.getSearchErrors(scope.content, scope.view.errors, scope.content.isB2B);
						}, true);

						if (!cUtilsObject.containsValueDeep(scope.view.errors, true)) {
							var searchParameters = _.cloneDeep(scope.content);

							if (searchParameters.businessNumber !== '' && searchParameters.businessNumber !== null) {
								searchParameters.businessNumber = formatBusinessId(searchParameters.businessNumber);
							}
							if (searchParameters.bookDateIni !== null) {
								searchParameters.bookDateIni = moment(searchParameters.bookDateIni).format('YYYY-MM-DD');
							}
							if (searchParameters.bookDateEnd !== null) {
								searchParameters.bookDateEnd = moment(searchParameters.bookDateEnd).format('YYYY-MM-DD');
							}
							if (searchParameters.checkInIni !== null) {
								searchParameters.checkInIni = moment(searchParameters.checkInIni).format('YYYY-MM-DD');
							}
							if (searchParameters.checkInEnd !== null) {
								searchParameters.checkInEnd = moment(searchParameters.checkInEnd).format('YYYY-MM-DD');
							}
							scope.view.loadingResults = true;
							scope.$applyAsync();
							scope.searchFn({
								data: searchParameters
							});
							$timeout(function() {
								scope.view.loadingResults = false;
								scope.$applyAsync();
							}, 2000);

						}
						else {
							formError();
						}

					};

					scope.collapseCalendar = function(type) {
						scope.view.collapseCalendar[type] = true;
					};

					function formError() {
						$uibModal.open({
							templateUrl: 'my-booking-form-error',
							size: 'sm'
						});
					}

					scope.closeModalCalendar = function() {
						window.scrollTo(0, 0);
						$timeout(function() {
							scope.view.openCalendarXs.arrival = false;
							scope.view.openCalendarXs.departure = false;
						}, 500);
					};

					scope.openCalendar = function(_type, _event) {
						closeCollapses(_type);
						if (_event) {
							_event.stopImmediatePropagation();
						}
						var noType;
						if (typeof _type !== 'undefined' && _type.indexOf('book') !== -1) {
							noType = _type === 'bookarrival' ? 'bookdeparture' : 'bookarrival';
						}
						else {
							noType = _type === 'arrival' ? 'departure' : 'arrival';
						}

						if (scope.view.smallView === false) {
							if (scope.view.collapseCalendar[noType] === true) {
								scope.view.collapseCalendar[_type] = !scope.view.collapseCalendar[_type];
								if (!scope.view.collapseCalendar[_type]) {
									scope.$broadcast('cCalendarDirective:goToMonth', _type);
								}
							}
							else {
								scope.view.collapseCalendar[noType] = true;
								$timeout(function() {
									scope.view.collapseCalendar[_type] = !scope.view.collapseCalendar[_type];
									if (!scope.view.collapseCalendar[_type]) {
										scope.$broadcast('cCalendarDirective:goToMonth', _type);
									}
								}, 500);
							}
						}
						else {
							scope.view.openCalendarXs[_type] = true;
							scope.$broadcast('cCalendarDirective:goToMonth', _type + '-xs');
						}

					};

					scope.cleanEmail = function(input) {
						return cUtilsFields.cleanEmail(input);
					};

					scope.cleanName = function(input) {
						//return cUtilsFields.cleanName(input);
						return HotelReservationParserService.cleanName(input, true);
					};

					scope.remainOpen = function(event) {
						event.stopImmediatePropagation();
					};

					function closeCollapses() {
						if (scope.view.closeCollapsesCount === 1) {
							angular.element(window).click(function() {
								scope.view.collapseCalendar.departure = true;
								scope.view.collapseCalendar.arrival = true;
								scope.view.collapseCalendar.bookdeparture = true;
								scope.view.collapseCalendar.bookarrival = true;
								scope.$apply();
							});
							scope.$on('$destroy', function() {
								angular.element(window).off('click');
							});
							scope.view.closeCollapsesCount++;
						}
					}

					scope.selectStatus = function(_statusData) {
						scope.content.statusBooking = _statusData;
						scope.view.showStatusOption = false;
					};

					scope.toggleStatusOption = function(_event, value) {
						closeCollapses();
						if (_event) {
							_event.stopImmediatePropagation();
						}
						if (typeof value !== 'undefined') {
							scope.view.showStatusOption = value;
						}
						else {
							scope.view.showStatusOption = !scope.view.showStatusOption;
						}
					};

					function checkForWidth(scope) {
						$timeout(function() {
							var win = angular.element($window);
							if (win !== null && typeof win !== 'undefined') {
								if (win.outerWidth() <= 991) {
									scope.view.properWidth = false;
								}
								if (win.outerWidth() <= 768) {
									scope.view.smallView = true;
								}
							}
							else {
								checkForWidth(scope);
							}
						}, 1000);
					}

					function addListeners(scope) {
						checkForWidth(scope);
						scope.$watch('content.checkInIni', function(newValue, oldValue) {
							if (newValue !== oldValue && !scope.view.collapseCalendar.arrival) {
								$timeout(function() {
									scope.view.collapseCalendar.arrival = true;
								}, 600);
								if (scope.content.checkInIni && (!scope.content.checkInEnd || newValue > scope.content.checkInEnd)) {
									scope.content.checkInEnd = '';
									$timeout(function() {
										scope.view.collapseCalendar.departure = false;
										scope.$broadcast('cCalendarDirective:goToMonth', 'departure');
									}, 950);
								}
							}
						});
						scope.$watch('content.checkInEnd', function(newValue, oldValue) {
							if (newValue !== oldValue && !scope.view.collapseCalendar.departure) {
								$timeout(function() {
									scope.view.collapseCalendar.departure = true;
								}, 600);
								if (scope.content.checkInEnd && (!scope.content.checkInIni || newValue < scope.content.checkInIni)) {
									scope.content.checkInIni = '';
									$timeout(function() {
										scope.view.collapseCalendar.arrival = false;
										scope.$broadcast('cCalendarDirective:goToMonth', 'arrival');
									}, 950);
								}
							}
						});
						scope.$watch('content.bookDateIni', function(newValue, oldValue) {
							if (newValue !== oldValue && !scope.view.collapseCalendar.bookarrival) {
								$timeout(function() {
									scope.view.collapseCalendar.bookarrival = true;
								}, 600);
								if (scope.content.bookDateIni && (!scope.content.bookDateEnd || newValue > scope.content.bookDateEnd)) {
									scope.content.bookDateEnd = '';
									$timeout(function() {
										scope.view.collapseCalendar.bookdeparture = false;
										scope.$broadcast('cCalendarDirective:goToMonth', 'bookdeparture');
									}, 950);
								}
							}
						});
						scope.$watch('content.bookDateEnd', function(newValue, oldValue) {
							if (newValue !== oldValue && !scope.view.collapseCalendar.bookdeparture) {
								$timeout(function() {
									scope.view.collapseCalendar.bookdeparture = true;
								}, 600);
								if (scope.content.bookDateEnd && (!scope.content.bookDateIni || newValue < scope.content.bookDateIni)) {
									scope.content.bookDateIni = '';
									$timeout(function() {
										scope.view.collapseCalendar.bookarrival = false;
										scope.$broadcast('cCalendarDirective:goToMonth', 'bookarrival');
									}, 950);
								}
							}
						});
					}

				}
			};
		});
});
