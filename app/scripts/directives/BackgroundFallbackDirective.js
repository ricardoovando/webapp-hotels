define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.directives.BackgroundFallbackDirective', [])
		.directive('backgroundFallback', function() {
			return {
				link: function(scope, element, attrs) {
					element.on('error', function() {
						element.parent().css('background-image', 'url("' + attrs.backgroundFallback + '")');
					});
				}
			};
		});
});
