define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.directives.OutsideClickDirective', [])
		.directive('outsideClick', function($document, $parse, $timeout) {
			return {
				restrict: 'A',
				link: function(scope, elem, attr) {
					var scopeExpression = attr.outsideClick,
						onDocumentClick = function(event) {
							var isChild = elem.find(event.target).length > 0;
							if (!isChild) {
								$timeout(function() {
									scope.$apply(scopeExpression);
								}, 100);
							}
						};
					$document.on("click", onDocumentClick);
					elem.on('$destroy', function() {
						$document.off("click", onDocumentClick);
					});
				}
			};
		});
});
