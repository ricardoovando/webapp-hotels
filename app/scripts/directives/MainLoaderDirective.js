define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.directives.MainLoaderDirective', [])
		.directive('mainLoader', function($interval) {
			return {
				restrict: 'E',
				templateUrl: function(element, attrs) {
					return attrs.templateUrl || 'views/directives/main-loader.html';
				},
				scope: {
					color: '=',
					message1: '=',
					message2: '=',
					isPaymentProcess: '=?',
					message3: '=?',
					limit: '=?',
				},
				controller: function($scope) {
					$scope.view = {
						step: 1
					};
					var icons = _.shuffle([
						'cch-athens',
						'cch-delhi',
						'cch-dubai',
						'cch-easter-island',
						'cch-giza',
						'cch-great-chenese-wall',
						'cch-london',
						'cch-moscow',
						'cch-new-york',
						'cch-paris',
						'cch-rome',
						'cch-sidney',
					]);
					var length = icons.length;
					var index = 0;
					var interval = $interval(function() {
						$scope.view.icon = icons[index];
						index = (index + 1) % (length);
					}, 600);

					$scope.$on('$destroy', function() {
						$interval.cancel(interval);
						$interval.cancel(intervalPayment);
					});

					if ($scope.isPaymentProcess === 1) {
						var intervalPayment = $interval(function() {
							$scope.view.step++;
						}, 13 * 1000, ($scope.limit - 2)); // cada 13 segundos actualiza
					}
				}
			};
		});
});
