define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.directives.HotelCategoryDirective', [])
		.directive('hotelCategory', function($window) {
			return {
				restrict: 'E',
				scope: {
					categories: '='
				},
				templateUrl: function(element, attrs) {
					return attrs.templateUrl || 'views/directives/hotel-category.html';
				},
				link: function(scope) {
					scope.view = {
						isB2B: (window.COOKIE_NAME && $window.SESSION_TOKEN) ? true : false
					};
				}
			};
		});
});
