define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.directives.HotelRoomsDirective', ['ngAnimate'])
		.directive('hotelRooms', function($window, $timeout, $state, HotelSearchDataService) {

			return {
				restrict: 'E',
				scope: {
					mode: '=',
					result: '=data',
					roomsSupplier: '=',
					hotelId: '=',
					CheckoutFn: '&callbackFn',
					quoteFn: '&quoteFn',
					isPreferred: '=',
					hotelPosition: '=?'
				},
				templateUrl: 'views/directives/hotel-rooms.html',
				controller: function($scope) {

					$scope.view = setViewData();

					$scope.view.checkoutAddress = $state.href('checkout', {}, {
						absolute: true
					});

					$scope.dictionary = {
						select: 'SELECCIONAR',
						quote: 'COTIZAR'
					};

					divideRoomsPerSupplier();
					setSabreFlag();
					addListener();

					function setSabreFlag() {
						$timeout(function() {
							//alert(HotelSearchDataService.getRoomSabreInfo($state.params));
							$scope.view.sabreWarningFlag = HotelSearchDataService.getRoomSabreInfo($state.params);
							$scope.$apply();
						}, 100);

					}

					function setViewData() {
						return {
							imageRoomSupplier: '',
							roomsPerSupplier: {},
							statePerSupplier: {},
							lastIndexPerSupplier: {},
							collapseAmenity: {},
							loaderCheckoutIndex: {},
							countPerSupplier: {},
							sabreWarningFlag: false,
							sabreExists: false,
							email: $window.USER_EMAIL.toUpperCase(),
							quote: (!window.QUOTE) ? false : true,
							isOnQuotation: ($window.QUOTE && $window.QUOTE !== '0') ? true : false
						};
					}

					function divideRoomsPerSupplier() {
						$scope.view = setViewData();
						$scope.view.roomsPerSupplier = $scope.roomsSupplier;
						if (typeof $scope.roomsSupplier !== 'undefined' && $scope.roomsSupplier.hasOwnProperty('SAB')) {
							$scope.view.sabreExists = true;
						}
						if (!_.isEmpty($scope.result)) {
							for (var i = 0; i < $scope.result.length; i++) {
								if (!$scope.view.statePerSupplier.hasOwnProperty($scope.result[i].supplierCode)) {
									$scope.view.countPerSupplier[$scope.result[i].supplierCode] = [];
									$scope.view.statePerSupplier[$scope.result[i].supplierCode] = [];
								}
								var room = $scope.result[i];
								room.generalIndex = i;
								var countRooms = $scope.view.countPerSupplier[$scope.result[i].supplierCode].length + 1;
								var state = (countRooms > 6) ? false : true;
								$scope.view.statePerSupplier[$scope.result[i].supplierCode].push(state);
								$scope.view.countPerSupplier[$scope.result[i].supplierCode].push(room);
								$scope.view.lastIndexPerSupplier[$scope.result[i].supplierCode] = 6;
							}
						}
						$scope.$applyAsync();
					}

					function addListener() {
						$scope.$watch('result', function(newValue, oldValue) {
							if (newValue !== oldValue) {
								divideRoomsPerSupplier();
							}
						}, true);
						$scope.$watch('hotelId', function(newValue, oldValue) {
							if (newValue !== oldValue) {
								divideRoomsPerSupplier();
							}
						}, true);

						$scope.$watchCollection(function() {
							return $state.params;
						}, function() {
							setSabreFlag();
						});
					}

					$scope.getNameSupplier = function(code) {
						switch (code) {
							case 'EAN':
								return 'Expedia Affiliate Network';
							case 'BKG':
								return 'Booking';
							case 'SAB':
								return 'Sabre';
							default:
								return '';
						}
					};

					$scope.getSabreWarning = function(supplier) {
						return ((($scope.view.sabreWarningFlag === true && supplier === 'SAB') || ($scope.view.sabreWarningFlag === true && $scope.view.sabreExists === true && $scope.mode === 'list')) ? 'Tarifa de Sabre no incluye niños.<br> Ver Políticas Generales' : '');
					};

					$scope.getRoomPolicy = function(room) {
						return "<b>&#80;olíticas del hotel</b><br>" + room.policyRoom;
					};

					$scope.getCancelationPolicy = function(room) {
						var msg = '';
						if (room.cancellationPolicy !== null && room.cancellationPolicy.length > 0) {
							msg += room.cancellationPolicy;
							//{{ ( (room.cancellationPolicy.length > 0 ) ? room.cancellationPolicy  : ( room.refundable===false ? 'Esta tarifa no permite devolución' : 'Sin información Disponible' )  ) + getChargePolicy(room)   }}
						}
						else {
							msg += (room.refundable === false ? 'Esta tarifa no permite devolución' : 'Sin información Disponible');
						}
						msg += '<br>';
						if (room.supplierCode === 'SAB') {
							if (room.guaranteeSurchargeRequired === 'D') {
								msg += '<br><b>Cargos por adelantado</b><br>Se realizarán cargos a La tarjeta de crédito en cualquier momento hasta por la totalidad del monto en reserva.';
							}
							else if (room.guaranteeSurchargeRequired === 'G') {
								msg += '<br><b>Cargos por adelantado</b><br>La tarjeta de crédito sera utilizada para garantizar la reserva. No se efectuarán cargos.';
							}
							else {
								msg += '<br><b>Cargos por adelantado</b><br>Sin información disponible';
							}
						}
						return msg;
					};

					$scope.goToQuote = function(flagBooking, index, room) {
						$scope.view.loaderCheckoutIndex = {};
						$scope.view.loaderCheckoutIndex[index] = {};
						$scope.view.loaderCheckoutIndex[index][room.supplierCode] = true;
						$scope.quoteFn({
							flagBooking: flagBooking,
							room: room,
							roomPosition: index + 1,
							hotelPosition: $scope.hotelPosition,
							doneFn: function() {
								$scope.view.loaderCheckoutIndex[index][room.supplierCode] = false;
							}
						});
					};

					$scope.goToCheckout = function(flagBooking, index, room) {
						$scope.view.loaderCheckoutIndex = {};
						$scope.view.loaderCheckoutIndex[index] = {};
						$scope.view.loaderCheckoutIndex[index][room.supplierCode] = true;
						$scope.$applyAsync();
						$scope.CheckoutFn({
							flagBooking: flagBooking,
							room: room,
							roomPosition: index + 1,
							hotelPosition: $scope.hotelPosition
						});
						$timeout(function() {
							$scope.view.loaderCheckoutIndex[index][room.supplierCode] = false;
							$scope.$applyAsync();
						}, 2000);

					};

					$scope.paginate = function(supplierCode, index, option) {
						var i;
						if (option === 'more') {
							for (i = index; i <= (index + 5) && i < $scope.view.roomsPerSupplier[supplierCode].length; i++) {
								$scope.view.statePerSupplier[supplierCode][i] = true;
							}
							$scope.view.lastIndexPerSupplier[supplierCode] = (i - 1);
						}
						else { //less
							//the last antecessor multiple of five minus 6 (the starting index)
							for (i = index; i >= (index - 5); i--) {
								if (i !== index && ((i - 5) % 5 === 0)) {
									break;
								}
								$scope.view.statePerSupplier[supplierCode][i] = false;
							}
							$scope.view.lastIndexPerSupplier[supplierCode] = i;
						}
					};

					$scope.getIconAmenity = function(amenity) {
						if (amenity.name === 'BREAKFAST') {
							if (!_.isNull(amenity.text) && amenity.text !== '') {
								if (amenity.text.toLowerCase().indexOf("todo incluido") > -1) {
									return "cch-amenities-all-inclusive";
								}
								else if (amenity.text.toLowerCase().indexOf("media") > -1) {
									return "cch-amenities-half-pension";
								}
								else if (amenity.text.toLowerCase().indexOf("completa") > -1) {
									return "cch-amenities-full-pension";
								}
								else {
									return "cch-amenities-breakfast";
								}
							}
							else {
								return "cch-amenities-breakfast";
							}
						}
						else if (amenity.name === 'INTERNET') {
							return "cch-wifi";
						}
						else if (amenity.name === 'PARKING') {
							return "cch-parking";
						}
						else {
							return "";
						}
					};

					$scope.checkImageSupplier = function(supplierCode, isFirst) {
						if (isFirst) {
							$scope.view.imageRoomSupplier = '';
						}
						if ($scope.view.imageRoomSupplier !== supplierCode) {
							$scope.view.imageRoomSupplier = supplierCode;
							return true;
						}
						else {
							return false;
						}
					};

					$scope.getTextAmenity = function(amenity) {
						if (amenity.name === 'BREAKFAST') {
							return (_.isEmpty(amenity.text) ? 'Desayuno Incluido' : amenity.text);
						}
						else if (amenity.name === 'INTERNET') {
							return (_.isEmpty(amenity.text) ? 'Internet Gratuito' : amenity.text);
						}
						else if (amenity.name === 'PARKING') {
							return (_.isEmpty(amenity.text) ? 'Estacionamiento Gratuito' : amenity.text);
						}
						else {
							return "";
						}
					};

					$scope.checkDataDetails = function() { //check if we have the data of the rooms
						if (!_.isEmpty($scope.result)) {
							return true;
						}
						else {
							return false;
						}
					};

					$scope.toggleAmenity = function(hotelId, roomId) {
						if ($scope.view.collapseAmenity.hasOwnProperty(hotelId)) {
							if ($scope.view.collapseAmenity[hotelId].hasOwnProperty(roomId)) {
								var state = !$scope.view.collapseAmenity[hotelId][roomId];
								$scope.view.collapseAmenity = {};
								$scope.view.collapseAmenity[hotelId] = {};
								$scope.view.collapseAmenity[hotelId][roomId] = state;
							}
							else {
								$scope.view.collapseAmenity = {};
								$scope.view.collapseAmenity[hotelId] = {};
								$scope.view.collapseAmenity[hotelId][roomId] = true;
							}
						}
						else {
							$scope.view.collapseAmenity = {};
							$scope.view.collapseAmenity[hotelId] = {};
							$scope.view.collapseAmenity[hotelId][roomId] = true;
						}
					};

				}
			};
		});
});
