define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.directives.MobileTap', [])
		.directive('mobileTap', function($timeout) {
			return {
				restrict: 'A',
				scope: {

				},
				link: function(scope, element, attr) {
					element.on('click', function() {
						if (attr.mobileTap === 'quick') {
							element.addClass('tapped');
							$timeout(function() {
								element.removeClass('tapped');
							}, 600);
						}
						if (attr.mobileTap === 'remain') {
							element.addClass('tapped');
						}
					});
				}
			};
		});
});
