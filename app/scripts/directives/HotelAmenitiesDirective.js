define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.directives.HotelAmenitiesDirective', [])
		.directive('hotelAmenities', function() {
			return {
				restrict: 'E',
				scope: {
					amenities: "="
				},
				templateUrl: function(element, attrs) {
					return attrs.templateUrl || 'views/directives/hotel-amenities.html';
				},
				controller: function($scope) {
					$scope.view = {};
					$scope.range = function(min, max, step) {
						step = step || 1;
						var input = [];
						for (var i = min; i <= max; i += step) {
							input.push(i);
						}
						return input;
					};
				}
			};
		});
});
