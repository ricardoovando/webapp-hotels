define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.directives.HotelResultPaginationDirective', [])
		.directive('hotelResultPagination', function() {
			return {
				restrict: 'E',
				templateUrl: 'views/directives/hotel-result-pagination.html',
				link: function(scope) {
					scope.view.pagination = {};
					addListener();

					scope.gotoPage = function(index) {
						var page = scope.view.pagination.showPages[index];
						scope.page('page', page.start, page.limit);
					};

					function generatePages() {
						scope.view.pagination.numberPages = 0;
						scope.view.pagination.currentPage = 0;
						scope.view.pagination.showPages = [];
						scope.view.pagination.visibleIndexes = [];
						if (scope.content.numberResults % 20 === 0) {
							scope.view.pagination.numberPages = (scope.content.numberResults / 20) - 1;
						}
						else {
							scope.view.pagination.numberPages = Math.floor(scope.content.numberResults / 20);
						}
						scope.view.pagination.currentPage = 0;
						for (var i = 0; i <= scope.view.pagination.numberPages; i++) {
							var page = {
								number: i + 1,
								current: false,
								start: i * 20,
								limit: 20
							};
							if (scope.content.filters.start === page.start && scope.content.filters.limit === page.limit) {
								page.current = true;
								scope.view.pagination.currentPage = i;
							}
							scope.view.pagination.showPages.push(page);
						}
						getVisibleIndexes();
					}

					function getVisibleIndexes() {
						var visibleIndexes = [];
						var bottomIndexes = [];
						var topIndexes = [];
						var current = scope.view.pagination.currentPage;
						var numberPages = scope.view.pagination.numberPages;
						var added = 0;
						for (var i = current + 1; i <= numberPages - 1 && added < 4; i++) {
							topIndexes.push(i);
							added++;
						}
						added = 0;
						for (i = current - 1; i >= 0 && added < 4; i--) {
							bottomIndexes.push(i);
							added++;
						}
						var diffTop = Math.abs(topIndexes.length - 4);
						var margin = (diffTop > 2) ? diffTop : 2;
						for (i = 0; i < margin && (i < bottomIndexes.length); i++) {
							visibleIndexes.push(bottomIndexes[i]);
						}
						var diffBottom = Math.abs(visibleIndexes.length - 4);
						margin = (diffBottom > 2) ? diffBottom : 2;
						for (i = 0; i < margin && (i < topIndexes.length); i++) {
							visibleIndexes.push(topIndexes[i]);
						}
						visibleIndexes.push(current);
						visibleIndexes.push(numberPages);
						visibleIndexes.sort(function(a, b) {
							return a - b;
						});
						scope.view.pagination.visibleIndexes = _.uniq(visibleIndexes);
					}

					function addListener() {
						scope.$watch('content.numberResults', function() {
							generatePages();
						});
					}

				}
			};
		});

});
