define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.directives.StarsRatingDirective', [])
		.directive('starsRating', function() {
			return {
				restrict: 'E',
				templateUrl: 'views/directives/stars-rating.html',
				scope: {
					max: '=?',
					rating: '@'
				},

				link: function(scope) {
					scope.stars = {
						fill: 0,
						half: 0,
						empty: 0
					};
					if (scope.max === undefined) {
						scope.max = 5;
					}
					scope.rating = parseFloat(scope.rating);

					if (Number.isInteger(scope.rating)) {
						scope.stars.fill = new Array(scope.rating);
						scope.stars.empty = new Array(scope.max - scope.rating);
					}
					else {
						scope.stars.fill = new Array(Math.floor(scope.rating));
						scope.stars.half = new Array(1);
						scope.stars.empty = new Array(scope.max - Math.ceil(scope.rating));
					}
				}
			};
		});
});
