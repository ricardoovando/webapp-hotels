define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.directives.HotelDetailsFiltersDirective', [])
		.directive('hotelDetailsFilters', function($window, $stateParams, $timeout, StaticDataService, HotelSearchDataService, HotelDetailsDataService) {
			return {
				restrict: 'E',
				templateUrl: 'views/directives/hotel-details-filters.html',
				scope: {
					defaultValues: '=',
					hotelInfo: '=',
					mode: '='
				},

				link: function(scope) {
					var sortTimer = null;
					var timer = null;
					var listener = null;

					scope.view = {
						collapse: StaticDataService.getHotelDetailsCollapseOptions(),
						options: StaticDataService.getHotelDetailsSelectTextOptions(scope.defaultValues),
						filters: StaticDataService.getHotelDetailsFilters(),
						sort: StaticDataService.getHotelDetailsSort(),
						currentCollapse: '',
						smallButtons: false,
						showButtons: true,
						edit: {
							value: true
						},
						filter: {
							value: true
						}
					};

					scope.content = {
						filters: HotelDetailsDataService.getHotelDetailsFiltersValues(scope.defaultValues),
						sort: {
							selectedSortKey: 'default'
						},
						search: {
							places: {
								destination: ''
							},
							dates: {
								departure: HotelSearchDataService.getDateDeparture(),
								arrival: HotelSearchDataService.getDateArrival()
							},
							passengers: {
								adults: HotelSearchDataService.getNrAdults(),
								children: HotelSearchDataService.getNrChildren(),
								rooms: HotelSearchDataService.getNrRooms()
							},
							rooms: HotelSearchDataService.getRooms()
						}
					};
					addListeners();
					getWidth(scope);

					function getWidth(scope) {
						var win = angular.element($window);
						if (typeof win !== 'undefined' && win !== null) {
							scope.view.smallButtons = (win.width() < 991);
						}
						else {
							$timeout(function() {
								getWidth(scope);
							}, 500);
						}
					}

					scope.selectFilter = function(index, type, event, value) {
						if (typeof scope.content.filters[type][value] !== 'undefined' && scope.content.filters[type][value].length > 0) {
							scope.content.filters[type][value] = false;
						}
						else {
							scope.content.filters[type][value] = value;
						}
						event.stopPropagation();
					};

					/*
					scope.enterCollapse = function() {
						if (collapseTimer) {
							$timeout.cancel(collapseTimer);
							collapseTimer = null;
						}
					};
					*/

					scope.closeCollapses = function() {

						//collapseTimer = $timeout(function() {
						scope.view.collapse.cancellation = true;
						scope.view.collapse.supplier = true;
						scope.view.collapse.amenities = true;
						scope.view.collapse.payment = true;
						scope.view.collapse.sort = true;
						scope.view.collapse.showRoomsOptions = true;
						scope.$emit('closeRoomsOptions');
						scope.$applyAsync();
						//	collapseTimer = null;
						//}, 100);
					};

					scope.toggleCollapse = function(_event, _type) {
						_event.stopImmediatePropagation();
						$timeout(function() {

							if (!scope.view.collapse.hasOwnProperty(_type)) {
								scope.view.collapse[_type] = false;
							}
							else {
								scope.view.collapse[_type] = !scope.view.collapse[_type];
							}
							if (scope.view.collapse[_type] === false) {
								scope.view.currentCollapse = _type;
							}
							else {
								scope.view.currentCollapse = '';
							}

							closeOtherCollapses(_type);

						}, 100);
					};

					scope.collapseSelectBox = function(keyEvent) {
						keyEvent.stopImmediatePropagation();
						//AnalyticsService.openSortSelector(scope.view.collapseSelectBox);
						scope.view.collapse.sort = !scope.view.collapse.sort;
						closeOtherCollapses('sort');
					};
					scope.sortResults = function(option) {
						scope.view.collapse.sort = !scope.view.collapse.sort;
						scope.content.sort.selectedSortKey = option.code;
						scope.view.options.sort = option.name;
						//AnalyticsService.sortResults(option.value);
						if (sortTimer) {
							$timeout.cancel(sortTimer);
							sortTimer = null;
						}
						sortTimer = $timeout(function() {
							var oldCriteria = HotelDetailsDataService.getSelectedSortCriteria();
							var newCriteria = option.code;
							if (oldCriteria !== newCriteria) {
								HotelDetailsDataService.setSelectedSortCriteria(option.code);
								//searchRooms($stateParams);
							}
							sortTimer = null;
						}, 700);
					};

					function closeOtherCollapses(_type) {
						_.forEach(scope.view.collapse, function(value, key) {
							if (_type !== key) {
								scope.view.collapse[key] = true;
								if (key === 'showRoomsOptions') {
									scope.$broadcast('closeRoomsOptions', true);
								}
							}
						});
					}

					function updateSearchInfo(stateData) {
						scope.content.search.dates.arrival = HotelSearchDataService.getDateArrival(stateData.searchboxData);
						scope.content.search.dates.departure = HotelSearchDataService.getDateDeparture(stateData.searchboxData);
						scope.content.search.passengers.rooms = HotelSearchDataService.getNrRooms(stateData.searchboxData);
						scope.content.search.passengers.adults = HotelSearchDataService.getNrAdults(stateData.searchboxData);
						scope.content.search.passengers.children = HotelSearchDataService.getNrChildren(stateData.searchboxData);
						scope.content.search.rooms = HotelSearchDataService.getRooms(stateData.searchboxData);
						scope.$applyAsync();
					}

					function addListeners() {

						scope.$on('SearchDataUpdated', function(event, stateData) {
							updateSearchInfo(stateData);
							scope.$emit('FilterDataUpdated', {
								searchboxData: stateData.searchboxData
								//,filterData: scope.content.filters
							}); //catched by hoteldetails controller
							scope.view.options = StaticDataService.getHotelDetailsSelectTextOptions();
							scope.view.filters = StaticDataService.getHotelDetailsFilters();
							scope.content.filters = HotelDetailsDataService.getHotelDetailsFiltersValues();
						});

						scope.$on('toggleRoomsOptions', function(event, state) {
							scope.view.collapse.showRoomsOptions = state;
							if (state === true) {
								scope.view.currentCollapse = 'showRoomsOptions';
								closeOtherCollapses('showRoomsOptions');
							}
							else {
								scope.view.currentCollapse = '';
							}
							scope.$applyAsync();
						});

						scope.$watch('defaultValues', function(newValue, oldValue) {
							if (newValue !== oldValue) {
								var flag = false;
								if (listener !== null) {
									listener();
									flag = true;
								}
								scope.view.options = StaticDataService.getHotelDetailsSelectTextOptions(scope.defaultValues),
									scope.content.filters = HotelDetailsDataService.getHotelDetailsFiltersValues(newValue);
								if (flag === true) {
									addListenerFilters();
								}
								scope.$applyAsync();
							}
						}, true);

						$timeout(function() {
							addListenerFilters();
						}, 1000);
					}

					function addListenerFilters() {
						listener = scope.$watch('content.filters', function(newValue, oldValue) {
							if (newValue !== oldValue) {
								if (timer) {
									$timeout.cancel(timer);
									timer = null;
								}
								scope.view.options = StaticDataService.getHotelDetailsSelectTextOptions(scope.content.filters);
								//alert(JSON.stringify(scope.view.options));
								scope.$applyAsync();
								timer = $timeout(function() {
									timer = null;
									//AnalyticsService.filterChanged(newValue, oldValue);
									//scope.$emit("DetailsFiltersUpdated", scope.content.filters);
									scope.$broadcast('sendSearchboxData', {
										filter: scope.content.filters
									});
								}, 1800);
							}
						}, true);
					}

					//search info

					scope.editSearch = function() {
						setInfoBoxStatus(!scope.view.edit.value, true, true, 'edit');
					};
					scope.openFilters = function() {
						setInfoBoxStatus(true, true, !scope.view.filter.value, 'filters');
					};

					scope.editSearchOnSticky = function() {
						$timeout(function() {
							setInfoBoxStatus(false, true, true, 'edit');
						}, scope.view.collapseTime);
					};
					scope.openFiltersOnSticky = function() {
						$timeout(function() {
							setInfoBoxStatus(true, true, false, 'filters');
						}, scope.view.collapseTime);
					};

					function setInfoBoxStatus(_edit, _calendar, _filters, _action) {
						scope.view.filter.value = _filters;
						scope.view.edit.value = _edit;
						_action = _action;
						//AnalyticsService.infoBoxStatus(_action);
					}
					//search info

				}
			};
		});
});
