define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.directives.TripAdvisorRatingDirective', [])
		.directive('tripAdvisorRating', function($uibModal) {
			return {
				restrict: 'E',
				scope: {
					tripRating: '=',
					min: '=',
					max: '=',
					numberReviews: '=',
					url: '=',
					mode: '=?'
				},
				templateUrl: 'views/directives/trip-advisor-rating.html',
				link: function(scope) {

					scope.view = {
						urlLogo: "img/tripadvisor/triplogo.png",
						urlTripAdvisorReview: scope.url,
						loader: true
					};

					scope.showReviews = function() {
						if (scope.url.length > 0) {
							$uibModal.open({
								size: 'lg',
								scope: scope,
								templateUrl: 'trip-advisor-review',
								controller: 'ModalController',
								resolve: {
									content: function() {
										return null;
									}
								}
							});
						}
					};

					scope.view.trip = {
						fill: 0,
						half: 0,
						empty: 0
					};
					if (scope.max === undefined) {
						scope.max = 5;
					}

					processRating(scope.tripRating);
					addListener();

					function addListener() {
						scope.$watch('url', function(newValue, oldValue) {
							if (newValue !== oldValue) {
								scope.view.urlTripAdvisorReview = newValue;
							}
						}, true);
						scope.$watch('tripRating', function(newValue, oldValue) {
							if (newValue !== oldValue) {
								processRating(scope.tripRating);
							}
						}, true);
					}

					function isInteger(value) {
						return typeof value === "number" && isFinite(value) && Math.floor(value) === value;
					}

					function processRating(rating) {
						scope.tripRating = parseFloat(rating);
						if (isNaN(scope.tripRating)) {
							scope.tripRating = 0;
						}
						if (isInteger(scope.tripRating)) {
							scope.view.trip.fill = new Array(scope.tripRating);
							scope.view.trip.half = [];
							scope.view.trip.empty = new Array(scope.max - scope.tripRating);
						}
						else {
							//alert(scope.rating);
							scope.view.trip.fill = new Array(Math.floor(scope.tripRating));
							scope.view.trip.half = new Array(1);
							scope.view.trip.empty = new Array(scope.max - Math.ceil(scope.tripRating));
						}

					}

				}
			};
		});
});
