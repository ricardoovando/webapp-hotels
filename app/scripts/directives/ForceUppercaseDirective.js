define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.directives.ForceUppercaseDirective', [])
		.directive('forceUppercase', function() {
			return {
				require: 'ngModel',
				link: function(scope, element, attrs, modelCtrl) {
					var capitalize = function(inputValue) {
						if (typeof inputValue === 'undefined') {
							inputValue = '';
						}
						var capitalized = inputValue.toUpperCase();
						if (capitalized !== inputValue) {
							modelCtrl.$setViewValue(capitalized);
							modelCtrl.$render();
						}
						return capitalized;
					};
					modelCtrl.$parsers.push(capitalize);
					capitalize(scope[attrs.ngModel]); // capitalize initial value
				}
			};
		});

});
