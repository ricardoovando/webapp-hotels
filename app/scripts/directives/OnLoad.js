define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.directives.OnLoad', [])
		.directive('onLoad', function() {
			return {
				restrict: 'A',
				replace: false,
				scope: {
					'onLoad': '=',
				},
				link: function(scope) {
					scope.onLoad();
				}
			};
		});
});
