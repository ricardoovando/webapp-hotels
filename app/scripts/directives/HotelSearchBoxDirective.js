define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.directives.HotelSearchBoxDirective', [])
		.directive('hotelSearchBox', function($timeout, $uibModal, $window, $state, $stateParams, GeoSearchDataService, HotelSearchDataService, StaticDataService, B2BDataService, AppCommonService) {

			return {
				restrict: 'E',
				templateUrl: 'views/directives/hotel-search-box.html',
				scope: {
					minimized: '=?',
					verticalAlign: '='
				},
				link: function(scope, element) {
					scope.minimized = (typeof scope.minimized === 'undefined') ? false : true;
					var validationField = (B2BDataService !== null) ? 'id' : 'dest_id';
					var placesTimeout;
					scope.view = {
						isB2B: (B2BDataService !== null) ? true : false,
						loader: {
							corporateCode: false
						},
						appType: $window.APP_TYPE,
						corporateType: (($stateParams.corporateCode && $stateParams.corporateCode.length > 1) ? 'C' : ($state.$current.name.indexOf('home') > -1 ? 'C' : 'V')),
						calendarLimits: {
							min: moment().add(0, 'd').format('YYYY-MM-DD'),
							max: moment().add(310, 'd').format('YYYY-MM-DD')
						},
						maxRooms: 3,
						maxChildren: 3,
						searchboxLimits: StaticDataService.getSearchboxLimits(),
						textSearchButton: (($state.$current.name.indexOf('hotel-details') > -1) ? 'Actualizar' : 'Buscar Hoteles'),
						selectedPlace: false,
						morePassengers: false,
						closeCollapsesCount: 1,
						isSearching: false,
						state: {
							value: 'arrival'
						},
						isInHome: true,
						collapse: {
							showCorporateCode: ($stateParams.corporateCode && $stateParams.corporateCode.length > 1) ? true : false,
							showRoomOption: false,
							showRoomsOption: false,
							showReservationTypeOption: false,
							childrenAges: []
						},
						properWidth: true,
						departureInitDate: '',
						collapseCalendar: {
							arrival: true,
							departure: true
						},
						search: {
							destination: {
								text: ''
							},
							dates: {
								departure: '',
								arrival: ''
							},
							trip: {
								type: ''
							}
						},
						errors: {
							corporateCode: -1,
							destination: false,
							departure: false,
							arrival: false,
							numberRooms: false,
							ages: false,
							children: []
						},
						passengers: {
							adults: [1, 2, 3, 4, 5, 6],
							children: [0, 1, 2, 3, 4, 5, 6]
						},
						filters: {
							airlines: null,
							airlinesCheckboxes: []
						},
						passengersErrors: {
							adults: false,
							children: false
						},
						smallView: false,
						hideResults: false,
						openCalendarXs: {
							arrival: false,
							departure: false
						},
						focus: {
							corporateCode: false
						},
						btnGroup: false
					};
					scope.content = {
						search: {
							places: {
								destination: ''
							},
							dates: {
								departure: (($state.$current.name.indexOf('home') > -1) ? null : HotelSearchDataService.getDateDeparture()),
								arrival: (($state.$current.name.indexOf('home') > -1) ? null : HotelSearchDataService.getDateArrival()),
							},
							passengers: {
								adults: HotelSearchDataService.getNrAdults(),
								children: HotelSearchDataService.getNrChildren(),
								rooms: HotelSearchDataService.getNrRooms()
							},
							rooms: HotelSearchDataService.getRooms(),
							corporateCode: HotelSearchDataService.getCorporateCode(),
							reservationType: HotelSearchDataService.getReservationType()
						},
						placesResults: []
					};

					var ErrorCallbacks = {
						ERROR_NETWORK: networkError,
						ERROR_INTERNAL: internalError,
						ERROR_NORESULT: noresults,
						NEMO_FAIL: function() {
							scope.view.errors.corporateCode = true;
							scope.view.loader.corporateCode = false;
						}
					};

					var OkCallbacks = {
						ORIGIN_SUCCESS: function(_place) {
							scope.content.search.places.destination = _place;
							scope.view.search.destination.text = (!(_.isEmpty(scope.content.search.places.destination))) ? scope.content.search.places.destination.regionNameLong : '';
							scope.view.selectedPlace = true;
						},
						NEMO_SUCCESS: function() {
							scope.view.errors.corporateCode = false;
							scope.view.loader.corporateCode = false;
						}
					};

					if (!scope.minimized) {
						fillPlacesAndSearch();
					}

					addListeners(scope);
					adjustDirective(scope, element);

					function closeChildrenSelects() {
						scope.view.collapse.childrenAges = [];
						for (var i = 0; i < scope.view.maxRooms; i++) {
							scope.view.collapse.childrenAges[i] = _.fill(new Array(scope.view.maxChildren), false);
						}
					}

					function closeCollapses(_notThis) {
						switch (_notThis) {
							case 'roomOption':
								scope.view.collapseCalendar.departure = true;
								scope.view.collapseCalendar.arrival = true;
								closeChildrenSelects();
								break;
							case 'departure':
							case 'arrival':
								scope.view.collapse.showRoomOption = false;
								closeChildrenSelects();
								break;
							default:
								scope.view.collapseCalendar.departure = true;
								scope.view.collapseCalendar.arrival = true;
								scope.view.collapse.showRoomOption = false;
								closeChildrenSelects();
								break;
						}
						if (scope.view.closeCollapsesCount === 1) {
							angular.element(window).click(function() {
								scope.view.collapseCalendar.departure = true;
								scope.view.collapseCalendar.arrival = true;
								scope.view.collapse.showRoomOption = false;
								scope.view.collapse.showReservationTypeOption = false;
								closeChildrenSelects();
								scope.$apply();
							});
							scope.$on('$destroy', function() {
								angular.element(window).off('click');
							});
							scope.view.closeCollapsesCount++;
						}
					}

					scope.range = function(min, max, step) {
						step = step || 1;
						var input = [];
						for (var i = min; i <= max; i += step) {
							input.push(i);
						}
						return input;
					};

					scope.focusResults = function(keyEvent) {
						if (keyEvent.keyCode === 40) {
							keyEvent.preventDefault();
							var element = angular.element('#result_id_0');
							if (element) {
								element.focus();
							}
						}
					};

					scope.selectCorporateType = function(type) {
						scope.view.corporateType = type;
						if (type === 'C') {
							scope.view.collapse.showCorporateCode = true;
						}
						else {
							scope.content.search.corporateCode = null;
							scope.view.collapse.showCorporateCode = false;
						}
					};

					scope.collapseCalendar = function(type) {
						scope.view.collapseCalendar[type] = true;
					};

					scope.validateCorporateCode = function() {
						scope.view.loader.corporateCode = true;
						if (scope.view.isB2B && scope.view.corporateType === 'C' && scope.view.appType !== 1) {
							if (scope.content.search.corporateCode != null && scope.content.search.corporateCode.toString().toLowerCase().length >= 2) {
								AppCommonService.validateSearchNemo(scope.content.search.corporateCode, OkCallbacks.NEMO_SUCCESS, ErrorCallbacks.NEMO_FAIL);
							}
							else {
								scope.view.errors.corporateCode = true;
								scope.view.loader.corporateCode = false;
							}
						}
						else {
							scope.view.errors.corporateCode = false;
							scope.view.loader.corporateCode = false;
						}
					};

					scope.keySelectPlace = function(keyEvent, index, item, type, _element) {
						if (keyEvent.keyCode === 13) {
							scope.onSelectResult(item);
							var element = null;
							if (_element === 'search-destination-input-id') {
								element = angular.element('#calendar-departure-id');
								element.focus();
							}
							else {
								element = angular.element('#search-destination-input-id');
								element.focus();
							}
						}
						var id = 0;
						if (keyEvent.keyCode === 40) {
							keyEvent.preventDefault();
							id = (index + 1);
							var element_down = angular.element('#result_id_' + id);
							if (element_down) {
								element_down.focus();
							}
						}
						if (keyEvent.keyCode === 38) {
							keyEvent.preventDefault();
							id = (index - 1);
							var element_up = angular.element('#result_id_' + id);
							if (element_up) {
								element_up.focus();
							}
						}
					};
					scope.hideResults = function() {
						scope.view.hideResults = true;
					};

					scope.searchPlace = function(_newValue) {
						scope.view.hideResults = false;
						scope.view.isSearching = true;
						console.info("[SearchBoxDirective]", "Searching with input: " + _newValue);

						var callbackOK = function(results) {
							scope.content.placesResults = _.cloneDeep(results);
							console.info("[SearchBoxDirective]", "Result: ", results);
							scope.view.isSearching = false;
						};

						var callbackError = function(response) {
							console.error("[SearchBoxDirective]", "Error searching. Response: ", response);
							scope.view.isSearching = false;
							internalError();
						};
						console.info("[SearchBoxDirective]", "Searching in server");
						GeoSearchDataService.searchPlace(_newValue, callbackOK, callbackError);
					};

					scope.onSelectResult = function(item) {
						var label = item.name;
						if (!B2BDataService && item.source && item.source.name) {
							label = item.source.name;
						}

						scope.view.search.destination.text = label;
						scope.content.search.places.destination = item;
						scope.content.placesResults = [];

						scope.view.selectedPlace = true;
						scope.view.hideResults = true;
					};

					scope.selectReservationType = function(_type) {
						scope.content.search.reservationType = _type;
						scope.view.collapse.showReservationTypeOption = false;
					};

					scope.addRoom = function(replaceRooms, adults, children) {
						if (typeof adults === 'undefined') {
							adults = 1;
						}
						if (typeof children === 'undefined') {
							children = 0;
						}
						if (typeof replaceRooms === 'undefined') {
							replaceRooms = false;
						}
						var extraRoom = {
							adults: adults,
							children: children,
							ages: []
						};
						if (replaceRooms) {
							scope.content.search.passengers.adults = adults;
							scope.content.search.passengers.children = children;
							scope.content.search.passengers.rooms = 1;
							scope.content.search.rooms = [];
							scope.content.search.rooms.push(extraRoom);
							scope.view.collapse.showRoomOption = false;
						}
						else {
							if (scope.content.search.rooms.length >= 3) {
								return;
							}

							scope.content.search.passengers.adults++;
							scope.content.search.passengers.rooms++;
							scope.content.search.rooms.push(extraRoom);
						}

						if (scope.content.search.passengers.rooms === 3) {
							scope.view.btnGroup = true;
						}
					};

					scope.deleteRoom = function() {
						var index = scope.content.search.rooms.length - 1;
						//always delete last room
						if (scope.content.search.passengers.rooms === 0) {
							return;
						}
						var room = scope.content.search.rooms[index];
						scope.content.search.passengers.adults = scope.content.search.passengers.adults - room.adults;
						scope.content.search.passengers.children = scope.content.search.passengers.children - room.children;
						scope.content.search.passengers.rooms--;
						scope.content.search.rooms.splice(index, 1);

						if (scope.content.search.passengers.rooms < 3) {
							scope.view.btnGroup = false;
						}
						else {
							scope.view.btnGroup = true;
						}
					};

					scope.addRemovePassenger = function(_type, action, index) {
						var indexChildren = null;
						if (action === 'remove') {
							if (_type === 'children') {
								if (scope.content.search.rooms[index].children === 0) {
									return;
								}
								indexChildren = scope.content.search.rooms[index].children - 1;
								scope.content.search.rooms[index].ages.splice(indexChildren, 1);

								scope.view.collapse.childrenAges[index].splice(indexChildren, 1);

								scope.content.search.rooms[index].children--;
								scope.content.search.passengers[_type]--;
							}
							else {
								if (scope.content.search.rooms[index].adults === 1) { //delete room
									return;
								}
								scope.content.search.rooms[index][_type]--;
								scope.content.search.passengers[_type]--;
							}
						}

						if (action === 'add') {
							if (_type === 'children') {
								if (scope.content.search.rooms[index].children === 3) {
									return;
								}
								indexChildren = scope.content.search.rooms[index].children;
								if (typeof scope.content.search.rooms[index].ages[indexChildren] !== "undefined") {
									scope.content.search.rooms[index].ages[indexChildren] = false;
									//scope.content.search.rooms[index].ages[indexChildren] = 0;
									if (typeof scope.view.collapse.childrenAges[index] === 'undefined') {
										scope.view.collapse.childrenAges[index] = [];
									}
									scope.view.collapse.childrenAges[index][indexChildren] = false;
								}
								else {
									scope.content.search.rooms[index].ages.push(false);
									if (typeof scope.view.collapse.childrenAges[index] === 'undefined') {
										scope.view.collapse.childrenAges[index] = [];
									}
									scope.view.collapse.childrenAges[index][indexChildren] = false;
								}
								scope.content.search.rooms[index].children++;
								scope.content.search.passengers[_type]++;
							}
							else {

								if (scope.content.search.rooms[index].adults >= 8) {
									return;
								}
								scope.content.search.rooms[index].adults++;
								scope.content.search.passengers[_type]++;
							}
						}
					};

					scope.toggleChildrenSelect = function(roomIndex, ageIndex, _event, status) {

						var oldValue = scope.view.collapse.childrenAges[roomIndex][ageIndex];
						closeCollapses({
							roomIndex: roomIndex,
							ageIndex: ageIndex
						});
						if (_event) {
							_event.stopImmediatePropagation();
						}

						if (typeof scope.view.collapse.childrenAges[roomIndex] === 'undefined') {
							scope.view.collapse.childrenAges = [];
							scope.view.collapse.childrenAges[roomIndex] = [];
						}
						if (typeof status !== 'undefined') {
							scope.view.collapse.childrenAges[roomIndex][ageIndex] = status;
						}
						else {
							scope.view.collapse.childrenAges[roomIndex][ageIndex] = !oldValue;
						}
					};

					scope.selectChildrenAge = function(roomIndex, ageIndex, value) {
						scope.view.collapse.childrenAges[roomIndex][ageIndex] = false;
						//scope.content.search.rooms[roomIndex].ages[ageIndex] = value;
						scope.content.search.rooms[roomIndex].ages[ageIndex] = value;
					};

					scope.togglePassengers = function() {
						scope.view.morePassengers = true;
					};

					scope.toggleRoomOption = function(_event, value) {
						closeCollapses('roomOption');
						if (_event) {
							_event.stopImmediatePropagation();
						}

						if (typeof value !== 'undefined') {
							scope.view.collapse.showRoomOption = value;
						}
						else {
							scope.view.collapse.showRoomOption = !scope.view.collapse.showRoomOption;
						}
						scope.$emit('toggleRoomsOptions', scope.view.collapse.showRoomOption); //catched by hoteldetails controller						
					};

					scope.toggleReservationTypeOption = function(_event, value) {
						closeCollapses('reservationTypeOption');
						if (_event) {
							_event.stopImmediatePropagation();
						}

						if (typeof value !== 'undefined') {
							scope.view.collapse.showReservationTypeOption = value;
						}
						else {
							scope.view.collapse.showReservationTypeOption = !scope.view.collapse.showReservationTypeOption;
						}
						scope.$emit('toggleReservationTypeOptions', scope.view.collapse.showReservationTypeOption); //catched by hoteldetails controller	
					};

					scope.toggleRooms = function(_event) {

						if (_event) {
							_event.stopImmediatePropagation();
						}

						scope.view.collapse.showRoomsOption = !scope.view.collapse.showRoomsOption;
						scope.view.collapse.showRoomOption = false;
					};

					scope.closeModalCalendar = function() {
						window.scrollTo(0, 0);
						$timeout(function() {
							scope.view.openCalendarXs.arrival = false;
							scope.view.openCalendarXs.departure = false;
						}, 500);
					};

					scope.openCalendar = function(_type, _event) {
						closeCollapses(_type);
						if (_event) {
							_event.stopImmediatePropagation();
						}
						var noType = _type === 'arrival' ? 'departure' : 'arrival';

						if (scope.view.smallView === false) {
							if (scope.view.collapseCalendar[noType] === true) {
								scope.view.collapseCalendar[_type] = !scope.view.collapseCalendar[_type];
								if (!scope.view.collapseCalendar[_type]) {
									scope.$broadcast('cCalendarDirective:goToMonth', _type);
								}
							}
							else {
								scope.view.collapseCalendar[noType] = true;
								$timeout(function() {
									scope.view.collapseCalendar[_type] = !scope.view.collapseCalendar[_type];
									if (!scope.view.collapseCalendar[_type]) {
										scope.$broadcast('cCalendarDirective:goToMonth', _type);
									}
								}, 500);
							}
						}
						else {
							scope.view.openCalendarXs[_type] = true;
							scope.$broadcast('cCalendarDirective:goToMonth', _type + '-xs');
						}

					};

					scope.remainOpen = function(event) {
						event.stopImmediatePropagation();
					};

					scope.goToSearch = function(filterData) {
						//Start watching changes to refresh error display/hide
						scope.$watch('content.search', function() {
							validateParameters();
						}, true);
						validateParameters();
						//return;
						if (!(_.includes(scope.view.errors, true))) {
							var stateData = {
								arrival: moment(scope.content.search.dates.arrival).format("YYYY-MM-DD"),
								departure: moment(scope.content.search.dates.departure).format("YYYY-MM-DD")
							};
							if (scope.content.search.corporateCode && scope.view.appType !== 1) {
								stateData.corporateCode = scope.content.search.corporateCode;
							}
							if (scope.content.search.reservationType) {
								stateData.reservationType = scope.content.search.reservationType;
							}

							if (!scope.minimized) {
								stateData.destination = scope.content.search.places.destination.id;
							}
							stateData = HotelSearchDataService.addRoomsToStateParams(scope.content, stateData);
							//AnalyticsService.searchFlights(scope.view.isInHome, Object.keys(stateData).map(function(key) {
							//	return stateData[key];
							//}).join('|'));
							if (B2BDataService === null) {
								var deepLink = GeoSearchDataService.getDeeplink(stateData, scope.content.search.places.destination, scope.view.smallView);
								$window.open(deepLink, '_blank');
								return;
							}

							if (!scope.minimized) {
								if (scope.content.search.places.destination.type === 'hotel') {
									stateData.hotel = scope.content.search.places.destination.id;
									stateData.name_hotel = HotelSearchDataService.getUrlName(scope.content.search.places.destination);
									$state.go('hotel-details', stateData);
								}
								else if (scope.content.search.places.destination.hasOwnProperty('source') && scope.content.search.places.destination.source.type === 'AIRPORT') {
									stateData.destination = scope.content.search.places.destination.source.cityId;
									stateData.name_region = HotelSearchDataService.getUrlName(scope.content.search.places.destination);
									if ($state.$current.name.indexOf('hotel-results') > -1) {
										//scope.$emit('SearchUpdated', stateData); //catched by hotelresults controller       
										AppCommonService.refreshUrl('hotel-results', stateData);
										$state.go('hotel-results', stateData);
									}
									else {
										$state.go('hotel-results', stateData);
									}
								}
								else {
									stateData.name_region = HotelSearchDataService.getUrlName(scope.content.search.places.destination);
									if ($state.$current.name.indexOf('hotel-results') > -1) {
										//scope.$emit('SearchUpdated', stateData); //catched by hotelresults controller		
										AppCommonService.refreshUrl('hotel-results', stateData);
										$state.go('hotel-results', stateData);
									}
									else {
										$state.go('hotel-results', stateData);
									}
								}
							}
							else {
								if ($state.$current.name.indexOf('hotel-details') > -1) {
									if (typeof filterData !== 'undefined') {
										scope.$emit('FilterDataUpdated', {
											searchboxData: stateData,
											filterData: filterData
										}); //catched by hoteldetails controller								  	
									}
									else {
										scope.$emit('SearchDataUpdated', {
											searchboxData: stateData
										}); //catched by hoteldetailsfilter directive								  	
									}
								}
							}
						}
						else {
							if (scope.view.errors.ages === true) {
								scope.view.collapse.showRoomsOption = true;
							}
							//AnalyticsService.searchFlightsFailedByParams(scope.view.isInHome, scope.view.errors);
						}
					};

					function validateParameters() {
						//Mandatory params
						if (!scope.minimized) {
							scope.view.errors.destination = (_.intersection([validationField], _.keys(scope.content.search.places.destination)).length !== 1);
						}
						scope.view.errors.departure = (!scope.content.search.dates.departure || !moment(scope.content.search.dates.departure).isValid());
						scope.view.errors.arrival = (!scope.content.search.dates.arrival || !moment(scope.content.search.dates.arrival).isValid());
						scope.view.errors.adults = !/\d/.test(scope.content.search.passengers.adults);
						scope.view.errors.numberRooms = ((scope.content.search.passengers.rooms === 0) ? true : false);
						scope.view.errors.ages = false;
						scope.view.errors.corporateCode = (scope.view.isB2B && scope.view.corporateType === 'C' && scope.view.appType !== 1) ? (scope.content.search.corporateCode === -1 ? true : scope.view.errors.corporateCode) : false;
						if (scope.view.errors.corporateCode === -1) {
							scope.validateCorporateCode();
						}
						if (!scope.view.errors.numberRooms) {
							for (var i = 0; i < scope.content.search.rooms.length; i++) {
								scope.view.errors.children[i] = [];
								for (var j = 0; j < scope.content.search.rooms[i].ages.length; j++) {
									if (scope.content.search.rooms[i].ages[j] === false) {
										scope.view.errors.children[i][j] = true;
										scope.view.errors.ages = true;
									}
									else {
										scope.view.errors.children[i][j] = false;
									}
								}
							}
						}
					}

					function fillPlacesAndSearch() {
						if (typeof $stateParams.destination !== 'undefined' && !_.isEmpty($stateParams.destination)) {
							HotelSearchDataService.getPlaceDestination($stateParams.destination, OkCallbacks, ErrorCallbacks);
						}
					}

					function internalError() {
						$uibModal.open({
							scope: scope,
							size: 'sm',
							templateUrl: 'hotels-error-internal'
						});
					}

					function networkError() {
						$uibModal.open({
							scope: scope,
							size: 'sm',
							templateUrl: 'hotels-error-network'
						});
					}

					function noresults() {
						$uibModal.open({
							scope: scope,
							size: 'sm',
							templateUrl: 'hotels-error-internal'
						});
					}

					function checkForWidth(scope) {
						$timeout(function() {
							var win = angular.element($window);
							if (win !== null && typeof win !== 'undefined') {
								if (win.outerWidth() <= 991) {
									scope.view.properWidth = false;
								}
								if (win.outerWidth() <= 768) {
									scope.view.smallView = true;
								}
							}
							else {
								checkForWidth(scope);
							}
						}, 1000);
					}

					function addListeners(scope) {

						scope.vals = [];
						scope.$on('closeRoomsOptions', function() {
							scope.view.collapse.showRoomOption = false;
						});
						scope.$on('$viewContentLoaded', function() {
							angular.element($window).click(function() {
								scope.$apply();
							});
							scope.$on('$destroy', function() {
								angular.element($window).off('click');
							});
						});
						scope.$on('sendSearchboxData', function(event, filtersData) {
							scope.goToSearch(filtersData.filter);
						});
						checkForWidth(scope);
						scope.$watch('view.search.destination.text', function(newValue, oldValue) {
							if (newValue !== oldValue && scope.view.selectedPlace === false) {
								clearTimeout(placesTimeout);
								if (newValue) {
									placesTimeout = setTimeout(function() {
										scope.searchPlace(newValue);
									}, 500);
								}
							}
							else {
								scope.view.selectedPlace = false;
							}
						});
						scope.$watch('content.search.dates.arrival', function(newValue, oldValue) {
							if (newValue !== oldValue && !scope.view.collapseCalendar.arrival) {
								$timeout(function() {
									scope.view.collapseCalendar.arrival = true;
								}, 400);
								if (scope.content.search.dates.arrival && (!scope.content.search.dates.departure || newValue >= scope.content.search.dates.departure)) {
									scope.content.search.dates.departure = '';
									$timeout(function() {
										scope.view.collapseCalendar.departure = false;
										scope.$broadcast('cCalendarDirective:goToMonth', 'departure');
									}, 550);
								}
							}
						});
						scope.$watch('content.search.dates.departure', function(newValue, oldValue) {
							if (newValue !== oldValue && !scope.view.collapseCalendar.departure) {
								$timeout(function() {
									scope.view.collapseCalendar.departure = true;
								}, 400);
								if (scope.content.search.dates.departure && (!scope.content.search.dates.arrival || newValue <= scope.content.search.dates.arrival)) {
									scope.content.search.dates.arrival = '';
									$timeout(function() {
										scope.view.collapseCalendar.arrival = false;
										scope.$broadcast('cCalendarDirective:goToMonth', 'arrival');
									}, 550);
								}
							}
						});
					}

					function adjustDirective(scope, element) {
						var path = element.inheritedData('$uiView') && element.inheritedData('$uiView').state; //cambiar por $state y variable global		
						if (path && path.self.name === 'hotels-results') {
							scope.view.isInHome = false;
						}
						if (scope.content.search.passengers.children > 0) {
							scope.view.morePassengers = true;
						}

					}

					scope.goToGroup = function() {
						var url = "https://www.cocha.com/viajes-en-grupo";
						$window.open(url, '_blank');
					};

					if (scope.content.search.corporateCode) {
						scope.validateCorporateCode();
					}

				}
			};
		});
});
