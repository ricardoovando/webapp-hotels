define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.directives.HotelSearchFiltersDirective', [])
		.directive('hotelSearchFilters', function($uibModal, $state, $window, $timeout, StaticDataService
			/*, AnalyticsService*/
		) {
			return {
				restrict: 'E',
				templateUrl: 'views/directives/hotel-search-filters.html',
				link: function($scope) {
					var timer = null;

					//$scope.view = {
					$scope.view.closeCollapsesCount = 1;
					$scope.view.collapseTime = 800;
					$scope.view.collapseFilters = true;
					$scope.view.quantityResults = 8;
					/* quantity for hotel name search */
					$scope.view.showButtons = $scope.buttons === "true";
					$scope.view.filtersValues = StaticDataService.getHotelDetailsFilters('hotel');
					$scope.view.collapse = StaticDataService.getHotelDetailsCollapseOptions();
					$scope.view.options = StaticDataService.getHotelDetailsSelectTextOptions();
					$scope.view.currentCollapse = '';
					$scope.view.searchText = '';
					$scope.view.placeHolderNameSearch = 'Ingresa el hotel que buscas';
					//};
					$scope.view.filtersValues.price = [];
					$scope.view.filtersValues.stars = [];
					$scope.view.filtersValues.tripAdvisor = [];

					$scope.content.searchFilters = {};
					$scope.content.searchFilters.edit = false;
					setDefaultFilters();

					if ($scope.options) { // setting filters values
						updateResults(false);
					}
					addListeners($scope);
					$scope.$applyAsync();
					//$scope.view.hotelNameList = HotelResultsDataService.getHotelNameResultsFound('nameSearch');

					function setInfoBoxStatus(_edit, _calendar, _filters, _action) {
						$scope.filters.value = _filters;
						$scope.edit.value = _edit;
						_action = _action;
						//AnalyticsService.infoBoxStatus(_action);
						$scope.$applyAsync();
					}

					function closeOtherCollapses(_type) {
						_.forEach($scope.view.collapse, function(value, key) {
							if (_type !== key) {
								$scope.view.collapse[key] = true;
							}
							$scope.$applyAsync();
						});
					}

					$scope.openFilters = function() {
						setInfoBoxStatus(true, true, !$scope.filters.value, 'filters');
					};
					$scope.openFiltersOnSticky = function() {
						$timeout(function() {
							setInfoBoxStatus(true, true, false, 'filters');
						}, $scope.view.collapseTime);
					};
					$scope.selectFilter = function(index, type, event, value) {
						if (typeof $scope.content.searchFilters[type][value] !== 'undefined' && $scope.content.searchFilters[type][value].length > 0) {
							$scope.content.searchFilters[type][value] = false;
						}
						else {
							$scope.content.searchFilters[type][value] = value;
						}
						if (event) {
							event.stopPropagation();
						}
						$scope.$applyAsync();
					};

					$scope.searchPreferredHotel = function(hotelId, hotelName) {
						$scope.view.placeHolderNameSearch = hotelName;
						updateResults(true, hotelId);
						//$scope.content.filters.preferredHotelId = hotelId;
						$scope.view.searchText = '';
						$scope.$applyAsync();
					};

					$scope.deleteNameSearch = function() {
						$scope.view.searchText = "";
					};
					$scope.restorePlaceHolderNameSearch = function() {
						$scope.view.placeHolderNameSearch = '';
					};
					$scope.closeCollapses = function() {
						$scope.view.collapse.cancellation = true;
						$scope.view.collapse.supplier = true;
						$scope.view.collapse.payment = true;
						$scope.view.collapse.category = true;
						$scope.view.collapse.amenities = true;
						$scope.view.collapse.foodPlan = true;
						$scope.$applyAsync();
					};

					$scope.toggleCollapse = function(_event, _type) {
						$scope.view.collapse[_type] = !$scope.view.collapse[_type];
						if ($scope.view.collapse[_type] === false) {
							$scope.view.currentCollapse = _type;
						}
						else {
							$scope.view.currentCollapse = '';
						}
						if (_event) {
							_event.stopImmediatePropagation();
						}
						$scope.$applyAsync();
						closeOtherCollapses(_type);
					};

					function addListeners($scope) {
						$scope.$watch('content.searchFilters', function(newValue, oldValue) {
							/*if ($scope.content.flagResetFilters === true) {
								$scope.content.flagResetFilters = false;
								$scope.$applyAsync();
								updateSelects();
								return;
							}*/
							if (newValue !== oldValue) {
								if (timer) {
									$timeout.cancel(timer);
									timer = null;
								}
								updateSelects($scope.content.searchFilters);
								timer = $timeout(function() {
									timer = null;
									updateResults(true);
									//AnalyticsService.filterChanged(newValue, oldValue);
								}, 1400);
							}
						}, true);
						/*
						$scope.$watch('content.flagResetFilters', function(newValue) {
							if (newValue === true) {
								setDefaultFilters();
							}
						}, true);
						*/
						$scope.$on('regionSearchFinished', function(event, request) {
							if ($scope.type === $scope.mode) {
								if (!request.filters.hasOwnProperty('isPreferred') || request.data.isPreferred === false) {
									$scope.view.placeHolderNameSearch = 'Ingresa el hotel que buscas';
									$scope.$applyAsync();
								}
							}
						});
					}

					function setDefaultFilters() {
						$scope.content.searchFilters.price = [0, 1000];
						$scope.content.searchFilters.stars = [0, 5];
						$scope.content.searchFilters.tripAdvisor = [0, 5];
						//$scope.content.searchFilters.paymentType = {};
						$scope.content.searchFilters.supplier = {};
						$scope.content.searchFilters.cancellation = {};
						$scope.content.searchFilters.category = {};
						$scope.content.searchFilters.amenities = {};
						$scope.content.searchFilters.foodPlan = {};
						$scope.content.searchFilters.preferredHotelId = null;
						$scope.$applyAsync();
					}

					//function updateResults($scope, bEmit) {
					function updateResults(bEmit, hotelId) {
						var parameters = {
							price: {
								min: $scope.content.searchFilters.price[0],
								max: $scope.content.searchFilters.price[1]
							},
							ratings: {
								min: $scope.content.searchFilters.stars[0],
								max: $scope.content.searchFilters.stars[1]
							},
							tripAdvisor: {
								min: $scope.content.searchFilters.tripAdvisor[0],
								max: $scope.content.searchFilters.tripAdvisor[1]
							},
							supplier: $scope.content.searchFilters.supplier,
							//paymentType: $scope.content.searchFilters.paymentType,
							cancellation: $scope.content.searchFilters.cancellation,
							category: $scope.content.searchFilters.category,
							amenities: $scope.content.searchFilters.amenities,
							foodPlan: $scope.content.searchFilters.foodPlan,
							//preferredHotelId: $scope.content.filters.preferredHotelId
							start: 0,
							limit: 20
						};
						if (hotelId) {
							parameters.preferredHotelId = hotelId;
						}
						//alert(JSON.stringify(parameters));
						console.info("[HotelSearchFiltersDirective]", "Updating filters params to:" + JSON.stringify(parameters));
						if (bEmit !== false) {
							$scope.filterFn(parameters, $scope.content.sort);
						}
						$scope.$applyAsync();
					}

					function updateSelects(parameters) {
						var cancellation = (parameters) ? parameters.cancellation : [];
						//var paymentType = (parameters) ? parameters.paymentType : [];
						var supplier = (parameters) ? parameters.supplier : [];
						var category = (parameters) ? parameters.category : [];
						var amenities = (parameters) ? parameters.amenities : [];
						var foodPlan = (parameters) ? parameters.foodPlan : [];
						$scope.view.options.category = StaticDataService.getCategoryTextSelected(category);
						$scope.view.options.cancellation = StaticDataService.getCancellationTextSelected(cancellation);
						//$scope.view.options.payment = StaticDataService.getPaymentTextSelected(paymentType);
						$scope.view.options.supplier = StaticDataService.getSupplierTextSelected(supplier);
						$scope.view.options.amenities = StaticDataService.getHotelAmenitiesTextSelected(amenities);
						$scope.view.options.foodPlan = StaticDataService.getFoodPlanTextSelected(foodPlan);
						//alert(JSON.stringify($scope.view.options));
						$scope.$applyAsync();
					}

					$timeout(function() {
						$scope.view.smallButtons = (angular.element($window).width() < 991);
					}, 500);
					angular.element($window).click(function() {
						$scope.view.collapse.cancellation = true;
						$scope.view.collapse.supplier = true;
						//$scope.view.collapse.payment = true;
						$scope.view.collapse.category = true;
						$scope.view.collapse.amenities = true;
						$scope.view.collapse.foodPlan = true;
						$scope.$apply();
					});
					$scope.$on('$destroy', function() {
						angular.element(window).off('click');
					});
				}
			};
		});
});
