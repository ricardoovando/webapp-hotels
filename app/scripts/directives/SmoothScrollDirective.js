define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.directives.SmoothScrollDirective', [])
		.directive('smoothScroll', function() {
			return {
				restrict: 'A',
				replace: false,
				scope: {
					'smoothScroll': '@',
				},
				link: function($scope, $element, $attrs) {
					initialize();

					function initialize() {
						createEventListeners();
					}

					function createEventListeners() {
						$element.on('click', function() {
							scrollTo($attrs.smoothScroll);
						});
					}

					function scrollTo(eID) {
						var i;
						var startY = currentYPosition();
						var stopY = elmYPosition(eID);
						if (stopY > startY) {
							if ($attrs.margin) {
								stopY = stopY - $attrs.margin;
							}
						}
						else {
							stopY = stopY + 10;
						}
						var distance = stopY > startY ? stopY - startY : startY - stopY;
						var speed = Math.round(distance / 25);
						if (speed >= 25) {
							speed = 25;
						}
						var step = Math.round(distance / 25);
						var leapY = stopY > startY ? startY + step : startY - step;
						var timer = 0;
						if (stopY > startY) {
							for (i = startY; i < stopY; i += step) {
								setTimeout('window.scrollTo(0, ' + leapY + ')', timer * speed);
								leapY += step;
								if (leapY > stopY) {
									leapY = stopY;
								}
								timer++;
							}
							return;
						}
						for (i = startY; i > stopY; i -= step) {
							setTimeout('window.scrollTo(0, ' + leapY + ')', timer * speed);
							leapY -= step;
							if (leapY < stopY) {
								leapY = stopY;
							}
							timer++;
						}
					}

					function currentYPosition() {
						// Firefox, Chrome, Opera, Safari
						if (window.pageYOffset) {
							return window.pageYOffset;
						}
						// Internet Explorer 6 - standards mode
						if (document.documentElement && document.documentElement.scrollTop) {
							return document.documentElement.scrollTop;
						}
						// Internet Explorer 6, 7 and 8
						if (document.body.scrollTop) {
							return document.body.scrollTop;
						}
						return 0;
					}

					function elmYPosition(eID) {
						var elm = document.getElementById(eID);
						var y = elm.offsetTop;
						var node = elm;
						while (node.offsetParent && node.offsetParent !== document.body) {
							node = node.offsetParent;
							y += node.offsetTop;
						}
						return y;
					}
				}
			};
		});
});
