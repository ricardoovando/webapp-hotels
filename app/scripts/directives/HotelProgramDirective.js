define(['angular'], function(angular) {
	'use strict';

	angular.module('COCHAHotels.directives.HotelProgramDirective', [])
		.directive('hotelProgram', function($timeout, $uibModal /*, AnalyticsService*/ ) {
			return {
				restrict: 'E',
				scope: {
					program: '=program',
					data: '=data',
					openCalendar: '=openCalendar',
					openOrigin: '=openOrigin',
					index: '@',
					state: '=state',
					functions: '='

				},
				templateUrl: 'views/directives/hotel-program.html',
				link: function(scope) {
					scope.view = {
						showElements: false,
						loadingElements: false,
						firstLoad: true
					};

					scope.loadElements = function() {
						//AnalyticsService.offerSelected(scope.program.title);

						scope.view.loadingElements = true;
						scope.view.showElements = true;
						if (scope.view.firstLoad) {
							$timeout(function() {
								scope.view.loadingElements = false;
								scope.view.firstLoad = false;
								scope.functions.openSelectors('open', 'offers', scope.index);
							}, 500);
						}
						else {
							scope.view.loadingElements = false;
							scope.functions.openSelectors('open', 'offers', scope.index);
						}

					};
					scope.viewDetails = function() {
						$uibModal.open({
							controller: 'ModalController',
							templateUrl: 'program-conditions',
							size: 'sm',
							resolve: {
								content: function() {
									return {
										conditions: scope.program.description
									};
								}
							}
						});
					};
				}
			};
		});
});
