Cocha Hotels
=============

## Instructions

#### How can I develop COCHA libraries?

1. Create folder `dev`

2. Inside the folder clone the wanted repos:

i.e:

```
git clone git@bitbucket.org:cocha-digital/cocha-access-local.git
git clone git@bitbucket.org:cocha-digital/cocha-access-remote.git
git clone git@bitbucket.org:cocha-digital/cocha-common-constants.git
git clone git@bitbucket.org:cocha-digital/cocha-common-directives.git
git clone git@bitbucket.org:cocha-digital/cocha-exceptions.git
git clone git@bitbucket.org:cocha-digital/cocha-utils-data.git
git clone git@bitbucket.org:cocha-digital/cocha-utils-filters.git
git clone git@bitbucket.org:cocha-digital/cocha-utils-translation.git
```

3. Modify `app/index.php` and set:

```
$useDevLibraries = true;
```

#### How can I locally run the project?

1. Install [Git](http://git-scm.com/downloads) and [NodeJS](http://nodejs.org/download/), if you don't have them already.

2. Open your terminal and download grunt-cli and bower: ```sh $ [sudo] npm install -g grunt-cli bower```

3. Now clone it: ```git clone <GIT PATH> project```

4. Then go to the project's folder: ```cd project/```

5. Install all dependencies: ```npm install && bower install```

6. And finally run: ```grunt```
Now you can see the website running at `localhost:8000` :D



#### How can publish code?

1. Make your changes to the code.

2. Before commit, you must execute the code formating Grunt task: ```grunt prepare```

3. If you want to send code to development environment, add to the comment of the last commit: "X.X.X RC" (i.e. "Version 2.1.0 RC"). You can see proccess in `http://ci.dev.cocha.com`